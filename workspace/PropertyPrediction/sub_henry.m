function [results] = sub_henry( k,compounds, T, specs)
%% Calculates deltaG of solvation for reactants and transition states
%
% Input:    k: unique number k used for identiying files 
%           compounds
%           T: temperature vector 
%           specs 
%           
%
% Output:   Henry Coefficients

%% Generate jobs "jobPrint"

    jobPrint = ''; % Initialize jobPrint         
    
    composition = zeros(1, length(compounds));
    composition(end) = 1;

    for w=1:length(T)
        Tk = num2str(T(w));
        jobPrintPart = 'henry xh={';   

        for i = 1:length(compounds)  
            jobPrintPart = [jobPrintPart,num2str(composition(i)),' '];
        end

        jobPrintPart = [jobPrintPart, '} Tk= ' Tk ' #Automatic Henry Law coefficient Calculation \n' ];  
        jobPrint = [jobPrint, jobPrintPart ];  

    end


 %% Call COSMOtherm
    
    [tempFile] = cosmoSingleCall( k, compounds, specs,jobPrint); %name of COSMOtherm output file ..., ...
   
 %% ReadOutputFile 
 
    [results,ok] = extract_Henry(tempFile,compounds,T,specs);
    
 %% Delete temporary files after calculation     
    
    if exist([tempFile, '.inp'])>0
        delete([tempFile, '.inp'])
    end
    if exist([tempFile, '.tab'])>0
        delete([tempFile, '.tab'])
    end
    if exist([tempFile, '.out'])>0
        delete([tempFile, '.out'])
    end
    
    
end   
    
function [results,ok] = extract_Henry(tempFile,compounds,T,specs)

% Check if .out file is empty
s = dir([tempFile,'.tab']); %
if s.bytes == 0
    result = NaN([1,length(specs.compounds)]); % Empty file
    ok = 0;
    disp('ERROR: tab file empty');
else
    ok = 1;
    fileID = fopen([tempFile,'.tab'],'r'); %r: read
    line = fgetl(fileID); %return next line as string
    formatSpec = '%*s %*s %f %*f %*f %*s %*s %*s';
    tempCount = 0;
    results = cell(length(T), length(compounds));
    
    while ~feof(fileID)
        if ~isempty(strfind(line,'Nr Compound')) % Find results part and read all results
            tempCount = tempCount+1;
            line = fgetl(fileID); 
            result = zeros(1,length(compounds));
            for i = 1:length(compounds)
				tempresult = textscan(line,formatSpec); % Read line
                if ~isempty(tempresult{1})
                    result(i) = cell2mat(tempresult);
                else
                    result(i) = NaN;
                end
                line = fgetl(fileID);

            end
            
            % Save results for one temperature and one "solvent"
            for i = 1:length(compounds)
                results{tempCount, i}=result(i);
            end
            
            % Next set
            if tempCount == length(T)
                tempCount = 0;
            end
        end        
        line = fgetl(fileID); 
    end
    fclose(fileID);
    
  
end    

end 
