function molStruct = job_Antoine(molStruct,compounds,specs)

%%
nComp = length(specs.system);       % number of Compounds
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system
T = linspace(specs.T_start, specs.T_end, specs.T_interval)'; 

%% Calculate Antoine Parameter for non-solvent components
parfor i = 1:nComp-nSolvs
    [antParam{i,1}, dhvap{i,1}] = sub_Antoine(i,compounds(1,i), T, specs);
end


%% Calculate Antoine Parameter for solvents
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

antParam_sol = cell(length(solvents),1);
dhvap_sol = cell(length(solvents),1);

if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating Antoine coefficients of ', num2str(length(molStruct)), ' solvent systems\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(solvents));
end

parfor k = 1:length(solvents)
    [antParam_sol{k}, dhvap_sol{k}]  = sub_Antoine(k,solvents(k), T, specs);   
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end

%% Merge and fill molStruct
for k=1:length(molStruct)
    if nComp-nSolvs > 0
        molStruct(k).AntoineParam = antParam;
        molStruct(k).dhvap = dhvap;
    else
        molStruct(k).AntoineParam = [];
        molStruct(k).dhvap = [];
    end
    for i=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+i)));
        molStruct(k).AntoineParam{end+1} =  antParam_sol{idxsolv};
        molStruct(k).dhvap{end+1} =  dhvap_sol{idxsolv};
    end
end

%% Calculate enthalpy of vaporization using Clausius-Clapeyron Equation
% Set Tcrit and pcrit
Tcrit = num2cell(1e+04*ones(nComp,length(molStruct)));
pcrit = num2cell(1e+08*ones(nComp,length(molStruct)));
[molStruct.Tcrit] = Tcrit{:};
[molStruct.pcrit] = pcrit{:};

% Fit dhvap for non-solvents
dhvap_para = cell(nComp-nSolvs, 1);
AntoineParam_nonsolvents = molStruct(1).AntoineParam;
dhvap_nonsolvents = molStruct(1).dhvap;
for k=1:nComp-nSolvs
    dhvap_para{k} = fit_dhvap(AntoineParam_nonsolvents{k},dhvap_nonsolvents{k},Tcrit{k}, T);
end

%Fit dhvap for solvents
parfor k = 1:length(solvents)
    dhvap_para_sol{k} = fit_dhvap(antParam_sol{k},dhvap_sol{k},Tcrit{end,k},T);
end

% Fill struct
for k=1:length(molStruct)
    if nComp-nSolvs > 0
    	molStruct(k).dhvap_para = dhvap_para;
    else
        molStruct(k).dhvap_para = [];
    end
    for i=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+i)));
        molStruct(k).dhvap_para{end+1} =  dhvap_para_sol{idxsolv};
    end
end

end


function [dHvap_para, dHvap_paraClausCla]  = fit_dhvap(AntoineParam, dhvap, Tcrit, T)
%Fit function to get parameter for DIPPR 106 for enthalpy of vaporization
%in kJ/kmol

% Get dhvap from Clausius-Clapeyron
R = 8.314;
dhvap_ClausCla = AntoineParam(2)./(AntoineParam(3) + T).^2 .* R .* T.^2;
dhvap(:,2) = dhvap(:,2) * 4184; % convert from kcal/mol to kJ/kmol

% Starting guess 
para0 = [0 0 0 0 0];

% Fit DIPPR 
options = optimoptions('lsqcurvefit','Display','off','MaxFunEvals',1e5, 'Algorithm','levenberg-marquardt');
tr = T/Tcrit;
dHvap_para = lsqcurvefit(@dHvap_DIPPR, para0, tr, dhvap(:,2), [],[], options);
dHvap_paraClausCla = lsqcurvefit( @dHvap_DIPPR, para0, tr, dhvap(:,2), [],[],options);
end


function fitted = dHvap_DIPPR(c,tr)
% Calculate dhvap according to dHvap-Polynomial 106
fitted = c(1) * (1-tr).^(c(2) + c(3)* tr + c(4)* tr .^ 2+ c(5)* tr .^ 3);
end
