function [int_heat,cp_ig,s_ig,v_ig,g_ig] = sub_iGproperties(k,name,T,specs)
%This sub function performs all steps necessary for predicting internal
%heat, cp_ig, S, V and G of a pure ideal gas. Inputs are the name of the
%component (name as in COSMO-CAMD or as in COSMObase, depending on mode), a scalar temperature or a vector of temperatures and the specs
%struct.
%Units: J/mol/K for cp_ig and s_ig; kJ/mol for all other quantities
%written by: Christoph Gertig

%name of Gaussian Files
GaussianFileName =  [name,'_c0']; 

%Modify GaussianFilename for file handling
GaussianFileName =  strrep(GaussianFileName, ',', '_');
GaussianFileName =  strrep(GaussianFileName, '(', '_');
GaussianFileName =  strrep(GaussianFileName, ')', '_');
 
%% perform QM and thermodynamic calculations to get iG properties
% generate file names
if strcmp(specs.Mode, 'optimization')
    k=specs.Number;
end

name_fchk = [GaussianFileName,'_', num2str(k),'.fchk'] ;
name_csv = [GaussianFileName,'_', num2str(k),'.csv'] ;
tempfolder = 'PropertyPrediction/temp/';

%make tmp working directory for TAMkin and change directory to tmp working directory
maindir = pwd;
tmp_wd_name = [tempfolder,GaussianFileName,'_', num2str(k), '_TAMkin'];
mkdir(tmp_wd_name);
cd([tmp_wd_name,'/']);

%%copy .fchk file to tmp wd
movefile(['../',name_fchk]);

%% generate tamkin input file & execute it
ok = tamkin_inputfile_system(name_fchk,name_csv,T);

%% execute TAMkin & transform output to matlab data type
% execute TAMkin.py
%output = py.TAMkin.TAMkinexe();
sysoutput = system(['python -W ignore TAMkin.py']);

%% extract results from .csv
[int_heat,cp_ig,s_ig,v_ig,g_ig] = extract_csv(name_csv, T);

%% remove python file and .csv files and .fchk file
system('rm *.py');
system('rm *.csv');
system('rm *.fchk');

%% go back from temporary working directory to working directory and remove temporary working directory
cd(maindir);
rmdir(tmp_wd_name);

end
