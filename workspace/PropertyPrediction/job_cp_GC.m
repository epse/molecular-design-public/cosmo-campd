function molStruct = job_cp_GC(molStruct,compounds,specs)
% Calculate gaseous heat capacities using GC-based Benson's method and then
% based on gas heat capacities and heat of vaporization the liquid heat
% capcacities
% Returns gaseous heat capacities as NASA polynomial

%% General
nComp = length(specs.system);           % number of Compounds
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system
T = linspace(specs.T_start, specs.T_end, specs.T_interval)'; 

% Get all solvents
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GASEOUS HEAT CAPACITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating gaseous heat capacities using GC of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(solvents));
end

%% Get cps for non-solvents
SMILES = specs.SMILES;
for k = 1:nComp-nSolvs
    cp_gas{k,1} = sub_cpig_GC(SMILES{k}, k);
end

%% Get cps for solvents
cp_gas_sol = cell(length(solvents),1);

% Find the SMILES of all solvents
SMILES = cell(length(solvents),1);
for k = 1:length(solvents)
    for j=1:nSolvs
        idxsolv = find(strcmp(solvents(k), compounds(:,nComp-nSolvs+j)));
        if ~isempty(idxsolv)
            idxsolv = idxsolv(1);
            break
        end
    end
    if iscell(molStruct(idxsolv).SMILES(j))
        SMILES{k} = molStruct(idxsolv).SMILES{j} ;
    else
        SMILES{k} = molStruct(idxsolv).SMILES ;
    end
end

% Call GC-method
parfor k = 1:length(solvents)
    cp_gas_sol{k,1} = sub_cpig_GC(SMILES{k}, k);
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
end

%% Merge and fill molStruct
for k=1:length(molStruct)
    if nComp-nSolvs > 0
    	molStruct(k).cp_gas_co = cp_gas;
    else
        molStruct(k).cp_gas_co = [];
    end
    for is=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+is)));
        molStruct(k).cp_gas_co{end+1} =  cp_gas_sol{idxsolv};
    end
end


end