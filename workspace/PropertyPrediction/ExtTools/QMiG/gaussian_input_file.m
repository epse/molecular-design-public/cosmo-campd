function [inputfile] = gaussian_input_file(energy_file,name_chk, tempfolder)

inputfile = strrep([tempfolder,name_chk],'.chk','.com') ;

%% search the atom coordinates in .energy file 
% open .energy file
engFile = fopen(energy_file, 'rt');
C = textscan(engFile,'%s %f %f %f');
atoms = C{1};
x = C{2};
y = C{3};
z = C{4};


for cellidx = 3:numel(atoms)
   a = atoms{cellidx};
   if strcmp(a,'$charge')
      end_atoms = cellidx-1;
      break;
   end
end
atoms = char(atoms);

fclose(engFile);
%% write input file for gaussian

    fileID=fopen(inputfile,'w');
% write header
    %fprintf(fileID,'$ RunGauss \n');
    text = ['%%chk=',tempfolder,name_chk,'\n'];
    fprintf(fileID,text);
    fprintf(fileID,'%%mem=640MW\n');
    fprintf(fileID,'%%nprocshared=1\n'); %%number of cores used by Gaussian
    fprintf(fileID,['# opt freq b3lyp tzvp int=grid=ultrafine\n\n']);
    
% define title section
    fprintf(fileID,'Title Card Required\n\n');
 
% define molecule specification -> write coordinates of the atoms in
% gaussian input file
    fprintf(fileID,'0 1\n');
    for i=3:end_atoms
        line =[atoms(i,:),' ',num2str(x(i),'%1.6f'),' ',num2str(y(i),'%1.6f'),' ',num2str(z(i),'%1.6f'), ' \n'];
        fprintf(fileID, line);
    end
    fprintf(fileID,'\n');
% end of file writing
    fclose(fileID);