function [ok] = getGaussianFiles(molStruct,SMILES_vec,specs)
% Get Gaussian Files either from Database or QM-calculations for each
% molecule in molStruct

%% General
tempfolder = 'PropertyPrediction/temp/';
alljobSMILES = cell(0);
alljobmols = cell(0);

%% Check or prepare calc list

for k=1:length(molStruct)
    % Define name and SMILES
    SMILES = SMILES_vec{k};
    name = molStruct(k).name;    
    
    %% Check if fchk-File already exists in Database
    SMILES =  strrep(SMILES, '#', '+');
    SMILES =  strrep(SMILES, '[', '^');
    SMILES =  strrep(SMILES, ']', '_');
    databasefile = dir([specs.paths.gaussdbdir,'/', SMILES ,'/', SMILES, '_c0.fchk']);

    %name of Gaussian Files
    GaussianFileName =  [name,'_c0']; 

    %Modify GaussianFilename for file handling
    GaussianFileName =  strrep(GaussianFileName, ',', '_');
    GaussianFileName =  strrep(GaussianFileName, '(', '_');
    GaussianFileName =  strrep(GaussianFileName, ')', '_');
    
    if strcmp(specs.Mode, 'screening')
        id = k;
    else
        id = specs.Number;
    end
    
    %% Option A: Gaussian File not found --> Prepare for QM-calc
    if isempty(databasefile)

        %determine path to .energy file
        if (strcmp(specs.Mode, 'optimization') || strcmp(specs.Mode, 'refinement')) && exist([specs.Gate{2}, '/', molStruct.name '_c0.energy'], 'file')
            energy_file = [specs.Gate{2},'/',name,'_c0.energy'];
        else
            % Import path definitions from header.m
            [~, ~, ~, dbPath] = main_header(specs);

            %determine initial character of solvent molecule
            molecule_name = name;
            initial_character = molecule_name(1);

            %determine whether first character is number or string
            [~,num_status] = str2num(initial_character);
            %num_status=1 --> number

            %in case initial_character is not a number: make sure to get lower case
            if num_status == 0
                case_status = isstrprop(initial_character,'upper'); %if case_status=1 --> upper case letter

                if case_status == 1
                   initial_character = lower(initial_character); 
                end

                if strcmp(initial_character, '(')  || strcmp(initial_character, '[')
                    initial_character = '0';
                end
            end
            energy_file = [dbPath,initial_character,'/',name, '_c0.energy'];
        end

        % generate input file
        name_chk = [GaussianFileName,'_', num2str(id),'.chk'] ;
        gaussian_input_file(energy_file,name_chk, tempfolder);
        alljobSMILES{end+1} = SMILES;
        alljobmols{end+1} = [GaussianFileName,'_', num2str(id)];

    else
        %% Option B: Copy from Database
        copyfile([specs.paths.gaussdbdir, '/', SMILES, '/', SMILES, '_c0.fchk'],[tempfolder,GaussianFileName,'_', num2str(id),'.fchk']);

    end

end

%% start gaussian calculation

if ~isempty(alljobmols)
    % Determine cores per job
    currentparpool = gcp('nocreate');
    if ~isempty(currentparpool) || strcmp(specs.Mode, 'optimization')
        if strcmp(specs.Mode, 'optimization')
            ncoresperjob = 4;
            ncores = 1;
        else
            ncores = currentparpool.NumWorkers;
            delete(currentparpool);
       
            numjobs = length(alljobmols);
            if numjobs==1
                ncoresperjob = ncores;
            elseif numjobs==2
                ncoresperjob = floor(ncores/numjobs);
                currentparpool=parpool(2);
            elseif numjobs==3
                ncoresperjob = floor(ncores/numjobs);
                currentparpool=parpool(3);
            else
                ncoresperjob = 4;
                currentparpool=parpool(ncores/ncoresperjob);
            end
        end
        
        % Change job files
        for k=1:length(alljobmols)
            content = fileread([tempfolder,alljobmols{k}, '.com']);
            expression = 'nprocshared=1';
            replace = ['nprocshared=', num2str(ncoresperjob)];
            newcontent = regexprep(content, expression, replace);
            fid = fopen([tempfolder,alljobmols{k}, '.com'], 'w');
            fwrite(fid, newcontent);
            fclose(fid);
        end
    
    else
        ncores = 1;
    end

    % start gaussian
    for k=1:length(alljobmols)
        gaussian_command = ['g09 "',tempfolder, alljobmols{k}, '.com"'];
        status = system(gaussian_command);
        if status ==1
            disp(['Error in gaussian calculation ', num2str(k), '!']);
        end
        
        % Check for normal termination
        [status result] = system(['tail -n 1 ', tempfolder, alljobmols{k}, '.log']);
        if isstrprop(result(end), 'cntrl')
            result(end) = []; 
        end
        successful = 0;
        if contains(result, 'Normal termination of Gaussian 09 at')
            successful = 1;
        end
        
        % If not successful run again with cartesian
        if ~successful
            content = fileread([tempfolder,alljobmols{k}, '.com']);
            expression = '# opt freq b3lyp tzvp int=grid=ultrafine';
            replace = '# opt=cartesian freq b3lyp tzvp int=grid=ultrafine';
            newcontent = regexprep(content, expression, replace);
            fid = fopen([tempfolder,alljobmols{k}, '.com'], 'w');
            fwrite(fid, newcontent);
            fclose(fid);
            gaussian_command = ['g09 "',tempfolder, alljobmols{k}, '.com"'];
            status = system(gaussian_command);
        end
    end
    delete(currentparpool);

    % create fchk file
    if ncores ~= 1
        parpool(ncores);
    end
    parfor k=1:length(alljobmols)
        formchk_command = ['formchk',' "',tempfolder, alljobmols{k}, '.chk"'];
        status = system(formchk_command);

        if status ==1
            disp('Error in fchk file creation!');
        end
    end

    %% Collect results and remove gaussian files
    for k=1:length(alljobmols)
        % Check if folder exists:
        databasedir = dir([specs.paths.gaussdbdir,'/', name]);
        if isempty(databasedir)
            mkdir([specs.paths.gaussdbdir,'/', name]);
        end

        
        % move all gaussian files to storage folder
        % %com
        % movefile([GaussianFileName,'.com'], [specs.paths.gaussdbdir, '/',  name, '.com']);
        % %chk
        % movefile([GaussianFileName,'.chk'], [specs.paths.gaussdbdir, '/', name, '.chk']);
        % %log
        % movefile([GaussianFileName,'.log'], [specs.paths.gaussdbdir, '/', name, '.log']);
        %fchk
        copyfile([tempfolder, alljobmols{k}, '.fchk'], [specs.paths.gaussdbdir,'/', alljobSMILES{k}, '/', alljobSMILES{k}, '_c0.fchk']);
        
        % remove all gaussian files
        delete([tempfolder, alljobmols{k}, '.log']);
        delete([tempfolder, alljobmols{k}, '.com']);
        delete([tempfolder, alljobmols{k}, '.chk']);        
    end

end

ok =1;

end

