function[int_heat,cp_ig,s_ig,v_ig,g_ig] = extract_csv(name_csv, T)
fileID = fopen(name_csv,'r');

%number of columns in Table
a = length(T)+1;
%formatSpec is %q for all columns
formatSpec = [repmat('%q', 1, a), '%[^\n\r]'];
delimiter = ',';
startRow = 1;

table = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

b =size(table{1});
c = size(table)-1;

% preallocate results file
int_heat = NaN(c(2)-1,1);
cp_ig = NaN(c(2)-1,1);
s_ig = NaN(c(2)-1,1);
v_ig = NaN(c(2)-1,1);
g_ig = NaN(c(2)-1,1);

for i=1:b(1)
    
    if contains(table{1}{i},'Internal heat')
        for z=2:c(2)
            int_heat(z-1,1) = str2double(table{z}{i+2});
        end
    end
    if contains(table{1}{i},'Heat capacity')
        for z=2:c(2)
            cp_ig(z-1,1) = str2double(table{z}{i+2});
        end
    end
    if contains(table{1}{i},'Entropy')
        for z=2:c(2)
            s_ig(z-1,1) = str2double(table{z}{i+2});
        end
    end
    if contains(table{1}{i},'Chemical potential')
        for z=2:c(2)
            v_ig(z-1,1) = str2double(table{z}{i+2});
        end
    end
    if contains(table{1}{i},'Free energy')
        for z=2:c(2)
            g_ig(z-1,1) = str2double(table{z}{i+2});
        end
    end
end
fclose(fileID);