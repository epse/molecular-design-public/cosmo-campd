function molStruct = job_T_melt_con(molStruct,compounds,specs)
% Property prediction to check T_melt constraint with COSMOquick SVP
% Calculates T_melt and checks if in allowed temperature range

%% General
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system
nComp = length(specs.system);       % number of Compounds

% Preallocate fields
for k=1:length(molStruct)
    molStruct(k).T_melt = NaN;
    molStruct(k).T_melt_ok = NaN;
end


%% Job procedure
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

T_melt_solvents = zeros(length(solvents),1);
T_melt_ok_solvents = zeros(length(solvents),1);

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating T_melt for ', num2str(length(solvents)), ' solvents\nProgress:\n'];
    fprintf(display_text);
    parfor_progress(length(solvents));
end

% Find the SMILES of all solvents
SMILES = cell(length(solvents),1);
for k = 1:length(solvents)
    for j=1:nSolvs
        idxsolv = find(strcmp(solvents(k), compounds(:,nComp-nSolvs+j)));
        if ~isempty(idxsolv)
            idxsolv = idxsolv(1);
            break
        end
    end
    if iscell(molStruct(idxsolv).SMILES(j))
        SMILES{k} = molStruct(idxsolv).SMILES{j} ;
    else
        SMILES{k} = molStruct(idxsolv).SMILES ;
    end
end


%% Calculate melting points by calling COSMOfrag
generalfragdir = specs.paths.ldirfrag;
index = strfind(generalfragdir, 'licensefiles');
generalfragdir = generalfragdir(1:index-1);
if strcmp(specs.Mode, 'screening')
    settingsxml = 'PropertyPrediction/temp/settings.xml';
    tempdir = 'PropertyPrediction/temp';
else
    settingsxml = ['PropertyPrediction/temp/temp_',specs.Number, '/settings_', specs.Number, '.xml'];
    tempdir = ['PropertyPrediction/temp/temp_',specs.Number];
    mkdir(tempdir);
end

%tempFile = 'PropertyPrediction/temp/melt_frag_mols';
if strcmp(specs.Mode, 'screening')
	tempFile = 'melt_frag_mols';
else
	tempFile = ['melt_frag_mols_', specs.Number];
end
fileID = fopen([tempFile, '.smi'],'wt');

for k=1:length(solvents)
    fprintf(fileID,[SMILES{k}, '\n']);    
end
fclose(fileID);

% Write the settings file
write_settingsxml(specs, settingsxml, generalfragdir);

% Execute COSMOfrag
if ispc
    command = ['java -cp "', generalfragdir, '/COSMOquick/COSMOquick.jar;', generalfragdir, '/extlib/*;" '];
    command = [command, ['-Djava.library.path="', generalfragdir , '/lib" de.cosmologic.cosmoquick.main.COSMOquick -settings ']];
    command = [command, [pwd, '/', settingsxml, ' -infile ', tempFile, '.smi -qspr "T(melting).propz"']];
    [status, cmdout] = dos(command);
elseif isunix
    command = [generalfragdir, '/jre/bin/java -cp "', generalfragdir, '/COSMOquick/COSMOquick.jar:', generalfragdir, '/extlib/*" '];
    command = [command, ['-Djava.library.path="', generalfragdir , '/lib" de.cosmologic.cosmoquick.main.COSMOquick -settings ']];
    command = [command, [pwd, '/', settingsxml, ' -infile ', tempFile, '.smi -qspr "T(melting).propz"']];
    [status, cmdout]  = system(command);
end

% Read results
FullResultsTable = readtable([tempFile,'.csv']);
T_melt_solvents = FullResultsTable{:, find(strcmp(FullResultsTable.Properties.VariableNames, 'Tm_K_'))};


parfor k = 1:length(solvents)
    T_melt_ok_solvents(k) = func_check_T_melt( T_melt_solvents(k), specs);
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end

%% Merge and fill molStruct
for k=1:length(molStruct)
    for i=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+i)));
        molStruct(k).T_melt(i) =  T_melt_solvents(idxsolv);
        molStruct(k).T_melt_ok(i) = T_melt_ok_solvents(idxsolv);
    end
    molStruct(k).T_melt_ok = min(molStruct(k).T_melt_ok);
end

%% Delete temporary directories
rmdir([tempdir, '/temp_mcos'], 's');
rmdir([tempdir, '/temp_qspr'], 's');
delete(settingsxml);
delete([tempFile,'.smi']);
delete([tempFile, '.csv']);
if ~strcmp(specs.Mode, 'screening')
    rmdir(tempdir);
end

end

function [T_melt_ok] = func_check_T_melt(T_melt, specs)
% Checking boiling point temperature of solvent 
% The lower and upper bound of acceptable boiling temperature are given in
% the "specs"-struct, fields T_melt_max and T_melt_min

%% Create bounds for checking T_melt
if strcmp(specs.T_melt_max, 'no_T_max')
    T_max = 1e99;
else
    T_max = specs.T_melt_max;
    if ischar(T_max)
        T_max = str2double(T_max);
    end
   
end

if strcmp(specs.T_melt_min, 'no_T_min')
    T_min = 0;
else
    T_min = specs.T_melt_min;
    if ischar(T_min)
        T_min = str2double(T_min);
    end
   
end

%% Check T_melt of given compound

if(T_melt > T_max)
        T_melt_ok = 0;
elseif(T_melt < T_min)
        T_melt_ok = 0;
else    T_melt_ok = 1;
end

end

function write_settingsxml(specs, settingsxml, generalfragdir)
index = strfind(settingsxml, ['settings_', specs.Number, '.xml']);
calculationdir = settingsxml(1:index-1);

settingsfile = fopen(settingsxml, 'wt');

fprintf(settingsfile,'<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(settingsfile,'<!--\n');
fprintf(settingsfile,'    Document   : settings.xml\n');
fprintf(settingsfile,'    Created on : 21. August 2015, 11:11\n');
fprintf(settingsfile,'    Author     : Christoph  Description:\n');
fprintf(settingsfile,'        Purpose of the document follows.\n');
fprintf(settingsfile,'-->\n');
fprintf(settingsfile,'<settings>\n');
fprintf(settingsfile,['    <cosmofrag_executable>', specs.paths.exefrag, '</cosmofrag_executable>\n']);
fprintf(settingsfile,['    <cosmofrag_license>', specs.paths.ldirfrag, '</cosmofrag_license>\n']);
fprintf(settingsfile,['    <cosmofrag_database>', specs.paths.cfdb,'</cosmofrag_database>\n']);
fprintf(settingsfile,['    <cosmors_parameterization>', generalfragdir,'/COSMOquick</cosmors_parameterization>\n']);
fprintf(settingsfile,'%s', ['<calculation_dir>', pwd, '/', calculationdir,'</calculation_dir>']);
fprintf(settingsfile,'\n');
fprintf(settingsfile,['<cosmotherm_executable>', specs.paths.exepath,'</cosmotherm_executable>\n']);
fprintf(settingsfile,['<cosmoquick_installdir>', generalfragdir,'/COSMOquick/</cosmoquick_installdir>\n']);
fprintf(settingsfile,'<actual_parameterization>BP_SVP_AM1_C30_1601.ctd</actual_parameterization>\n');
fprintf(settingsfile,'   <use_external_cosmotherm>True</use_external_cosmotherm>\n');
fprintf(settingsfile,'</settings>\n');

fclose(settingsfile);
end
