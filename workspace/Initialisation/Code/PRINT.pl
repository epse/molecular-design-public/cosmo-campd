#!/usr/bin/perl

print "PRINT OK \n";


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#               all subs about screen output
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

sub print_header{

# ----------
#
# just as sub name said
#
# ----------

    my $text="
    ########################################################################\n\n
    \t\t\tsplit functional groups\n\n
    ########################################################################\n\n";

    system("echo \"$text\""); 

}

sub print_i{

# ----------
#
# sreen output: information of this algorithmus 
#
# ----------
    
    # attention: it is four blanks not a tab
    my $text = "General Command list:
    ./splitGroup.pl -i: help information
    ./splitGroup.pl -mat <file.mat>: a certain amount of molecules will be ranked then splited
    ./splitGroup.pl -txt <file.txt>: translate USMILES to one SDF-file with ranking, then split them
    ./splitGroup.pl -dir <directory>: split molecules in one directory without ranking 
    ./splitGroup.pl -sdf <file.sdf>: split molecules in one SDF-file
    
    
Attention: 
    1) Input data (MAT, TXT, SDF or DIR) must be stored in directory Input,
    2) Command input is just file/directory name without paths,
    3) Directory Output is always cleaned before new start.
    \n";	       

    system("echo \"$text\""); 

    die "\n";
}

sub print_line{

# ----------
#
# screen output for information
#
# ----------
    
    my ($in) = @_;
    
    print "\n------------------------------------\n";
    print "\t$in...\n\n";
}

sub print_screen_log{

# ----------
#
# screen and log file output for information
# Attention: this LOG-file is only for COSMO-CAMD
#
# ----------
    
    my ($in) = @_;
    
    print "$in";
    print LOGLEA "$in" if($tool_para{LOGLEA});
    
}

sub print_debug{

# ----------
#
# screen output for debug
#
# ----------

    if ($tool_para{DEBUG} > 0){
    
        print "\ndebug: IN SUB <@_[0]>";
        
        foreach my $i(1..@_){
            if(@_[$i] ne ''){
                print "  with Input:" if($i == 1) ;
                print "\t@_[$i]";
            }
        }
        
        print "\n\n";

    }
}

sub print_findgroup{

# ----------
#
# screen output for information: which groups of a molecule are found
#
# ----------

    my($which_group) = @_;
    
    print("\t\tfind group: $which_group\n");
    
}

sub print_footer{

# ----------
#
# sreen output: footer
#
# ----------

    &print_line('Finished');

    print "\toutput file:\n";
    
    print "\t\t $fo_mol:\tmolecule group information\n" if(-e "$path_output/$fo_mol");
    print "\t\t $fo_str:\tlea-string \n" if(-e "$path_output/$fo_str");
    print "\t\t $fo_lib:\tfragment library\n" if(-e "$path_output/$fo_lib");
   
    print "\t\t $fo_allmol:\tall molecule from directory\n" if(-e "$path_output/$fo_allmol");
    print "\t\t $screen_sdf:\tall molecule from directory\n" if(-e "$path_output/$screen_sdf");
    
    
    print "\t\t $unique_sdf:\tunique fragment\n" if(-e "$path_output$unique_sdf");

    my @failed = split(' ', $failed_mol);
    
    $i = @failed;
    
    print "\n\t$i molecules are not splited because of too many atoms: \n\t\t Nr.@failed\n" if($i > 0);
    
    
    # attention: it is four blanks not a tab
    my $text="
    ########################################################################\n\n
    \t\t\t\tAll DONE!\n\n";

    system("echo \"$text\""); 

}

sub sendmail{

# ----------
#
# Send an email to address specified as $recipient
# copied from MAIN
# ----------
    
    local $reason = $_[0];

    $from = 'LEA3D@pc-est3.ltt.rwth-aachen.de';

    $name = ucfirst($recipient);
    $name =~ s/\..*//;
    
    if ($reason) {
        $subject = 'successfully splited';

        $message = "Dear $name,\n\nYour fragmentation of $i_mol molecules is successfully completed.\n\nAll the best and take care,\nLEA";
    }
    else {
        $subject = 'fragmentation failed';
        
        $message = "Dear $name,\n\nYour fragmentation of $i_mol molecules is terminated abnormally. Please check the LOG-file.\n\nAll the best and take care,\nLEA";
    }

    open(MAIL, "|/usr/sbin/sendmail -t");

    # Email Header
    print MAIL "To: $recipient\n";
    print MAIL "From: $from\n";
    print MAIL "Subject: $subject\n\n";
    # Email Body
    print MAIL $message;

    close(MAIL);
}
