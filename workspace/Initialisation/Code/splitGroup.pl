#!/usr/bin/perl

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    MAIN Code to decomposite the chemical functional groups of molecules.
#
#    - Stand August 2018
#
#    - written by Yifan Wang(a), supervised by Lorenz Fleitmann(b).
#      a: Institute of Thermodynamics and Thermal Process Engineering at University of Stuttgart
#      b: Chair of Technical Thermodynamics at RWTH Aachen
#
#    - Info:
#        1) Subroutines are also found in READ.pl, FIND.pl, WRITE.pl, SPLIT.pl, SORT.pl, PRINT.pl and handle_MATfile.
#        2) Setting files are needed: setting.in and setting_group.in
#        3) Some Code are original from LEA3D.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#=============================================================================#
#
#                                     MAIN
#
#=============================================================================#

#---------- header ----------

use File::Copy;
use File::Path qw(remove_tree);
use List::Util qw(min max);
use List::MoreUtils qw(uniq);
use Scalar::Util qw(looks_like_number);


# call subroutines
require "./READ.pl";
require "./WRITE.pl";
require "./PRINT.pl";
require "./FIND.pl";
require "./SPLIT.pl";
require "./SORT.pl";

# define global files name
&edit_file('define');

# clean directories
&edit_file('delete_a');

# print header
&print_header; 

#---------- get input and check enviroment ----------

# script input 
# $operation = -sdf, -dir, -txt, -i, -mat 
# $inputfile = data name in directory Input without path
local($operation, $inputfile) = @ARGV;

# check directories and files
if (!(-e $path_output)||(!(-e $path_input))||(!(-e $path_temp))){
    die "\nErr: Your enviroment is wrong! Please have a look if directory ../Input/, ../Output/, ./temp/ exist!\n\n\n";
}

# check input data
if($operation eq '-i'){
     &print_i;
}
elsif(($operation ne '-sdf') && ($operation ne '-dir') && ($operation ne '-txt') && ($operation ne '-mat') ){
    die "\nErr: Cannot understand your input, please try ./splitGroup.pl -i\n\n\n";
}

if (!(-e "$path_input/$inputfile")||(-z "$path_input/$inputfile") || $inputfile eq ''){
    die "\nErr: Cannot find the input data in read_directory $path_input$inputfile or the input data is empty!\n\n\n";
}

#---------- read setting.in ----------
&read_setting_in('1');

#---------- call subroutines according to $operation ----------
if ($operation eq '-sdf'){
    
    &print_line("Input is one SDF-file: $inputfile");
    
    copy("$path_input/$inputfile",  "./$inputfile") or die "\nErr: can't copy file $inputfile!\n";;
}
elsif($operation eq '-dir'){
    
    &print_line("Input is one SDF-directory: $inputfile");
    
    # if input is a directory, call the sub -> without ranking
    &read_directory; 
}
elsif($operation eq '-txt'){
    
    &print_line("Input is one TXT-file: $inputfile");
    
    # translate USMILES to SDF files
    &write_usmiles_2_sdf($inputfile);
    
    $inputfile = $screen_sdf;
}
elsif($operation eq '-mat'){
    
    &print_line("Input is one MAT-file: $inputfile");
    
    # sort molecule according to molStruct.score
    # write USMILES of TOP TOPMOL (see setting.in) molecules in TXT-files
    print "\t\tCalling MATLAB-script...\n\n";
    
    system("$generalFolder/MATLAB/bin/matlab -nojvm -nosplash -nodisplay -singleCompThread -r \"chdir\('$path_local'\), handle_MATfile('$inputfile', $tool_para{TOPMOL}, '$screen_txt'), quit \" " );;
   
    print "\t\tMATLAB-script done!\n\n";
    
    # translate USMILES to SDF files
    &write_usmiles_2_sdf;
    
    $inputfile = $screen_sdf;
}

# read setting_group.in file
&read_setting_groupin('1');

#---------- find cycle ----------
# find if there is a cycle every molecule
# if yes, change the bond information

# to remember the combination of cycle
# @ncycle = '';
# @cycle_atom = '';
$inputfile_ori = $inputfile;

@remember_charge = '';
&find_cycle($inputfile, 1);

#---------- global variables ----------
$i_mol = 0, $i_lego = 0, $i_moli = 0;  

$split_one_anchor = 0;
$fragDelete = '';
$failed_mol = '';

@mol='', @molping='';
@lego ='', @legono ='', @typelego ='', @points ='';

#---------- read inputfile ----------
#(big deal in it :D)

local *DOC;

open(DOC, ">$ft_lib");

    &read_origin_mol($inputfile);

close DOC;

#---------- post-processing ----------

$i_moli = 0;
$mol_start = 1;
$mol_end = @molping;

# delete new_input file
unlink $inputfile;


# file custom.sdf: change atoms H to X, write ><POINT> info
&change_H2X($fo_lib, $tool_para{SUBSTITUTION});

# write summary.out
&write_split_result($fo_str, 'all');

# delete temporary files
&edit_file('delete_e');

# delete redundant fragments
&sort_redundant;

#&change_H2X($fo_lib, 'withH'); # earlier the operator is withH

# change combination of cycle in custom.sdf
# @cycle_atom = '';
# @ncycle = '';
# @cycle_atom = '';
&find_cycle($fo_lib, 0);

# file new_collection.sdf: change atoms H to X, write ><POINT> info
$fo_lib_new = 'new_'.$fo_lib;

&change_H2X($fo_lib_new, $tool_para{SUBSTITUTION});

# @cycle_atom = '';
# @ncycle = '';
# @cycle_atom = '';
&find_cycle($fo_lib_new, 0);

# move files to directory Output and rename
&edit_file('end');

