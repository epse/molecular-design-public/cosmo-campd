function [ ] = handle_MATfile( nameMAT, topMOL, txtFile )
    
    %
    % read *.mat file from screening in directory Input
    %
    % Note to me
    % Function -> Input is mat name
    % add note -> delete temp directory

    %% user define parameters 
    % name of the MAT-file 
%     % Attention: only name needed, no path please
%     if isempty(nameMAT)
%          error('\nErr: input MAT-file %s is empty! \n\n',nameMAT);
%     end
%     
%     if isempty(topMOL)
%         warning('\nWarn: the number of molecules to split is empty! Set to 20!\n\n'); 
%         topMOL = 20;
%     end
    
    %nameMAT = 'test_small.mat';

    % the number of molecules which should be fragmented
    % topMOL = 20;

    %% processing
    
    pathMAT = strcat('../Input/', nameMAT);
    load(pathMAT);


    % ranking and read
    [~,index] = sortrows([molStruct.Score].');
    molStruct = molStruct(index(end:-1:1)); clear index

    if length(molStruct) < topMOL

        error('\nErr: There are only %d molecules from Screening. \n     But you have defined %d molecules to output. Please have a check.\n\n', n, topMOL);

    end

    molecules = cell(4, topMOL);

    header = {'#nr.' 'name' 'USMI.' 'score'};

    for i = 1:topMOL

        molecules{1, i} = i;
        molecules{2, i} = molStruct(i).name;
        molecules{3, i} = molStruct(i).SMILES;
        molecules{4, i} = molStruct(i).Score;
    end


    % write TXT-file in directory temp
    
    pathTXT = strcat('./temp/', txtFile);;
    
    fp = fopen(pathTXT, 'wt');

    formatSpec = '%d\t%s\t%s\t%.3f\n';

    fprintf(fp, '%s\t%s\t%s\t%s\n', header{:});

    fprintf(fp, formatSpec, molecules{:});

    fclose(fp);

    % screen output
    % disp('TXT-file top_mol_from_screening.txt is written in directory temp!');


end

