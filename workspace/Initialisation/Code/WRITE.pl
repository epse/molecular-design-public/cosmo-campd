#!/usr/bin/perl

print "WRITE OK \n";

#=============================================================================#
#       
#               all subs about writing things
#               
#=============================================================================#

sub write_mol_info{

# ----------
#
#    to write the functional groups information of each molecule
#
# ----------
    
    &print_debug("write_mol_info");
    
    local *INFO;
   
    
    open(INFO, ">>$fo_mol");
    
    # file header
    if ($i_mol eq 1){
        print INFO "# # # # # # # # # # # #\n\n";
        print INFO "\# molecule information\n\n";
        print INFO "\#\t-Summary of the functional groups of each molecule\n\n";

        print INFO ("# # # # # # # # # # # #\n\n");
    }
    
    # each group
    print INFO "Nr.$i_mol: $ligne[1]\n";
    
    print INFO "There are $ngroupAll functional groups: $ngroup_single single groups, $ngroup_multi multi groups and $ncycle[$i_mol] cycles.\n";
    
    print INFO ("\ngroup\tNr.\trelevant atoms\n\n");

    # write cycle
  #  print INFO "-cycle\t$ncycle[$i_mol]\tatoms [ $cycle_atom[$i_mol] ]\t\n" if ($ncycle[$i_mol] > 0);
    
    # write functional groups
    foreach $key(sort keys %nrGroup){
    
        if($nrGroup{$key} > 0){
            $combineGroup{$key} = $groupAnchor{$key};
            $combineGroup{$key} =~ s/#/ /g;
            
            if($key eq 'cycle'){
                print INFO "-$key\t$ncycle[$i_mol]\t[ $cycle_atom[$i_mol] ]\t\n" if ($ncycle[$i_mol] > 0);
            }
            else{
                print INFO "-$key\t$nrGroup{$key}\t[ $combineGroup{$key} ]\n"
            }
        }
    }
    
    print INFO "\n\$\$\$\$\n\n\n";

    close INFO;
    
};

sub change_mol_2_frag{

# ----------
#
#    this molecule is too complex to split
#       -> all atom H is changed to atom X
#       -> this molecule is used as one fragment
#
# ----------
    my $atomH = 0, $if_change = 0;
    local *IN;

    unlink $ft_copy_all;
    
    # change atom H to X
    
    foreach my $i(0..@replace_charge_0-1){
        
        my @replace_item = split('\+', $replace_charge_0[$i]);
    
        $ligne[$replace_item[0]] = $replace_item[1];
    }
    
    foreach my $i(0..@ligne){
        $ligne[$i] =~ s/ H / X /g;
    }
    
    # write the structure info in diss_all.sdf
    open (IN, ">$ft_copy_all");
        print IN @ligne;
    close IN;

    $i_lego++;
    $mp_main_tmp = $i_lego;
    $mol_main_tmp = '1*'.$combine_atomH;
    $i_moli++;

};

sub copy_all_in_one{

# ----------
#
#   write/copy all fragments(before split one-anchor-groups) in one file.
#
# ----------
    
    my ($write_file) = @_;
    
    &print_debug("copy_all_in_one", $write_file);
    
    local *IN;
    my @line, $nr_atom, @atomTemp = '', $nr_atomX = 0;
    
    # read the number of atoms of this molecule
    @line  = split(' ', $ligneTemp[4]);
    $nr_atom  = $line[0];

    # count the number of atom X
    foreach my $m(5..5 + $nr_atom){
        
        @line = split(' ', $ligneTemp[$m]);
        
        if($line[3] eq 'X'){
            $nr_atomX++;
        }
        
        die "\nErr: One fragment has atom Y.\n\n" if($line[3] eq 'Y');
    }
    
    # check, if a fragment is made up of only atom X
    if($nr_atomX != $nr_atom){
        
        # if none of defined functional groups is found in one molecule, this molecule is defined as one fragment.
        if(($mol_main_tmp eq '') && ($mp_main_tmp eq '')){
            
            print "\t\t-> This molecule is defined as 1 fragment.\n" if($ngroup_single == 0);

            if (($ngroup_single == 0) && ($ngroup_multi == 0)){
                
                foreach my $i(0..@ligneTemp){
                    $ligneTemp[$i] =~ s / H / X /g;
                }
            
                $i_lego++;
                $mp_main_tmp = $i_lego;
                $mol_main_tmp = '1*'.$combine_atomH;
                $i_moli++;
            }
        }
        
        # write the structure info in file
        open(IN, ">>$write_file");
        
            print IN @ligneTemp;
            
        close IN;
        $nr_add_frag++;
    }
    else{
        die "\nErr: The fragment $i_lego is made up of only atom X. Please have a check of the $i_mol. molecule. \n\n";
    }
    
}


sub write_ligne{

# ----------
# 
#    write the decomposed fragment inclusive Y atom(s)
#
# ----------

    &print_debug('writeLigne');
    
    my $li;

    # add information <tempXatom> for sub split_reactant.
    # the anchor atom X muss not be retained.
    
    $ligne[$line_write+2] = $ligne[$line_write];
    $ligne[$line_write] = "> <tempXatom>\n";
    $ligne[$line_write+1]= "$tempXatom\n";

    
#     if ($is_last == 0){
    # written in different files, if the call-me-subs are different
    foreach $li (1..@ligne-1){
        
        if(($ngroup_multi == 0) && ($split_one_anchor == 0)){
            print DOC $ligne[$li] if($ligne[$li] ne "");
        }
        else{
            print TEMPDOC $ligne[$li] if($ligne[$li] ne "");
        }
        
        if($li == 4 + $nbatom){
                
            foreach $li2(0..@ligneplus-1){
                if(($ngroup_multi == 0) && ($split_one_anchor == 0)){
                    print DOC $ligneplus[$li2];
                }
                else{
                    print TEMPDOC $ligneplus[$li2];
                }
            };
        };	
    };

     
   
    #lego/fragment is always added, as lang as the fragment info is written.
    $i_lego++;

    # copy the origin structure information in @ligne
    
   # print "@ligne\n++@ligneTemp\n\n";
    
    @ligne = @ligneTemp;
#     foreach $li (0..@ligne-1){
#         $ligne[$li] = $ligneTemp[$li];
#     }
}

sub move_all_2_diss{

# ----------
#
# if all multi-anchor-groups are splited, copy all fragments to dissociated.sdf 
# -> next step is to split one-anchor-groups
#
# ----------

    local (*COLL, *READ);
    
    unlink $ft_diss;
    
    # check if the number of fragments more than $tool_para{LONGGEN}
    my @count = split(' ', $mp_main_tmp);
        
    if(@count > $tool_para{LONGGEN}){
        
        print "\n\t\tThe number of fragments is more than $tool_para{LONGGEN}. So it will be written as one fragment.\n";
        
        @ligne = '';
        @ligne = @ligne_origin;
        $i_lego = $i_lego - @count;
        $i_moli = $i_moli - @count;
        
        &change_mol_2_frag;
        
    }

    # write fragment in custom.sdf
    open (COLL, ">>$fo_lib");
    open (READ, "<$ft_copy_all");
    
    while(<READ>){
        print COLL $_;
    }
    close COLL;
    close READ;
    
    unlink $ft_copy_all;

}


sub write_split_result{

# ----------
# 
#    write the split result in summary.OUT
#    before write, the order of molping of each molecule muss be sorted, so that LEA3D could read and rebuild the molecule
#
# ----------
    
    local ($sumfile, $oper) = @_;
    
    &print_debug('write_split_result', $sumfile, $oper);
    
    
    local (*OUT, $i, $j = 0);
    
    # sort the order of molping -> for LEA3D to read 
    &sort_molping if ($oper eq 'all');
    
    # screen output
    &print_line("Writing lea-strings in $sumfile") if($tool_para{WEIGHT} && $sumfile !~ /^new_/);
    
    &print_line("Writing lea-strings in $fo_str") if(!$tool_para{WEIGHT} && $sumfile =~ /^new_/);
    
    # write the combination's information in summary.out || new_summary.out
    open(OUT, ">$sumfile");
    
    print OUT "\n\t\tresult from screening:\n\n";
    print OUT "|generation\t|Rank\t|name\t|Score\t|Fragments\t|\n";
    
    for $i(0..@mol){
    
        if($mol[$i] ne ''){
            $j++;
            print OUT "|0\t|$j\t|$mol_name[$j-1]\t|$mol_score[$j-1]\t|\t$mol[$i] \/\t$molping[$i]|\n" ;
            
        }
    }
    close OUT;
    
    # screen output
    if($tool_para{WEIGHT} && $sumfile !~ /^new_/){
        
        &print_screen_log("\t\t$j lea-strings are written in $sumfile\n\n");
    }
    
    if(!$tool_para{WEIGHT} && $sumfile =~ /^new_/){
        
        &print_screen_log("\t\t$j lea-strings are written in $fo_str\n\n");
    }
}

sub change_H2X{
    
# ----------
#
# change all atom H of each fragment to atom X, as a free anchor
#
# ----------
    
    local($f_in, $operator) = @_;
     
    if ($operator){
    
        &print_line ("Changing all H atom to anchor X atom and Add <POINT> info");
    }
    else{
        &print_line ("Add <POINT> info");
    }
    
    &print_debug ('change_H2X', $f_in, $nb = 0);
    
    local $ecrit = 1, $pasvu = 1, $point = "", $point2 = "", $f_out = "H_".$f_in;	
    
    local (*DOC);
    
    # read library file, find auch atom H and change them to atom X.
    open(DOC, ">$f_out"); 
    
        &read_all_frag($f_in, 'change_H2X', $operator);
    
    close(DOC);
 
    rename $f_out, $f_in;
    
    if(($tool_para{WEIGHT})  && ($f_in !~ /^new_/) ){
        &print_screen_log("\n\t\t$nb fragments are writing in $fo_lib\n\n");
    }
    elsif((!$tool_para{WEIGHT}) && ($f_in =~ /^new_/)){
        &print_screen_log("\n\t\t$nb fragments are writing in $fo_lib\n\n");
    }
    #print "\t\t$nb fragments are writing in $f_in\n\n";	

}

sub write_usmiles_2_sdf{

# ----------
#
# tTranslate USMILES to SDF-files
#
# ----------

    local ($screen_file) = @_;
    local *IN, $each_line, $i_line = 0, @mol_nr='', @mol_USMILES='';
    
    @mol_name='';
    @mol_score='';
    
    
    if ($screen_file eq ''){
        
        $screen_file = $path_temp.$screen_txt;
        
        print "\n\tDefault input TXT-file: $screen_file\n"; 
    }
    else{
        $screen_file = $path_input.$screen_file;
    }
    
    # read USMILES from TXT-file
    &print_screen_log("\n\tNr.\tname\tUSMILES\tscore\n\n");
    
    open(IN, "<$screen_file") or die "\nErr: can't open the file $screen_file!\n";;
        while(<IN>){
        
            $each_line = $_;
            
            if (($each_line ne '') && ($each_line !~ /^#/) && (($each_line ne ' '))){
            
                @get = split(' ', $each_line);
            
                $mol_nr[$i_line] = $get[0];
                
                $mol_name[$i_line] = $get[1];
                
                $mol_USMILES[$i_line] = $get[2];
                
                $mol_score[$i_line] = $get[3];
                
                &print_screen_log("\tMol $mol_nr[$i_line]\t$mol_name[$i_line]\t$mol_USMILES[$i_line]\t$mol_score[$i_line]\n");
                
                $i_line++;
            } 
        }
    close IN;

    &print_screen_log("\n\tRead $i_line molecules.\n");

    
    # write SDF-files
    local *inSDF, *outSDF, *readSDF, $screen_file_out, $change_line;

    $screen_file_out =$screen_sdf; 

    remove_tree($path_temp."sdf");
    remove_tree($path_output."sdf"); 
  
    mkdir $path_temp."sdf/";
    unlink $screen_file_out;
    unlink $screen_sdf;
   
    open(outSDF, ">$screen_file_out");

    foreach $current_mol(0..@mol_USMILES-1){

        #print "$mol_USMILES[$current_mol]\n";
        
        local $temp_sdf = $path_temp."temp.smi";
        
        unlink $temp_sdf;
        
        open(inSDF, ">$temp_sdf");
        
            print inSDF "$mol_USMILES[$current_mol]\n";
       
        close inSDF;
        
        # call COSMOquick to write SDF-file
     #   my $sdf_file_tmp = $path_temp."sdf/".$current_mol.'_'.$mol_name[$current_mol].".sdf";
         
        my $sdf_file_tmp = $path_temp."sdf/".$current_mol.'_'.".sdf";
      
      
        `$path_RDKit $temp_sdf $sdf_file_tmp`; 

        $i_line = 0;
        
        # copy each SDF-file in one SDF-file
        
        open(readSDF, "<$sdf_file_tmp");
            
            while(<readSDF>){
            
                if($i_line == 0){
                    $change_line = $mol_name[$current_mol]."----------".$mol_USMILES[$current_mol]."----------No.$mol_nr[$current_mol]\n";
                    print outSDF $change_line;
                }
                else{
                    print outSDF $_;
                }
                
                $i_line++;
            }
        close readSDF;
        
        unlink $temp_sdf;
    }

    close outSDF;
    
    copy($screen_file, "$path_output/.start_USMI");
    
    print "\n\tTranslation is done!\n";
}

#=============================================================================#
#       
#               all subs about file things
#
#=============================================================================#

sub edit_file{

# ----------
#
# define and delete the gobal files 
#
# ----------

    my ($operator) = @_;
    
    &print_debug('edit_file', $operator);
    
    if ($operator eq 'define'){
        
        # define
        
        # fo: acronym of file output
        # ft: acronym of file temp
        # fi: acronym of file in
        # path: paths
        # screen: result from screening
        
        # define paths
        chomp($path_local = `pwd`);
        $path_temp = './temp/';
        $path_input = '../Input/'; 
        $path_output = '../Output/';
        
        # require RDKit paths for tTranslation of USMILES to SDF-file
        $path_RDKit = '../../LEA_run_options.pl';
        
        require "$path_RDKit" if (-e $path_RDKit) or die "Err: Can't find file $path_RDKit";
       
        $path_RDKit =$generalFolder.$rdkitdir;

        # define files
        $fi_setin_group = 'setting_group.in'; # !!!!
        $fi_setin_general = 'setting.in';
        
        $fo_str = 'custom.out';
        $fo_lib = 'custom.sdf';
        $fo_mol = 'mol_group_info.txt';
        $fo_allmol = 'all_mol_fromDir.sdf';
        
        $fo_log_lea = '.log_lea';
        #$fo_log_main = 'log.main';
        
        $ft_lib = 'outputTemp.sdf';
        $ft_lib_t = 'outputTemp_1.sdf';
        $ft_copy_all = 'diss_all.sdf';
        $ft_single = 'dissociated_ONE.sdf';
        $ft_diss = 'dissociated.sdf';
        
        $screen_txt = 'top_mol_from_screening.txt';
        $screen_sdf = 'screen.sdf';
        $unique_sdf = 'unique.sdf';

        # writing LOG-file, only for lea Output (hidden data)
        open(LOGLEA, ">$fo_log_lea") or die "Err: can't open $fo_log_lea!\n";;

    }
    elsif ($operator eq 'delete_a'){
        
        # delete all files before the algorithmus starts 
        unlink $ft_diss;
        unlink $fo_str;
        unlink $ft_lib;
        unlink $ft_lib_t;
        unlink $fo_mol;
        unlink $ft_copy_all;

        unlink $fo_lib;
        unlink 'new_custom.sdf';
        unlink $fo_allmol;
        
        # empty directory
        unlink glob "'../Output/*.*'";
        unlink glob "'./temp/*.*'";
        unlink glob "'dissociated*'";
        
    }
    elsif($operator eq 'delete_e'){
        
        # delete all temporary files before the algorithmus stops
        unlink $ft_lib if (-e $ft_lib);
        unlink $ft_diss if(-e $ft_diss);
    
        unlink glob "'dissociated*'";
        unlink glob "'./temp/*.*'";
    }
    elsif($operator eq 'end'){
        
        # move files to OUTPUT directory
        if ($tool_para{WEIGHT}){
            
            rename $fo_str, "$path_output$fo_str";
            rename $fo_lib, "$path_output$fo_lib";
            rename "new_$fo_lib", "$path_output$unique_sdf";
            unlink "new_$fo_str";

        }
        else{
            
            rename "new_$fo_lib", "$path_output$fo_lib";
            rename "new_$fo_str", "$path_output$fo_str";

            unlink $fo_str, $fo_lib;
        }
        
        if ($operation eq '-mat' || $operation eq '-txt' || $operation eq '-dir'){
            
            if($tool_para{SDF}){
            
                rename $fo_allmol, "$path_output$fo_allmol" if(-e $fo_allmol);
                rename $screen_sdf, "$path_output$screen_sdf" if(-e $screen_sdf);
            
            }
            
            if ($operation eq '-mat' || $operation eq '-txt'){
                
                if($tool_para{SDFDIR}){
                    system("mv $path_temp/sdf $path_output"); 
                }
                else{
                    system("rm -r $path_temp/sdf");
                }
                
                if ($operation eq '-mat'){
                    
                    rename "$path_temp/top_mol_from_screening.txt", "$path_output/top_mol_from_screening.txt" if($tool_para{TXT});
                }
            }
        }

        # group information of each molecule
        rename $fo_mol, "$path_output$fo_mol" if($tool_para{MOLINFO});
        
        # log file
        rename $fo_log_lea, "$path_output$fo_log_lea" if($tool_para{LOGLEA});               

        unlink $inputfile_ori;
        
        &print_footer;
        
        close LOGLEA;
        
        &sendmail('1') if($tool_para{SENDMAIL});
            
    }
    else{
        
        die "\nErr: This operator $operator isn't in sub edit_file defined.\n\n";
    }
}



