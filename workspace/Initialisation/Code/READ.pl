#!/usr/bin/perl

print "READ OK \n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#               all subs about reading things
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


sub read_directory{
    
# ----------
#
#    If input is a directory, copy all info.sdf in file all_mol_fromDir.sdf.
#    The name of inputfile is then changed to all_mol_fromDir.sdf.
#
# ----------

    # screen output
    &print_line("Read all molecules in directory $inputfile");
    
    
    # define local variables
    local *IN, *OUT;
    my @bn = '', $ibn = 0, $imol = 0;
          
    
    # find all .sdf files 
    unlink $fo_allmol if(-e $fo_allmol);
    
    chop(my $tmp = `ls $path_input$inputfile/*.sdf`); 
    
    
    my @getbase = split(' ',$tmp); 
    
    die "Err: There is no sdf file in the directory $path_input$inputfile.\n\n\n" if (@getbase == 0);
    
    foreach $i(0..@getbase-1){
        
        if(-e $getbase[$i]){
            $bn[$ibn] = $getbase[$i];
            $ibn++; 
        };	
    };
    
    # read each .sdf and copy information in file: all_mol_fromDir.sdf
    open(IN,">$fo_allmol");
    
    foreach my $i (0..@bn-1){
        
        print("\t Copying $bn[$i]...\n");
        
        open(OUT,"<$bn[$i]");
            
            while(<OUT>){
                print IN $_;
                $imol++ if($_ =~ /\$\$\$\$/);
            }
        close OUT;
    }
    close IN;
    
    die "Err: There is no molecule information in the file(s). Please check your inputfile.\n\n\n" if($imol == 0);
    
    # redefine the file's name to be read latter
    $inputfile = $fo_allmol;
    
    
    # screen output
    print ("\n\t$imol molecules from $ibn file are read from the directory  written in the file $fo_allmol.");
    
    
    
}

sub read_setting_groupin{

# ----------
#
#    read the file setting_group.in to know which functional groups should be splited.
#
# ----------
    
    # define variables
    my ($if_print) = @_;

    local *IN;
    my $n_close = 0, $key, @get;
    
    &print_debug('read_setting_groupin');
    
    # screen output
    &print_line("Reading setting_group.in") if ($if_print);

    # if the file setting_group.in cannot be found
    if(!(-e $fi_setin_group) || !(-f $fi_setin_group)){
        die "\nErr: Can not find file setting_group.in \n";
	};
    
    # define and copy Hash -> the group names are important and muss be identical in the code
    %typeGroup = (
            # one anchor
            'OH',           'one',
            'C(H)(H)H',     'one',
            'C(=O)OH',      'one',
            'N(H)H',        'one',
            'VII',          'one',
            'C+N',          'one',
            
            # two anchors
            'C=O',          'more',
            'C(=O)C=O',     'more',
            'C(=O)O',       'more',
            'O',            'more',
            'NH',           'more',
            'C+C',          'more',
            
            # three anchors
            'N',            'more',
            
            # four anchors
            'C=C',          'more',
            
            # cycle
            'cycle',        'more',
    );

    # save initial HASH
    %funcGroup = %{typeGroup};

    foreach $key(keys %funcGroup){
        $funcGroup{$key} = 0;
    }

    %iniGroup0 = %{funcGroup};
    
    
    # read and save the information from the file setting_group.in in Hash
    open(IN,"<$fi_setin_group");
    
    while(<IN>){

        @get = split(' ',$_);
        
        if(($get[0]!~/^#/) && ($get[0] ne '')){
            
            $funcGroup{$get[0]} = $get[1];
            $n_close++ if ($get[1] == 0);
        };
    };
    close(IN);

    die "\nWarning: All groups are closed. Please check the file setting_group.in.\n" if ($n_close eq keys %funcGroup);
    
    # screen output
    if ($if_print){

        &print_screen_log("\n\tThese functional groups are selected to be splited:\n\t");
    
        foreach $key(sort keys %funcGroup){

            &print_screen_log("-$key  ") if ($funcGroup{$key} == 1);

        }
        
        &print_screen_log("\n");
    }
}

sub read_setting_in{
    
    
# ----------
#
#    read the file setting.in to get the set up value.
#
# ----------
    
    # define variables
    my ($if_print) = @_;

    local *IN;
    my $n_close = 0, $key, @get;
    
    &print_debug('read_setting_in');
    
    
    # screen output
    &print_line("Reading setting.in") if ($if_print);

    # if the file setting_group.in cannot be found
    if(!(-e $fi_setin_general) || !(-f $fi_setin_general)){
        die "Err: Cannot find file setting_group.in \n";
	};
    
    
    # define and copy Hash -> parameter
    %tool_para = (
        'SUBSTITUTION',         0,
        'WEIGHT',               0,
        'DEBUG',                0,
        'TOPMOL',               0,
        'NRNONH',               0,
        'NRCH3',                0,
        'SENDMAIL',             0,
     
        'MOLINFO',              0,
        'LOGLEA',               0,
        'TXT',                  0,
        'SDF',                  0,
        'SDFDIR',               0,
    );
        
    
    # read and save the information from the file setting_group.in in Hash
    open(IN,"<$fi_setin_general");
    
    while(<IN>){

        @get = split(' ',$_);
        
        if(($get[0]!~/^#/) && ($get[0] ne '')){

            $tool_para{$get[0]} = $get[1];
           
        };
    };
    close(IN);

    # screen and log file output
    if ($if_print){
    
        &print_screen_log("\n\tcontrol parameters:\n");

        if ($tool_para{SUBSTITUTION}){
            &print_screen_log("\t\tWith substitution of H atom\n");
        }
        else{
            &print_screen_log("\t\tWithout substitution of H atom\n");
        }
        
        if ($tool_para{WEIGHT} == 1){
        
            &print_screen_log("\t\t\With weighing of fragments\n");
        }
        else{
        
            &print_screen_log("\t\tWithout eighing of fragments\n");
        }

        if ($tool_para{DEBUG} == 1){
        
            &print_screen_log("\t\tDebugging modul\n");
        }
    }

#     $if_delete = 1; # if delete the redundant fragments
#     $if_XH = 'withH';
#     #$if_XH = 'noH';
#     $all_sdf_file = 0;
#     
#     # to debug
#     $tool_para{DEBUG} = 0; # only for debug (>= 1)
#     

    # need to add
    
}

sub read_origin_mol{


# ----------
# 
#    read each molecule and handle it
#
# ----------

    # screen output
    &print_debug("read_origin_mol");
    &print_line("Reading $inputfile");
    
    # define variables
    my $newmol = 1, $i, $j, $k, $line_num, $eachline, $blanc, @get;
    
    local $writeoperator, $atomlourd, $rem_CH3 = $funcGroup{CH3}, $atomH, $combine_atomH, $i_charge,  
          @fonc, @ifonc, @covfonc, @replace_charge_0, @replace_charge_1, 
          *INMAIN;
          
    # read molecules
    open(INMAIN, "<$_[0]");

    while(<INMAIN>){
        
        $eachline=$_;

        if ($newmol){
            
            # local variables
            $line_num = 0;
            $line_write = 0;
            $writeoperator = 1;
            $i = 1;
            $j = 0;
            $k = 0; 
            $newmol = 0;
            $atomlourd = 0;
            $blanc = " ";
            $atomH = 0;
            $combine_atomH = 0;
            
            $i_charge = 0;
            @replace_charge_0 = '';
            @replace_charge_1 = '';
            
            
            # global variables
            $i_mol++;
            @fonc = '';
            @ifonc = '';
            @covfonc = '';
            @ligne = "";
        }
        
        @get = split(' ', $_);
        
        $line_num++;

        $writeoperator = 1 if ($eachline =~/\$\$\$\$/);

        if ($eachline =~/\$\$\$\$/){
            
            $writeoperator = 0;
            $line_write++;
            
            chomp($_);
            $ligne[$line_write]=$_."\n";
        }
         
        if ($writeoperator){
            $line_write++;
            $ligne[$line_write]=$_;
        }
        
        # read line 4
        if ($line_num == 4){
            $nbatom = $get[0];
            $nbbond = $get[1];
            
            die "\nErr: There is something wrong in your inputfile. Please check if the sdf format is right.\n\n\n" if ((!(looks_like_number($nbatom))) || (!(looks_like_number($nbbond))));
        }
        
        # read and save information of each atom
        if (($line_num > 4) && ($i <= $nbatom)){
            $corx[$i] = $get[0];
            $cory[$i] = $get[1];
            $corz[$i] = $get[2];
            $atom[$i] = $get[3];
            
            if(($atom[$i] eq 'H') && ($atomH == 0)){
                $atomH = $i;
            }
            
            $atomlourd++ if($atom[$i] ne 'H'); 

            $i++;
        }
        
        # read and bond information
        if (($line_num > 4) && ($i > $nbatom) && ($j <= $nbbond)){
            
            if ($j == 0) {
                $j++;
            }
            else{
                
                @coller=split(' *',$get[0]);
                @coller2=split(' *',$get[1]);
                
                if(@coller==6 && $get[1] ne ""){
                    $get[0]=$coller[0].$coller[1].$coller[2];
                    $get[2]=$get[1];
                    $get[1]=$coller[3].$coller[4].$coller[5];
                }   
                elsif(@coller==6 && $get[1] eq ""){
                        $get[0]=$coller[0].$coller[1];
                        $get[1]=$coller[2].$coller[3].$coller[4];
                        $get[2]=$coller[5];
                }
                elsif(@coller==5){
                    if($_=~/^\s/){
                        $get[0]=$coller[0].$coller[1];
                        $get[2]=$get[1];
                        $get[1]=$coller[2].$coller[3].$coller[4];
                    }
                    else{
                        $get[0]=$coller[0].$coller[1].$coller[2];
                        $get[2]=$get[1];
                        $get[1]=$coller[3].$coller[4];
                    };
                }
                elsif(@coller2==4){
                        $get[1]=$coller2[0].$coller2[1].$coller2[2];
                        $get[2]=$coller2[3];
                }
                elsif(@coller==7){
                        $get[0]=$coller[0].$coller[1].$coller[2];
                        $get[1]=$coller[3].$coller[4].$coller[5];
                        $get[2]=$coller[6];
                };
                
                die "\nErr: There is something wrong in your inputfile. Please check if the sdf format is right.\n\n\n" 
                if ((!(looks_like_number($get[0]))) || (!(looks_like_number($get[1]))) || (!(looks_like_number($get[2]))) );
                
                if($combine_atomH == 0){
                    
                    if ($get[0]== $atomH){
                        $combine_atomH = $get[1];
                    }
                    elsif ($get[1] == $atomH){
                        $combine_atomH = $get[0];
                    }
                }	
                
                # change bond because of charge
                
                if(($remember_charge[$i_mol] =~/--$get[0]-$get[1]-/) || ($remember_charge[$i_mol] =~/--$get[1]-$get[0]-/)){
#                 print "$remember_charge[$i_mol] =~/-$get[0]-$get[1]/\n\n ";
#                 print "line is: $ligne[$line_write]";
#                     print "$get[0]++$get[1]++$get[2]\n";
                    $get[2]++; 
                    
                    @get_tmp = '';
                    @get_tmp = @get;
                    
                    foreach my $item(0..@get_tmp-1){ 
                        if($get_tmp[$item] < 10){
                            $get_tmp[$item] = '  '.$get_tmp[$item];
                        }
                        elsif($get_tmp[$item] < 100){
                            $get_tmp[$item] = ' '.$get_tmp[$item];
                        }
                    }
                    
                    $replace_charge_0[$i_charge] = $line_write.'+'.$ligne[$line_write];
                    $replace_charge_1[$i_charge] = join('', @get_tmp);
                    $replace_charge_1[$i_charge] =$line_write.'+'.$replace_charge_1[$i_charge]."\n";  
                    
                    $i_charge++;
                }
                
                # remember the combination information of each bond
                $fonc[$get[0]]=$fonc[$get[0]].$blanc.$get[2].'-'.$atom[$get[1]].$blanc;
                $ifonc[$get[0]]=$ifonc[$get[0]].$blanc.$get[1].$blanc;
                $covfonc[$get[0]]=$covfonc[$get[0]].$blanc.$get[2] if($atom[$get[1]] ne 'H');
                $nbondAtom[$get[0]]=$nbondAtom[$get[0]]+$get[1];
                
                $fonc[$get[1]]=$fonc[$get[1]].$blanc.$get[2].'-'.$atom[$get[0]].$blanc;
                $ifonc[$get[1]]=$ifonc[$get[1]].$blanc.$get[0].$blanc;
                $covfonc[$get[1]]=$covfonc[$get[1]].$blanc.$get[2] if($atom[$get[0]] ne 'H');
                $nbondAtom[$get[1]]=$nbondAtom[$get[1]]+$get[1];

                $j++;
            }
        }
        
        # finish to read one molecule 
        if ($eachline =~/\$\$\$\$/){
            
           # unlink glob "'dissociated*'";
            
            # operator of the file dissociated_nr.sdf.
            $new_dissociated = 1;
            
            # the LEA String and its fragments of each molecule
            $mol_main_tmp = '';  
            $mp_main_tmp = '';  
            
            # open to split single-anchor-fragment
            $remember_one_anchor = 0;
            $split_one_anchor = 0; 
            $close_CH3 = 0;
            $nr_add_frag = 0;
            
            # remember original combination
            @ligne_origin = '';
            @ligne_origin = @ligne;
            
            # screen output
            print "\n\tThis is molecule: $i_mol\n";
            
            print "\t\tThere are more than $tool_para{NRNONH} heavy atoms. I am trying to split it...
            \n" if ($atomlourd > $tool_para{NRNONH});
           
            # about cycle
            @cycle_one_mol = split(' ', $cycle_atom[$i_mol]);
            $split_cycle = 1;

            # check if fused Rings are included 
            if(@cycle_one_mol > 2){
                &check_cycle;
            }
            else{
                $cycle_atoms_one = $cycle_atom[$i_mol];
                $cycle_atoms_one =~ s/ /-/g;
                $cycle_atoms_one = '-'.$cycle_atoms_one.'-';
            }

	    
            # find the functional groups of this molecule
            &find_group('1');
            
            if($ngroupAll > 0){
                
                foreach $i_replace(0..@replace_charge_1-1){
                
                    my @replace_item = split('\+', $replace_charge_1[$i_replace]);
    
                    $ligne[$replace_item[0]] = $replace_item[1] if($replace_item[0] ne '');
                    $ligne_origin[$replace_item[0]] = $replace_item[1] if($replace_item[0] ne '');
                }
            
            }
            # check comlexity
            if(($ngroup_multi > 1) && ($atomlourd > $tool_para{NRNONH})){
                
                print "\n\t\t-> This molecule is too hard for me and it will be written as one fragment.\n";
                $failed_mol = $failed_mol.' '.$i_mol;
                
                # if the molecule too hard to split
                &change_mol_2_frag;
                
                &write_mol_info;
            }
            else{
                
                if($ngroup_single > 0){

                    if(($ngroup_single == $nrGroup{'C(H)(H)H'}) && ($nrGroup{'C(H)(H)H'} > $tool_para{NRCH3}) && ($ngroup_multi != 0)){
                        
                        $remember_one_anchor = 0;
                        $close_CH3 = 1;
                        
                        print "\n\t\t-> Split -CH3 closed!\n";
                    }
                    elsif($nrGroup{'C(H)(H)H'} > $tool_para{NRCH3}){  # changed --Yifan
                    
                        $close_CH3 = 1;
                        $remember_one_anchor = 1;
                        print "\n\t\t-> Split -CH3 closed!\n";
                    }
                    else{
                    
                        $close_CH3 = 0;
                        $remember_one_anchor = 1;
                    }
                }
                
                # write the information of groups in file: mol_group_info.txt, die read by sub find_group
                &write_mol_info;
                
                # to split the f-groups
                &control_cut('is_origin', '');

                unlink $ft_single;
                
                $split_one_anchor = 1 if ($remember_one_anchor == 1);
                
                # to split single anchor
                if ($split_one_anchor == 1){
                    
                    my $OPENTMP;
                    
                    my $filetemp = $ft_copy_all;
                                
                    open ($OPENTMP, "<$filetemp");
                        
                        &read_temp_frag(*$OPENTMP);
                    
                    close $OPENTMP;
                    
                    unlink $ft_copy_all;
                    rename $ft_single, $ft_copy_all;
                    
                }
            }
            
            # move all fragment to dis
            &move_all_2_diss;
            
            $funcGroup{CH3} = $rem_CH3; 
            $mol[$i_mol] = $mol_main_tmp;
            $molping[$i_mol] = $mp_main_tmp;
            $split_one_anchor = 0;
            $newmol=1;
            
#             print "mol and molping in read_origin_mol: $mol[$i_mol] \t$molping[$i_mol] \n";

        }
        
        
    }
    close INMAIN;
    
    
}

sub read_temp_frag{

# ----------
#
#   read fragments
#
# ----------
    
    my $new_frag = 1, $i_frag = 0, $conv2, $i, $j, $k, $writeoperator, $compt, @get, @coller, @coller2;
    local $i_addfrag = 0, $line_write, @fonc, @ifonc, @covfonc, @ligne;

    local *OPENTEMPAll = shift;

    while(<OPENTEMPAll>){

        $conv2=$_;
        
        if ( $new_frag){
             
             $i_frag++;
             $compt = 0;
             $line_write = 0;
             $writeoperator = 1;
             $i = 1;
             $j = 0;
             $k = 0; 
             $blanc=" ";
             $new_frag = 0;
             $atomlourd = 0;
             
             @fonc='';
             @ifonc='';
             @covfonc='';
             @ligne='';
        }

        @get = split(' ', $_);
        $compt++;

        $writeoperator = 1 if ($conv2 =~/\$\$\$\$/);
        
        if ($writeoperator){
            $line_write++;
            $ligne[$line_write]="$_";
        }
        
        if (($get[0] eq 'M') && ($get[1] eq 'END')){
            
            $writeoperator = 0;  
        }
        if ($compt == 4){
            
            $nbatom = $get[0];
            $nbbond = $get[1];
            
            die "\nErr: There is something wrong in your inputfile. Please check if the sdf format is right.\n\n\n" if ((!(looks_like_number($nbatom))) || (!(looks_like_number($nbbond))));
        }
        
        if (($compt > 4) && ($i <= $nbatom)){
            
            $corx[$i] = $get[0];
            $cory[$i] = $get[1];
            $corz[$i] = $get[2];
            $atom[$i] = $get[3];
            $atomlourd++ if($atom[$i] ne 'H'); 

            $i++;
        }
        
        if (($compt > 4) && ($i > $nbatom) && ($j <= $nbbond)){
            
            if ($j == 0) {
                $j++;
            }
            else{
                
                @coller=split(' *',$get[0]);
                @coller2=split(' *',$get[1]);
                
                if(@coller==6 && $get[1] ne ""){
                    $get[0]=$coller[0].$coller[1].$coller[2];
                    $get[2]=$get[1];
                    $get[1]=$coller[3].$coller[4].$coller[5];
                }   
                elsif(@coller==6 && $get[1] eq ""){
                        $get[0]=$coller[0].$coller[1];
                        $get[1]=$coller[2].$coller[3].$coller[4];
                        $get[2]=$coller[5];
                }
                elsif(@coller==5){
                    if($_=~/^\s/){
                        $get[0]=$coller[0].$coller[1];
                        $get[2]=$get[1];
                        $get[1]=$coller[2].$coller[3].$coller[4];
                    }
                    else{
                        $get[0]=$coller[0].$coller[1].$coller[2];
                        $get[2]=$get[1];
                        $get[1]=$coller[3].$coller[4];
                    };
                }
                elsif(@coller2==4){
                        $get[1]=$coller2[0].$coller2[1].$coller2[2];
                        $get[2]=$coller2[3];
                }
                elsif(@coller==7){
                        $get[0]=$coller[0].$coller[1].$coller[2];
                        $get[1]=$coller[3].$coller[4].$coller[5];
                        $get[2]=$coller[6];
                };
                
                die "\nErr: There is something wrong in your inputfile. Please check if the sdf format is right.\n\n\n" 
                if ((!(looks_like_number($get[0]))) || (!(looks_like_number($get[1]))) || (!(looks_like_number($get[2]))) );
                
                $fonc[$get[0]]=$fonc[$get[0]].$blanc.$get[2].'-'.$atom[$get[1]].$blanc;
                $ifonc[$get[0]]=$ifonc[$get[0]].$blanc.$get[1].$blanc;
                $covfonc[$get[0]]=$covfonc[$get[0]].$blanc.$get[2] if(($atom[$get[1]] ne 'H') && ($atom[$get[1]] ne 'X'));
                $nbondAtom[$get[0]]=$nbondAtom[$get[0]]+$get[1];
                
                $fonc[$get[1]]=$fonc[$get[1]].$blanc.$get[2].'-'.$atom[$get[0]].$blanc;
                $ifonc[$get[1]]=$ifonc[$get[1]].$blanc.$get[0].$blanc;
                $covfonc[$get[1]]=$covfonc[$get[1]].$blanc.$get[2] if(($atom[$get[0]] ne 'H') && ($atom[$get[1]] ne 'X'));
                $nbondAtom[$get[1]]=$nbondAtom[$get[1]]+$get[1];

                $j++;
            }
        }
        
        if ($conv2 =~/\$\$\$\$/){
            
            &read_setting_groupin if ($split_one_anchor == 0);
            
            if ($split_one_anchor == 0){
            
                $istratom = $nbatom;
                
                # find cycle combination of fragment
                &cyclesdf('sub');
                
                @cycle_one_mol = split(' ', $cycle_atom_sub);
                $split_cycle = 1;
                
                if(@cycle_one_mol > 2){
                    &check_cycle;
                }
                else{
                    $cycle_atoms_one = $cycle_atom_sub;
                    $cycle_atoms_one =~ s/ /-/g;
                    $cycle_atoms_one = '-'.$cycle_atoms_one.'-';
                }

            }

            &find_group;
            
         #   <STDIN> if($tool_para{DEBUG});
            
            if ($split_one_anchor == 0){

                &control_cut('is_temp_frag', $i_frag);
                
            }
            elsif($split_one_anchor == 1){
                
                if(($ngroup_single == $nrGroup{'C(H)(H)H'}) && ($close_CH3) && ($nrGroup{'C(H)(H)H'} != 0)){
                    
                    $ngroup_single = 0;
                    &control_cut('is_one_anchor', $i_frag + $i_addfrag);
                    
                }
                else{
            
                    $funcGroup{CH3} = 0 if ($close_CH3);
                    &control_cut('is_one_anchor', $i_frag + $i_addfrag);
               }
            }   
            
            $new_frag = 1;
        }
        
    }

}

sub read_all_frag{

# ----------
#
# read fragment for different subs 
#
# ----------
    
    my ($readfile, $which_call, $operator) = @_;
    
    &print_debug('read_all_frag', $readfile, $which_call) if ($tool_para{DEBUG});
    
    local *READ;
    
    local $new_one = 1, $conv2, $compt, $i, $j, $k, $blanc, $comp2, $nbatom, $nbond, $istratom, $lignedata, $flagdata, $differentiel, $differentielx;
    
    local @bond, @listb, @typeb, @type, @corx, @cory, @corz, @lignecor, @lignebond, @ligne, @copylign, $nomol, @fonc; @ifonc;

    open(READ, "<$readfile");
    
    while(<READ>){

        $conv2=$_;

        if ($new_one){

            $compt = 0;
            $i = 1;
            $j = 0;
            $k = 0;
            
            $blanc=" ";
            @bond='';
            @listb='';
            
            @fonc = '';
            @ifonc = '';
            
            @typeb='';
            @type='';
            @corx='';
            @cory='';
            @corz='';
            
            @lignecor='';
            @lignebond='';
            @ligne = '';
            
            $lignedata='';
            $flagdata=0;
            $new_one=0;
            $differentiel=0;
            $differentielx=0;
            $nr_mol++;
            
            # add number of each fragment
            if ($which_call eq 'sort_redundant'){

                chop($conv2);
                $conv2 = $conv2."---frag.$nr_mol\n";
            }
            
        };

        
        @get = split(' ',$_);
        
        $ligne[$compt] = $_;
       
        
        $LIGEND[$nr_mol]=$LIGEND[$nr_mol].$conv2 if ($which_call eq 'sort_redundant');
        
        $compt++;
        
        if (($conv2 =~/^M  CHG/) && ($which_call eq 'find_cycle') && ($readfile eq $inputfile)){
        
            # check the line items
            
            die if (($get[0] ne 'M') || ($get[1] ne 'CHG'));
            
            foreach my $item(2..@get-1){
                
                die if(!looks_like_number($get[$item]));
            }
            
            my $charge_par = $get[2]/2;
            
            foreach $item(0..$charge_par-1){
                
                die if (($get[4*$item+4] > 1) || ($get[4*$item+6] > 1));
                
                if ($remember_charge[$nr_mol] eq ''){
                    $remember_charge[$nr_mol] = '--'.$get[4*$item+3].'-'.$get[4*$item+5].'-';
                }
                else{
                    $remember_charge[$nr_mol] = $remember_charge[$nr_mol].'-'.$get[4*$item+3].'-'.$get[4*$item+5].'-';
                }
            
            }
            
#             print "remeber:$remember_charge[$nr_mol]\n";
        }
        
        

        if (($compt > 4) && ($i <= $nbatom)){
            
            if ($which_call eq 'change_H2X'){
                
                $comp2 = $compt-4;
                
                if ($operator){
                    if ($get[3] eq 'H'){
                        $_ =~ s/ H / X /g;
                        $get[3] = 'X';
                    }
                }
                
                $point = $point.'-'.$comp2.'-' if ($get[3] eq "X");
            
            }
            else{
                
                $corx[$i]=$get[0];
                $cory[$i]=$get[1];
                $corz[$i]=$get[2];
                $type[$i]=$get[3]; 
            
                $lignecor[$i]=$_;

            }
            
            $i++;
        };

        if (($compt > 4) && ($i > $nbatom) && ($j <= $nbond)){
            
            if ($j == 0){
                $j++;
                $point = $point."-" if ($which_call eq 'change_H2X');
            }
            else{
            
                @coller=split(' *',$get[0]);
                @coller2=split(' *',$get[1]);
                
                if(@coller==6 && $get[1] ne ""){
                    $get[0]=$coller[0].$coller[1].$coller[2];
                    $get[2]=$get[1];
                    $get[1]=$coller[3].$coller[4].$coller[5];
                }
                elsif(@coller==6 && $get[1] eq ""){
                    $get[0]=$coller[0].$coller[1];
                    $get[1]=$coller[2].$coller[3].$coller[4];
                    $get[2]=$coller[5];
                }
                elsif(@coller==5){
                    
                    if($_=~/^\s/){
                        $get[0]=$coller[0].$coller[1];
                        $get[2]=$get[1];
                        $get[1]=$coller[2].$coller[3].$coller[4];
                    }
                    else{
                        $get[0]=$coller[0].$coller[1].$coller[2];
                        $get[2]=$get[1];
                        $get[1]=$coller[3].$coller[4];
                    };
                }
                elsif(@coller==4){
                    if($_=~/^\s/){
                        $get[0]=$coller[0];
                        $get[2]=$get[1];
                        $get[1]=$coller[1].$coller[2].$coller[3];
                    }
                    else{
                        $get[0]=$coller[0].$coller[1].$coller[2];
                        $get[2]=$get[1];
                        $get[1]=$coller[3];
                    };					
                }
                elsif(@coller2==4){
                    $get[1]=$coller2[0].$coller2[1].$coller2[2];
                    $get[2]=$coller2[3];
                }
                elsif(@coller==7){
                    $get[0]=$coller[0].$coller[1].$coller[2];
                    $get[1]=$coller[3].$coller[4].$coller[5];
                    $get[2]=$coller[6];
                };
                
                die "\nErr: There is something wrong by the molecule $nr_mol in your inputfile. Please check if the sdf format is right.\n\n\n" 
                
                if ((!(looks_like_number($get[0]))) || (!(looks_like_number($get[1]))) || (!(looks_like_number($get[2]))) );
                
                if (($which_call eq 'find_cycle') ||($which_call eq 'sort_redundant')){
                    
                    $bond[$get[0]]=$bond[$get[0]].$blanc.$get[1].$blanc.$get[2];
                    $listb[$get[0]]=$listb[$get[0]].$blanc.$get[1];
                    $typeb[$get[0]]=$typeb[$get[0]].$blanc.$get[2];

                    $bond[$get[1]]=$bond[$get[1]].$blanc.$get[0].$blanc.$get[2];
                    $listb[$get[1]]=$listb[$get[1]].$blanc.$get[0];
                    $typeb[$get[1]]=$typeb[$get[1]].$blanc.$get[2];
                    
                    $fonc[$get[0]]=$fonc[$get[0]].$blanc.$get[2].'-'.$type[$get[1]].$blanc;
                    $fonc[$get[1]]=$fonc[$get[1]].$blanc.$get[2].'-'.$type[$get[0]].$blanc;
                    
                    $ifonc[$get[0]]=$ifonc[$get[0]].$blanc.$get[1].$blanc;
                    $ifonc[$get[1]]=$ifonc[$get[1]].$blanc.$get[0].$blanc;
                    
                    $lignebond[$j]=$_;
                }
                elsif($which_call eq 'change_H2X'){
                    
                    if($point=~/\-$get[0]\-/){

                        $point2=$point2."-".$get[1];
                    }
                    elsif($point=~/-$get[1]-/){
                        $point2=$point2."-".$get[0];
                    };
                }

                $j++;
            };
        };
        
        if($which_call eq 'change_H2X'){
            
            if($_=~/<POINTS>/){

                if($point2 ne ''){
                    $point2=~s/^-//;
                    $point2=~s/-$//;
                    
                    print DOC "> <POINTS>\n";
                    print DOC "$point2\n";
                    print DOC "\n";
                    
                };
                $ecrit=0;
                $pasvu=0;
            }
            elsif($_=~/^\$\$\$\$/){
                if($pasvu){
                
                    if($point2 ne ''){
                    
                        $point2 =~ s/^-//;
                        $point2 =~ s/-$//;
                        print DOC "> <POINTS>\n";
                        print DOC "$point2\n";
                        print DOC "\n";
                    };
                };
                
                $nb++;
                $point="";
                $point2="";
                $pasvu=1;

                print DOC $_;	
            }
            elsif($ecrit){
                print DOC $_;
            };
                $ecrit=1 if($get[0] eq ''); 	
        
        }

        if($compt > 4 && $i > $nbatom && $j > $nbond){
            $flagdata=1 if($_=~/^>/ && ($_=~/CAS/ || $_=~/cas/ || $_=~/NAME/ || $_=~/MDLNUMBER/ || $_=~/ZINC/));
        };	
        
        if ($compt == 4){
            
            $nbatom = $get[0];
            $nbond = $get[1];

            @coller=split(' *',$nbatom);
            
            if(@coller > 3 && @coller == 6){
                $nbatom=$coller[0].$coller[1].$coller[2];
                $nbond=$coller[3].$coller[4].$coller[5];
            }
            elsif( @coller > 3 && @coller == 5){
                
                if($_=~/^\s/){
                    $nbatom=$coller[0].$coller[1];
                    $nbond=$coller[2].$coller[3].$coller[4];
                }
                else{
                    $nbatom=$coller[0].$coller[1].$coller[2];
                    $nbond=$coller[3].$coller[4];
                };
            };
            
            die "\nErr: There is something wrong by molecule $nr_mol in your inputfile. Please check if the sdf format is right.\n\n\n" if ((!(looks_like_number($nbatom))) || (!(looks_like_number($nbond))));
        };
        
        
        if ($which_call eq 'sort_redundant'){
            
            if($readPOINT eq 1){
                
                chomp($XPOINT[$nr_mol] = $conv2);
            
                $readPOINT = 0;
            }
            
            $readPOINT = 1 if ($conv2 =~/POINTS/);
        
        
        }
        
        if ($conv2 =~/\$\$\$\$/){
            
            if ($which_call eq 'find_cycle'){

                @copylign = ''; 
                @copylign = @lignecor;
                
                $istratom = $nbatom;
                
                &cyclesdf;
            
                &changebond;
            }
            elsif($which_call eq 'sort_redundant'){
                
                $FONC[$nr_mol] = join('+++', @fonc);

                $IFONC[$nr_mol] = join('+++', @ifonc);

                $NBATOM[$nr_mol] = $nbatom;
                $NBBOND[$nr_mol] = $nbond;
                $ATOM[$nr_mol] = join('+++', @type);

            }
            
            $flagdata=0;
            $new_one=1;
        }
        
    } 
    
    close (READ);

}

sub read_str{

# ----------
#
# read mol and molping from summary.out
#
# ----------


    local *FILESUM;
    my @line_temp = '', @line_final= '';
    
    (@mol, @molping)= ('','');
    
    open(FILESUM, "<$_[0]");
    
    while(local $row = <FILESUM>){ 
        
        chomp $row;
        
        # see summary file
        @line_temp = split(/\s*\|\s*/,$row); 
        @line_final = split(/\s*\/\s*/,$line_temp[5]);

        # save mol and molping
        if ($. >= 5){
            $mol[$.-5]     = $line_final[0];
            $molping[$.-5] = $line_final[1];
        }
    }
            
    close FILESUM;
}
