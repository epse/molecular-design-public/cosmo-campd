#!/usr/bin/perl

print "FIND OK \n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#               all subs about finding things
#               - groups
#               - cycles
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 



#=============================================================================#
#
#                                 about functional groups
#
#=============================================================================#

sub find_group{

# ----------
#
#    find which groups does a molecule/fragment have
#
# ----------
    
    &print_debug("find_group");
    
    # initialisation of variables
    local ($if_print) = @_;
    my $anotherC = '';    
    
    # to count
    %nrGroup = {%iniGroup0};
    
    # to write
    %combineGroup = {%iniGroup0};
    
    # to split
    %groupAnchor = {%iniGroup0};
    %groupAnchorG = {%iniGroup0};

    $ngroup_single = 0;
    $ngroup_multi = 0;
    $ngroupAll = 0;
    
    # to remember the combination information of each group
    @groupCYCanchor = '';
    @groupCYCanchorG = '';
    
    
    @groupOanchor = '';
    @groupOanchorG = '';
    
    @groupCC3anchor = '';
    @groupCC3anchorG = '';
    
    @groupCC2anchor = '';
    @groupCC2anchorG = '';
    
    @groupCO2O1anchor = '';
    @groupCO2O1anchorG = '';
    
    @groupCO2anchor = '';
    @groupCO2anchorG = '';
    
    @groupCO2CO2anchor = '';
    @groupCO2CO2anchorG = '';
    
    @groupCN3anchor = '';
    @groupCN3anchorG = '';
    
    @groupNHHanchor = '';
    @groupNHHanchorG = '';
    
    @groupNHanchor = '';
    @groupNHanchorG = '';
    
    @groupNanchor = '';
    @groupNanchorG = '';
    
    my $used = 0, $used_atoms = '-', $used_atoms_cycle = '-', $nrcycle = 0;
    # read each atom to find groups
    foreach $bi(1..$nbatom){
    
#         print "used atom: $used_atoms\n";
       # print "$cycle_atoms_one++$bi\n";
        
        # find cycle
        if(($cycle_atoms_one =~ /-$bi-/)){

            foreach my $each_cycle(0..@cycle_one_mol-1){

#                 print "each: $bi+++$cycle_one_mol[$each_cycle]\n";
                
                $cycle_one_mol[$each_cycle]= '-'.$cycle_one_mol[$each_cycle].'-';
                
                if(($cycle_one_mol[$each_cycle] =~ /-$bi-/) && ($used_atoms_cycle !~ /-$bi-/)){
                
                    $recordGroupA = $bi;
                    $recordGroupG = '';
                    $pnot = '';
                    $pHX = '';
                    
                    @det = split(' ', $fonc[$bi]);
                    @detF = split(' ',$ifonc[$bi]);
                
                   #print "@det\n@detF\n\n\n";
                    
                    foreach $k(0..@det-1){
                        
                        chomp($det[$k]);
                        
                        @det1 = split('-', $det[$k]);
                        
                        $recordGroupA = $recordGroupA.'-'.$detF[$k] if($det1[0] > 5);
                        
                     #   print "bi is: $bi $detF[$k] $det[$k] $used_atoms_cycle\n";
                        
                        if(($det1[0] < 5) && ($used_atoms_cycle!~ /-$detF[$k]-/)){
                            
                            if(($det[$k] eq '1-H') || ($det[$k] eq '1-X')){
                                if ($pHX eq ''){
                                    $pHX = $detF[$k];
                                }
                                else{
                                    $pHX = $pHX.'-'.$detF[$k];
                                }
                            }
                            else{
                                if($pnot eq ''){
                                    $pnot = $detF[$k];
                                }
                                else{
                                    $pnot = $pnot.'-'.$detF[$k];
                                }
                            }
                        }
                    }
            
                    $recordGroupA = '' if ($pnot eq '');
            
                    #print "not is: $pnot\n";
                    
                    $used_atoms_cycle = $used_atoms_cycle.'-'.$pnot.'-';
                    
                  
                    
                    if($recordGroupA ne ''){
                         
                        $recordGroupA = $recordGroupA.'-'.$pHX if ($pHX ne '') ;

                        
                        if($pnot =~ /-/){
                            
                            @temp = split( '-', $pnot);
                            
                            @recordGroupA_tmp = '';
                            
                           # die if (@temp != 2);

                            foreach $i_temp(0..@temp-1){
                                
                                $recordGroupA_tmp[$i_temp] = $recordGroupA; 
                                
                                @det = split(' ', $fonc[$temp[$i_temp]]);
                                @detF = split(' ',$ifonc[$temp[$i_temp]]);

                                
                                if ($recordGroupG eq ''){
                                    $recordGroupG = $temp[$i_temp];
                                }
                                else{
                                    $recordGroupG = $recordGroupG.'+'.$temp[$i_temp];                                
                                }
                                
                               
                                
                                foreach $k(0..@det-1){
				    my $tmp_record = '-'.$recordGroupG.'-';
				    
                                    $recordGroupG = $recordGroupG.'-'.$detF[$k] if(($detF[$k] != $bi) && (($det[$k] ne '1-X') ) && ($tmp_record !~ /-$detF[$k]-/));
                                }
                                
                                foreach $i_temp2(0..@temp-1){
                                    $recordGroupA_tmp[$i_temp] = $recordGroupA.'-'.$temp[$i_temp2] if($i_temp2 != $i_temp);
                                }
                                
                                
                            }
                            
                            $recordGroupA=join('+', @recordGroupA_tmp);
                        }
                        else{
			  
			    
			     
                            $recordGroupG = $pnot;
                            @det = split(' ', $fonc[$recordGroupG]);
                            @detF = split(' ',$ifonc[$recordGroupG]);
                            
                          
                            foreach $k(0..@det-1){
				
				my $tmp_record = '-'.$recordGroupG.'-';
				
                                $recordGroupG = $recordGroupG.'-'.$detF[$k] if(($detF[$k] != $bi) && ($tmp_record !~ /-$detF[$k]-/));
                                
#                                 print "test:\n$detF[$k]\n$recordGroupG \n\n";
				$tmp_record = '';
                            }
                        }

#                         print "hier:$each_cycle\n$recordGroupA\n$recordGroupG\n";
                        
                        if($funcGroup{'cycle'}){
                            if ($groupCYCanchor[$nrcycle]eq ''){
                                
                                $groupCYCanchor[$nrcycle] = $recordGroupA;
                                $groupCYCanchorG[$nrcycle] = $recordGroupG;
                            }
                            else{
                                $groupCYCanchor[$nrcycle] =$groupCYCanchor[$nrcycle].'+'.$recordGroupA;
                                $groupCYCanchorG[$nrcycle] = $groupCYCanchorG[$nrcycle].'+'.$recordGroupG;
                            }
                        
                            $groupAnchor{'cycle'} = join('#', @groupCYCanchor);
                            $groupAnchorG{'cycle'} = join('#',@groupCYCanchorG );
                            
                            
                                
                            $car = $groupAnchor{'cycle'};
                            $nrGroup{'cycle'} = $car =~ s/#/#/g;
                            $nrGroup{'cycle'}++;
                            
                            $nrcycle++;
                        
                        }
#                         print "cycle:$groupAnchor{'cycle'}\n$groupAnchorG{'cycle'}\n$nrGroup{'cycle'}\n";
                    }   
                }
            }
        }
        
        if ($atom[$bi] eq 'C'){
            
            # find C(=O)O
            if(($fonc[$bi] =~ / 2-O /) && ($fonc[$bi] =~ / 1-O /) && (($fonc[$bi] =~ / 1-C /) || ($fonc[$bi] =~ / 1-H /) )){
                
                $used = 0;
                
                @det  = split(' ', $fonc[$bi]);
                @detF = split(' ',$ifonc[$bi]);
                
                $pO2 = '';
                $pO1 = '';
                $pO2O1not = '';
                
                # find (=O) and (O)
                foreach $k(0..@det-1){
                    
                    if($det[$k] eq '2-O'){
                        $pO2 = $detF[$k];
                        $used = 1 if ($atom[$pO2] eq '');
                        $atom[$pO2]='';
                    }
                    elsif($det[$k] eq '1-O'){
                        $pO1 = $detF[$k];
                        $used = 1 if ($atom[$pO1] eq '');
                        $atom[$pO1]='';
                    }
                    else{
                        if ($pO2O1not eq ''){
                            $pO2O1not = $detF[$k];
                        }
                        else{
                            $pO2O1not = $pO2O1not.'-'.$detF[$k];
                        }
                    }
                }
                
                
                @det1=split(' ', $fonc[$pO1]);
                $k_c = 0;
                
                foreach $k(0..@det1-1){
                    chmod($det1[$k]);
                    $k_c++ if (($det1[$k] eq '1-C') || ($det1[$k] eq '1-H') || ($det1[$k] eq '1-X'));
                }
                
                if (($k_c eq @det1) && ($used == 0)){
                
                    $recordGroupA = $pO2.'-'.$pO1;
                    $recordGroupG = '';    
                    
                    $endj = 1;
                    $bi1 = $bi;
                    $if_COOH = 0;
                    
                    # if the atom (O) combines with atom (H) or atom (X)
                    if($fonc[$pO1] =~ / 1-X /){
                        $endj = 0;   
                    }
                    elsif($fonc[$pO1] =~ / 1-H /){
                        $endj = 0;
                        $if_COOH = 1;
                    }
                    # if the molecule/fragment is HC(=O)OH, HC(=O)OX, XC(=O)OH,  XC(=O)OX
                    if(($endj == 0) && (($fonc[$bi] =~ / 1-H /) || ($fonc[$bi] =~ / 1-X /))){
                        $endj = -1; 
                        &print_findgroup("C(=O)O")if ($if_print && $funcGroup{'C(=O)O'} && (!$if_COOH));
                        &print_findgroup("C(=O)OH")if ($if_print && $funcGroup{'C(=O)OH'} && $if_COOH);
                    }
                    
                    # if the molecule/fragment is HC(=O)O..., XC(=O)O... (... means not H or X atom)
                    if(($endj == 1) && (($fonc[$bi] =~ / 1-H /) || ($fonc[$bi] =~ / 1-X /))){
                        
                        $endj = 0;
                        $bi1 = $pO1;

                        @det1=split(' ', $fonc[$pO1]);
                        @detF1=split(' ',$ifonc[$pO1]);
                        
                        foreach my $k(0..@det1-1){
                            $pO2O1not = $detF1[$k] if ($detF1[$k] ne $bi); 
                        }
                        
                        $recordGroupA = $bi;
                    }

                    # remember the information of combination
                    foreach my $j(0..$endj){

                        if($j>0){
                            
                            $bi1 = $pO1;
                            
                            @det1=split(' ', $fonc[$pO1]);
                            @detF1=split(' ',$ifonc[$pO1]);
                        
                            foreach my $k(0..@det1-1){
                                $pO2O1not = $detF1[$k] if ($detF1[$k] ne $bi); 
                            }
                            
                            $recordGroupG = '';
                            $recordGroupA = $bi;
                        }

                        @det1=split(' ', $fonc[$pO2O1not]);
                        @detF1=split(' ',$ifonc[$pO2O1not]);
                    
                    
                        foreach $k(0..@det1-1){
                    
                            if($detF1[$k] ne $bi1){
                        
                                if ($recordGroupG eq ''){
                                    $recordGroupG = $detF1[$k];
                                }
                                else{
                                    $recordGroupG= $recordGroupG.'-'.$detF1[$k];
                                }
                            }
                        }

                        if($if_COOH && $funcGroup{'C(=O)OH'}){

                            if($used_atoms !~ /-$bi-/ ){
                                
                                $used_atoms = $used_atoms.'-'.$pO2O1not.'-';
                                
                                $recordGroupA = $bi1."-".$recordGroupA;
                                $recordGroupG = $pO2O1not."-".$recordGroupG;
                                
                                if($groupAnchor{'C(=O)OH'} eq ''){
                                    $groupAnchor{'C(=O)OH'} = $recordGroupA;
                                    $groupAnchorG{'C(=O)OH'}= $recordGroupG;
                                }
                                else{
                        
                                    $groupAnchor{'C(=O)OH'} = $groupAnchor{'C(=O)OH'}.'#'.$recordGroupA;
                                    $groupAnchorG{'C(=O)OH'}= $groupAnchorG{'C(=O)OH'}.'#'.$recordGroupG;
                        
                                }
                        
                                $nrGroup{'C(=O)OH'}++;
                                
                                $ngroup_single++;
                                $ngroupAll++;
                            }
                            &print_findgroup("C(=O)OH")if ($if_print);
                            
                     #       print "COOH: $groupAnchor{'C(=O)OH'}\n$groupAnchorG{'C(=O)OH'}\n$used_atoms\n";
                        }
                        else{
                            
                            $used_atoms = $used_atoms.'-'.$bi1.'-'.$recordGroupA;
                             
                            if ($groupCO2O1anchor[$nrGroup{'C(=O)O'}] eq ''){
                            
                                $groupCO2O1anchor[$nrGroup{'C(=O)O'}]=$bi1."-".$recordGroupA;
                                $groupCO2O1anchorG[$nrGroup{'C(=O)O'}]=$pO2O1not."-".$recordGroupG;
                            
                            }
                            else{
                            
                                $groupCO2O1anchor[$nrGroup{'C(=O)O'}]=$groupCO2O1anchor[$nrGroup{'C(=O)O'}].'+'.$bi1."-".$recordGroupA;
                                $groupCO2O1anchorG[$nrGroup{'C(=O)O'}]=$groupCO2O1anchorG[$nrGroup{'C(=O)O'}].'+'.$pO2O1not."-".$recordGroupG;
                            }
                        }
                        $record_atoms = $record_atoms.' '.$groupCO2O1anchor[$nrGroup{'C(=O)O'}].' ';
                    }

                    if (($endj ne -1) && (!$if_COOH)){
                            
                        if($funcGroup{'C(=O)O'}){
#                             $ngroupCO2O1++;
                            $nrGroup{'C(=O)O'}++;
                               
                            $groupAnchor{'C(=O)O'} = join('#', @groupCO2O1anchor);
                            $groupAnchorG{'C(=O)O'} = join('#',@groupCO2O1anchorG);   

                            $ngroup_multi++;
                            $ngroupAll++;
                         }   
                            &print_findgroup("C(=O)O")if ($if_print);
                    }
                }
                $used = 0;
            }
            # find C(=O)N
#             elsif(($fonc[$bi]=~/ 2-O /)&&($fonc[$bi]=~/ 1-N /)& (($fonc[$bi] =~ / 1-C /) || ($fonc[$bi] =~ / 1-H /))){
#                 warn "Warning: Find group C(=O)N, which is not defined.\n\n\n" if($used_atoms !~ /-$bi-/);
#             }
            
            # find C(=O)C(=O) or C=O 
            elsif(($fonc[$bi]=~/ 2-O /) && ($used_atoms !~ /-$bi-/)){

                # how many C and H/X does atom bi combine with
                $car = $fonc[$bi];
                $nccC = $car =~ s/ 1-C / 1-C /g;
                $nccH = $car =~ s/ 1-H / 1-H /g;
                $nccX = $car =~ s/ 1-X / 1-X /g;
                
                $nccH = $nccH + $nccX;
                
                @det=split(' ', $fonc[$bi]);
                @detF=split(' ',$ifonc[$bi]);
                
                $findCO2CO2 = 0;
                $noread = 1;
                
                # if atom $bi combines more than one 1-C...

                if($nccC > 1){
                    foreach $k(0..@det-1){
                        if(($det[$k] eq '1-C') && ($fonc[$detF[$k]] =~ / 2-O /)){
                            
                            # find another C=O
                            $car = $fonc[$detF[$k]];
                            $nccC = $car =~ s/ 1-C / 1-C /g;
                            $findCO2CO2++ if ($nccC > 1);
                            $noread = 0 if ($nccC == 1);
                        }
                    }
                }
                elsif(($nccC == 1) && ($nccH == 1)){
                    
                    foreach $k(0..@det-1){
                    
                        if(($det[$k] eq '1-C') && ($fonc[$detF[$k]] =~ / 2-O /)){
                            
                            $car = $fonc[$detF[$k]];
                        
                            $nccC = $car =~ s/ 1-C / 1-C /g;
                            $noread = 0 if ($nccC == 1);
                            
                        }
                    }
                }
                
#                 print "$findCO2CO2\t$funcGroup{CO2}\t$funcGroup{CO2CO2}\t$noread\n";
                
                $findCO2CO2 = 0 if (($funcGroup{'C(=O)C=O'} == 0) && ($findCO2CO2 == 1));

                if($findCO2CO2 > 1 ){
                    warn "Warning: There is a C(=O)C(=O)C(=O)\n" if($findCO2CO2 > 1);
                }
                elsif(($findCO2CO2 == 1) && ($nccH < 1)){
                    
                    # find C(=O)C(=O) 
                    
                    # remember combination information

                    $pO2 = '';
                    $pO2H = '';
                    $pO2not = '';
                    
                    $pC = '';
                    $pCO2 = '';
                    $pCO2H = '';
                    $pCO2not = '';

                    foreach $k(0..@det-1){
                        
                        chomp($det[$k]);
                     
                        if($used_atoms !~ /-$detF[$k]-/){
                        
                                if($det[$k] eq '2-O'){  
                                    $pO2 = $detF[$k];
                                }
                                elsif(($det[$k] eq '1-C') && ($fonc[$detF[$k]] =~ / 2-O /)){
                                    
                                
                                    
                                    $pC = $detF[$k];
                                    $atom[$bi]='';
                                    $atom[$pC]='';
                                    
                                    @det1=split(' ', $fonc[$detF[$k]]);
                                    @detF1=split(' ',$ifonc[$detF[$k]]);

                                    foreach $k1(0..@det1-1){
                                        
                                        if($det1[$k1] eq '2-O'){ 
                                            $pCO2 = $detF1[$k1];
                                
                                        }
                                        elsif(($detF1[$k1] ne $bi) && ($det1[$k1] eq '1-C' )){
                                            $pCO2not = $detF1[$k1];
                                        }
                                        elsif(($detF1[$k1] ne $bi) && (($det1[$k1] eq '1-H')||($det1[$k1] eq '1-X'))){
                                            $pCO2H = $detF1[$k1];
                                        }
                                    }
                                    
                                  #  print "k is $detF[$k]\n$fonc[$detF[$k]],$pCO2not\n";
                                }
                                else{
                                    if($pO2not eq ''){
                                    
                                        $pO2not = $detF[$k];
                                    }
                                    else{
                                        $pO2not = $pO2not.'-'.$detF[$k];
                                    }
                                }
                                
                        }
                        else{
                            $findCO2CO2 = 0;
                        }
                    }
                    
                    
                    if($findCO2CO2){
                    $recordGroupA = '';
                    $recordGroupG = '';

                    $endj = 1;
                    $endj = 0 if ($pCO2H ne '');

                    $recordGroupA =  $pO2.'-'.$pC;
                    
                    $bi1 = $bi;
                    
                  #  print "hier:$endj,not $pCO2not,$pCO2H, $bi,$pC\n";
                    
                    foreach my $j(0..$endj){
                        
                        if($j > 0){
                            $pO2not = $pCO2not;
                            $bi1 = $pC;
                            $recordGroupA = $pCO2.'-'.$bi;
                        }
                        
                        @det1=split(' ', $fonc[$pO2not]);
                        @detF1=split(' ',$ifonc[$pO2not]);
                                    
                        $recordGroupG = '';
                                
                        foreach $k(0..@det1-1){
                    
                            if($detF1[$k] ne $bi1){
                    
                                if ($recordGroupG eq ''){
                                    $recordGroupG = $detF1[$k];
                                }
                                else{
                                    $recordGroupG= $recordGroupG.'-'.$detF1[$k];
                                }
                            }
                        }
                            
                        if ($groupCO2CO2anchor[$nrGroup{'C(=O)C=O'}] eq ''){
            
                            $groupCO2CO2anchor[$nrGroup{'C(=O)C=O'}]=$bi1."-".$recordGroupA;
                            $groupCO2CO2anchorG[$nrGroup{'C(=O)C=O'}]=$pO2not."-".$recordGroupG;
            
                        }
                        else{
            
                            $groupCO2CO2anchor[$nrGroup{'C(=O)C=O'}]=$groupCO2CO2anchor[$nrGroup{'C(=O)C=O'}].'+'.$bi1."-".$recordGroupA;
                            $groupCO2CO2anchorG[$nrGroup{'C(=O)C=O'}]=$groupCO2CO2anchorG[$nrGroup{'C(=O)C=O'}].'+'.$pO2not."-".$recordGroupG;
                        }
                        
                        $used_atoms = $used_atoms.'-'.$bi1.'-'.$recordGroupA;
                    }
                    
                    if($funcGroup{'C(=O)C=O'}){
                        
                        $nrGroup{'C(=O)C=O'}++;
                        
                        $groupAnchor{'C(=O)C=O'} = join('#', @groupCO2CO2anchor);
                        $groupAnchorG{'C(=O)C=O'} = join('#',@groupCO2CO2anchorG);   
                            
                        $ngroup_multi++;
                        $ngroupAll++;
                        &print_findgroup("C(=O)C(=O)")if ($if_print);
                    }
                    
                    }
                }
                elsif(($findCO2CO2 == 0) && ($funcGroup{'C=O'}) && ($noread == 1)){

                    $pO2 = '';
                    $pO2H = '';
                    $pO2not = '';

                    if($nccH == 2){
                        
                      #  if($funcGroup{'C=O'}){
                            #$ngroupCO2++;
                            #$nrGroup{'C=O'}++;
                            &print_findgroup("C=O")if ($if_print);
                       # }
                    }
                    else{
                        if(($nccC == 2)||(($nccH == 1) && ($nccC == 1))){

                            foreach $k(0..@det-1){
                                if($det[$k] eq '2-O'){  
                                    $pO2 = $detF[$k];
                                }
                                elsif(($det[$k] eq '1-H') || ($det[$k] eq '1-X')){
                                    $pO2H= $detF[$k];
                                }
                                else{
                                    if($pO2not eq ''){
                                        $pO2not = $detF[$k];
                                    }
                                    else{
                                        $pO2not = $pO2not.'-'.$detF[$k];
                                    }
                                }
                            }

                            $recordGroupA = '';
                            $recordGroupG = '';
                        
                            $endj = 1;
                            @pO2not1 = split('-',$pO2not);
                            
                            if ($nccH == 1){
                                $endj = 0;
                                $recordGroupA = $pO2.'-'.$pO2H;
                            }
                            else{
                            
                                $recordGroupA = $pO2.'-'.$pO2not1[@pO2not1-1];
                            }
                            $bi1 = $bi;

                            foreach my $j(0..$endj){
                                
                                if($j > 0){
                                    $recordGroupA = $pO2.'-'.$pO2not1[0];
                                }
                                
                                @det1=split(' ', $fonc[$pO2not1[$j]]);
                                @detF1=split(' ',$ifonc[$pO2not1[$j]]);
                                    
                                $recordGroupG = '';
                                
                                foreach $k(0..@det1-1){

                                    if($detF1[$k] != $bi1){
                    
                                        if ($recordGroupG eq ''){
                                            $recordGroupG = $detF1[$k];
                                        }
                                        else{
                                            $recordGroupG= $recordGroupG.'-'.$detF1[$k];
                                        }
                                    }
                                }
                            
                                if ($groupCO2anchor[$nrGroup{'C=O'}] eq ''){
                    
                                    $groupCO2anchor[$nrGroup{'C=O'}]=$bi1."-".$recordGroupA;
                                    $groupCO2anchorG[$nrGroup{'C=O'}]=$pO2not1[$j]."-".$recordGroupG;
                    
                                }
                                else{
                    
                                    $groupCO2anchor[$nrGroup{'C=O'}]=$groupCO2anchor[$nrGroup{'C=O'}].'+'.$bi1."-".$recordGroupA;
                                    $groupCO2anchorG[$nrGroup{'C=O'}]=$groupCO2anchorG[$nrGroup{'C=O'}].'+'.$pO2not1[$j]."-".$recordGroupG;
                                }
                                
                                $used_atoms = $used_atoms.'-'.$bi1.'-'.$recordGroupA;
                            }
                            
                       #     if($funcGroup{'C=O'}){
                                
#                                 $ngroupCO2++;
                                $nrGroup{'C=O'}++;
                                
                                $groupAnchor{'C=O'} = join('#', @groupCO2anchor);
                                $groupAnchorG{'C=O'} = join('#',@groupCO2anchorG);   
                            
                                $ngroup_multi++;
                                $ngroupAll++;
                                &print_findgroup("C=O")if ($if_print);
                                
                             #   print "@groupCO2anchor\n@groupCO2anchorG\n";
                        #    }
                            
                        }
                    }       
                
                
                }
            }
            
            # find C+C
            elsif(($fonc[$bi]=~/ 3-C /) && ($fonc[$bi] !~ / 1-H /) && ($fonc[$bi] !~ / 1-X /)){

                @det = split(' ', $fonc[$bi]);
                @detF = split(' ', $ifonc[$bi]);
 
                $pCC3 = '';
                $pCC3Not = '';

                foreach $k(0..@det-1){
                    
                    chomp($det[$k]);
                    
                    if ($det[$k] eq '3-C'){
                       
                        if ($pCC3 eq ''){
                            
                            $pCC3 = $detF[$k];
                        }
                        else{
                            
                            $pCC3 =$pCC3.'-'.$detF[$k];
                        }
                    }
                    else{

                        if ($pCC3Not eq ''){
                            $pCC3Not = $detF[$k];
                        }
                        else{
                            $pCC3Not =$pCC3Not.'-'.$detF[$k];
                        }
                    }
                }
           
                $recordGroup = '';

                my $findSameAtom = 0;
                my @tempFindSame = '';
                
                for $i(0..@groupCC3anchor-1){
                
                    @tempFindSame = split('\-|\+', $groupCC3anchor[$i]);

                    if($tempFindSame[1] == $bi){
                        $findSameAtom = 1;
                        last;
                    }
                }

                $endj = 1;
                
                $endj = 0 if(($fonc[$pCC3] =~ / 1-H /) || ($fonc[$pCC3] =~ / 1-X /));

                if(!$findSameAtom){
                    my $bi1 = $bi;
                
                    foreach my $j(0..$endj){
                    
                        if($j>0){
                        
                            $pCC3 = '';
                            $pCC3Not = '';
                        
                            foreach $k(0..@det-1){
                    
                                if ($det[$k] eq '3-C'){
                                    $pCC3 =$pCC3.'-'.$k;
                                }
                                else{
                                    $pCC3Not =$pCC3Not.'-'.$k;
                                }
                            }
                    
                            $recordGroup = '';
                            $pCC3 = $detF[$pCC3];
                            $pCC3Not = $detF[$pCC3Not];
                        }
                    
                        @det1 = split(' ', $fonc[$pCC3Not]);
                        @detF1 = split(' ', $ifonc[$pCC3Not]);
                
                        foreach $k(0..@det1-1){
                    # print ("bi is $bi\n");
                            if($detF1[$k] ne $bi1){
                                if ($recordGroup eq ''){
                                    $recordGroup=$detF1[$k];
                                }
                                else{
                                    $recordGroup= $recordGroup.'-'.$detF1[$k];
                                }

                            }
                        }
                        if ($groupCC3anchor[$nrGroup{'C+C'}] eq ''){
                            $groupCC3anchor[$nrGroup{'C+C'}]=$bi1."-".$pCC3;
                            $groupCC3anchorG[$nrGroup{'C+C'}]=$pCC3Not."-".$recordGroup;
                        }
                        else{
                            $groupCC3anchor[$nrGroup{'C+C'}]=$groupCC3anchor[$nrGroup{'C+C'}].'+'.$bi1."-".$pCC3;
                            $groupCC3anchorG[$nrGroup{'C+C'}]=$groupCC3anchorG[$nrGroup{'C+C'}].'+'.$pCC3Not."-".$recordGroup;
                        }
                        
                        @det = split(' ', $fonc[$pCC3]);
                        @detF = split(' ', $ifonc[$pCC3]);
                        $bi1 = $pCC3;

                    }
                    
                   if($funcGroup{'C+C'}){
                        
                       # print "@groupCC3anchor\n@groupCC3anchorG\n";
#                         $ngroupCC3++;
                        $nrGroup{'C+C'}++;
                        
                        $groupAnchor{'C+C'} = join('#', @groupCC3anchor);
                        $groupAnchorG{'C+C'} = join('#',@groupCC3anchorG);   
                            
                        $ngroup_multi++;
                        $ngroupAll++;
                        &print_findgroup("C+C")if ($if_print);
                    }
                    
                }            
            }
            
            # find C=C
            elsif($fonc[$bi]=~/ 2-C / && ($bi != $anotherC)){
                
                $car=$fonc[$bi];
                $nocc=$car=~s/ 2-C / 2-C /g;

                my $noccH=$car=~s/ 1-H | 1-X / 1-H /g;
                
                if(($nocc == 1) && ($noccH != 2)){
                    
                    @det=split(' ', $fonc[$bi]);
                    @detF=split(' ',$ifonc[$bi]);
    
                    $pCC2 = '';
                    $pCC2H = '';
                    $pCC2Not = '';
                 
                    foreach $k(0..@det-1){
                        
                        chomp($detF[$k]);
                        
                        if ($det[$k] eq '2-C'){
                                #print("hierhier is $pCC2 \n");
                            $pCC2 =$detF[$k];
                            
                                #print("hierhier is $pCC2 \n");
                        }
                        elsif(($det[$k] eq '1-H') || ($det[$k] eq '1-X') ){
                            $pCC2H = $detF[$k];
                            
                        }
                        elsif(($det[$k] ne '1-H') && ($det[$k] ne '2-C') && ($det[$k] ne '1-X')){
                            if ($pCC2Not eq ''){
                                $pCC2Not = $detF[$k];
                            }
                            else{
                                $pCC2Not =$pCC2Not.'-'.$detF[$k];
                            }
                        }
                    }
                    
                    $car = $fonc[$pCC2];
                    $nocc = $car =~ s/ 2-C / 2-C /g;

                    if ($nocc == 1){    
                        
                        $anotherC = $pCC2;
                        $recordGroup = '';

                        my $findSameAtom = 0;
                        my $findCombineAtom = 0;
                        my @tempFindSameAnchor = '';
                        my @tempFindSameAnchor1 = '';

                        for $i(0..@groupCC2anchor-1){
                        
                            @tempFindSameAnchor = split('\+', $groupCC2anchor[$i]);

                            
                            foreach $j(0..@tempFindSameAnchor-1){
                                
                                @tempFindSameAnchor1 = split('\-', $tempFindSameAnchor[$i]);
                                
                                if(($bi == $tempFindSameAnchor1[1])){
                                    $findSameAtom = 1;
                                    last;
                                }
                                
                            }
                        }

                        $car = $fonc[$pCC2];
                        $nocc1 = $car =~ s/ 1-H | 1-X / 1-H /g;

                        $car = $fonc[$bi];
                        $nocc2 = $car =~ s/ 1-H | 1-X / 1-H /g;
                        
        
                        $nocc = $nocc1  + $nocc2;

                        $endj = 0 if ($nocc1 == 2);
                        $endj = 1 if ($nocc1 < 2);;
                        
                        if(!$findSameAtom){
                            my $bi1 = $bi;
                            
                            foreach my $j(0..$endj){
                            
                                if($j>0){
                                
                                    $pCC2 = '';
                                    $pCC2H = '';
                                    $pCC2Not = '';
                                
                                    foreach $k(0..@det-1){
                            
                                        if ($det[$k] eq '2-C'){
                                            $pCC2 =$detF[$k];
                                        }
                                        elsif(($det[$k] eq '1-H') || ($det[$k] eq '1-X')) {
                                                $pCC2H = $detF[$k];
                                        }
                                        elsif($det[$k] =~ /1-/){
                                            
                                            if ($pCC2Not eq ''){
                                                $pCC2Not = $detF[$k];
                                            }
                                            else{
                                                $pCC2Not =$pCC2Not.'-'.$detF[$k];
                                            }
                                        }
                                        else{
                                            die "\nErr: The combination of atom $atom[$bi] is not defined!\n\n\n";
                                        }
                                    }
                                }
                                
                                my @pCC2Not1 = split('-', $pCC2Not);
                                
                                
                                foreach my $ipCC2Not1(0..@pCC2Not1-1){
                                    
                                    $recordGroup = '';
                                    $recordGroup1 = '';
                                    
                                    @det1 = split(' ', $fonc[$bi1]);
                                    @detF1 = split(' ', $ifonc[$bi1]);

                                    foreach $k(0..@det1-1){
                                # print ("bi is $bi\n");
                                        if($detF1[$k] ne $pCC2Not1[$ipCC2Not1]){
                                            
                                            if ($recordGroup1 eq ''){
                                                $recordGroup1=$detF1[$k];
                                            }
                                            else{
                                                $recordGroup1= $recordGroup1.'-'.$detF1[$k];
                                            }
                                        #print("$k\t$detNot1[$k]\n");
                                        }
                                    }
                                    
                                    @det1 = split(' ', $fonc[$pCC2Not1[$ipCC2Not1]]);
                                    @detF1 = split(' ', $ifonc[$pCC2Not1[$ipCC2Not1]]);

                                    foreach $k(0..@det1-1){
                                # print ("bi is $bi\n");
                                        if($detF1[$k] ne $bi1){
                                            if ($recordGroup eq ''){
                                                $recordGroup=$detF1[$k];
                                            }
                                            else{
                                                $recordGroup= $recordGroup.'-'.$detF1[$k];
                                            }
                                        #print("$k\t$detNot1[$k]\n");
                                        }
                                    }
                                    
                                    if ($groupCC2anchor[$nrGroup{'C=C'}] eq ''){
                                        
                                        $groupCC2anchor[$nrGroup{'C=C'}]=$bi1."-".$recordGroup1;

                                        $groupCC2anchorG[$nrGroup{'C=C'}]=$pCC2Not1[$ipCC2Not1]."-".$recordGroup;
                                        
                                    }
                                    else{
                                        
                                        $groupCC2anchor[$nrGroup{'C=C'}]=$groupCC2anchor[$nrGroup{'C=C'}].'+'.$bi1."-".$recordGroup1;
                                    
                                        $groupCC2anchorG[$nrGroup{'C=C'}]=$groupCC2anchorG[$nrGroup{'C=C'}].'+'.$pCC2Not1[$ipCC2Not1]."-".$recordGroup;
                                    }
                                    
                                    $used_atoms = $used_atoms.'-'.$bi1.'-'.$recordGroup1;
#                                     print("CC2 $groupCC2anchor[$nrGroup{'C=C'}] $groupCC2anchorG[$nrGroup{'C=C'}]\n");
                                }
                                
                                @det = split(' ', $fonc[$pCC2]);
                                @detF = split(' ', $ifonc[$pCC2]);
                                $bi1 = $pCC2;
                            }   
                        }
                        
                          if($funcGroup{'C=C'}){ 

#                             $ngroupCC2++;
                            $nrGroup{'C=C'}++;
                            
                            $groupAnchor{'C=C'} = join('#', @groupCC2anchor);
                            $groupAnchorG{'C=C'} = join('#',@groupCC2anchorG);   
                            
                            $ngroupAll++;
                            $ngroup_multi++;
                         #   print "@groupCC2anchor\n@groupCC2anchorG\n";
                            
                            &print_findgroup("C=C")if ($if_print);
                        }
                    }
                }
                elsif($nocc eq 2){
                     warn "Warning: C=C=C is not defined!\n"; 
                }
            } 
        
            # find CH3
            elsif($fonc[$bi]=~/ 1-H /){

                $car = $fonc[$bi];
                $nccH = $car =~ s/ 1-H / 1-H /g;
                $nccX = $car =~ s/ 1-X / 1-X /g;

                if (($nccH == 3) && ($nccX != 1)){

                    @det=split(' ', $fonc[$bi]);
                    @detF=split(' ',$ifonc[$bi]);
                    
                    $pH = '';
                    $pHnot = '';
                    
                    $recordGroupG = '';
                    $recordGroupA = '';
                    
                    foreach $k(0..@det-1){
                        if(($det[$k] eq '1-H') || ($det[$k] eq '1-X')){
                            
                            if($pH eq ''){
                                $pH = $detF[$k];
                            }
                            else{
                                $pH = $pH.'-'.$detF[$k];
                            }
                        }
                        else{
                            $pHnot = $detF[$k];
                        }
                    }
                    
                    $recordGroupG = $pHnot;
                    $recordGroupA = $bi.'-'.$pH;
                    
                   
                    @det=split(' ', $fonc[$pHnot]);
                    @detF=split(' ',$ifonc[$pHnot]);
                    
                    foreach $k(0..@det-1){
                        if ($detF[$k] ne $bi){
                            $recordGroupG = $recordGroupG.'-'.$detF[$k];
                        }
                    }
                
                    if($funcGroup{'C(H)(H)H'}){
                        
                        if($used_atoms !~ /-$bi-/ ){
                            
                            $used_atoms = $used_atoms.'-'.$pHnot.'-';
                            
                            if($groupAnchor{'C(H)(H)H'} eq ''){
                                $groupAnchor{'C(H)(H)H'} = $recordGroupA;
                                $groupAnchorG{'C(H)(H)H'}= $recordGroupG;
                            }
                            else{
                    
                                $groupAnchor{'C(H)(H)H'} = $groupAnchor{'C(H)(H)H'}.'#'.$recordGroupA;
                                $groupAnchorG{'C(H)(H)H'}= $groupAnchorG{'C(H)(H)H'}.'#'.$recordGroupG;
                    
                            }
                    
                            $nrGroup{'C(H)(H)H'}++;
#                             $combineGroup{'C(H)(H)H'} = $groupAnchor{'C(H)(H)H'};
#                             $combineGroup{'C(H)(H)H'} =~s/&/ /g;
                            
                            $ngroup_single++;
                            $ngroupAll++;
                        }
                        
                        &print_findgroup("CH3")if ($if_print);
                    }

                }
            }
            
            # find C+N
            elsif(($fonc[$bi]=~/ 3-N /) && ($fonc[$bi] !~/ 1-X /)){
                
                $pg = '';
                $pgnot = '';
                    
                @det=split(' ', $fonc[$bi]);
                @det1=split(' ',$ifonc[$bi]);
 
                $recordGroupG = '';
                $recordGroupA = '';
                
                local $need_to_split = 0;
                
                foreach $k(0..@det-1){
                    
                    if($det[$k] eq '3-N'){
                        $pg = $det1[$k];
                    }
                    elsif($det[$k] eq '1-H'){
                        $need_to_split = 0;
                    }
                    elsif($det[$k] eq '1-C'){
                        $pgnot = $det1[$k];
                         $need_to_split = 1;
                    }
                }
                
                if ($need_to_split){
                    
                    $recordGroupA = $bi.'-'.$pg;
                    $recordGroupG = $pgnot;

                    @det=split(' ', $fonc[$pgnot]);
                    @detF=split(' ',$ifonc[$pgnot]);
                    
                    foreach $k(0..@det-1){
                        if ($detF[$k] ne $bi){
                            $recordGroupG = $recordGroupG.'-'.$detF[$k];
                        }
                    }
    
                #  print "@groupCN3anchor\n@groupCN3anchorG\n";
                
                    if($funcGroup{'C+N'}){
                        
                        if($used_atoms !~ /-$bi-/ ){
                
                            $used_atoms = $used_atoms.'-'.$pgnot.'-';
                
                        
                            if ($groupAnchor{'C+N'} eq ''){
                                $groupAnchor{'C+N'} = $recordGroupA;
                                $groupAnchorG{'C+N'}= $recordGroupG;
                            }
                            else{
                                $groupAnchor{'C+N'} =$groupAnchor{'C+N'}.'#'.$recordGroupA;
                                $groupAnchorG{'C+N'}=$groupAnchorG{'C+N'}.'#'.$recordGroupG;
                                    
                            }

                            $nrGroup{'C+N'}++;
                            #$combineGroup{'C+N'} = join(' ', @groupCN3anchor);
#                             $combineGroup{'C+N'} = $groupAnchor{'C+N'};
                            $ngroup_single++;
                            $ngroupAll++;
                            &print_findgroup('C+N')if ($if_print);
                        }
                    }
                }
            }
        }
        
        elsif ($atom[$bi] eq 'O'){
            
            $recordGroup = '';
            
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
            
            $car=$fonc[$bi];
            $nocc=$car=~s/ 1-C / 1-C /g;
            
            # find OH
            if(($fonc[$bi] =~ / 1-H /) && ($fonc[$bi] !~ / 1-X /)){
        
                $pH = 0;
                $pHnot = 0;    
                
                foreach $k(0..@det-1){
                    $pH = $det1[$k] if($det[$k]=~/1-H/);
                    $pHnot = $det1[$k] if($det[$k]!~/1-H/);
                }
                
                @detNot=split(' ', $fonc[$pHnot]);
                @detNot1=split(' ',$ifonc[$pHnot]);
                
                foreach $k(0..@detNot-1){
                   # print ("bi is $bi\n");
                    if($detNot1[$k] ne $bi){
                        if ($recordGroup eq ''){
                            $recordGroup=$detNot1[$k];
                        }
                        else{
                            $recordGroup= $recordGroup.'-'.$detNot1[$k];
                        }
                        #print("$k\t$detNot1[$k]\n");
                    }
                }
                if(($fonc[$bi]=~/ 1-C /)){
                    
                    #@det=split(' ', $fonc[$bi]);
                    $p = 0;
                    
                    foreach $k(0..@det-1){
                        $iatom=$det1[$k] if($det[$k]=~/1-C/);
                    }

                    if($fonc[$iatom]!~/ 2-O /){
                        
                        if($funcGroup{'OH'}){
                            
                            if($used_atoms !~ /-$bi-/ ){
                            
                                $used_atoms = $used_atoms.'-'.$pHnot.'-';
                                
                                if ($groupAnchor{'OH'} eq ''){
                                    $groupAnchor{'OH'} = $bi.'-'.$pH;
                                    $groupAnchorG{'OH'}=$pHnot.'-'.$recordGroup;
                                }
                                else{
                                    $groupAnchor{'OH'} =$groupAnchor{'OH'}.'#'.$bi.'-'.$pH;
                                    $groupAnchorG{'OH'}=$groupAnchorG{'OH'}.'#'.$pHnot.'-'.$recordGroup;
                                    
                                }
                                
                                $nrGroup{'OH'}++;
                        
#                                 $combineGroup{'OH'} = $groupAnchor{'OH'};
                                $ngroup_single++;
                                $ngroupAll++;
                                &print_findgroup("OH")if ($if_print);
                            }
                        }
                    }
                }
                else{
                
                    $noccH=$car=~s/ 1-H / 1-H /g;
                    if ( $noccH ne 2){ 
                        
                        if($funcGroup{'OH'}){
                            
                            if($used_atoms !~ /-$bi-/ ){
                            
                                $used_atoms = $used_atoms.'-'.$pHnot.'-';
                                
                                if ($groupAnchor{'OH'} eq ''){
                                    $groupAnchor{'OH'} = $bi.'-'.$pH;
                                    $groupAnchorG{'OH'}=$pHnot.'-'.$recordGroup;
                                }
                                else{
                                    $groupAnchor{'OH'} =$groupAnchor{'OH'}.'#'.$bi.'-'.$pH;
                                    $groupAnchorG{'OH'}=$groupAnchorG{'OH'}.'#'.$pHnot.'-'.$recordGroup;
                                    
                                }
                            
    #                         print("OH:$groupOHanchor[$ngroupOH]++$groupOHanchorG[ $ngroupOH]\n");

                                $nrGroup{'OH'}++;                        

                                $ngroup_single++;
                                $ngroupAll++;
                                &print_findgroup("OH")if ($if_print);
                            }
                        }
                    }
            
                }
           }
            
            # find C-O-C
            elsif($nocc == 2){
            
                @det=split(' ', $fonc[$bi]);
                @detF=split(' ',$ifonc[$bi]);
                
                $pO = '';
                $pOnot = '';
                
                $pO = $bi;
                
                foreach $k(0..@detF-1){
                    
                    if ($pOnot eq ''){
                        $pOnot =$detF[$k];
                    }
                    else{
                        $pOnot =$pOnot.'-'.$detF[$k];
                    }
                }    

                @pOnot1 = split('-', $pOnot);
                
                my $isO = 1;
                
                foreach $ipnot1(0..@pOnot1-1){
                    $isO = 0 if ($fonc[$pOnot1[$ipnot1]] =~ / 2-O /);
                }
                
                if($isO){
                
                    foreach $ipnot1(0..@pOnot1-1){
                        
                        $recordGroupA = '';
                        $recordGroupG = '';
                        
                        @detNot=split(' ', $fonc[$pOnot1[$ipnot1]]);
                        @detNot1=split(' ',$ifonc[$pOnot1[$ipnot1]]);
                    
                        foreach $k(0..@detNot-1){
                        
                            if($detNot1[$k] != $bi){
                                if ($recordGroupG eq ''){
                                    $recordGroupG=$detNot1[$k];
                                }
                                else{
                                    $recordGroupG= $recordGroupG.'-'.$detNot1[$k];
                                }
                            }
                    
                        }
                        
                        foreach $ipnot2(0..@pOnot1-1){
                            $recordGroupA = $pOnot1[$ipnot2] if($ipnot2 != $ipnot1);
                        }
                        
                        
                        if($groupOanchor[$nrGroup{'O'}] eq ''){
                            $groupOanchor[$nrGroup{'O'}]=$bi.'-'.$recordGroupA;
                            $groupOanchorG[$nrGroup{'O'}]=$pOnot1[$ipOnot1].'-'.$recordGroupG;
                        }
                        else{
                        
                            $groupOanchor[$nrGroup{'O'}]=$groupOanchor[$nrGroup{'O'}].'+'.$bi.'-'.$recordGroupA;
                            $groupOanchorG[$nrGroup{'O'}]=$groupOanchorG[$nrGroup{'O'}].'+'.$pOnot1[$ipnot1].'-'.$recordGroupG;
    
                        }
                        
#                         print "$groupOanchor[$nrGroup{'O'}]\n$groupOanchorG[$nrGroup{'O'}]\n";
                    }
                
                    if($funcGroup{'O'}){
#                         $ngroupO++;
                        $nrGroup{'O'}++;
                    
                        $groupAnchor{'O'} = join('#', @groupOanchor);
                        $groupAnchorG{'O'} = join('#',@groupOanchorG);   
                            
                        $ngroup_multi++;
                        $ngroupAll++;
                        &print_findgroup("O")if ($if_print);
                    }
                }
           }
        }
        
        elsif ($atom[$bi] eq 'N'){
    
            $car = $fonc[$bi];
            $nccH = $car =~ s/ 1-H / 1-H /g;
            $nccC = $car =~ s/ 1-C / 1-C /g;
            
            $need_to_split = 0;
            
            if (($nccH == 2) && ($nccC == 1)){
                &print_findgroup('NHH')if ($if_print);
                $need_to_split = 1;
                
            }
            elsif(($nccH == 1) && ($nccC == 2)){
                &print_findgroup('NH')if ($if_print);
                $need_to_split = 1;
            }
            elsif(($nccH == 0) && ($nccC == 3)){
                &print_findgroup('N')if ($if_print);
                $need_to_split = 1;
            }
            
            if($need_to_split){
                
                $pH = '';
                $pHnot = '';
                
                @det  = split(' ', $fonc[$bi]);
                @detF = split(' ',$ifonc[$bi]);
                
                foreach $k(0..@det-1){
                    
                    if($det[$k] eq '1-C'){
                        
                        if($pHnot eq ''){
                        
                            $pHnot = $detF[$k];
                        }
                        else{
                            $pHnot = $pHnot.'-'.$detF[$k];
                        }
                    }
                    elsif($det[$k] eq '1-H'){
                        if($pH eq ''){
                        
                            $pH = $detF[$k];
                        }
                        else{
                            $pH = $pH.'-'.$detF[$k];
                        }
                    }
                   
                }
                
            
                if (($nccH == 2) && ($nccC == 1)){
                    
                    $recordGroupA = $bi.'-'.$pH;
                    $recordGroupG = $pHnot;

                    @det=split(' ', $fonc[$pHnot]);
                    @detF=split(' ',$ifonc[$pHnot]);
                    
                
                    foreach $k(0..@det-1){
                        if ($detF[$k] ne $bi){
                            $recordGroupG = $recordGroupG.'-'.$detF[$k];
                        }
                    }
                    
                    if($funcGroup{'N(H)H'}){
                    
                        if($used_atoms !~ /-$bi-/ ){
                            
                            if ($groupAnchor{'N(H)H'} eq ''){
                                $groupAnchor{'N(H)H'} = $recordGroupA;
                                $groupAnchorG{'N(H)H'}= $recordGroupG;
                            }
                            else{
                                $groupAnchor{'N(H)H'} =$groupAnchor{'N(H)H'}.'#'.$recordGroupA;
                                $groupAnchorG{'N(H)H'}=$groupAnchorG{'N(H)H'}.'#'.$recordGroupG;
                                    
                            }
                            
                            $used_atoms = $used_atoms.'-'.$bi.'-'.$pHnot.'-';
                            
                            $nrGroup{'N(H)H'}++;

                            $ngroup_single++;
                            $ngroupAll++;
                        # &print_findgroup("NHH")if ($if_print);
                    }
                    }
                    
                }
                elsif(($nccH == 1) && ($nccC == 2)){

                    @temp = split('-', $pHnot);
                    
                    $used_atoms = $used_atoms.'-'.$bi.'-';
                    
                    foreach $itemp(0..@temp-1){
                        
                        $recordGroupA= '';
                        $recordGroupG = '';
                        $recordGroupA = $bi.'-'.$pH;
                        $recordGroupG = $temp[$itemp];
                        
                        
                        
                        @det=split(' ', $fonc[$temp[$itemp]]);
                        @detF=split(' ',$ifonc[$temp[$itemp]]);
                        
                        foreach $k(0..@det-1){
                            if ($detF[$k] ne $bi){
                                $recordGroupG = $recordGroupG.'-'.$detF[$k];
                            }
                        }
                        
                        if ($groupNHanchor[$nrGroup{'NH'}] eq ''){
                            $groupNHanchor[$nrGroup{'NH'}] = $recordGroupA;
                            $groupNHanchorG[$nrGroup{'NH'}] = $recordGroupG;

                            foreach $itemp2(0..@temp-1){
                                $groupNHanchor[$nrGroup{'NH'}] = $groupNHanchor[$nrGroup{'NH'}].'-'.$temp[$itemp2] if($itemp2 ne $itemp);
                            }
                        }
                        else{
                            $groupNHanchor[$nrGroup{'NH'}] = $groupNHanchor[$nrGroup{'NH'}].'+'.$recordGroupA;
                            $groupNHanchorG[$nrGroup{'NH'}] = $groupNHanchorG[$nrGroup{'NH'}] .'+'.$recordGroupG;
                            
                            foreach $itemp2(0..@temp-1){
                                $groupNHanchor[$nrGroup{'NH'}] = $groupNHanchor[$nrGroup{'NH'}].'-'.$temp[$itemp2] if($itemp2 ne $itemp);
                            }
                            
                        }
                    }
                    
                    if($funcGroup{'NH'}){
                        
#                         $ngroupNH++;
                        $nrGroup{'NH'}++;
                        
                        $groupAnchor{'NH'} = join('#', @groupNHanchor);
                        $groupAnchorG{'NH'} = join('#',@groupNHanchorG);   

                        $ngroup_multi++;
                        $ngroupAll++;
                    # &print_findgroup("NHH")if ($if_print);
                    
                    }
                }
                elsif(($nccH == 0) && ($nccC == 3)){
                    
                    @temp = split('-', $pHnot);
                    
                    foreach $itemp(0..@temp-1){
                        
                        $recordGroupA= '';
                        $recordGroupG = '';
                        
                        $recordGroupA = $bi;
                        
                        $recordGroupG = $temp[$itemp];
                        
                        @det=split(' ', $fonc[$temp[$itemp]]);
                        @detF=split(' ',$ifonc[$temp[$itemp]]);
                        
                        foreach $k(0..@det-1){
                            if ($detF[$k] ne $bi){
                                $recordGroupG = $recordGroupG.'-'.$detF[$k];
                            }
                        }
                        
                        if ($groupNanchor[$nrGroup{'N'}] eq ''){
                            $groupNanchor[$nrGroup{'N'}] = $recordGroupA;
                            $groupNanchorG[$nrGroup{'N'}] = $recordGroupG;

                            foreach $itemp2(0..@temp-1){
                                $groupNanchor[$nrGroup{'N'}] = $groupNanchor[$nrGroup{'N'}].'-'.$temp[$itemp2] if($itemp2 ne $itemp);
                            }
                        }
                        else{
                            $groupNanchor[$nrGroup{'N'}] = $groupNanchor[$nrGroup{'N'}].'+'.$recordGroupA;
                            $groupNanchorG[$nrGroup{'N'}] = $groupNanchorG[$nrGroup{'N'}] .'+'.$recordGroupG;
                            
                            foreach $itemp2(0..@temp-1){
                                $groupNanchor[$nrGroup{'N'}] = $groupNanchor[$nrGroup{'N'}].'-'.$temp[$itemp2] if($itemp2 ne $itemp);
                            }
                            
                        }
                    }
                 
                    if($funcGroup{'N'}){
                        
#                         $ngroupN++;
                        $nrGroup{'N'}++;
                        
                        $groupAnchor{'N'} = join('#', @groupNanchor);
                        $groupAnchorG{'N'} = join('#',@groupNanchorG);   

                        $ngroup_multi++;
                        $ngroupAll++;
                    # &print_findgroup("NHH")if ($if_print);
                    
                    }
                
                }
            }
        }
        
        elsif (($atom[$bi] eq 'F') || ($atom[$bi] eq 'Cl') || ($atom[$bi] eq 'Br') || ($atom[$bi] eq 'I')){
            
            if ($fonc[$bi] =~ / 1-C /){
                &find_halogen($atom[$bi], $if_print);
            }
            
        }
    }
    
    if($nrGroup{'cycle'}){
        $ngroup_multi = $ngroup_multi + @cycle_one_mol;
        $ngroupAll = $ngroupAll + @cycle_one_mol;
        $nrGroup{'cycle'} = @cycle_one_mol;
    }

    &print_findgroup("cycle")if (($if_print) && ($ncycle[$i_mol] > 0));
    print("\n\t\tFound $ngroupAll groups: $ngroup_single single groups, $ngroup_multi multi groups, including $nrGroup{'cycle'} cycles.\n\n") if ($if_print);
    
    &check_organic if ($if_print);
    
}

sub find_halogen{
    
    local ($elem, $if_print) = @_;
    
    $recordGroup = '';
    $pNot = '';

    @det=split(' ', $fonc[$bi]);
    @det1=split(' ',$ifonc[$bi]);
            
    foreach $k(0..@det-1){
        $pNot = $det1[0];
        die if($k > 0);
    }

    @detNot=split(' ', $fonc[$pNot]);
    @detNot1=split(' ',$ifonc[$pNot]);
    
    foreach $k(0..@detNot-1){
        if ($detNot1[$k] ne $bi){
            if ($recordGroup eq ''){
                $recordGroup=$detNot1[$k];
            }
            else{
                $recordGroup= $recordGroup.'-'.$detNot1[$k];
            }
        }
    }
    
    
     if($funcGroup{'VII'}){
        
        if($used_atoms !~ /-$bi-/ ){
            
            $used_atoms = $used_atoms.'-'.$pNot.'-';
            
            if ($groupAnchor{'VII'} eq ''){
                $groupAnchor{'VII'} = $bi;
                $groupAnchorG{'VII'}=$pNot.'-'.$recordGroup;
            }
            else{
                $groupAnchor{'VII'} =$groupAnchor{'VII'}.'#'.$bi;
                $groupAnchorG{'VII'}=$groupAnchorG{'VII'}.'#'.$pNot.'-'.$recordGroup;
                
            }

#             $combineGroup{'VII'} = $groupAnchor{'VII'};
            $nrGroup{'VII'}++;
            $ngroup_single++;
            $ngroupAll++;
        }
        &print_findgroup("$elem") if($if_print); 
    }
    
}


#=============================================================================#
#
#                                 check organic
#
#=============================================================================#


sub check_organic{

# ----------
#
#    only a easy check, if the molecule might be an inorganic compound
#
# ----------
    
    my $all_atoms = '';
    
    $all_atoms = join('-', @atom);
    
    $all_atoms = '-'.$all_atoms.'-';
    
    print "\t\t-> This molecule might be an inorganic compound.\n" if ($all_atoms !~ /-C-/);
    warn "\nWarning: There is no H-atom in this molecule. The lea-string is invalid!\n\n" if ($all_atoms !~ /-H-/);
    
}

#=============================================================================#
#
#                                     cycles
#
#=============================================================================#

sub find_cycle{

# ----------
#
#    find if a molecule do have a cycle structure. 
#    find yes, change the bond information + 5
#
# ----------
    
    &print_debug("find_cycle");
    
    local($fileread, $if_plus) = @_;
    
    local $filewrite = 'cyc_'.$fileread, $nr_mol = 0;
    
    @ncycle = '';
    @cycle_atom = '';
    
    unlink $filewrite;

    &read_all_frag($fileread, 'find_cycle');
    
    $inputfile = $filewrite if ($if_plus == 1);
    
    rename $filewrite, $fileread if ($if_plus == 0);
    
}    

sub cyclesdf{

# ----------
#
#    1. comes originally from LEA3D
#    2. find cycles
#
# ----------
    
    &print_debug("cyclesdf");
    
    local ($if_sub) = @_;
 
    local $nbcycle=0, @cycle='', $debug=0;
    local $listpassage, $listedirecte, $contact1, $suite, $suitei,$suitefinale;
    local @gsuite4;
    
    $cycle_atom_sub = '';
    
    foreach $cyc (1..$istratom){

        foreach $cyc3 (1..$istratom){

            $listpassage="";
            $listedirecte="";
            $contact1=-1;
            $suite="";
            $suitei="";
            $suitefinale="";
            @gsuite4=split(' ',$ifonc[$cyc3]);
            
            if(@gsuite4 > 1){
                foreach $cyc2 (0..@gsuite4-1){
                    $contact1=$cyc3 if($gsuite4[$cyc2] == $cyc);
                };
            };
            
            if($contact1 > -1){
                
                print "depart $cyc puis via $contact1\n" if($debug);
                
                @gsuite=split(' ',$ifonc[$contact1]);
                
                if(@gsuite > 1){
                
                    foreach $cyc2 (0..@gsuite-1){
                    
                        if($gsuite[$cyc2] != $cyc){
                        
                            $listpassage=$listpassage." $gsuite[$cyc2] ";
                            $suite=$suite." $gsuite[$cyc2] ";
                            $suitei=$suitei." $contact1 ";
                        };
                    };
                    
                    @gsuite2=split(' ',$suite);
                    @gsuite3=split(' ',$suitei);
                
                    $qs=@gsuite2-1;
                    
                    while($gsuite2[0] ne ""){
                        
                        print "\t suite $suite\n" if($debug);
                        
                        @fget=split(' ',$ifonc[$gsuite2[$qs]]);
                        $endsub=0;
                        
                        foreach $cyc2 (0..@fget-1){
                        
                            if($listedirecte!~/ $gsuite2[$qs]-$fget[$cyc2] /){
                                
                                if($fget[$cyc2]==$cyc && $fget[$cyc2]!=$gsuite3[$qs] && $listpassage!~/ $fget[$cyc2] /){
                                    
                                    $endsub=1;
                                    
                                    @gsuite5=split(' ',$suitei);
                                    
                                    foreach $qs3 (0..@gsuite5-1){
                                        if($suitefinale!~/ $gsuite5[@gsuite5-1-$qs3] /){
                                            $suitefinale=$suitefinale." $gsuite5[@gsuite5-1-$qs3] ";
                                        };
                                    };	
                                    
                                    $suitefinale=" $gsuite2[$qs] ".$suitefinale if($suitefinale!~/ $gsuite2[$qs] /);
                                    $suitefinale=$suitefinale." $cyc " if($suitefinale!~/ $cyc /);
                                    print "suite finale $suitefinale\n" if($debug);
                                    
                                    $cycle[$nbcycle]=$suitefinale;
                                    
                                    $nbcycle++;
                                    
                                    $suitefinale="";
                                    
                                    $listedirecte=$listedirecte." $gsuite2[$qs]-$fget[$cyc2] ";
                                }	
                                elsif($fget[$cyc2]!=$gsuite3[$qs] && $listpassage!~/ $fget[$cyc2] /){
                                    
                                    $endsub=1;
                                    print "\t add $fget[$cyc2]\n" if($debug);
                                    $listpassage=$listpassage." $fget[$cyc2] ";
                                    $suite=$suite." $fget[$cyc2] ";
                                    $suitei=$suitei." $gsuite2[$qs] ";
                                };
                            };
                        };
                        
                        if($endsub==0){
                            $gsuite2[$qs]="";
                            $suite=join(' ',@gsuite2);
                            $suite=" ".$suite." ";
                            $gsuite3[$qs]="";
                            $suitei=join(' ',@gsuite3);
                            $suitei=" ".$suitei." ";
                        };
                        @gsuite2=split(' ',$suite);
                        @gsuite3=split(' ',$suitei);
                        $qs=@gsuite2-1;
                    };
                };
            };
        };
    };

###################################################################################
############### ELIMINATION DES DOUBLES

    $longc=$nbcycle-1;
    
    foreach $lc (0..$longc-2){
        foreach $lc2 (($lc+1)..$longc-1){
            if ($cycle[$lc2] ne ''){
                
                $nbzero=0;
                @get1= split(' ',$cycle[$lc]);
                @get2= split(' ',$cycle[$lc2]);
                
                if (@get1 == @get2){
                    foreach $lc3 (0..@get1-1){
                        foreach $lc4 (0..@get2-1){
                            if ($get1[$lc3] == $get2[$lc4]){
                                $get2[$lc4] = 0;
                                $nbzero ++;
                            };
                        };
                    };
                    $cycle[$lc2]='' if ($nbzero == @get2);
                };
            };
        };
    };

    $nbc = 0;
    $nbc56 = 0;
    @cyclef='';

    foreach $lc (0..$longc-1){
        if ($cycle[$lc] ne ''){
            print "unique $cycle[$lc]\n" if($debug);
            $cyclef[$nbc]=$cycle[$lc];
            $nbc++;
            @get56=split(' ',$cycle[$lc]);
            $long56=@get56;
            $nbc56++ if($long56<=6);	
        };
    };
    
    $ncycle[$nr_mol] = $nbc if($if_sub ne 'sub');
    
    foreach $i(0..@cyclef){
        
        my $tmp = $cyclef[$i];
        $tmp =~ s/  /-/g;
        
        if($if_sub eq 'sub'){
            $cycle_atom_sub = $cycle_atom_sub.' '.$tmp;
        }
        else{
            $cycle_atom[$nr_mol] = $cycle_atom[$nr_mol].' '.$tmp;
        }
    }
   
   
   
#     print "test: $cycle_atom_sub\n";
    print "$nbc56 cycles a 3, 4, 5 ou 6 atomes\n"  if($debug);
    print "$nbc cycles\n" if($debug);
    print "\n" if($debug);
};

sub changebond{

# ----------
#
#   change bond of cycles: +5/-5
#
# ----------
    
    &print_debug("changebond");
    
    local ($if_sub) = @_;
    
    local $ccyc = @cyclef;
    
    my $i, $j, $k, $h, $g, @c_atom, @c_ifonc, @c_fonc, $add_atom = '', $count = 0, @all_mol = '';

    # check ring-combination: if Polycyclic 
    &check_cycle('main');
   
    # rewrite input file: the bond inside cycles +5 or -5
    
#     print "all mol is: @all_mol\n";
    open(WRITE, ">>$filewrite");
    
    foreach $iligne(0..@ligne-1){
    
        if($iligne > 4){
        
            @getligne = '';
            @getligne = split(' ', $ligne[$iligne]);
            
            foreach $i_all_mol(0..@all_mol-1){
                
                $all_mol[$i_all_mol] = '-'.$all_mol[$i_all_mol].'-';
            
                if(($all_mol[$i_all_mol] =~ /-$getligne[0]-/) && ($all_mol[$i_all_mol] =~ /-$getligne[1]-/) && ($getligne[0] ne '') && ($getligne[0] ne ' ')){
                    
                    if($if_plus){
                        $getligne[2] = $getligne[2] + 5;
                    }
                    else{
                        $getligne[2] = $getligne[2] - 5;
                    }
                
                    if(( $getligne[0] < 100 )  && ( $getligne[0] > 9 ) && ( $getligne[1] > 9) && ($getligne[1] < 100) ){
                        $ligne[$iligne] = ' '.$getligne[0].' '.$getligne[1];
                    }
                    elsif(($getligne[0] < 10) && ( $getligne[1] > 9) && ( $getligne[1] < 100)){
                        $ligne[$iligne] = '  '.$getligne[0].' '.$getligne[1];
                    }
                    elsif(($getligne[0] < 100) && ($getligne[0] > 9) && ($getligne[1] < 10)){
                        $ligne[$iligne] = ' '.$getligne[0].'  '.$getligne[1];
                    }
                    elsif(($getligne[0] < 10) && ($getligne[1] < 10)){
                        $ligne[$iligne] = '  '.$getligne[0].'  '.$getligne[1];
                    }
                    else{
                        die "Err: Not defined!\n";
                    }
                    
                    foreach my $i(2..@getligne-1){
                        
                        $ligne[$iligne] = $ligne[$iligne].'  '.$getligne[$i];
                    }
                    
                    $ligne[$iligne] = $ligne[$iligne]."\n";
                }
            }
        }
        print WRITE $ligne[$iligne];
    }

    close WRITE; 

}


sub check_cycle{

# ----------
#
#    check the number of cycles to split
#    check if fused Rings are included 
#
# ----------
    &print_debug("check_cycle");
    
  #  print "find: @cycle_one_mol\n";

    local ($operator) = @_, @old_cycle_one_mol = '', @new_cycle_one_mol = '', @atoms_each_cycle = '';
    my $kk = 0, $n_match = 0; 
    
    if ($operator eq 'main'){

        @old_cycle_one_mol = split(' ', $cycle_atom[$nr_mol]);
    }
    else{
        @old_cycle_one_mol = @cycle_one_mol;
    }

    @sorted = sort { length $a < length $b } @old_cycle_one_mol;
    @old_cycle_one_mol = @sorted;
#     print "nach: @old_cycle_one_mol\n";
    
    foreach my $k(0..@old_cycle_one_mol-1){
        
        $old_cycle_one_mol[$k] =~ s/ /-/g;
        
#         print "$old_cycle_one_mol[$k]\n";
        
        if($old_cycle_one_mol[$k] ne ''){
            
            $all_cyc_atoms = '-'.$old_cycle_one_mol[$k].'-';
            
            $atom_big_cycle = $old_cycle_one_mol[$k];

            foreach my $h($k+1..@old_cycle_one_mol-1){

                $old_cycle_one_mol[$h] =~ s/ /-/g;
                
                $n_match = 0; 
                
                @atoms_each_cycle = split('-', $old_cycle_one_mol[$h]);
                
                foreach my $g(0..@atoms_each_cycle-1){
                    
                  #  print "$n_match and $all_cyc_atoms =~ /-$atoms_each_cycle[$g]-/\n\n";
                    
                    if(($all_cyc_atoms =~ /-$atoms_each_cycle[$g]-/) && ($atoms_each_cycle[$g] ne '')){
                        
                        if($n_match < 2){
                            $n_match++;
                        }
                        elsif($n_match >1){
                            $atom_big_cycle = $atom_big_cycle.'-'.$old_cycle_one_mol[$h];
                            $old_cycle_one_mol[$h] = '';
                            $n_match++;
                            last;
                        }
                    }
                }
            }
            
            #print "bigatom: $atom_big_cycle\n";
            
            my @array = split('-', $atom_big_cycle);
            
            
            foreach $g(0..@array-1){
                
                if(($fonc[$array[$g]] =~ /2-/) || ($fonc[$array[$g]] =~ /7-/)){
                    
                    @c_fonc = split(' ', $fonc[$array[$g]]);
                    @c_ifonc = split(' ', $ifonc[$array[$g]]);
                    
                    foreach $h(0..@c_fonc-1){
                     $array[@array] = $c_ifonc[$h] if($c_fonc[$h] =~/2-/);
                     $array[@array] = $c_ifonc[$h] if($c_fonc[$h] =~/7-/);
                    }
                }
            }
            
            @uniq_times = uniq @array;
            
            $new_cycle_one_mol[$kk] = join('-', @uniq_times);
            
          #  print "big:$new_cycle_one_mol[$kk]\n";
            
            $kk++;
        }
    }

    if ($operator eq 'main'){
        
        @all_mol = '';
        @all_mol = @new_cycle_one_mol;
    
    }
    else{
        @cycle_one_mol = '';
        @cycle_one_mol = @new_cycle_one_mol;
        $cycle_atoms_one = join('-', @cycle_one_mol);
        $cycle_atoms_one = '-'.$cycle_atoms_one.'-';
    }
}

#=============================================================================#
#
#                                 change mol and molping
#
#=============================================================================#
sub change_mol_mp{

# ----------
#
#    change the mol and molping, wenn the split happens
#
# ----------

    my $new_mol = '', $new_mp = '', @new_split_mol_sub = '', $i, $j, $j1; 
    
    my @split_mp_sub = split(' ', $mp_sub_tmp); 
    my @split_mol_sub = split(/[' ',-]/, $mol_sub_tmp); 
    
    my @split_mp_main = split(' ', $mp_main_tmp);
    my @split_mol_main = split(' ', $mol_main_tmp); 
    
#     print "\n\nvor:$mp_sub_tmp\t$mol_sub_tmp // $mp_main_tmp\t$mol_main_tmp\n";
            
#     print "ifrag_change: $ifrag_change\n";

    foreach $i(0..@split_mp_sub-1){
                
        if ($split_mp_sub[$i] < $ifrag_change){
            
            if ($new_mp eq ''){
                $new_mp = $split_mp_sub[$i];
            }
            else{
                $new_mp = $new_mp.' '.$split_mp_sub[$i];
            }
            
            foreach my $isplit_mol_sub(0..@split_mol_sub-1){
            
                if ($split_mol_sub[$isplit_mol_sub] =~ /^$split_mp_sub[$i]\*/){
                    $new_split_mol_sub[$isplit_mol_sub] = $split_mol_sub[$isplit_mol_sub];
                    $split_mol_sub[$isplit_mol_sub] = '';
                }
            }
        }
      
        elsif($split_mp_sub[$i] == $ifrag_change){
            
            my @frag_after_tmp = @frag_change_after; 

            foreach $j(0..@split_mp_main-1){
                
                my $add_one_ele =  $ifrag_change+$j;

#                 print "$split_mp_sub[$i] == $ifrag_change";
                
                if ($new_mp eq ''){
                
                    $new_mp = $add_one_ele;
                
                }
                else{
                    $new_mp = $new_mp.' '.$add_one_ele;
                
                }
            
                foreach $j1(0..@frag_change_after-1){
                    
                    my @change_mp = split('\*', $frag_change_after[$j1]);
                    
#                     print "vor @change_mp === @frag_change_after\n";
                    
                    if($change_mp[0] == ($j+1)){
                    
                        $change_mp[0] = $add_one_ele;
                        $frag_after_tmp[$j1] = join('*', @change_mp);
                        $frag_change_after[$j1]  = '';
                    }
                    
#                     print "nach @change_mp\n";
                }
            }
            
            
        #   print "hier hier @frag_after_tmp\n\n";
            
            my @frag_change_after = @frag_after_tmp;


            foreach $j(0..@split_mol_main-1){
                
                $split_mol_main[$j] = '-'.$split_mol_main[$j];
                
                foreach $j1(0..@split_mp_main-1){
                    
                    my $add_one_ele = $ifrag_change+$j1;
                    
                    $split_mol_main[$j]=~s/$split_mp_main[$j1]\*/$add_one_ele\*/g;
                    
                    $split_mol_main[$j]=~s/ $split_mp_main[$j1]\*/ $add_one_ele\*/g;
                    
                    $split_mol_main[$j]=~s/-$split_mp_main[$j1]\*/-$add_one_ele\*/g;

                }
                
                $split_mol_main[$j] =~ s/^.//;
                
                if ($new_mol eq ''){
                    $new_mol = $split_mol_main[$j];
                }
                else{
                    $new_mol = $new_mol.' '.$split_mol_main[$j];
                }
            }
        
            foreach my $k(0..@split_mol_sub-1){
                    
                my @frag1 = split('\*', $split_mol_sub[$k]);
                
                if (($frag1[0] eq $ifrag_change) && ($ifrag_change_anchor =~/-$frag1[1]-/)){
                    
                    my @frag_change_before = split('-', $ifrag_change_anchor);
                
                    foreach my $i_change_before(0..@frag_change_before-1){
                            
                        if ($frag_change_before[$i_change_before] eq $frag1[1]){
                            
                            $new_split_mol_sub[$k] = $frag_change_after[$i_change_before];
                            
                            $split_mol_sub[$k]='';
                        }
                    }
                }
            }
        }
        
        elsif ($split_mp_sub[$i] > $ifrag_change){

            my $add_one_ele = $split_mp_sub[$i] + @split_mp_main - 1;
            
            my $new_mp_tmp = ' '.$new_mp.' '; 

            if($new_mp_tmp !~ / $add_one_ele /){
            
                $new_mp = $new_mp.' '.$add_one_ele;
                
                
                foreach my $k(0..@split_mol_sub-1){
                    
                    if($split_mol_sub[$k] =~ /$split_mp_sub[$i]\*/){
                        
                        my @temp = split('\*', $split_mol_sub[$k]);
                        
                        $new_split_mol_sub[$k]= $add_one_ele.'*'.$temp[1];
                        $split_mol_sub[$k] = ''; 
                                            
                    }
                }
            }
        }
    }
    
    
    # neu mol string
    for ($i = 0; $i < @new_split_mol_sub; $i += 2) {
        $new_mol = $new_mol.' '.$new_split_mol_sub[$i].'-'.$new_split_mol_sub[$i+1];
    }
    
    $mol_main_tmp = $new_mol;
    $mp_main_tmp = $new_mp;
    
    $mol_sub_tmp = '';
    $mp_sub_tmp = '';
    
    $i_lego--;
    $i_moli--;
                
 #  print "nach $mol_main_tmp\t$mp_main_tmp\n";

}

