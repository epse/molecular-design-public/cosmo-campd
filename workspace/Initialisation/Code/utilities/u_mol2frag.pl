#!/usr/bin/perl

# # # # # # # # # # # # # # # # #
#
#    Utility:
#    - change a molecule directly to fragment
#        -> change all atom H to X
#        -> add ><POINT> information
#
# # # # # # # # ## # # # # ## # # 

local($f_in) = @ARGV;

local $f_out = 'fragments.sdf';

local (*IN, *OUT, $point = "", $point2 = "");

my $new_mol = 1, $i_mol = 0, $compt, $nbatom, $nbond, @get, @coller;


&print_header;

# if inputfile is not correct
if ($f_in eq ''){
    die "Err: You need to give a file name.\n\tLike: ./mol2frag.pl <file>\n\n\n";
}

if (!(-e $f_in)||(-z $f_in)){
    die "\nErr: Cannot find the inputfile!\n\n\n";
}
if (!(-f $f_in)){
    die "\nErr: Not a file name, please check your inputfile\n\n\n";
}

unlink $f_out if(-e $f_out);

print "\tReading $f_in...\n\n";	

open(OUT, ">$f_out");
open(IN,  "<$f_in"); 
    
    while(<IN>){

        if ($new_mol){

            $i_mol++;
            $compt=0;
            $i=1;
            $j=0;

            $new_mol=0;

        };

        @get = split(' ',$_);
        
        $compt++;

        if (($compt > 4) && ($i <= $nbatom)){

            $comp2 = $compt-4;

            if ($get[3] eq 'H'){
                $_ =~ s/ H / X /g;
                $get[3] = 'X';
            }
        
            $point=$point."-".$comp2."-" if ($get[3] eq "X");
            
            $i++;
        };

        if (($compt > 4) && ($i > $nbatom) && ($j <= $nbond)){
            
            if ($j == 0){
                $j++;
            }
            else{
                
                if($point=~/\-$get[0]\-/){

                    $point2=$point2."-".$get[1];
                }
                elsif($point=~/-$get[1]-/){
                    $point2=$point2."-".$get[0];
                };

                $j++;
            };
        };
        
        if ($compt == 4){
            
            $nbatom=$get[0];
            $nbond=$get[1];

            @coller=split(' *',$nbatom);
            
            if(@coller > 3 && @coller == 6){
                $nbatom=$coller[0].$coller[1].$coller[2];
                $nbond=$coller[3].$coller[4].$coller[5];
            }
            elsif( @coller > 3 && @coller == 5){
                if($_=~/^\s/){
                    $nbatom=$coller[0].$coller[1];
                    $nbond=$coller[2].$coller[3].$coller[4];
                }
                else{
                    $nbatom=$coller[0].$coller[1].$coller[2];
                    $nbond=$coller[3].$coller[4];
                };
            };
        };
        
        if($_=~/^\$\$\$\$/){
            
            $point2 =~ s/^-//;
            $point2 =~ s/-$//;
           
	    	print OUT "> <POINTS>\n";
            print OUT "$point2\n";
            print OUT "\n";
            
            $point="";
            $point2="";

            $new_mol=1;
            
        };
  
        print OUT $_;	
        
    } 
    
close (IN);
close (OUT);

print "\t$i_mol fragments are writing in $f_out.\n\n\n";	


#------------------------------------------------------------------
#------------------------------------------------------------------
# subs

sub print_header{

# ----------
#
# just print header
#
# ----------

    my $text="
    ####################################################################\n\n
    \t\t\tchange molecules to fragments\n\n
    ####################################################################\n\n";

    system("echo \"$text\""); 

}



