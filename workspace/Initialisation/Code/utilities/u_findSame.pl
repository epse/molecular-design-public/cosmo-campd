#!/usr/bin/perl

# # # # # # # # # # # # # # # # #
#
#   Utility:
#     - to compare USMILES from two files ( one txt and COSMO-CAND log file)
#        
#
# # # # # # # # # # # # # # # # #

local ($file) = @ARGV;
local *IN, $file1, $file2;

$file1 = $file.'.txt';
$file2 = $file.'.log';

# check enviroment
die "Err: $file1 doesn't exist.\n\n" if(!-e $file1);
die "Err: $file2 doesn't exist.\n\n" if(!-e $file2);

die "Err: $file1 is empty.\n\n" if(-z $file1);
die "Err: $file2 is empty.\n\n" if(-z $file2);

# print header
my $text="
    ########################################################################\n\n
    \t\t\tcompare USMILES from two files\n\n
    ########################################################################\n\n";

system("echo \"$text\""); 


my $i = 0, $j = 0, $if_read = 0, $line = '', @mol_USMILES = '', @mol_USMILES_2 = '', @get = ''; @nr = '';


# read USMILES from TXT file
open (IN, $file1) or die "Err: Can't open the file $file1.\n";

  while(<IN>){
  
    my @get = split(' ', $_);
    
    $nr[$i] = $i+1;
    $mol_USMILES[$i] = $get[@get-1];
    $mol_USMILES[$i] =~ s/\#/\+/g;
    $mol_USMILES[$i] =~ s/\[/\^/g;
    $mol_USMILES[$i] =~ s/\]/_/g;
    $i++;
  }

close IN;

print "\tread $i USMILES from $file1\n\n";


#read USMILES from log file of COSMO-CAMD 
$i = 0;
open (IN, $file2);

  while(<IN>){
    
    $line = $_;
    
    if($if_read){
      
      @get = split(' ', $_);
    
      $mol_USMILES_2[$i] = $get[@get-1];
      $if_read = 0;

      $i++;
    }
   
    if ($line =~ /^Molecule No./){
      $if_read = 1;
    }
    
  }

close IN;

print "\tread $i USMILES from $file2\n\n\n";

foreach $i(0..@mol_USMILES){
  
#   #print all USMILES
#   print "$nr[$i]\t$mol_USMILES[$i]\t\t\t$mol_USMILES_2[$i]\n";
  
  if (($mol_USMILES[$i] ne $mol_USMILES_2[$i]) ){#&& ($mol_USMILES[$i] !~/N\(=O\)=O/)){
    
    print "$nr[$i]\t$mol_USMILES[$i]\t\t\t$mol_USMILES_2[$i]\n";
    print "\n-------------------------------------------------\n\n";
    $j++;
  }
}

$i = @mol_USMILES;

print "\tThere are $j different USMILES-Paar of $i\n\n\n";
