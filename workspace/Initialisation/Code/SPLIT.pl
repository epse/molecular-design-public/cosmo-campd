#!/usr/bin/perl

print "SPLIT OK\n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#               all subs about split things
#               
#       
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


#=============================================================================#
#
#                         control split loop
#
#=============================================================================#
sub control_cut{

# ----------
#
#    differentiate between single- and multi-anchor-groups 
#    if multi-anchor-groups: go to the split-loop (a big deal)
#
# ----------

    ($which_read_this_sub, local $ifrag_change) = @_; 
    
    &print_debug("controlCut", $which_read_this_sub, $ifrag_change);
    
    local $ifrag_change_anchor = '-';
    
    # to control the split of multi-anchor-molecule, every time only one group
    local $goto_next = 1; 

    # copy the structure information of each molecule
    @ligneTemp = '';
    @ligneTemp = @ligne;
    
#     print "@ligneTemp";
    

    if(($which_read_this_sub eq 'is_temp_frag') || ($which_read_this_sub eq 'is_origin')){ 
    
        if ($ngroup_multi == 0){
            
            # after split the molecule because of multi-anchor-groups, copy all fragments in one file diss_all.sdf -> to split one anchor groups. This sub is found in WRITE.pl
            # or there is no multi-anchor-groups, only single-anchor-groups
           
            &copy_all_in_one($ft_copy_all);
            
        }
        else{
            #---------- if there are more than one multi-anchor-fragment ----------
            
            # define variable
            my $ft_lib_1 = $ft_lib_t;
            local  $mol_sub_tmp = '', $mp_sub_tmp = '';
        
            #---------- 
            
            # mol and molping (anchor) musst be always changed, if a new decomposition is happened.
            # to record which molping need to be splited. 
            
            if($which_read_this_sub eq 'is_temp_frag'){

                die "\nErr: $mol and $molping of fragment $i_frag is leer.\n\n\n" if (($mol_main_tmp eq '') || ($mp_main_tmp eq ''));
                
                $mol_sub_tmp = $mol_main_tmp;
                $mp_sub_tmp = $mp_main_tmp;
                $mol_main_tmp = '';
                $mp_main_tmp = '';
               
                my @split_mp = split(' ', $mp_sub_tmp); 
                
#                print "split_mp is:@split_mp\n$ifrag_change++$nr_add_frag\n";
                
                $ifrag_change = $split_mp[$nr_add_frag];

                my @frag1 = split(' ', $mol_sub_tmp);
                
                foreach my $ifrag1(0..@frag1-1){
                    
                    my @frag2= split('-',$frag1[$ifrag1]);
                    
                    foreach my $ifrag2(0..@frag2-1){
                        
                        if ($frag2[$ifrag2] =~ /$ifrag_change\*/){
                            
                            my @temp = split('\*',$frag2[$ifrag2]);
                            $ifrag_change_anchor = $ifrag_change_anchor.$temp[1].'-';
                        }
                    }
                }
            }

            
            # to split the groups and write the intermediate data in the file outputTemp_1.sdf
            #---------- 
            
            open(TEMPDOC, ">$ft_lib_1");
        
                &fonction_multi;
         
            close TEMPDOC;
        

            local $mol_start = $i_mol, $mol_end = $i_mol;
        
            &split_reactant($ft_lib_1, 'section', $ifrag_change_anchor);
            
            if($which_read_this_sub eq 'is_temp_frag'){
            
                &change_mol_mp;   
            }

#             print "\ntest1:$mol_main_tmp==$mp_main_tmp\n";
#             print "test2:$mol_sub_tmp, $mp_sub_tmp\n";
         
            $dis_file_new = 'dissociated_'.$new_dissociated.'.sdf'; #$disFilenameNew
            
#             print "file: $dis_file_new\n";
            
            copy($ft_diss, $dis_file_new);
            
            $new_dissociated++;  
            
            my $OPENTMP;

            open ($OPENTMP, "<$dis_file_new");
        
                &read_temp_frag(*$OPENTMP);
           
            close $OPENTMP;

            unlink $ft_lib_1;

        }
    }
    elsif(($which_read_this_sub eq 'is_one_anchor')){
        
        if(($ngroup_single == 0)){
            
            &copy_all_in_one($ft_single);
        }
        else{
            
            my $ft_lib_1 = $ft_lib_t;

            $mol_sub_tmp = '';
            $mp_sub_tmp = '';

            $mol_sub_tmp = $mol_main_tmp;
            $mp_sub_tmp = $mp_main_tmp;
            $mol_main_tmp = '';
            $mp_main_tmp = '';

            my @split_mp = split(' ', $mp_sub_tmp);
            
            $ifrag_change = $split_mp[$ifrag_change-1];

            my @frag1 = split(' ', $mol_sub_tmp);
                
            foreach my $ifrag1(0..@frag1-1){
                
                my @frag2= split('-',$frag1[$ifrag1]);
                
                foreach my $ifrag2(0..@frag2-1){
                    
                    if ($frag2[$ifrag2] =~ /$ifrag_change\*/){
                        my @temp = split('\*',$frag2[$ifrag2]);
                        $ifrag_change_anchor = $ifrag_change_anchor.$temp[1].'-';
                    }
                }
            }
    
            open(TEMPDOC, ">$ft_lib_1");
        
                &fonction_single;
        
            close TEMPDOC;
                
            $mol_start = $i_mol;
            $mol_end = $i_mol;
            
         #   print "ifrag_change_anchor is $ifrag_change_anchor\n";
            
            $becauseofVII = 1 if($ngroupVII > 0);
            
            &split_reactant($ft_lib_1, 'section', $ifrag_change_anchor);
            
            unlink $ft_lib_1;
            
          #  print "test:$mol_main_tmp==$mp_main_tmp\n";
          #  print "123:$mol_sub_tmp, $mp_sub_tmp\n";
            
            if (($mol_sub_tmp ne '') && ($mp_sub_tmp ne '')){
                
                &change_mol_mp;
            }

         #   print "in main: $mol_main_tmp==$mp_main_tmp\n";
            
            local (*COLL, *READ);
            
          
            open (COLL, ">>$ft_single");
            open (READ, "<$ft_diss");
            
            while(<READ>){
                print COLL $_;
            }
            
            close COLL;
            close READ;
            
            unlink $ft_diss;

        }
    }

}

sub fonction_multi{

# ----------
# 
# give the important information to split the muti-anchor-groups. Attention: the rank hier is important
# 
# ----------
    
    &print_debug('fonction_multi');
    
    @ligneplus = '';
    
    local $lp = 0, $tempXatom = '', $finbond = 5 + $nbatom + $nbbond;
    
    my $record_a, $record_g; 
    
     foreach my $key(sort keys %funcGroup){
        
        @anchor = '';
        
        $key = 'cycle' if ($nrGroup{'cycle'} >0);
        
        if(($funcGroup{$key}) && ($nrGroup{$key} > 0) && ($typeGroup{$key} eq 'more')  && ($goto_next)){
            
          #  print "\t\tSplitting $key...\n";#if($tool_para{DEBUG} == 1);
            
            $record_a = '';
            $record_g = '';
            
            @groupAnchor_1 = split('\#', $groupAnchor{$key});
            @groupAnchorG_1 = split('\#', $groupAnchorG{$key});
            
            foreach $i(0..0){
                
                if($groupAnchor_1[$i] =~/\+/){
                    
                    @tempgroupAnchor = split('\+', $groupAnchor_1[$i]);
                    @tempgroupAnchorG= split('\+', $groupAnchorG_1[$i]);
                    
                    foreach $itemp(0..@tempgroupAnchor-1){
                    
                        @getanchor = split('-', $tempgroupAnchor[$itemp]);
                        $record_a = $record_a.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];

                        &updateLigne;
                        &write_ligne;
                        
                        if ($mp_main_tmp eq ''){
                            $mp_main_tmp = $i_lego;
                        }
                        else{
                            $mp_main_tmp =$mp_main_tmp.' '.$i_lego;
                        }
                    }
                
                    @ligneplus = '';
                    $lp = 0;
                    
                    foreach $itemp(0..@tempgroupAnchor-1){
                
                        @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                        $record_g = $record_g.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
                        
                        &updateLigne;
                    }
                
                    &write_ligne;
                    $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                }
                else{
                    
                    my $recordtempXatom = '', $checkcombine = '#';
                
                    for $j(0..0){
                        
                        @getanchor = split('-', $groupAnchor_1[0]);
                        $tempXatom = $getanchor[0];
                    
                        if($checkcombine !~ /#$groupAnchor_1[0]#/){
                            if ($j == 0){
                                $recordtempXatom = $getanchor[0];
                                $record_a = $record_a.'-'.$getanchor[0];
                            }
                            else{
                                $record_a = $record_a.'-'.$recordtempXatom;
                            }
                            &updateLigne;
                            $checkcombine = $checkcombine.$groupAnchorG_1[0].'#';
                        }           
                    }
                    
                    if ($mp_main_tmp eq ''){
                        $mp_main_tmp = ($i_lego+1);
                    }
                    else{
                        $mp_main_tmp = $mp_main_tmp.' '.($i_lego+1);
                    }
                    
                    &write_ligne;
                
                    $lp = 0;
                    @ligneplus = "";
                
                    foreach $j(0..0){
                
                        @getanchor = split('-', $groupAnchorG_1[$j]);
                        
                        $tempXatom = $getanchor[0];
                        $record_g = $record_g.'-'.$tempXatom;
                        
                        if($checkcombine !~ /#$groupAnchor_1[0]#/){
                            $checkcombine = $checkcombine.$groupAnchorG_1[$j].'#';
                            
                            &updateLigne;
                            &write_ligne;
                            
                            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                        }
                    }
                }
                
                @record_a1 = '';
                @record_g1 = '';
                @record_mp = '';
        
                @record_a1 = split('-', $record_a);
                @record_g1 = split('-', $record_g);
                @record_mp = split(' ', $mp_main_tmp);
            
                if($groupAnchor_1[$i]=~ /\+/){
                
                    my $j = @record_a1-1;
                  
                    foreach $ii(1..@record_a1-1){
                    
                        if($mol_main_tmp eq ''){
                            $mol_main_tmp = $record_mp[$ii-1].'*'.$record_g1[$ii].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$ii];
                        }
                        else{
                            $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$ii-1].'*'.$record_g1[$ii].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$ii];
                        }
                        $j--;
                    }
                }
                else{
                
                    my $j = 1;
            
                    foreach $ii(1..@record_a1-1){
                    
                        if($mol_main_tmp eq ''){
                            $mol_main_tmp = $record_mp[0].'*'.$record_g1[$ii].'-'.$record_mp[$j].'*'.$record_a1[$ii];
                        }
                        else{
                        
                            $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$ii].'-'.$record_mp[$j].'*'.$record_a1[$ii];
                        }
                        $j++;
                    }
                last;
                }
            }
        
            $goto_next = 0;
            $funcGroup{$key} = 0;
            }
        }
}

sub fonction_single{
# 
# give the important information to split the single-anchor-groups. Attention: the rank hier is not important
# 
    &print_debug('fonction_single');
    
    @ligneplus='';
    local $lp=0;
    local $tempXatom = '', @anchor = '', @anchorG = '', @keyrange = '';
    
    $finbond=5+$nbatom+$nbbond;

    $record_a = '';
    $record_g = '';

    foreach my $key(sort keys %funcGroup){
        
        @anchor = '';
       
        if(($funcGroup{$key}) && ($nrGroup{$key} > 0) && ($typeGroup{$key} eq 'one')){

            print "\t\tSplitting $key...\n" if($tool_para{DEBUG} == 1);
            
            @anchor = split('#', $groupAnchor{$key});
            
            
            die "Err: the number of group $key and the remembered anchor mismatch!!!" if(@anchor != $nrGroup{$key});
            
            for $i(0..$nrGroup{$key} -1){
            
                @getanchor = split('-', $anchor[$i]);
                $tempXatom = $getanchor[0];

                $record_a = $record_a.'-'.$tempXatom;
                &updateLigne;
            } 
            
            
            
        }
    }
    
     &write_ligne;

    if ($mp_main_tmp eq ''){
        $mp_main_tmp = $i_lego;
    }
    else{
        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
    }

    @ligneplus='';
    local $lp=0;
    
    foreach my $key(sort keys %funcGroup){
        
        @anchorG = '';
        
        if(($funcGroup{$key}) && ($nrGroup{$key} > 0) && ($typeGroup{$key} eq 'one')){
            
            @anchorG = split('#', $groupAnchorG{$key});
            
            die "Err: the number of group $key and the remembered anchor mismatch!!!" if(@anchorG != $nrGroup{$key});
            
            for $i(0..$nrGroup{$key}-1){
                
                @getanchor = split('-', $anchorG[$i]);
            
                $record_g = $record_g.'-'.$getanchor[0];

                $tempXatom = $getanchor[0];

                &updateLigne;
                &write_ligne;
                $i_addfrag++; 
            
                $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                    
            }
        }
    }
  
    @record_a1 = '';
    @record_g1 = '';
    @record_mp = '';
            
                
    @record_a1 = split('-', $record_a);
    @record_g1 = split('-', $record_g);
    @record_mp = split(' ', $mp_main_tmp);
    
    my $j = 1;

    foreach $i(1..@record_a1-1){
                    
        if($mol_main_tmp eq ''){
            $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
        }
        else{
            $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
        }
        $j++;
    }
    
}

sub updateLigne{
    
# ----------
#
#    find the relative atom combination, break the combination with atom X and Y
#
# ----------
    
    &print_debug('updateLigne');
    
    my $iatom, $ei, $ianchor, $atomx = 'X', $newatom, @getstr = '', @get1 = '';
    
    $getstr[4] ="0";
    $getstr[5] ="0";
    $getstr[6] ="0";
    $getstr[7] ="0";
    $getstr[8] ="0";
    $getstr[9]= "0";
    $getstr[10]="0";
    $getstr[11]="0";
    $getstr[12]="0";
    $getstr[13]="0";
    $getstr[14]="0";
    $getstr[15]="0";

    $saveXatom = $saveXatom.'-'.$getanchor[0]; # global variable

    for $iatom(1..$nbatom){
       

       
        if($iatom == $getanchor[0]){
                
            @getstr=split(' ', $ligne[4+$iatom]);
            
            $newatom=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$corx[$iatom],$cory[$iatom],$corz[$iatom],$blanc,$atomx,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
            
            $ligne[$iatom+4]=$newatom;

              
            foreach $ei (5+$nbatom..$finbond){
                
                @get=split(' ',$ligne[$ei]);

                
                if($get[0]==$iatom || $get[1]==$iatom){
                
                    
                    foreach $ianchor(1..@getanchor-1){
                        
                        
                        if(($get[0]==$getanchor[$ianchor] )|| ($get[1]==$getanchor[$ianchor])){
                            
                            
                        #    print "$get[0] == $get[1]==$iatom==$getanchor[$ianchor]==$ei==$finbond==$lp\n";
                        
                            
                            $atomH="Y";
                            $ligneplus[$lp]=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$corx[$iatom],$cory[$iatom],$corz[$iatom],$blanc,$atomH,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
                            
                            @get1=split(' ',$ligne[4]);
                            $get1[0]++;
                            $ligne[4]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$nbatom+1+$lp,$get1[1];
                            $newAtomY = 0;

                            if($get[0]==$iatom){
                                $ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$nbatom+1+$lp,$get[1],$get[2];
                            }
                            else{
                                $ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$nbatom+1+$lp,$get[0],$get[2];
                            };
                        
                          #  print "$ligne[$ei]\n";
                            $lp++;
                        } 
                    }
                }
            }
        }
    }
   
}

#=============================================================================#
#
#                         control split loop
#
#=============================================================================#

sub split_reactant{

# ----------
#
#   delete the useless atoms, like Y and the atoms, which combine with atom Y
#
# ----------


    ($file, $who_call_me, $ifrag_change_anchor) = @_; 
    
    &print_debug('split_reactant', $file, $who_call_me, $ifrag_change_anchor);

    local *INSP;
  
    if(($file eq '' )||(!(-f $file))){
        print "usage: \n nosel <file.sdf>\n";
        exit;
    };

	unlink $ft_diss;

	local $new_frag=1;
	$flagNum = 0;

    @frag_change_before = split('-', $ifrag_change_anchor); 
    
    @frag_change_after = '';

   # print "frag_change is +++111@frag_change_before+++@frag_change_after\n";
    
	open(INSP,"<$file");
	
	while(<INSP>){
		
		if($new_frag){
			
            $masse=0;
			$i_moli++;
			$compt=0;
			$ig=1;
			$jg=0;
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@bond='';
			@listb='';
			@typeb='';
			$blanc=' ';	
			@radius='';
			@lignebond='';		
			$atomlourd=0;
			$new_frag=0;
			$keepX="";
            $readtempXatom = 0;
			@sdf='';
			$flagNum++;
		};
		
		@getstr = split(' ',$_);
	
		$sdf[$compt]=$_;
		$compt++;
        
        
        # let defined atom retain
        if ($readtempXatom){
            @keepXatom = split(" ", $_);

            $keepX = $keepXatom[0];
            $readtempXatom = 0;
        }
        
		if (($compt > 4) && ($ig <= $istratom)){
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];

			$atomlourd ++ if($getstr[3] ne 'H' && $getstr[3] ne 'X');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{

                @coller=split(' *',$getstr[0]);
                @coller2=split(' *',$getstr[1]);
                if(@coller==6 && $getstr[1] ne ""){
                    $getstr[0]=$coller[0].$coller[1].$coller[2];
                    $getstr[2]=$getstr[1];
                    $getstr[1]=$coller[3].$coller[4].$coller[5];
                }
                elsif(@coller==6 && $getstr[1] eq ""){
                    $getstr[0]=$coller[0].$coller[1];
                    $getstr[1]=$coller[2].$coller[3].$coller[4];
                    $getstr[2]=$coller[5];
                }
                elsif(@coller==5){
                    if($_=~/^\s/){
                        $getstr[0]=$coller[0].$coller[1];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                    }
                    else{
                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[3].$coller[4];
                    };
                }
                elsif(@coller==4){
                    if($_=~/^\s/){
                        $getstr[0]=$coller[0];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[1].$coller[2].$coller[3];
                    }
                    else{
                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[3];
                    };					
                }
                elsif(@coller2==4){
                    $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                    $getstr[2]=$coller2[3];
                }
                elsif(@coller==7){
                    $getstr[0]=$coller[0].$coller[1].$coller[2];
                    $getstr[1]=$coller[3].$coller[4].$coller[5];
                    $getstr[2]=$coller[6];
                };

                die "\nErr: There is something wrong in the file $file. Please check if the sdf format is right. @getstr\n\n\n" 
                if ((!(looks_like_number($getstr[0]))) || (!(looks_like_number($getstr[1]))) || (!(looks_like_number($getstr[2]))) );
                
				$bond[$getstr[0]]=$bond[$getstr[0]].$blanc.$getstr[1].$blanc.$getstr[2];
				$listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];
				$typeb[$getstr[0]]=$typeb[$getstr[0]].$blanc.$getstr[2];

				$bond[$getstr[1]]=$bond[$getstr[1]].$blanc.$getstr[0].$blanc.$getstr[2];
				$listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];
				$typeb[$getstr[1]]=$typeb[$getstr[1]].$blanc.$getstr[2];


				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
				$lignebond[$jg]=$_;
				$jg++;
			};
		};
		

		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];
			
			@coller=split(' *',$istratom);
			
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

			die "\nErr: There is something wrong in your inputfile. Please check if the sdf format is right.\n\n\n" if ((!(looks_like_number($istratom))) || (!(looks_like_number($istrbond))));
		};
		
		if ($_=~/<tempXatom>/){
            $readtempXatom = 1;
        }
	
		if ($_=~/\$\$\$\$/){
			$new_frag=1;

			if($istratom > 0){
             
                &molping_find if ($who_call_me ne 'check_last');

                &convert;

                if ($who_call_me ne 'check_last'){
                    
                    &molping_change;

                    $mol_main_tmp = ' '.$mol_main_tmp.' ';
                    
                    @moltemp = split(' ',$mol_main_tmp);
                    
                    $mol_main_tmp = join(' ', @moltemp);

                    if($one){

                        open(OUT,">>$ft_diss");
                        
                        foreach $p (0..@sdf-1){
                            print OUT "$sdf[$p]";
                        };
                        close(OUT);
                    }
                    else{
                    };
                };
			};
			
			
		};
	
	
	};
	
	close(INSP);

  #  print "+++222frag_change is +++@frag_change_before+++@frag_change_after\n";
}

sub convert{

# ----------
#
#    copied from LEA3D
#
# ----------

	&print_debug('convert');
	
	$one=0;
	$many=0;
	$suite=" 1 ";
	$continue=1;
	$bi=1;
	$pos=0;
	$findAnchoral = 0; 

	while($continue){

        @get=split(' ',$ifonc[$bi]);
                
        
        foreach $k (0..@get-1){	
            $suite=$suite."$get[$k] " if ($suite!~/ $get[$k] /); 	
        };
        
        @get2=split(' ',$suite);
                
        $pos++;

                
        if($pos == @get2){
            $continue=0;
        }
        else{
            $bi=$get2[$pos];
        };

	};

    @get2=split(' ',$suite);
    $nbacc= @get2;
    $acc=$suite;
    
#     print "hooooo: @get2==$istratom\n\n"; 
    
    if(@get2 == $istratom){ # Yifan changed
        
        $one=1;
        
        #### needed to be check Yifan
      #  if(($ngroupVII)){
            
        foreach $iterm(0..@frag_change_before){
            
            $frag_change_after[$iterm] = $flagNum.'*'.$frag_change_before[$iterm];
            $frag_change_before[$iterm] = '';
        }
      #  }
        
    }
    else{ 
        	#SPLIT LES FICHIERS
        @geto=split(' ',$suite);
		$atl=0;
		
		#print "suite is: $suite\n";
		
		foreach $m (0..@geto-1){
			$atl++ if($atom[$geto[$m]] ne 'H' && $atom[$geto[$m]] ne 'X');
		};       	
       
		&print($suite, $ifrag_change_anchor) if($suite=~/ $keepX /);

		$many++ if($suite=~/ $keepX /);

		$continue=1;
		
		while($continue){
            
            $suite=" ";
            $bi='';
		    
            foreach $k (1..$istratom){	
                $bi=$k if($acc!~/ $k /);
            };
            
            $suite=$suite."$bi ";
           
            $cont=1;
            $pos=0;

            while($cont){
                @get=split(' ',$ifonc[$bi]);

                foreach $k (0..@get-1){	
                
                        $suite=$suite."$get[$k] " if ($suite!~/ $get[$k] /); 	
                };
                
                @get2=split(' ',$suite);
                $pos++;

                if($pos == @get2){
                
                    $cont=0;
                }
                else{
                    $bi=$get2[$pos];
                };	
                
              
            };

			$acc=$acc.' '.$suite;
			@getd=split(' ',$acc);
		  	$continue=0 if(@getd >= $istratom);
		  	$continue=0 if($bi eq '');
		  	
		  	
		  	
		  	@geto=split(' ',$suite);
		  	$atl=0;
		  	foreach $m (0..@geto-1){
		  		$atl++ if($atom[$geto[$m]] ne 'H' && $atom[$geto[$m]] ne 'X');
		  	};
			#&print($suite) if($atl > 4);
			&print($suite, $ifrag_change_anchor) if($suite=~/ $keepX /);
			$many++ if($suite=~/ $keepX /);
        };
    }; 
    $one;
};

sub print{

#
# copied from LEA3D
#
    &print_debug('print');
    
    (local $atomes)=@_;

    $atomes=' '.$atomes.' ';
    
	@getp=split(' ',$atomes);

 	#print "natom: $n_atom++@getp\n";
	
# 	foreach $iatom(0.. @getp-1){
#         
#         if ($getp[$iatom] > $n_atom){
#           
# #             $is_last = 1;
# 
# #         print "is_last is111: $is_last\n";
#         }
# 	}
#     
    if ($who_call_me ne 'check_last'){
        
        $longa = @getp;
        
        $longb=0;
        $gh=1;
        @ligneb='';
        $iFragAnchor = 1;
    
    #$findAnchorNu = 0;
    # 	print("yuavor @findAnchor\n");
        
        my @findAnchorTemp = @findAnchor;
        
        foreach $f (($istratom+5-1)..@sdf-1){ # 5-1 to begin index 0 see $compt++
            #print "$f $sdf[$f]\n";
            
                @getb=split(' ',$sdf[$f]);

                @coller=split(' *',$getb[0]);
                @coller2=split(' *',$getb[1]);
                
                if(@coller==6 && $getb[1] ne ""){
                        $getb[0]=$coller[0].$coller[1].$coller[2];
                        $getb[2]=$getb[1];
                        $getb[1]=$coller[3].$coller[4].$coller[5];
                }
                elsif(@coller==6 && $getb[1] eq ""){
                        $getb[0]=$coller[0].$coller[1];
                        $getb[1]=$coller[2].$coller[3].$coller[4];
                        $getb[2]=$coller[5];
                }
                elsif(@coller==5){
                        if($sdf[$f]=~/^\s/){
                                $getb[0]=$coller[0].$coller[1];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[2].$coller[3].$coller[4];
                        }
                        else{
                                $getb[0]=$coller[0].$coller[1].$coller[2];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[3].$coller[4];
                        };
                }
                elsif(@coller==4){
                        if($sdf[$f]=~/^\s/){
                                $getb[0]=$coller[0];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[1].$coller[2].$coller[3];
                        }
                        else{
                                $getb[0]=$coller[0].$coller[1].$coller[2];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[3];
                        };					
                }
                elsif(@coller2==4){
                        $getb[1]=$coller2[0].$coller2[1].$coller2[2];
                        $getb[2]=$coller2[3];
                }
                elsif(@coller==7){
                        $getb[0]=$coller[0].$coller[1].$coller[2];
                        $getb[1]=$coller[3].$coller[4].$coller[5];
                        $getb[2]=$coller[6];
                };

            
                $gh=0 if($sdf[$f]=~/^>/ || $getb[0] eq '' || $sdf[$f]=~/^M/ || $sdf[$f]=~/^\$\$\$\$/);
    

                if($gh){

                    if($atomes=~/ $getb[0] /){
                      
                        $a1='';
                        $a2='';

                        foreach $p (0..@getp-1){ # INFO @getp=split(' ',$atomes);

                        #  $findAnchoral = 0;            
                            
                            if($getb[0] eq $getp[$p]){
                                
                                $a1=($p+1);
                            
                                foreach $ifindAnchor(0..@findAnchor-1){
                                    
                                 #   print("yuaTest+++$getp[$p]+$findAnchor[$ifindAnchor]) && ($findAnchoral)\n");
                                    
                                    if ($getp[$p] eq $findAnchor[$ifindAnchor]){
                                       # print("find this 1\n");
                                        $findAnchor[$ifindAnchor] = '';
                                        $findAnchorTemp[$ifindAnchor] = $a1;
                                        $ifindAnchorTemp++;
                                    }
                                }
                                
                                foreach $iFragAnchor(0..@frag_change_before-1){
                                    
                                    if (($getp[$p] eq $frag_change_before[$iFragAnchor]) && ($keepX ne $frag_change_before[$iFragAnchor])){
                                        $frag_change_before[$iFragAnchor]= '';
                                        $frag_change_after[$iFragAnchor]= $flagNum.'*'.$a1;
                                    }
                                }
                            }
                            
                            if($getb[1] eq $getp[$p]){
                                
                                $a2=($p+1);
                                
                                foreach $ifindAnchor(0..@findAnchor-1){
                                    
                                #     print("yuaTest+++$getp[$p]+$findAnchor[$ifindAnchor]) && ($findAnchoral)\n");
                                    
                                    if( $getp[$p]==$findAnchor[$ifindAnchor] ){
                                 #         print("find this 2\n");
                                        $findAnchor[$ifindAnchor]  = '';
                                        $findAnchorTemp[$ifindAnchor] = $a2;
                                        $findAnchoral = 1;
                                        $findAnchorNu++;
                                    }
                                }
                                
                                foreach $iFragAnchor(0..@frag_change_before-1){
                                    if (($getp[$p] eq $frag_change_before[$iFragAnchor]) && ($keepX ne $frag_change_before[$iFragAnchor])){
                                        
                                        $frag_change_before[$iFragAnchor]= '';
                                        $frag_change_after[$iFragAnchor]=$flagNum.'*'.$a2;
                                    }
                                }
                                
                            }
                        };
                        
                       # print "after hier: @frag_change_after\n";
                        
                        $z="  0  0  0  0";
                        $ligneb[$longb]=sprintf"%3s%3s%3s  0  0  0  0",$a1,$a2,$getb[2];
                        
                      #  print "$a1,$a2,$getb[2]\n\n";
                        
                        $longb++;
    
                    };
                };
        };
        
        @findAnchor = @findAnchorTemp;

        open(OUT,">>$ft_diss");
        
        foreach $p (0..2){
            print OUT "$sdf[$p]";
        };
        
        printf OUT "%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
        
        
        foreach $f (0..@getp-1){
            print OUT "$sdf[$getp[$f]+3]";
        };

        foreach $f (0..@ligneb-1){
            print OUT "$ligneb[$f]\n";
        };	
                
        print OUT "M  END\n";
        
        $ecritfin=0;
        
        foreach $mp (0..@sdf-1){
            $ecritfin=1 if($sdf[$mp]=~/^>/);	
            print OUT "$sdf[$mp]" if($ecritfin);
        };
        
        print OUT "\$\$\$\$\n" if($ecritfin==0);	
        close(OUT);		
    };
};

sub molping_find{

# ----------
#
#   find the atoms in LEA-string need to be changed
#
# ----------

    &print_debug('molping_find');
    
    my $nanchor = 0, $tmp_mp = '', @fragment_mp = '';
    
    @findAnchor = '';

    
    foreach my $i_mol_frag($mol_start..$mol_end){

        $tmp_mp = ' '.$molping[$i_mol_frag].' ' if ($who_call_me eq 'main');
        $tmp_mp = ' '.$mp_main_tmp.' ' if ($who_call_me eq 'section');

        if($tmp_mp =~ / $i_moli /){

            @fragment_mp = split(' ', $molping[$i_mol_frag]) if ($who_call_me eq 'main');
            @fragment_mp = split(' ', $mp_main_tmp) if ($who_call_me eq 'section');
            
            foreach my $i(0..@fragment_mp-1){
                
                my $ele = $fragment_mp[$i];
                
                foreach my $j($i+1..@fragment_mp-1){
                    
                    if($fragment_mp[$j] == $ele){
                        $fragment_mp[$j] = '';
                    }
                }
            }
    
            foreach $i_molping(0..@fragment_mp-1){
                
                if ($fragment_mp[$i_molping] == $i_moli){ 
                       
                    @fragment_mol = split(' ',$mol[$i_mol_frag]) if ($who_call_me eq 'main');
                    @fragment_mol = split(' ',$mol_main_tmp) if ($who_call_me eq 'section');

                    foreach $ifrag_mol(0..@fragment_mol-1){
                    
                        @fragment_mol1 = split('-',$fragment_mol[$ifrag_mol]);
                        
                        foreach $ifrag_mol1(0..@fragment_mol1-1){

                            if ($fragment_mol1[$ifrag_mol1] =~/$i_moli\*/){
                                
                                @fragment_mol2=split('\*', $fragment_mol1[$ifrag_mol1]);
                                
                                $findAnchor[$nanchor] = $fragment_mol2[1];
#                               
                                $nanchor++;
                                
                                die "\nErr: defined in SPLIT.pl\n\n\n" if ($nachor > 1);

                            }
                            $fragment_mol[$ifrag_mol]=$fragment_mol2[0].'*'.$fragment_mol2[1];
                        }
                    }
                }
            }
        }
    }
    
#    print "findAnchor is @findAnchor\n\n\n";

}


sub molping_change{

# ----------
#
#   change the atoms in LEA-string need to be changed
#
# ----------

    &print_debug('molping_change');

    my $tmp_mp = '';
    $nanchor = 0;
    
    foreach my $i_mol_frag($mol_start..$mol_end){
        
        $tmp_mp = ' '.$molping[$i_mol_frag].' ' if ($who_call_me eq 'main');
        $tmp_mp = ' '.$mp_main_tmp.' ' if ($who_call_me eq 'section');
        
        if($tmp_mp =~ / $i_moli /){
            
            @fragment_mp = split(' ', $molping[$i_mol_frag]) if ($who_call_me eq 'main');
            @fragment_mp = split(' ', $mp_main_tmp) if ($who_call_me eq 'section');
            
            foreach $i(0..@fragment_mp-1){
                my $ele = $fragment_mp[$i];
                
                foreach $j($i+1..@fragment_mp-1){
                    if($fragment_mp[$j] eq $ele){
                        $fragment_mp[$j] = '';
                    }
                }
            }
        
            foreach $i_molping(0..@fragment_mp-1){
            
                if ($fragment_mp[$i_molping] eq $i_moli){
                    
                    @fragment_mol = split(' ',$mol[$i_mol_frag]) if ($who_call_me eq 'main');
                    @fragment_mol = split(' ',$mol_main_tmp) if ($who_call_me eq 'section');
                    
                    foreach $ifrag_mol(0..@fragment_mol-1){
                    
                        @fragment_mol1 = split('-',$fragment_mol[$ifrag_mol]);
                        
                        foreach $ifrag_mol1(0..@fragment_mol1-1){

                            if ($fragment_mol1[$ifrag_mol1] =~/$i_moli\*/){
                            
                                @fragment_mol2=split('\*', $fragment_mol1[$ifrag_mol1]);
                                
                                $fragment_mol2[1] = $findAnchor[$nanchor];
                                
                                $fragment_mol1[$ifrag_mol1]=join('*', @fragment_mol2);
                                
                                $nanchor++;
                                
                                die if ($nachor > 1);
                            }
                        }
                      
                        $fragment_mol[$ifrag_mol] = join('-', @fragment_mol1); 
                    }
                    
                    $mol[$i_mol_frag] = join(' ', @fragment_mol) if ($who_call_me eq 'main');
                    $mol_main_tmp = join(' ', @fragment_mol) if ($who_call_me eq 'section');
                }
            }
        }
    }

}

