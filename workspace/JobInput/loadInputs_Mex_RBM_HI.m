%
%% Input specifications for LEA3D optimization "Mex-RBM"
%

%% Specify necessary parameters

% objective function
objfunc = 'Mex-RBM';

% components
%system{1} = 'hydroxymethylfurfural';
system{1} = 'dihydro-5-methyl-2(3h)-furanone';
system{2} = 'h2o';
system{3} = 'Solvent X';
components = system;

% COSMO Files Location (compound(1),compound(2),Solvents)
Location = {'COSMObase', 'COSMObase', 'Gate'};

% SMILES Codes for heat capacities
SMILES{1} = 'CC1OC(CC1)=O'; 
SMILES{2} = 'O'; 


% Set NRTL Temperature vector
T_start = 298.15; % /C
T_end = 573.15; % /C 
T_interval = 12; % Number of points in T interval    

% Input to process
n_in  = [ 5 95 0 ]/100; %kmol /s

% Set temperature and pressure of extraction column
Extraction.pExtr = 1.013; %/bar
Extraction.purity = 0;
Extraction.iRaffinate = 2;
Extraction.iSolvent = 3;
Extraction.xS = 'LLE';
Extraction.Tk_Extr_max = 273.15+80;
Extraction.Tk_Extr_min = 273.15+25;

% Define RBM:
RBM.compsontop = [2 3];
RBM.Top_vap = 0;

% TOPOLOGY
Topology.p_azeo = 1; % bar
% Allow mixtures with azeotropes between:
Topology.Homoazeotropes = {[2 3]};
Topology.Heteroazeotropes = {[2 3]};
% Boiling point order requirement
%           component | No in boiling point sequence (descending)
Topology.BP_order =      {1,          1};

% HEAT INTEGRATION
HI = 1;

% Variable temperature = specify temperature bounds
Tc_Extr_min = 25; %/C
Tc_Extr_max = 80; %/C

% Specify temperatures for T_sat constraint
T_boil_min = 298.15; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Specify temperatures for T_sat constraint
T_melt_min = 'no_T_min';        % Kelvin or 'no_T_min'
T_melt_max = 293.15;    % Kelvin or 'no_T_max'

% LLE constraint:
LLEreq = 1;
LLEcomp = '2 3';
T = 323.15;

% Conformer treatment
nconf_max = 10; % set nconf_max = Inf for all confomers

%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;

specs.Number = noofcalc;
specs.Objfunc = objfunc;
specs.system = system;
specs.Solvent = solvent;
specs.SMILES = SMILES;
specs.COSMOpara = parametrization;
specs.Mode = 'optimization';


% Write solvent specific compounds
specs.compounds = system;
specs.compounds{length(system)} = solvent;

% Create FileLocation
FileLocation = cell(length(system),2);
% Add Compounds as the first column
for i = 1:length(system)-1
    FileLocation{i,1} = system{1,i};
    FileLocation{i,2} = Location{i};
end
%Add Solvents to first column
FileLocation{end,1} = solvent;
FileLocation{end,2} = Location{end};

% Eliminate double entrys and save
[tempMatrix,idx] = unique(FileLocation(:,1),'stable');
specs.FileLocation = [tempMatrix FileLocation(idx,2)];

% Write objective function specific specifications
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.composition = 0:0.1:1;
specs.n_in = n_in;
specs.Extraction = Extraction;
specs.RBM = RBM;
specs.Topology = Topology;
specs.HI = HI;


% Conformer treatment
specs.nconf = zeros(1,length(system));
specs.nconf(:) = nconf_max;

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;
specs.T_melt_min = T_melt_min;
specs.T_melt_max = T_melt_max;
specs.LLEreq = LLEreq;
specs.LLEcomp = LLEcomp;
specs.T = T;

% General paths
load('Paths/user.mat');
specs.paths = paths;
specs.maindir = pwd;
specs.Gate = gate;


