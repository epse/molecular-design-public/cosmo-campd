%
%% Input specifications for LEA3D optimization "CO-Process"
%

%% Specify necessary parameters

% objective function
objfunc = 'CO-Process';

% components
%system = {'h2', 'co2', 'co', 'h2o', 'methylformate', 'methanol', 'Solvent_X'};
system = {'methane', 'h2', 'co2', 'co', 'h2o', 'dimethylformamide', 'dimethylamine', 'Solvent X'};
components = system;

% COSMO Files Location (compound(1),compound(2),Solvents)
Location = {'COSMObase', 'COSMObase', 'COSMObase', 'COSMObase', 'COSMObase', 'COSMObase', 'COSMObase', 'Gate'};


% SMILES Codes for heat capacities
SMILES{1} = 'C'; 
SMILES{2} = '[H][H]'; 
SMILES{3} = 'C(=O)=O'; 
SMILES{4} = 'CO'; 
SMILES{5} = 'O'; 
SMILES{6} = 'CN(C)C=O'; 
SMILES{7} = 'CNC';  

% Set NRTL Temperature vector 
T_start = 223.15;   % Kelvin  
T_end = 623.15;     % Kelvin  
T_interval = 17;    % Number of points in T interval    

% Specify temperatures for T_sat constraint
T_boil_min = 298.15;        % Kelvin or 'no_T_min'
T_boil_max = 623.15;    % Kelvin or 'no_T_max'

% Specify temperatures for T_sat constraint
T_melt_min = 'no_T_min';        % Kelvin or 'no_T_min'
T_melt_max = 263.15;    % Kelvin or 'no_T_max'

% LLE constraint:
LLEreq = 1;
LLEcomp = '5 8';
T = 323.15;

% Input flows to flowsheet
n_in = [7/3 1 1 0 0 0 1 0];        % Feed to Extraction column kmol/s from Jens 2019 (To integrate

% ABSORPTION
% Set temperature and pressure of extraction column
Absorption.pAbs = 60;              %/bar -> Jens 2019
% Feed
Absorption.xS = [ 0 0 0 0 0 0 0 1];
Absorption.tF = 10;                % Celsius, Jens 2019
Absorption.tS = -30;               % Celsius, Rectisol, CRP: -30
Absorption.purity = 0.0;           % Purity in Raffinate
Absorption.iSolute = 3;         % Carrier of value component
Absorption.iSolvent = 8;
Absorption.relcomps = [1 3 8];

% VLLE-REACTOR
VLLEReactor.v = [0, -1, -1, 0, 1, 1, -1, 0];
VLLEReactor.T = 323.15;
VLLEReactor.phases = 'VLLE';
%VLLEReactor.deltaGR_ig = 9920; % [J/mol] (Values from Jens 2016 @25°C)
VLLEReactor.deltaGR_ig = 12550; % [J/mol] (Jens Value corrected using vant Hoff and dhrxn)
VLLEReactor.dhrxn =-65.04*1000; %[kJ/kmol] (calculated using excel sheet @25°C)
VLLEReactor.liquids = 5:8;
VLLEReactor.firstliquid = 5; % Extract phase
VLLEReactor.xi0 = 0.9;
VLLEReactor.reactionphase = 1;   % 1: extraction solvent, 2: catalyst solvent

% STOICHIOMETRIC REACTOR
StoichReactor.v = [0, 0, 0, 1, 0, -1, 1, 0]; 
StoichReactor.dhrxn = 62.49*1000; %[kJ/kmol] (calculated using excel sheet @ 25°C)
StoichReactor.turnover = 1; % full turnover of storage molecule
StoichReactor.targetcompound = 6;
StoichReactor.T = 273.15+280;   % Supronowicz GreenChem., 2015, 17,2904

% DISTILLATION 1
% Temperature = boiling point of mixture
% Presure is being optimized
% Define RBM:
RBM1.compsontop = 7;
RBM1.Top_vap = 0;
RBM1.relcomps = 5:8;

% DISTILLATION 2
% Temperature = boiling point of mixture
% Presure is being optimized
% Define RBM:
RBM2.relcomps = 5:8;
RBM2.compsI = 5;
RBM2.compsII = 8;

% HEAT INTEGRATION 
HI = 1; % YES = 1 or NO = 0

% TOPOLOGY
Topology.p_azeo = 1; % bar
% Allow mixtures with azeotropes between:
Topology.Homoazeotropes = {[1 2 3];[1 2 3 8]; [6 8]};
Topology.Heteroazeotropes = {[1 2 3];[1 2 3 8]; [5 8];  [6 8]};
% Boiling point order requirement
%           component | No in boiling point sequence (descending)
Topology.BP_order = {[5 8],          1} ;
  
% Conformer treatment
nconf_max = 10;

%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;

specs.Number = noofcalc;
specs.Objfunc = objfunc;
specs.SMILES = SMILES;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'optimization';

% Write solvent specific compounds
specs.system = system;
specs.compounds = system;
specs.compounds{length(system)} = solvent;

% Create FileLocation
FileLocation = cell(length(system),2);
% Add Compounds as the first column
for i = 1:length(system)-1
    FileLocation{i,1} = system{1,i};
    FileLocation{i,2} = Location{i};
end
%Add Solvents to first column
FileLocation{end,1} = solvent;
FileLocation{end,2} = Location{end};

% Eliminate double entrys and save
[tempMatrix,idx] = unique(FileLocation(:,1),'stable');
specs.FileLocation = [tempMatrix FileLocation(idx,2)];



% Write objective function specific specifications
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.composition = 0:0.1:1;

% Conformer treatment
specs.nconf = zeros(1,length(system));
specs.nconf(:) = nconf_max;

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;
specs.T_melt_min = T_melt_min;
specs.T_melt_max = T_melt_max;
specs.LLEreq = LLEreq;
specs.LLEcomp = LLEcomp;
specs.T = T;

% Process Model
specs.Topology = Topology;
specs.n_in = n_in;
specs.Absorption = Absorption;
specs.VLLEReactor = VLLEReactor;
specs.StoichReactor = StoichReactor;
specs.RBM1 = RBM1;
specs.RBM2 = RBM2;
specs.HI = HI;

% General paths
load('Paths/user.mat');
specs.paths = paths;
specs.maindir = pwd;
specs.Gate = gate;

