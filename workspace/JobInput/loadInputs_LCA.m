%
%% Input specifications for LEA3D optimization "Mex-RBM"
%

%% Specify necessary parameters

% objective function
objfunc = 'LCA';

% components
%components{1} = 'hydroxymethylfurfural';
system{1} = 'dihydro-5-methyl-2(3h)-furanone';
system{2} = 'h2o';
system{3} = 'Solvent X';
components = system;

% COSMO Files Location (compound(1),compound(2),Solvents)
Location = {'COSMObase', 'COSMObase', 'Gate'};


% Set NRTL Temperature vector
T_start = 298.15; % /K
T_end = 498.15; % /K
T_interval = 10; % Number of points in T interval    

% Input to process
n_in  = [ 5 95 0 ]/100; %kmol /s

% Set temperature and pressure of extraction column
Extraction.pExtr = 1.013; %/bar
% Fixed temperature
Extraction.Tk_Extr_min = 298.15; %/K
Extraction.Tk_Extr_max = 353.15; %/K
Extraction.optimizeTExtr = 1;
Extraction.purity = 0;
Extraction.iRaffinate = 2;
Extraction.iSolvent = 3;
Extraction.xS = 'LLE';

% Set temperature and pressure for RBM:
% DISTILLATION
% Define RBM:
RBM.compsontop = [2 3];
RBM.Top_vap = 0;
RBM.pRBM = 1;%/bar
RBM.TkRBM = 298.15; %/K

% Specify temperatures for T_sat constraint
T_boil_min = 298.15; % Kelvin or 'no_T_min'
T_boil_max = 698.15; % Kelvin or 'no_T_max'

% LLE constraint:
LLEreq = 1;
LLEcomp = '2 3';
T = 298.15;

% TOPOLOGY
Topology.p_azeo = 1; % bar
% Allow mixtures with azeotropes between:
Topology.Homoazeotropes = {[2 3]};
Topology.Heteroazeotropes = {[2 3]};
% Boiling point order requirement
%           component | No in boiling point sequence (descending)
Topology.BP_order =      {1,          1};


% Number and distribution of segments for sigma-Descriptors (Zhou 2015: 8, Zhou 2014: 12)
sigmasegments = [[1,12];[13,20];[21,25];[26,30];[31,35];[36,40];[41,48];[49,60]];
%sigmasegments = [[1,5];[6,10];[11,15];[16,20];[21,25];[26,30];[31,35];[36,40];[41,45];[46,50];[51,55];[56,60]];


% Conformer treatment
nconf_max = 10; % set nconf_max = Inf for all confomers

%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;

specs.Number = noofcalc;
specs.Objfunc = objfunc;
specs.system = system;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'optimization';

% Write solvent specific compounds
specs.compounds = system;
specs.compounds{length(system)} = solvent;

% Create FileLocation
FileLocation = cell(length(system),2);
% Add Compounds as the first column
for i = 1:length(system)-1
    FileLocation{i,1} = system{1,i};
    FileLocation{i,2} = Location{i};
end
%Add Solvents to first column
FileLocation{end,1} = solvent;
FileLocation{end,2} = Location{end};

% Eliminate double entrys and save
[tempMatrix,idx] = unique(FileLocation(:,1),'stable');
specs.FileLocation = [tempMatrix FileLocation(idx,2)];


% Write objective function specific specifications
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.composition = 0:0.1:1;
specs.n_in = n_in;
specs.Extraction = Extraction;
specs.RBM = RBM;
specs.Topology = Topology;

% Conformer treatment
specs.nconf = zeros(1,length(system));
specs.nconf(:) = nconf_max;

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;
specs.LLEreq = LLEreq;
specs.LLEcomp = LLEcomp;
specs.T = T;

specs.sigmasegments = sigmasegments;

% General paths
load('Paths/user.mat');
specs.paths = paths;
specs.maindir = pwd;
specs.Gate = gate;




