function index = find_Name(molStruct,name)

index = [];
for k = 1:length(molStruct)
    if strcmp(molStruct(k).name,name)
        index = k;       
    end
end


end