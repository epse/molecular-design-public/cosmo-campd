function [gamma]=getGamma(x, n_comp,AlphaPlace,TauPlace, T)
%NRTL activity coefficient calculation

% AlphaPlace & TauPlace -> Placeholder for certain Parameter
% If the temperature is an existing input argument, the placeholders are 
% Parameters to calculate alpha and tau, otherwise the placeholders are
% already alpha and tau

   if exist('T') == 0
       alpha = AlphaPlace;
       tau   = TauPlace;
   else
      AlphaParam = AlphaPlace;
      TauParam   = TauPlace;
      [alpha, tau] = getAlphaTau(AlphaParam, TauParam, T);
   end 
   
% G
    G=eye(n_comp,n_comp);
    for i=1:n_comp
        for j=1:n_comp
            if i~=j
                G(i,j) = exp(-alpha(i,j)*tau(i,j));
            end
        end
    end

%%

%B(i)
    for i=1:n_comp
        B(i)=0;
        for j=1:n_comp
            B(i) = B(i) + tau(j,i)*G(j,i)*x(j);
        end
    end

%A(i)
    for i=1:n_comp
        A(i)=0;
        for l=1:n_comp
            A(i) = A(i) + G(l,i)*x(l);
        end
    end


%% gamma(i)
%%
    for i=1:n_comp
        summe_lngamma(i)=0;
        for j=1:n_comp
            summe_lngamma(i) = summe_lngamma(i) + x(j)*G(i,j)/ A(j)*(tau(i,j)-B(j)/A(j));
        end
    end

    for i=1:n_comp
        lngamma(i) = B(i)/A(i)+summe_lngamma(i);
        gamma(i) = exp(lngamma(i));
    end

end

