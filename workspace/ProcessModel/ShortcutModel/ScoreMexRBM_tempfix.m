function [ molStruct ] = ScoreMexRBM_tempfix( molStruct, specs )
% Call mextraction and RBM shortcut to evaluate molecule

% Preallocate fields
properties = struct; % Create struct for properties
nComp = length(specs.system); % Number of compounds
for i=1:length(molStruct)
    molStruct(i).ExtrResults = struct;   
    molStruct(i).RBMresults = struct; 
    molStruct(i).Topology= struct;
    properties(i).AntoineParam    = molStruct(i).AntoineParam;
    properties(i).AlphaParam      = molStruct(i).AlphaParam;
    properties(i).TauParam        = molStruct(i).TauParam;  
    properties(i).dhvap_para      = molStruct(i).dhvap_para;  
end

% Add paths to Toolbox, Mextraction and RBM
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));
addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
addpath(genpath([specs.maindir,'/ProcessModel/Toolbox']));

%% Check Topology
write_init(nComp,'RBM');

p_azeocheck = specs.RBM.pRBM;

parfor i=1:length(molStruct)
    [azeo, hetaz, homaz, T_boil_sequence] = check_topology(properties(i), nComp, p_azeocheck);
    molStruct(i).Topology.azeo = azeo;
    molStruct(i).Topology.Heteroazeo = hetaz;
    molStruct(i).Topology.Homoazeo = homaz;
    molStruct(i).Topology.T_boil_sequence = T_boil_sequence;
end

% Exclude systems with impossible separations because of azeotropes 
% Allowed azeotropes
Homoazeotropes = specs.Topology.Homoazeotropes;
Heteroazeotropes = specs.Topology.Heteroazeotropes;

for i=1:length(molStruct)
    % Heteroazeotropes
    ok = zeros(size(molStruct(i).Topology.Heteroazeo, 1)); % Number of heteroazeotropes in system
    molStruct(i).Topology.ok = 1;
    
    for j=1:size(molStruct(i).Topology.Heteroazeo, 1)
        for k=1:length(Heteroazeotropes)
            if isequal(molStruct(i).Topology.Heteroazeo(1), Heteroazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Heteroazeo,1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        continue;
    end

    % Homoazeotropes
    ok = zeros(size(molStruct(i).Topology.Homoazeo, 1)); % Number of homoazeotropes in system
    for j=1:size(molStruct(i).Topology.Homoazeo, 1)
        for k=1:length(Homoazeotropes)
            if isequal(molStruct(i).Topology.Homoazeo(1), Homoazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Homoazeo, 1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        continue;
    end
    
end

% Exclude systems with impossible separations because of boiling point sequence
% Works for specifying bottom product
BP_order = specs.Topology.BP_order;

for i=1:length(molStruct)
    [~, index] = sort([molStruct(i).Topology.T_boil_sequence{:,1}], 'descend');  % Sorting of boiling points descending order
    for j=1:size(BP_order,1)
        if ~sum(index(BP_order{j,2}) == BP_order{j,1})
            molStruct(i).Topology.ok = 0;
            continue;
        end
    end
end


%% Extraction

write_init(nComp,'Mextraction');

% Set inputs for extraction
p_Extr = specs.Extraction.pExtr;
T_Extr = specs.Extraction.TkExtr;
n_in = specs.n_in;
purity = specs.Extraction.purity;
iRaffinate = specs.Extraction.iRaffinate;
iSolvent = specs.Extraction.iSolvent;

for i=1:length(molStruct)

    if molStruct(i).Topology.ok
        
        xS = specs.Extraction.xS;
        
        % Execute Mextration
        [molStruct(i).ExtrResults.n_in, n_out, Smin, extrResult, ok] = main_Mextraction(properties, T_Extr, p_Extr, n_in, purity, xS, nComp, iRaffinate, iSolvent);

         % Save Results
        molStruct(i).ExtrResults.extrResult = extrResult;
        %molStruct(i).ExtrResults.n_in = n_in;
        molStruct(i).ExtrResults.n_out = n_out;
        molStruct(i).ExtrResults.Smin = Smin;
        molStruct(i).ExtrResults.T_Extr_opt = T_Extr;
        molStruct(i).ExtrResults.ok = ok;

    else
        molStruct(i).ExtrResults.extrResult = NaN;
        molStruct(i).ExtrResults.n_in = NaN;
        molStruct(i).ExtrResults.n_out = NaN;
        molStruct(i).ExtrResults.Smin = NaN;
        molStruct(i).ExtrResults.ok = 0;
    end 
    
end

rmpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));


%% DISTILLATION
write_init(nComp,'RBM');

% Set inputs for distillation column
p_RBM = specs.RBM.pRBM;

Top_vap = 0;
compsontop = specs.RBM.compsontop;

for i=1:length(molStruct)
    
    if molStruct(i).Topology.ok && molStruct(i).ExtrResults.ok
        F_RBM = molStruct(i).ExtrResults.n_out(1,:); % Phase 1 is extractphase
        T_RBM = bubble_point( properties(i), F_RBM/sum(F_RBM), p_RBM );

        % Execute RBM
        [n_input,n_out,Qreb, Qcond, RBMresults, ok] = main_RBM_cp(molStruct(i), F_RBM, p_RBM, T_RBM-0.1, compsontop, Top_vap, nComp);

        molStruct(i).RBMresults.rbmResults = RBMresults;
        molStruct(i).RBMresults.n_in = n_input;
        molStruct(i).RBMresults.n_out = n_out;
        molStruct(i).RBMresults.Qreb = Qreb;
        molStruct(i).RBMresults.Qcond = Qcond;
        molStruct(i).RBMresults.ok = ok;
    
    else
        molStruct(i).RBMresults.rbmResults = NaN;
        molStruct(i).RBMresults.n_in = NaN;
        molStruct(i).RBMresults.n_out = NaN;
        molStruct(i).RBMresults.Qreb = NaN;
        molStruct(i).RBMresults.Qcond = NaN;
        molStruct(i).RBMresults.ok = 0;
    end

end

rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);

end
