function [ molStruct ] = ScoreCOProcess( molStruct, specs, comments )
% Optimize over mextraction and RBM shortcut to evaluate molecule

% Preallocate fields
nComp = length(specs.system); % Number of compounds
for i=1:length(molStruct)
    molStruct(i).cp_gas_co = NaN;
    molStruct(i).cp_gas = NaN;
    molStruct(i).cp_liq_co = NaN;
    molStruct(i).cp_liq = NaN;
    molStruct(i).Topology = struct;
    molStruct(i).AbsResults = NaN;
    molStruct(i).VLLEReacResults = NaN;
    molStruct(i).StoichReacResults = NaN;
    molStruct(i).Dist1Results = NaN;
    molStruct(i).Dist2Results = NaN;
    molStruct(i).HeatX = NaN;
    molStruct(i).P_comp = NaN;
    molStruct(i).Exergydemand = NaN;
    molStruct(i).opt_vars = NaN;
    molStruct(i).Score = NaN;
end
%opt_vars = zeros(length(molStruct), 4);

% Suppresses output during optimization
specs.Mode = 'optimization';

% Add paths
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/']));
addpath([specs.maindir,'/ProcessModel/Toolbox']);

%% Check Topology
write_init(nComp,'Topology');

p_azeocheck = specs.Topology.p_azeo;

parfor i=1:length(molStruct)
    [azeo, hetaz, homaz, T_boil_sequence] = check_topology(molStruct(i), nComp, p_azeocheck);
    molStruct(i).Topology.azeo = azeo;
    molStruct(i).Topology.Heteroazeo = hetaz;
    molStruct(i).Topology.Homoazeo = homaz;
    molStruct(i).Topology.T_boil_sequence = T_boil_sequence;
end

% Exclude systems with impossible separations because of azeotropes 
% Allowed azeotropes
Homoazeotropes = specs.Topology.Homoazeotropes;
Heteroazeotropes = specs.Topology.Heteroazeotropes;

for i=1:length(molStruct)
    % Heteroazeotropes
    ok = zeros(size(molStruct(i).Topology.Heteroazeo, 1)); % Number of heteroazeotropes in system
    molStruct(i).Topology.ok = 1;
    
    for j=1:size(molStruct(i).Topology.Heteroazeo, 1)
        for k=1:length(Heteroazeotropes)
            if isequal(molStruct(i).Topology.Heteroazeo(j,1), Heteroazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Heteroazeo,1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        continue;
    end

    % Homoazeotropes
    ok = zeros(size(molStruct(i).Topology.Homoazeo, 1)); % Number of homoazeotropes in system
    for j=1:size(molStruct(i).Topology.Homoazeo,1)
        for k=1:length(Homoazeotropes)
            if isequal(molStruct(i).Topology.Homoazeo(j,1), Homoazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Homoazeo, 1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        continue;
    end
    
end

% Exclude systems with impossible separations because of boiling point sequence
% Works for specifying bottom product
BP_order = specs.Topology.BP_order;

for i=1:length(molStruct)
    [~, index] = sort([molStruct(i).Topology.T_boil_sequence{:,1}], 'descend');  % Sorting of boiling points descending order
    for j=1:size(BP_order,1)
        if ~sum(index(BP_order{j,2}) == BP_order{j,1})
            molStruct(i).Topology.ok = 0;
            continue;
        end
    end
end

% Write init-Files
write_init(3,'Absorption');
write_init(nComp,'Reactor');
write_init(4,'RBM');

%% Optimize Exergy demand after heat integration
for k=1:length(molStruct)
    if molStruct(k).Topology.ok
        
        % Get expensive Gaussian cps now
        addpath([specs.maindir,'/PropertyPrediction/']);
        molStruct = job_iGproperties(molStruct,specs.compounds,specs);
        molStruct = job_cp_liq(molStruct,specs.compounds,specs);
        rmpath([specs.maindir,'/PropertyPrediction/']);
        
        
        options = optimoptions('fmincon', 'Algorithm', 'sqp', 'Display', 'off','SpecifyObjectiveGradient',true,'MaxFunEvals', 1000, 'OptimalityTolerance', 1e-9, 'ConstraintTolerance', 1e-9, 'StepTolerance', 1e-9);
        %options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'MaxFunEvals', 1000, 'Display', 'off','SpecifyObjectiveGradient',true);
        %options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'MaxFunEvals', 1000, 'Display', 'off');
       
        minbnd = [1, 0.5, 1, 1];
        maxbnd = [200, 5, 5, 5];
                
        % MULTISTART
        points = ones(8,4)+1e-3;
        points(2:4,2) = linspace(0.5+1e-3,5-1e-3,3);
        points([1 4],1) = 30;
        points(5,:) = [minbnd(1)+rand*(maxbnd(1)-minbnd(1)) minbnd(2)+rand*(maxbnd(2)-minbnd(2)) minbnd(3)+rand*(maxbnd(3)-minbnd(3)) minbnd(4)+rand*(maxbnd(4)-minbnd(4))];
        points(6,:) = [minbnd(1)+rand*(maxbnd(1)-minbnd(1)) minbnd(2)+rand*(maxbnd(2)-minbnd(2)) minbnd(3)+rand*(maxbnd(3)-minbnd(3)) minbnd(4)+rand*(maxbnd(4)-minbnd(4))];
        points(7,:) = [minbnd(1)+rand*(maxbnd(1)-minbnd(1)) minbnd(2)+rand*(maxbnd(2)-minbnd(2)) minbnd(3)+rand*(maxbnd(3)-minbnd(3)) minbnd(4)+rand*(maxbnd(4)-minbnd(4))];
        points(8,:) = [minbnd(1)+rand*(maxbnd(1)-minbnd(1)) minbnd(2)+rand*(maxbnd(2)-minbnd(2)) minbnd(3)+rand*(maxbnd(3)-minbnd(3)) minbnd(4)+rand*(maxbnd(4)-minbnd(4))];
       
        pool=parpool(4);
        properties = molStruct(k);
        parfor i=1:length(points)
            func = @(vars) objwithgrad(vars, properties, specs, comments, i);
            %func = @(vars) calcMexRBM(vars, molStruct(k), specs, comments);
            problem = createOptimProblem('fmincon','objective',func,'x0',points(i,:),'lb',minbnd,'ub',maxbnd,'options',options);
            [xmin,fmin,flag,outpt] = fmincon(problem);            
        end

        delete(pool);
        
        % Get the one from all
        % Get gate content
        content = dir([specs.Gate{3}, '/LEA_', specs.Number, '_*.mat']);

        % add all structs from gate to cumulative molStruct
        if ~isempty(content)
            molStruct_exists = 0;
            for i=1:length(content)
                ismatfile = strfind(content(i).name, '.mat');
                if ~isempty(ismatfile)
                    temp_struct = load([specs.Gate{3},'/', content(i).name]);
                    name_of_loaded_struct = fieldnames(temp_struct);
                    if molStruct_exists
                        tempmolStruct = [tempmolStruct,  temp_struct.(name_of_loaded_struct{1})];
                    else
                        tempmolStruct = temp_struct.(name_of_loaded_struct{1});
                        molStruct_exists = 1;
                    end
                end
            end
        
        % Rank all
        [~,index1] = sortrows([tempmolStruct.Score].'); 
        tempmolStruct = tempmolStruct(index1(end:-1:1)); 
        clear index1
        
        molStruct(k) = tempmolStruct(1);   
        
        end
        
    end
end

   
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [f,g] = objwithgrad(vars, properties, specs,comments, point)
% Calculate objective f
f = calcCOProcess(vars, properties, specs,comments, point);

if nargout > 1 % gradient required
    h = 1e-6;
    g = zeros(4,1);
    g(1) = calcCOProcess([vars(1)+h vars(2) vars(3) vars(4)], properties, specs,comments, point);
    g(2) = calcCOProcess([vars(1) vars(2)+h vars(3) vars(4)], properties, specs,comments, point);
    g(3) = calcCOProcess([vars(1) vars(2) vars(3)+h vars(4)], properties, specs,comments, point);
    g(4) = calcCOProcess([vars(1) vars(2) vars(3) vars(4)+h], properties, specs,comments, point);  
    g(1) = (g(1)-f)/h;
    g(2) = (g(2)-f)/h;
    g(3) = (g(3)-f)/h;
    g(4) = (g(4)-f)/h;
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [E_dem, AbsResults, VLLEReacResults, StoichReacResults, Dist1Results, Dist2Results, HeatX, P_comp] = calcCOProcess(vars, properties, specs, comments, point)

%% General specifications
% Vars
p_reactor = vars(1);
n_h2o = vars(2);
p_dst1 = vars(3);
p_dst2 = vars(4);

% Initialize outputs
AbsResults = struct;
VLLEReacResults = struct;
StoichReacResults = struct;
Dist1Results = struct;
Dist2Results = struct;
HeatX = [NaN, NaN, NaN];
P_comp = NaN;

%% ABSORPTION
relcomps = specs.Absorption.relcomps;
n_in = specs.n_in(relcomps);
p_abs = specs.Absorption.pAbs;
xS = specs.Absorption.xS(relcomps); 
tF = specs.Absorption.tF;                 
tS = specs.Absorption.tS;             
purity = specs.Absorption.purity;         
iSolute = find(specs.Absorption.iSolute == relcomps);         
iSolvent = find(specs.Absorption.iSolvent == relcomps); 

Absproperties = properties;
Absproperties.AntoineParam = Absproperties.AntoineParam(relcomps);
Absproperties.AlphaParam = Absproperties.AlphaParam(relcomps,relcomps);
Absproperties.TauParam = Absproperties.TauParam(relcomps,relcomps);
Absproperties.dhvap_para = Absproperties.dhvap_para(relcomps);
Absproperties.HenryCoeff = Absproperties.HenryCoeff(relcomps);
Absproperties.cp_gas_co = Absproperties.cp_gas_co(relcomps);
Absproperties.cp_liq_co = Absproperties.cp_liq_co(relcomps);

[n_in, n_out, Smin, AbsResult, ok] = main_absorption(n_in, Absproperties, p_abs, xS, tF, tS, purity, iSolute, iSolvent, length(n_in));

AbsResults.n_in = n_in;
AbsResults.n_out = n_out;
AbsResults.Smin = Smin;
AbsResults.AbsResult = AbsResult;
AbsResults.ok = ok;

if Smin == 0
    E_dem = 1e20;
    return
end

% COMPRESSOR
h2index = find(strcmp(specs.system,'h2'));
n_in = specs.n_in(h2index); % Hydrogen in kmol/s
p1 = 1;
p2 = p_reactor;
T = 298.15; 
P_comp = compressor( n_in, p1, p2, T ); % kW

% HEAT EXCHANGER for heating up the hydrogen to reactor temperature
T1 = T;
T2 = specs.VLLEReactor.T;
h2properties = properties;
h2properties.cp_gas_co = h2properties.cp_gas_co(h2index);
[ Q, T1, T2 ] = heatexchanger( h2properties, n_in, T1, T2, 1, 1);
HeatX(1, :) = [ T1, T2, abs(Q) ];

% HEAT EXCHANGER
T1 = AbsResults.AbsResult.tN+273.15;
T2 = specs.VLLEReactor.T;
[ Q, T1, T2 ] = heatexchanger( Absproperties, AbsResults.n_out(1,:), T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% VLLE-REACTOR
% Define input flow
n_in(specs.Absorption.relcomps) = n_out(1,:);
idx_h2 = find(strcmp(specs.system,'h2'));
n_in(idx_h2) = specs.n_in(idx_h2); % Hydrogen
idx_dma = find(strcmp(specs.system,'dimethylamine'));
n_in(idx_dma) = specs.n_in(idx_dma); % DMA
idx_h2o = find(strcmp(specs.system,'h2o'));
n_in(idx_h2o) = n_h2o; % Water

% Specifications
p = p_reactor;
v = specs.VLLEReactor.v;
T = specs.VLLEReactor.T; % (Values from Jens 2016)
phases = specs.VLLEReactor.phases;
deltaGR_ig = specs.VLLEReactor.deltaGR_ig; % [kJ/kmol] (Values from Jens 2016)
xi0 = specs.VLLEReactor.xi0;
liquids = specs.VLLEReactor.liquids;
firstliquid = specs.VLLEReactor.firstliquid;
reactionphase = specs.VLLEReactor.reactionphase;

[n_in, n_out, xi] = EquilibriumReactor(n_in, properties, p, T, phases, deltaGR_ig, v, xi0, liquids, firstliquid, reactionphase);

VLLEReacResults.n_in = n_in;
VLLEReacResults.n_out = n_out;
VLLEReacResults.xi = xi;
VLLEReacResults.ok = ok;

if isnan(VLLEReacResults.xi)
    E_dem = 1e20;
    return
end

% Heat of Reaction
dhrxn = specs.VLLEReactor.dhrxn; % kJ/kmol 
Q = VLLEReacResults.xi * dhrxn;
HeatX(end+1, :) = [ specs.VLLEReactor.T, specs.VLLEReactor.T-0.01, abs(Q) ];

% HEAT EXCHANGER for vaporizing
n_in = n_out(3,:);
n_in(1:3) = 0;
T1 = specs.VLLEReactor.T;
T2 = specs.StoichReactor.T;            % Supronowicz GreenChem., 2015, 17,2904
[ Q, T1, T2, Q_latent, Q_sensible1, Q_sensible2, T_boil, T_dew ] = heatexchanger( properties, n_in, T1, T2, 0, 1, 1);
HeatX(end+1, :) = [ T_boil, T_dew, abs(Q_latent) ];
HeatX(end+1, :) = [ T1, T_boil, abs(Q_sensible1) ];
HeatX(end+1, :) = [ T_dew, T2, abs(Q_sensible2) ];

%% STOICHIOMETRIC REACTOR
% better with eq reactor as well (?)
n_in = n_out(3,:);
n_in(1:3) = 0;
v = specs.StoichReactor.v; 
targetcompound = specs.StoichReactor.targetcompound;
turnover = specs.StoichReactor.turnover; 
[n_in, n_out] = StoichiometricReactor(n_in, turnover, targetcompound, v);

StoichReacResults.n_in = n_in;
StoichReacResults.n_out = n_out;

% Heat of Reaction
dhrxn = specs.StoichReactor.dhrxn; % kJ/kmol
Q = -(turnover * n_in(targetcompound)) / v(targetcompound) * dhrxn;
HeatX(end+1, :) = [ T2, T2+0.01, abs(Q) ];

%% DISTILLATION 1
n_in = n_out;
relcomps = specs.RBM1.relcomps;
compsontop = find(specs.RBM1.compsontop == relcomps);  
Top_vap = specs.RBM1.Top_vap;

% HEAT EXCHANGER after reactor for condensation
idx_co = find(strcmp(specs.system,'co'));
n_out(idx_co) = 0;
T_cond =  bubble_point( properties, n_out/sum(n_out), 1 );
T1 = specs.StoichReactor.T;
[ Q, T1, T2, Q_latent, Q_sensible1, Q_sensible2, T_boil, T_dew ] = heatexchanger( properties, n_out, T1, T_cond, 1, 0, 1);
HeatX(end+1, :) = [ T_dew, T_boil, abs(Q_latent) ];
HeatX(end+1, :) = [ T1, T_dew, abs(Q_sensible2) ];

% HEAT EXCHANGER before distillation
T_RBM1 =  bubble_point( properties, n_out/sum(n_out), p_dst1 );
[ Q, T1, T2] = heatexchanger( properties, n_out, T_cond, T_RBM1, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

RBMproperties = properties;
RBMproperties.AntoineParam = RBMproperties.AntoineParam(relcomps);
RBMproperties.AlphaParam = RBMproperties.AlphaParam(relcomps,relcomps);
RBMproperties.TauParam = RBMproperties.TauParam(relcomps,relcomps);
RBMproperties.dhvap_para = RBMproperties.dhvap_para(relcomps);
RBMproperties.cp_gas_co = RBMproperties.cp_gas_co(relcomps);
RBMproperties.cp_liq_co = RBMproperties.cp_liq_co(relcomps);

[n_in,n_out,Qreb, Qcond, RBMresults, ok] = main_RBM_cp(RBMproperties, n_in(relcomps), p_dst1, T_RBM1-0.1, compsontop, Top_vap, length(relcomps));

Dist1Results.n_in = n_in;
Dist1Results.n_out = n_out;
Dist1Results.Qreb = Qreb;
Dist1Results.Qcond = Qcond;
Dist1Results.rbmResults = RBMresults;
Dist1Results.ok = ok;
Tdew_cond = dew_point( RBMproperties, n_out(1,:)/sum(n_out(1,:)), p_dst1 );
Tdew_reb = dew_point( RBMproperties, n_out(2,:)/sum(n_out(2,:)), p_dst1 );
HeatX(end+1, :) = [ Dist1Results.rbmResults.Treb+0.01, Tdew_reb, abs(Qreb) ];
HeatX(end+1, :) = [ Tdew_cond, Dist1Results.rbmResults.Tcond-0.01, abs(Qcond) ];

% Heat exchanger of top product
T1 = Dist1Results.rbmResults.Treb;
T2 = specs.VLLEReactor.T;
[ Q, T1, T2] = heatexchanger( RBMproperties, n_out(1,:), T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q)  ];

%% DISTILLATION 2
n_in = n_out(2,:);
relcomps = specs.RBM2.relcomps;
compsI =  find(specs.RBM2.compsI == relcomps);
compsII =  find(specs.RBM2.compsII == relcomps);

RBMproperties = properties;
RBMproperties.AntoineParam = RBMproperties.AntoineParam(relcomps);
RBMproperties.AlphaParam = RBMproperties.AlphaParam(relcomps,relcomps);
RBMproperties.TauParam = RBMproperties.TauParam(relcomps,relcomps);
RBMproperties.dhvap_para = RBMproperties.dhvap_para(relcomps);
RBMproperties.cp_gas_co = RBMproperties.cp_gas_co(relcomps);
RBMproperties.cp_liq_co = RBMproperties.cp_liq_co(relcomps);

% Check for relevant heteroazeotrope
[~, RBMproperties.Topology.Heteroazeo, ~, ~] = check_topology(properties, length(VLLEReacResults.n_in), p_dst2);
heteroazeotropic = 0;
for i=1:size(RBMproperties.Topology.Heteroazeo, 1)
    if sum(RBMproperties.Topology.Heteroazeo{i,1} == relcomps([compsI, compsII]))==2
        heteroazeotropic = 1;
        break
    end
end

if heteroazeotropic %% If heteroazeotrope between water and solvent
    % HETEROAZEOTROPIC DISTILLATION = 2 Columns
    [n_in,n_out,Qreb1, Qcond1, Qreb2, Qcond2, RBMresults, ok] = main_HetRBM_cp(RBMproperties,n_in,p_dst2,compsI, compsII, length(relcomps));

    Dist2Results.n_in = n_in;
    Dist2Results.n_out = n_out;
    Dist2Results.Qreb1 = Qreb1;
    Dist2Results.Qcond1 = Qcond1;
    Dist2Results.Qreb2 = Qreb2;
    Dist2Results.Qcond2 = Qcond2;
    Dist2Results.rbmResults = RBMresults;
    Dist2Results.ok = ok;
    
    % If erroneous in het distillation
    if ok<0
        E_dem = 1e20;
        return
    end

    HeatX(end+1, :) = [ Dist2Results.rbmResults(1).Treb, Dist2Results.rbmResults(1).Treb+0.01, abs(Qreb1) ];
    HeatX(end+1, :) = [ Dist2Results.rbmResults(1).Tcond, Dist2Results.rbmResults(1).Tcond-0.01, abs(Qcond1) ];
    HeatX(end+1, :) = [ Dist2Results.rbmResults(2).Treb, Dist2Results.rbmResults(2).Treb+0.01, abs(Qreb2) ];
    HeatX(end+1, :) = [ Dist2Results.rbmResults(2).Tcond, Dist2Results.rbmResults(2).Tcond-0.01, abs(Qcond2) ];
       
else
    % NORMAL DISTILLATION
    T_RBM2 =  bubble_point( RBMproperties, n_in/sum(n_in), p_dst2 );
    Top_vap = 0;
    if RBMproperties.Topology.T_boil_sequence{relcomps(compsI)} < RBMproperties.Topology.T_boil_sequence{relcomps(compsII)}
        compsontop = compsI;
    else
        compsontop = compsII;
    end
    [n_in,n_out,Qreb, Qcond, RBMresults, ok] = main_RBM_cp(RBMproperties, n_in, p_dst2, T_RBM2-0.1, compsontop, Top_vap, length(relcomps));

    Dist2Results.n_in = n_in;
    Dist2Results.n_out = n_out;
    Dist2Results.Qreb = Qreb;
    Dist2Results.Qcond = Qcond;
    Dist2Results.rbmResults = RBMresults;
    Dist2Results.ok = ok;
    
    HeatX(end+1, :) = [ Dist2Results.rbmResults.Treb, Dist2Results.rbmResults.Treb+0.01, abs(Qreb) ];
    HeatX(end+1, :) = [ Dist2Results.rbmResults.Tcond, Dist2Results.rbmResults.Tcond-0.01, abs(Qcond) ];
    
    T_water = RBMproperties.Topology.T_boil_sequence{relcomps(compsI)}+273.15;
    T_solvent = RBMproperties.Topology.T_boil_sequence{relcomps(compsII)}+273.15;
end

% HEAT EXCHANGER before distillation
T1 = Dist1Results.rbmResults.Treb;
if heteroazeotropic
    T2 = Dist2Results.rbmResults(1).Tcond;
else
    T2 = T_RBM2;
end
[ Q, T1, T2 ] = heatexchanger( RBMproperties, n_in, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

% HEAT EXCHANGER for solvent recovery
if heteroazeotropic
    T1 = Dist2Results.rbmResults(2).Treb;
else
    T1 = T_solvent;
end
T2 = specs.Absorption.tS+273.15;
[ Q, T1, T2 ] = heatexchanger( RBMproperties, n_out(2,:), T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

% HEAT EXCHANGER for water out
if heteroazeotropic
    T1 = Dist2Results.rbmResults(1).Treb;
else
    T1 = T_water;
end
T2 = 298.15;
[ Q, T1, T2] = heatexchanger( RBMproperties, n_out(1,:), T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT INTEGRATION
% Check for errors in temperatures or heat duties
if sum(isnan(HeatX), 'all') ~= 0
    E_dem = 1e20;
    return
end

cold_util = { 'refrig', 233; 
              'cw'    , 283 };
hot_util = {  'LP'    , 410; 
              'furnace', 750}; %K
HRAT = 10;
[casc,HU,CU,E_total, E_heat, E_cool,PPoint,~,~] = heatintegration_LP(abs(HeatX), hot_util, cold_util, HRAT, specs);

%% EXERGY EVALUATION
% CO Production
idx_co = find(strcmp(specs.system,'co'));
nCO = StoichReacResults.n_out(idx_co);

% sum up
E_dem = P_comp + E_heat + E_cool;

% Exergyloss per kmol CO
E_dem = E_dem / nCO;

%close all

if isnan(E_dem)
    E_dem = 1e20;
end

%% Save procedure
properties.AbsResults = AbsResults;
properties.VLLEReacResults = VLLEReacResults;
properties.StoichReacResults = StoichReacResults;
properties.Dist1Results = Dist1Results;
properties.Dist2Results = Dist2Results;
properties.Exergydemand = E_dem;
properties.HeatX = HeatX;
properties.P_comp = P_comp;
properties.opt_vars = vars;

% Calculate Score
if isnan(properties.Exergydemand)
    properties.Score = 0;
else
    properties.Score = 1/properties.Exergydemand*100000;
end

if exist([specs.Gate{3},'/LEA_', specs.Number, '_', num2str(point),'.mat'], 'file')
    load([specs.Gate{3},'/LEA_', specs.Number, '_', num2str(point),'.mat']);
    if isnan(molStruct.Exergydemand)
        molStruct = properties;
        save([specs.Gate{3}, '/LEA_', specs.Number,'_', num2str(point)], 'molStruct')  % Save Struct
        printcomments(molStruct, comments, specs, point)                  % Save LEA
    elseif properties.Exergydemand < molStruct.Exergydemand
        molStruct = properties;
        save([specs.Gate{3}, '/LEA_', specs.Number,'_', num2str(point)], 'molStruct')
        printcomments(molStruct, comments, specs, point)                  % Save LEA
    end
else
    molStruct = properties;
    save([specs.Gate{3}, '/LEA_', specs.Number,'_', num2str(point)], 'molStruct')
    printcomments(molStruct, comments, specs, point)                  % Save LEA
end
    


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Print Comments routine for LEA
function printcomments(molStruct, comments, specs, point)
    if ~(isnan(molStruct.Exergydemand) || molStruct.Exergydemand==1e20)
        comments{length(comments)+1} = '--------------------------------------------------\nMinimum Exergy Loss:\n';
        comments{length(comments)+1} = ['Exergy Demand =   ', num2str(molStruct.Exergydemand/1000),' kJ/mol\n\n'];
        comments{length(comments)+1} = ['optimal process conditions =   ', num2str(molStruct.opt_vars),'\n\n'];
    else
        molStruct.Score = 0;
        comments{length(comments)+1} = '--------------------------------------------------\nOptimization did not succeed: Score = 0\n';
    end
    
    
        % Check for higher Score:
    higherScore = 1;
    if exist([specs.Gate{4},'/out_LEA_', specs.Number, '_', num2str(point), '.txt'], 'file')
        output_LEA = fopen([specs.Gate{4}, '/out_LEA_', specs.Number, '_', num2str(point), '.txt'],'rt');
        currentscore = fgetl(output_LEA);
        formatSpec = '%f';
        currentscore = textscan(currentscore,formatSpec); % Read line
        currentscore = currentscore{1};
        if currentscore > molStruct.Score
            higherScore = 0;
        end
    end
    
    if higherScore
        output_LEA = fopen([specs.Gate{4}, '/out_LEA_', specs.Number, '_', num2str(point), '.txt'],'wt');

        % Score
        formatSpec = '%10.6f';
        fprintf(output_LEA, formatSpec, molStruct.Score);
        fprintf(output_LEA, '\n');

        formatSpec = '%1.0f';
        fprintf(output_LEA, formatSpec, molStruct.ConCheck);
        fprintf(output_LEA, '\n');

        % Output further comments and intermediate results of scoring
        for i=1:length(comments)
            fprintf(output_LEA, comments{i});
        end

        fclose(output_LEA);
    end
    
    
end
