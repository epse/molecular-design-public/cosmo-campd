function [n_in, n_out, Smin, AbsResult, ok] = main_absorption(n_in, properties, p_abs, xS, tF, tS, purity, iSolute, iSolvent, nComp)
%% Calculate absorption column for one given solvent

%% ATTENTION: Currently init is hardcoded for Absorption: 
%%            Three components, first two modelled with Henry coefficients
%%            The third component is always the solvent


%% Initialize mabsorption
system = [pwd, '/ProcessModel/ShortcutModel/Absorption/init.dat']; % Folder for temporary files
hdl=mabsorption('init',system); % absorption column

% Preallocate fields
n_out = zeros(2, length(n_in));
Smin = 0;

%% Antoine-Parameter
AntoineParam =properties.AntoineParam;

%for i = 1 : nComp
for i = nComp : nComp
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mabsorption('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,i);
end


%% NRTL-Parameter
% AlphaParam = properties.AlphaParam;
% TauParam = properties.TauParam;		
% 
%  for i = 1 : (nComp-1)
%     for j = (i + 1) : nComp
%         alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i,j}(2); % mexprops expects alpha for C
%         alphat = AlphaParam{i, j}(2) ;
%         tau12_1 = TauParam{i, j}(1) ;
%         tau12_2 = TauParam{i, j}(2) ;
%         tau12_3 = TauParam{i, j}(3) ;
%         tau12_4 = TauParam{i, j}(4) ;
%         tau21_1 = TauParam{j, i}(1) ;
%         tau21_2 = TauParam{j, i}(2) ;
%         tau21_3 = TauParam{j, i}(3) ;
%         tau21_4 = TauParam{j, i}(4) ;
%         ok = mabsorption('set_nrtl_alpha',hdl,alpha,alphat,i,j);
%         ok = mabsorption('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j);
%         ok = mabsorption('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i);
%     end
%  end

%% Henry Coefficients
HenryCoeff = properties.HenryCoeff;
 for i = 1 : (nComp-1)
     j = 1;
        henc1 = HenryCoeff{i,j}(1)+log(1e5) ;
        henc2 = HenryCoeff{i,j}(2);
        henc3 = HenryCoeff{i,j}(3);
        henc4 = HenryCoeff{i,j}(4);
        henc5 = HenryCoeff{i,j}(5);
        henc6 = 10;
        henc7 = 2000;
        ok = mabsorption('set_henry',hdl,henc1,henc2,henc3,henc4,henc5,henc6,henc7,i,3) ;
 end
 
 
%% Enthalpy of vaporization
for i=1:nComp
    dhvap_para = properties.dhvap_para{i}; % see below: *1000 accounts for change from kJ/kmol to J/kmol
    dhvap_para(6:7) = [ 1 1000];
    ok = mabsorption('set_hvl',hdl,dhvap_para(1)*1000,dhvap_para(2),dhvap_para(3),dhvap_para(4),dhvap_para(5),dhvap_para(6),dhvap_para(7),i) ;
end
    
%% heat capacity gaseous cpig
for i=1:nComp
    NASA = properties.cp_gas_co{i} * 8314; %kJ/kmol/Ks
    NASA(6:11) = [0 1 2000 0 0 0];
    ok = mabsorption('set_cpig',hdl,NASA(1),NASA(2),NASA(3),NASA(4),NASA(5),NASA(6),NASA(7),NASA(8),NASA(9),NASA(10),NASA(11),i) ;
end

%% calculate ABSORPTION
xF = n_in/sum(n_in);

% Require a break of 0.11 sec to make absorption work every time
pause(0.11);


[ok, AbsResult] = mabsorption('MABSORPTION',hdl, p_abs, xF , xS , tF , tS, purity, iSolute , iSolvent);

clear mex

%% Mole balance
if(ok>0)
    n_out(1,:) = AbsResult.LN * AbsResult.xN * sum(n_in); % Liquid flow
    n_out(2,:) = AbsResult.V1 * AbsResult.y1 * sum(n_in);  % Vapour flow 
    Smin = AbsResult.Smin * sum(n_in) ;
    n_in(2,:) = AbsResult.Smin * AbsResult.xs * sum(n_in); % Solvent flow in
end


end
