function [ molStruct ] = ScoreMexRBM_tempfix_Tdec( molStruct, specs )
% Call mextraction and RBM shortcut to evaluate molecule

% Preallocate fields
properties = struct; % Create struct for properties
nComp = length(specs.system); % Number of compounds
for i=1:length(molStruct)
    molStruct(i).ExtrResults = struct;   
    molStruct(i).RBMresults = struct; 
    molStruct(i).Topology= struct;
    properties(i).AntoineParam    = molStruct(i).AntoineParam;
    properties(i).AlphaParam      = molStruct(i).AlphaParam;
    properties(i).TauParam        = molStruct(i).TauParam;  
    properties(i).dhvap_para      = molStruct(i).dhvap_para;  
    %properties(i).cp_gas_co      = molStruct(i).cp_gas_co; 
end

% Add paths to Toolbox, Mextraction and RBM
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));
addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
addpath(genpath([specs.maindir,'/ProcessModel/Toolbox']));

%% Check Topology
write_init(nComp,'Topology');

p_azeocheck = specs.Topology.p_azeo;

parfor i=1:length(molStruct)
    [azeo, hetaz, homaz, T_boil_sequence] = check_topology(properties(i), nComp, p_azeocheck);
    molStruct(i).Topology.azeo = azeo;
    molStruct(i).Topology.Heteroazeo = hetaz;
    molStruct(i).Topology.Homoazeo = homaz;
    molStruct(i).Topology.T_boil_sequence = T_boil_sequence;
end

% Exclude systems with impossible separations because of azeotropes 
% Allowed azeotropes
Homoazeotropes = specs.Topology.Homoazeotropes;
Heteroazeotropes = specs.Topology.Heteroazeotropes;

for i=1:length(molStruct)
    % Heteroazeotropes
    ok = zeros(size(molStruct(i).Topology.Heteroazeo, 1)); % Number of heteroazeotropes in system
    molStruct(i).Topology.ok = 1;
    
    for j=1:size(molStruct(i).Topology.Heteroazeo, 1)
        for k=1:length(Heteroazeotropes)
            if isequal(molStruct(i).Topology.Heteroazeo(1), Heteroazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Heteroazeo,1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        continue;
    end

    % Homoazeotropes
    ok = zeros(size(molStruct(i).Topology.Homoazeo, 1)); % Number of homoazeotropes in system
    for j=1:size(molStruct(i).Topology.Homoazeo, 1)
        for k=1:length(Homoazeotropes)
            if isequal(molStruct(i).Topology.Homoazeo(1), Homoazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Homoazeo, 1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        continue;
    end
    
end

% Exclude systems with impossible separations because of boiling point sequence
% Works for specifying bottom product
BP_order = specs.Topology.BP_order;

for i=1:length(molStruct)
    [~, index] = sort([molStruct(i).Topology.T_boil_sequence{:,1}], 'descend');  % Sorting of boiling points descending order
    for j=1:size(BP_order,1)
        if ~sum(index(BP_order{j,2}) == BP_order{j,1})
            molStruct(i).Topology.ok = 0;
            continue;
        end
    end
end

% Write init-Files
write_init(nComp,'Mextraction');
write_init(nComp,'RBM');


%% Evaluate

for i=1:length(molStruct)

    if molStruct(i).Topology.ok
        
        vars = molStruct(i).opt_vars;
        
        [Qreb, ExtrResults, RBMresults, molStruct(i)] = calcMexRBM(vars, molStruct(i), specs)
    end
 
end

rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Qreb, ExtrResults, RBMresults, properties] = calcMexRBM(vars, properties, specs)

nComp = length(specs.system);
T_Extr = vars(1);
T_Dec = vars(2);
p_RBM = vars(3);

%% Mextraction
% Set inputs for extraction
p_Extr = specs.Extraction.pExtr;
purity = specs.Extraction.purity;
iRaffinate = specs.Extraction.iRaffinate;
iSolvent = specs.Extraction.iSolvent;

system = [iRaffinate iSolvent];
xFeed = [0.7 0.3];
x0 = [1 0];
xI = zeros(1, nComp);
xII = zeros(1, nComp);
[xI(system),  xII(system)] = calcPhaseSplit(properties.AlphaParam(system, system), properties.TauParam(system, system), T_Dec, xFeed, x0);
xS = xII;

for i=1:5

    % Execute Mextration
    [n_in, n_out, Smin, extrResult, ok] = main_Mextraction(properties, T_Extr, p_Extr, specs.n_in, purity, xS, nComp, iRaffinate, iSolvent);

    % Save Results
    ExtrResults.extrResult = extrResult;
    ExtrResults.n_in = n_in;
    ExtrResults.n_out = n_out;
    ExtrResults.Smin = Smin;
    ExtrResults.T_Extr_opt = T_Extr;
    ExtrResults.ok = ok;
    
    if ok == 0 || ok < 0
        Qreb = 1e20;
        return;
    end

    % Decanter composition inlet
    x_Dec_in = zeros(1,3);
    x_Dec_in(1,system) = n_out(1,system) / sum(n_out(1,system));
    
    if x_Dec_in(iRaffinate) > xII(iRaffinate) && x_Dec_in(iRaffinate) < xI(iRaffinate)
        %% Gesetz der abgewandten Hebelarme wenn Decanter:
        nI = sum(n_out(1,system)) * (x_Dec_in(iRaffinate) - xII(iRaffinate))/(xI(iRaffinate)-xII(iRaffinate));
        nII = sum(n_out(1,system)) - nI;
        nI = nI * xI;
        nII = nII * xII;
        ExtrResults.Decanter = 1;
        ExtrResults.T_Dec_opt = T_Dec;
    else
        % No Decanter
        nI = zeros(1,nComp);
        nII = zeros(1,nComp);
        nII(system) = n_out(1,system);
        ExtrResults.Decanter = 0;
    end

    %% Waste water and make-up streams
    n_waste = nI + n_out(2,:);
    n_makeup = n_in(2,:) - nII;
        
    % Actual xS
    ExtrResults.n_waste = n_waste;
    ExtrResults.n_makeup = n_makeup;
    ExtrResults.xS_old = xS;
    S = nII;
    S(iSolvent) = S(iSolvent)+n_makeup(iSolvent);
    xS = S/sum(S);
    ExtrResults.xS_neu = xS;
    
    % Stop if no more iteration necessary
    if abs(ExtrResults.xS_neu(iRaffinate)- ExtrResults.xS_old(iRaffinate)) < 1e-9
        break
    end

end


%% RBM
% Inputs for distillation column
% Feed Input
F_vap = 0;
compsontop = specs.RBM.compsontop;
F_RBM = n_out(1,:); % Phase 1 is extractphase
T_RBM = bubble_point( properties, F_RBM/sum(F_RBM), p_RBM );
if isnan(T_RBM)
    Qreb = 1e20;
    return;
end

% Execute RBM
[n_in,n_out,Qreb, Qcond, rbmResults, ok] = main_RBM(properties, F_RBM, p_RBM, T_RBM-0.1, compsontop, F_vap, nComp);

% Save Results
RBMresults.n_in = n_in;
RBMresults.n_out = n_out;
RBMresults.Qreb = Qreb;
RBMresults.Qcond = Qcond;
RBMresults.rbmResults = rbmResults;
RBMresults.p_RBM_opt = p_RBM;
RBMresults.ok = ok;

if isnan(Qreb)
    Qreb = 1e20;
end


%% Save procedure
properties.ExtrResults = ExtrResults;
properties.RBMresults = RBMresults;
properties.Qreb = Qreb/1000; %kJ/s --> kJ/kmol --> kJ/mol
properties.opt_vars = vars;   


end
