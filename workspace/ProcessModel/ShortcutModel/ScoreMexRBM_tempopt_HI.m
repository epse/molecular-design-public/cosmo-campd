function [ molStruct ] = ScoreMexRBM_tempopt_HI( molStruct, specs, comments )
% Optimize over mextraction and RBM shortcut to evaluate molecule

% Preallocate fields
nComp = length(specs.system); % Number of compounds
for i=1:length(molStruct)
    molStruct(i).cp_gas_co = NaN;
    molStruct(i).cp_gas = NaN;
    molStruct(i).cp_liq_co = NaN;
    molStruct(i).cp_liq = NaN;
    molStruct(i).ExtrResults = struct;
    molStruct(i).ExtrResults.Smin = NaN;
    molStruct(i).RBMresults = struct;
    molStruct(i).RBMresults.Qreb = NaN;
    molStruct(i).Topology = struct; 
    molStruct(i).opt_vars = NaN;
    molStruct(i).HeatX = NaN;
    molStruct(i).Exergydemand = NaN;
    molStruct(i).Score = NaN;
end

% Suppresses output during optimization
specs.Mode = 'optimization';

% Add paths
addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/ProcessEquipment']));
addpath([specs.maindir,'/ProcessModel/Toolbox']);

% Write init-Files
write_init(nComp,'Mextraction');
write_init(nComp,'RBM');

%% Check Topology
write_init(nComp,'Topology');

p_azeocheck = specs.Topology.p_azeo;

parfor i=1:length(molStruct)
    [azeo, hetaz, homaz, T_boil_sequence] = check_topology(molStruct(i), nComp, p_azeocheck);
    molStruct(i).Topology.azeo = azeo;
    molStruct(i).Topology.Heteroazeo = hetaz;
    molStruct(i).Topology.Homoazeo = homaz;
    molStruct(i).Topology.T_boil_sequence = T_boil_sequence;
end

% Exclude systems with impossible separations because of azeotropes 
% Allowed azeotropes
Homoazeotropes = specs.Topology.Homoazeotropes;
Heteroazeotropes = specs.Topology.Heteroazeotropes;

for i=1:length(molStruct)
    % Heteroazeotropes
    ok = zeros(size(molStruct(i).Topology.Heteroazeo, 1)); % Number of heteroazeotropes in system
    molStruct(i).Topology.ok = 1;
    molStruct(i).Topology.ok = 1;
    
    for j=1:size(molStruct(i).Topology.Heteroazeo, 1)
        for k=1:length(Heteroazeotropes)
            if isequal(molStruct(i).Topology.Heteroazeo(j,1), Heteroazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Heteroazeo,1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        molStruct(i).Topology.ok = 0;
        continue;
    end

    % Homoazeotropes
    ok = zeros(size(molStruct(i).Topology.Homoazeo, 1)); % Number of homoazeotropes in system
    for j=1:size(molStruct(i).Topology.Homoazeo,1)
        for k=1:length(Homoazeotropes)
            if isequal(molStruct(i).Topology.Homoazeo(j,1), Homoazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Homoazeo, 1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        molStruct(i).Topology.ok = 0;
        continue;
    end
    
end

% Exclude systems with impossible separations because of boiling point sequence
% Works for specifying bottom product
BP_order = specs.Topology.BP_order;

for i=1:length(molStruct)
    [~, index] = sort([molStruct(i).Topology.T_boil_sequence{:,1}], 'descend');  % Sorting of boiling points descending order
    for j=1:size(BP_order,1)
        if ~sum(index(BP_order{j,2}) == BP_order{j,1})
            molStruct(i).Topology.ok = 0;
            continue;
        end
    end
end


%% Optimize Tk_Extr
for k=1:length(molStruct)
    if molStruct(k).Topology.ok
        
        % Get expensive Gaussian cps now
        addpath([specs.maindir,'/PropertyPrediction/']);
        molStruct = job_iGproperties(molStruct,specs.compounds,specs);
        molStruct = job_cp_liq(molStruct,specs.compounds,specs);
        rmpath([specs.maindir,'/PropertyPrediction/']);
               
        options = optimoptions('fmincon', 'Algorithm', 'sqp', 'Display', 'off','SpecifyObjectiveGradient',true,'MaxFunEvals', 1000, 'OptimalityTolerance', 1e-9, 'ConstraintTolerance', 1e-9, 'StepTolerance', 1e-9);
        %options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'MaxFunEvals', 1000, 'Display', 'off','SpecifyObjectiveGradient',true);
        %options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'MaxFunEvals', 1000, 'Display', 'off');
        
        T_opt_max = min([molStruct(k).T_boil, specs.Extraction.Tk_Extr_max, [molStruct(k).Topology.T_boil_sequence{:,1}]+273.15]);
        
        minbnd = [specs.Extraction.Tk_Extr_min, 1];
        maxbnd = [T_opt_max, 2];
        
        % MULTISTART
        randoms = [0.7577    0.7431    0.3922    0.6555    0.1712    0.7060    0.0318    0.2769    0.0462    0.0971    0.8235    0.6948];
        points = zeros(8,2);
        points(1,:) = [298.15+1e-3 1.0+1e-3];
        points(2,:) = [T_opt_max-1e-3 1.0+1e-3];
        points(3,:) = [323.15 1.1];
        points(4,:) = [T_opt_max-1e-3 1.5];        
        points(5,:) = [minbnd(1)+randoms(1)*(maxbnd(1)-minbnd(1)) minbnd(2)+randoms(3)*(maxbnd(2)-minbnd(2)) ];
        points(6,:) = [minbnd(1)+randoms(4)*(maxbnd(1)-minbnd(1)) minbnd(2)+randoms(6)*(maxbnd(2)-minbnd(2)) ];
        points(7,:) = [minbnd(1)+randoms(7)*(maxbnd(1)-minbnd(1)) minbnd(2)+randoms(9)*(maxbnd(2)-minbnd(2)) ];
        points(8,:) = [minbnd(1)+randoms(10)*(maxbnd(1)-minbnd(1)) minbnd(2)+randoms(12)*(maxbnd(2)-minbnd(2)) ];
      
       
        pool=parpool(4);
        properties = molStruct(k);
        parfor i=1:length(points)
            func = @(vars) objwithgrad(vars, properties, specs, comments, i);
            %func = @(vars) calcMexRBM(vars, molStruct(k), specs, comments);
            problem = createOptimProblem('fmincon','objective',func,'x0',points(i,:),'lb',minbnd,'ub',maxbnd,'options',options);
            [xmin,fmin,flag,outpt] = fmincon(problem);            
        end

        delete(pool);
        
        % Get the one from all
        % Get gate content
        content = dir([specs.Gate{3}, '/LEA_', specs.Number, '_*.mat']);

        % add all structs from gate to cumulative molStruct
        if ~isempty(content)
            molStruct_exists = 0;
            for i=1:length(content)
                ismatfile = strfind(content(i).name, '.mat');
                if ~isempty(ismatfile)
                    temp_struct = load([specs.Gate{3},'/', content(i).name]);
                    name_of_loaded_struct = fieldnames(temp_struct);
                    if molStruct_exists
                        tempmolStruct = [tempmolStruct,  temp_struct.(name_of_loaded_struct{1})];
                    else
                        tempmolStruct = temp_struct.(name_of_loaded_struct{1});
                        molStruct_exists = 1;
                    end
                end
            end

            % Rank all
            [~,index1] = sortrows([tempmolStruct.Score].'); 
            tempmolStruct = tempmolStruct(index1(end:-1:1)); 
            clear index1

            molStruct(k) = tempmolStruct(1);
        end
          
    end
end

rmpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
   
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [f,g] = objwithgrad(vars, properties, specs,comments, point)
% Calculate objective f
f = calcMexRBM(vars, properties, specs,comments, point);

if nargout > 1 % gradient required
    h = 1e-6;
    g = zeros(2,1);
    g(1) = calcMexRBM([vars(1)+h vars(2)], properties, specs,comments, point);
    g(2) = calcMexRBM([vars(1) vars(2)+h], properties, specs,comments, point);
    g(1) = (g(1)-f)/h;
    g(2) = (g(2)-f)/h;
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [E_total, ExtrResults, RBMresults] = calcMexRBM(vars, properties, specs,comments, point)

nComp = length(specs.system);
T_Extr = vars(1);
p_RBM = vars(2);

%% Mextraction
% Set inputs for extraction
p_Extr = specs.Extraction.pExtr;
purity = specs.Extraction.purity;
iRaffinate = specs.Extraction.iRaffinate;
iSolvent = specs.Extraction.iSolvent;

system = [iRaffinate iSolvent];
xFeed = [0.7 0.3];
x0 = [1 0];
xI = zeros(1, nComp);
xII = zeros(1, nComp);
[xI(system),  xII(system)] = calcPhaseSplit(properties.AlphaParam(system, system), properties.TauParam(system, system), T_Extr, xFeed, x0);
xS = xII;

for i=1:5

    % Execute Mextration
    [n_in, n_out, Smin, extrResult, ok] = main_Mextraction(properties, T_Extr, p_Extr, specs.n_in, purity, xS, nComp, iRaffinate, iSolvent);

    % Save Results
    ExtrResults.extrResult = extrResult;
    ExtrResults.n_in = n_in;
    ExtrResults.n_out = n_out;
    ExtrResults.Smin = Smin;
    ExtrResults.T_Extr_opt = T_Extr;
    ExtrResults.ok = ok;
    
    if ok == 0 || ok < 0
        E_total = 1e20;
        return;
    end

    % Decanter composition inlet
    x_Dec_in = zeros(1,3);
    x_Dec_in(1,system) = n_out(1,system) / sum(n_out(1,system));
    
    if x_Dec_in(iRaffinate) > xII(iRaffinate) && x_Dec_in(iRaffinate) < xI(iRaffinate)
        %% Gesetz der abgewandten Hebelarme wenn Decanter:
        nI = sum(n_out(1,system)) * (x_Dec_in(iRaffinate) - xII(iRaffinate))/(xI(iRaffinate)-xII(iRaffinate));
        nII = sum(n_out(1,system)) - nI;
        nI = nI * xI;
        nII = nII * xII;
        ExtrResults.Decanter = 1;
    else
        % No Decanter
        nI = zeros(1,nComp);
        nII = zeros(1,nComp);
        nII(system) = n_out(1,system);
        ExtrResults.Decanter = 0;
    end

    %% Waste water and make-up streams
    n_waste = nI + n_out(2,:);
    n_makeup = n_in(2,:) - nII;
        
    % Actual xS
    ExtrResults.n_waste = n_waste;
    ExtrResults.n_makeup = n_makeup;
    ExtrResults.xS_old = xS;
    S = nII;
    S(iSolvent) = S(iSolvent)+n_makeup(iSolvent);
    xS = S/sum(S);
    ExtrResults.xS_neu = xS;
    
    % Stop if no more iteration necessary
    if abs(ExtrResults.xS_neu(iRaffinate)- ExtrResults.xS_old(iRaffinate)) < 1e-9
        break
    end

end

%% HEAT EXCHANGER for heating up the feed to extraction temperature
T1 = 298.15;
T2 = T_Extr;
n_inHx = n_in(1,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for heating the extract to boiling point
T1 = T_Extr;
F_RBM = n_out(1,:); % Phase 1 is extractphase
T_RBM = bubble_point( properties, F_RBM/sum(F_RBM), p_RBM );
if isnan(T_RBM)
    E_total = 1e20;
    return;
end
T2 = T_RBM;
[ Q, T1, T2 ] = heatexchanger( properties, F_RBM, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for cooling the raffinate
T1 = T_Extr;
T2 = 298.15;
n_inHx = n_waste;
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for heating up the solvent stream to extraction temperature
T1 = 298.15;
T2 = T_Extr;
n_inHx = n_makeup;
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];


%% RBM
% Inputs for distillation column
% Feed Input
F_vap = 0;
compsontop = specs.RBM.compsontop;

% Execute RBM
[n_in,n_out,Qreb, Qcond, rbmResults, ok] = main_RBM_cp(properties, F_RBM, p_RBM, T_RBM-0.1, compsontop, F_vap, nComp);

% Save Results
RBMresults.n_in = n_in;
RBMresults.n_out = n_out;
RBMresults.Qreb = Qreb;
RBMresults.Qcond = Qcond;
RBMresults.rbmResults = rbmResults;
RBMresults.p_RBM_opt = p_RBM;
RBMresults.ok = ok;

if isnan(Qreb)
    Qreb = 1e20;
end

% Heat Exchanger of RBM
T1 = RBMresults.rbmResults.Treb;
T2 = T1+0.01;
Q= Qreb;
HeatX(end+1, :) = [ T1, T2, abs(Q) ];
T1 = RBMresults.rbmResults.Tcond;
T2 = T1-0.01;
Q= Qcond;
HeatX(end+1, :) = [ T1, T2, abs(Q) ];


%% HEAT EXCHANGER for cooling the mixture for decanter
T1 = RBMresults.rbmResults.Tcond;
T2 = T_Extr;
n_inHx = n_out(1,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for cooling GVL for product
T1 = RBMresults.rbmResults.Treb;
T2 = 298.15;
n_inHx = n_out(2,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];


%% HEAT INTEGRATION
cold_util = { 'refrig', 233; 
              'cw'    , 283 };
hot_util = {  'LP'    , 410; 
              'HP', 558.15}; %K
HRAT = 10;
[casc,HU,CU,E_total, E_heat, E_cool,PPoint,~,~] = heatintegration_LP(abs(HeatX), hot_util, cold_util, HRAT, specs);

%% Save procedure
properties.ExtrResults = ExtrResults;
properties.RBMresults = RBMresults;
properties.Exergydemand = E_total/1000; %kJ/s --> kJ/kmol --> kJ/mol
properties.HeatX = HeatX;
properties.opt_vars = vars;

% Calculate Score
if isnan(properties.Exergydemand)
    properties.Score = 0;
else
    properties.Score = 1/properties.Exergydemand;
end

if exist([specs.Gate{3},'/LEA_', specs.Number,  '_', num2str(point), '.mat'], 'file')
    load([specs.Gate{3},'/LEA_', specs.Number,  '_', num2str(point), '.mat']);
    if isnan(molStruct.Exergydemand)
        molStruct = properties;
        save([specs.Gate{3}, '/LEA_', specs.Number, '_', num2str(point), ], 'molStruct')  % Save Struct
        printcomments(molStruct, comments, specs, point)                  % Save LEA
    elseif properties.Exergydemand < molStruct.Exergydemand
        molStruct = properties;
        save([specs.Gate{3}, '/LEA_', specs.Number, '_', num2str(point), ], 'molStruct')
        printcomments(molStruct, comments, specs, point)                  % Save LEA
    end
else
    molStruct = properties;
    save([specs.Gate{3}, '/LEA_', specs.Number, '_', num2str(point), ], 'molStruct')
    printcomments(molStruct, comments, specs, point)                  % Save LEA
end
    


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Print Comments routine for LEA
function printcomments(molStruct, comments, specs, point)
    comments{length(comments)+1} = '--------------------------------\nMextraction Results:\n';
    if isnan(molStruct.RBMresults.Qreb)
        molStruct.Score = 0;
        if ~isnan(molStruct.ExtrResults.Smin)   
            comments{length(comments)+1} = ['S_min  =        ', num2str(molStruct.ExtrResults.Smin),'\n'];
            comments{length(comments)+1} = ['T_Extr =        ', num2str(molStruct.ExtrResults.T_Extr_opt),'\n'];  
            comments{length(comments)+1} = '--------------------------------------------------\nResults of RBM:\n';
            comments{length(comments)+1} = ['No solution of RBM shortcut for this solvent!\n'];
        else
            comments{length(comments)+1} = ['No solution of Mextraction shortcut for this solvent.\n'];
        end

    else
        molStruct.Score = 1/molStruct.Exergydemand;
        comments{length(comments)+1} = ['S_min  =        ', num2str(molStruct.ExtrResults.Smin),'\n'];
        comments{length(comments)+1} = ['T_Extr =        ', num2str(molStruct.ExtrResults.T_Extr_opt),'\n'];      
        comments{length(comments)+1} = '--------------------------------------------------\nResults of RBM:\n';
        comments{length(comments)+1} = ['Q_reb =        ', num2str(molStruct.RBMresults.Qreb/1000),' kJ/mol\n'];
        comments{length(comments)+1} = ['p_RBM_opt =        ', num2str(molStruct.RBMresults.p_RBM_opt),' bar\n\n'];
        comments{length(comments)+1} = '--------------------------------------------------\nExergy demand after HI:\n';
        comments{length(comments)+1} = ['E_total =        ', num2str(molStruct.Exergydemand),' kJ/mol\n\n'];
    end
    
    
    % Check for higher Score:
    higherScore = 1;
    if exist([specs.Gate{4},'/out_LEA_', specs.Number, '_', num2str(point), '.txt'], 'file')
        output_LEA = fopen([specs.Gate{4}, '/out_LEA_', specs.Number, '_', num2str(point), '.txt'],'rt');
        currentscore = fgetl(output_LEA);
        formatSpec = '%f';
        currentscore = textscan(currentscore,formatSpec); % Read line
        currentscore = currentscore{1};
        if currentscore > molStruct.Score
            higherScore = 0;
        end
    end
    
    if higherScore
        output_LEA = fopen([specs.Gate{4}, '/out_LEA_', specs.Number, '_', num2str(point), '.txt'],'wt');

        % Score
        formatSpec = '%10.6f';
        fprintf(output_LEA, formatSpec, molStruct.Score);
        fprintf(output_LEA, '\n');

        formatSpec = '%1.0f';
        fprintf(output_LEA, formatSpec, molStruct.ConCheck);
        fprintf(output_LEA, '\n');

        % Output further comments and intermediate results of scoring
        for i=1:length(comments)
            fprintf(output_LEA, comments{i});
        end

        fclose(output_LEA);
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%