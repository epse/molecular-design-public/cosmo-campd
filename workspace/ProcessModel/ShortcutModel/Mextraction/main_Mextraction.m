function [n_in, n_out, Smin, extrResult, ok] = main_Mextraction(properties, T_Extr, p_Extr, n_in, purity, xS, nComp, iRaffinate, iSolvent)

%% Init Mextraction
system = [pwd, '/ProcessModel/ShortcutModel/Mextraction/init.dat']; % Folder for temporary files 
hpd = mextraction('init',system);

% Predefine variables
n_out = zeros(2,length(n_in));
Smin = NaN;
extrResult = struct;

%% Check if valid input
if (max(n_in) == 0)
    ok = 0;
    return;
end

%% Calculate alpha_ij and tau_ij for given temperature
    
[alpha, tau] = getAlphaTau(properties.AlphaParam, properties.TauParam, T_Extr);

for i = 1:nComp
    for j=1:nComp
        if i<j
            ok = mextraction('set_alpha', hpd, alpha(i,j), i, j) ;
            ok = mextraction('set_tau', hpd, tau(i,j), i, j) ;
            ok = mextraction('set_tau', hpd, tau(j,i), j,i) ;
        end
    end
end

% for i=1:(nComp)
%     for j=(i):nComp
%         alpha   = alphaParam{i,j}(1) + 273.15*alphaParam{i,j}(2); % mexrbm expects alpha for C
%         alphat  = alphaParam{i,j}(2) ;
%         tau12_1 = tauParam{i,j}(1) ;
%         tau12_2 = tauParam{i,j}(2) ;
%         tau12_3 = tauParam{i,j}(3) ;
%         tau12_4 = tauParam{i,j}(4) ;
%         tau21_1 = tauParam{j,i}(1) ;
%         tau21_2 = tauParam{j,i}(2) ;
%         tau21_3 = tauParam{j,i}(3) ;
%         tau21_4 = tauParam{j,i}(4) ;
%         ok = mextraction('set_nrtl_alpha',hpd,alpha,alphat, i, j) ;
%         ok = mextraction('set_nrtl_tau',hpd,tau12_2,tau12_1,tau12_3,tau12_4, i, j) ;
%         ok = mextraction('set_nrtl_tau',hpd,tau21_2,tau21_1,tau21_3,tau21_4, j,i) ;
%     end
% end
% 
%     
%% Calculate Results

% Feed
xF = n_in / sum(n_in) ;

% Temperature in Celsius
t_Extr = T_Extr-273.15;

[extrResult,ok] = mextraction('EXMIN',hpd,t_Extr,p_Extr,xF,xS,purity,iRaffinate,iSolvent);
clear mex

if(ok>0)
    n_out(1,:) = extrResult.EN * extrResult.xen * sum(n_in); % Extract flow
    n_out(2,:) = extrResult.R1 * extrResult.xr1 * sum(n_in);  % Raffinate flow 
    Smin = extrResult.S * sum(n_in) ;
    n_in(2,:) = extrResult.S * extrResult.xs * sum(n_in); % Solvent flow in
end


clear mex

end