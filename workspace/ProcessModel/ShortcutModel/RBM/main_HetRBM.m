function [n_in,n_out, Qreb1, Qcond1, Qreb2, Qcond2, RBMresults, ok] = main_HetRBM(properties,n_in,p,compsI, compsII, nComp)
%% Execute RBM model for heteroazeotropic distillation
% First decanter, then two columns. Each column with azeotrop as distillate
% and desired mixtures as bottom products
% Only one heterogeneous azeotrope is expected
% Input:    n_in (inflow of only relevant species)
% Output:   nout (outflow (matrix with two rows, column1: row 1, column2: row 2))


%% Initiate mex
system = [pwd, '/ProcessModel/ShortcutModel/RBM/init.dat']; % Folder for temporary files 
hdl=mexrbm('init',['',system]);   

%% Initialize solution vector
n_out = zeros(2,length(n_in));
Qreb1 = NaN;
Qcond1 = NaN;
Qreb2 = NaN;
Qcond2 = NaN;
RBMresults = struct;

%% Check if valid input
if (max(n_in) == 0)
    ok = 0;
    return;
end

antoineParam    = properties.AntoineParam;
alphaParam      = properties.AlphaParam;
tauParam        = properties.TauParam;
dhvap           = properties.dhvap_para;
    
%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 
% spec) to ln(p)= A+B/(T+C) in Pa

for i=1:nComp
    antc1 = antoineParam{i,1}(1) + log(100) ;
    antc2 = -antoineParam{i,1}(2);
    antc3 = antoineParam{i,1}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 1000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8, antc9,i) ;
end
    
%% Set NRTL parameters

for i=1:(nComp-1)
    for j=(i+1):nComp
        alpha   = alphaParam{i,j}(1) + 273.15*alphaParam{i,j}(2); % mexrbm expects alpha for C
        alphat  = alphaParam{i,j}(2) ;
        tau12_1 = tauParam{i,j}(1) ;
        tau12_2 = tauParam{i,j}(2) ;
        tau12_3 = tauParam{i,j}(3) ;
        tau12_4 = tauParam{i,j}(4) ;
        tau21_1 = tauParam{j,i}(1) ;
        tau21_2 = tauParam{j,i}(2) ;
        tau21_3 = tauParam{j,i}(3) ;
        tau21_4 = tauParam{j,i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,i,j) ;
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j) ;
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i) ;
    end
end

%% Set parameters for heat of vaporization
for i=1:nComp 
    antc1 = dhvap{i,1}(1)*1000; % Change from kJ/kmol to J/kmol
    antc2 = dhvap{i,1}(2);
    antc3 = dhvap{i,1}(3);
    antc4 = dhvap{i,1}(4);
    antc5 = dhvap{i,1}(5);
    antc6 = 0;
    antc7 = 1000;
    ok = mexrbm('set_hvl',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,i) ;
end

%% Calculate results
% Topology
azeo = mexrbm('azeo',hdl,[],[],[],[],p);

hetaz = cell(0);
homaz = cell(0);

for j=1:length(azeo)
    % Find physically meaningful homogeneous azeotropes
    if (azeo(j).type == 1)
        homaz{end+1,1} = find(azeo(j).x);
        homaz{end,2} = azeo(j).t;
        homaz{end,3} = azeo(j).x;
    
    % Find physically meaningful heterogeneous azeotropes
    elseif (azeo(j).type == 3)
        hetaz{end+1,1} = find(azeo(j).x); 
        hetaz{end,2} = azeo(j).t;
        hetaz{end,3} = azeo(j).x;
        hetaz{end,4} = azeo(j).xI;
        hetaz{end,5} = azeo(j).xII;
    end
end

% Boiling point sequence
T_boil_sequence = cell(nComp+size(homaz,1)+size(hetaz,1),2);
index = 1;
for i=1:length(azeo)
    if ~(azeo(i).type == 7 || azeo(i).type == 17)
        T_boil_sequence{index,1} = azeo(i).t;
        if azeo(i).type == 4097
            T_boil_sequence{index,2} = 'pure';
            T_boil_sequence{index,3} = find(azeo(i).x);
        else
            T_boil_sequence{index,2} = 'azeo';
            T_boil_sequence{index,3} = find(azeo(i).x);
        end
        index = index + 1;
    end
end


%% Molar balances

% consider only components that are present
compspresent = [compsI, compsII];

for i=1:size(hetaz,1) 
    if strcmp(num2str(hetaz{i,1}), num2str(compspresent))
        t_hetaz = hetaz{i,2};
        xAz = hetaz{i,3};
        xI = hetaz{i,4};
        xII = hetaz{i,5};
    end
end


% Define phases for feed
if (xI(compsI) > xII(compsI))
    xF1 = xI;
    xF2 = xII;
else
    xF1 = xII;
    xF2 = xI;
end

% molefractions
f1.z = xF1;
d1.z = xAz; % distillate is azeotrope
b1.z = zeros(1, length(xF1));
b1.z(compsI) = n_in(compsI)/sum(n_in(compsI));
    
f2.z = xF2;
d2.z = xAz; % distillate is azeotrope
b2.z = zeros(1, length(xF2));
b2.z(compsII) = n_in(compsII)/sum(n_in(compsII));

% molar flows
b1.flow = sum(n_in(compsI));
d1.flow = (b1.z(compsI(1)) - f1.z(compsI(1))) / (f1.z(compsI(1)) - d1.z(compsI(1))) * b1.flow; % "Gesetz der abgewandten Hebelarme"
f1.flow = b1.flow + d1.flow;
    
b2.flow = sum(n_in(compsII));
d2.flow = (b2.z(compsII(1)) - f2.z(compsII(1))) / (f2.z(compsII(1)) - d2.z(compsII(1))) * b2.flow; % "Gesetz der abgewandten Hebelarme"
f2.flow = b2.flow + d2.flow;


%% Calculate column 1
D_result1 = mexrbm('CALC_RBM',hdl,p,azeo,b1.z,d1.z,(d1.flow/f1.flow),t_hetaz,0); 

%% Calculate column 1
D_result2 = mexrbm('CALC_RBM',hdl,p,azeo,b2.z,d2.z,(d2.flow/f2.flow),t_hetaz,0); 
    
%% write results
if (D_result1(5) == 1 && D_result2(5) == 1)
    
    RBMresult1.Qreb = D_result1(1);
    RBMresult1.Qcond = D_result1(2);
    RBMresult1.Treb = D_result1(3);  % Careful! Temperature only accurate for homogeneous mixtures
    RBMresult1.Tcond = D_result1(4); % Careful! Temperature only accurate for homogeneous mixtures
    RBMresult1.ok = D_result1(5);
    RBMresult1.reflux = D_result1(6);
    RBMresult1.boilup =D_result1(7);
    
    RBMresult2.Qreb = D_result2(1);
    RBMresult2.Qcond = D_result2(2);
    RBMresult2.Treb = D_result2(3);  % Careful! Temperature only accurate for homogeneous mixtures
    RBMresult2.Tcond = D_result2(4); % Careful! Temperature only accurate for homogeneous mixtures
    RBMresult2.ok = D_result2(5);
    RBMresult2.reflux = D_result2(6);
    RBMresult2.boilup =D_result2(7);
    
    RBMresults = [RBMresult1; RBMresult2];
    
    Qreb1 = RBMresult1.Qreb  * f1.flow / 10^3; %kJ/s
    Qcond1 = RBMresult1.Qcond  * f1.flow / 10^3; %kJ/s    
    Qreb2 = RBMresult2.Qreb * f2.flow  / 10^3; %kJ/s
    Qcond2 = RBMresult2.Qcond * f2.flow  / 10^3; %kJ/s
        
    n_out(1,:) = b1.flow * b1.z;
    n_out(2,:) = b2.flow * b2.z;
    
else
    ok = -1;
end
%% Clear
if exist([pwd,'/minenreport.rep'], 'file')
    delete([pwd,'/minenreport.rep'])
end
clear mex;

end