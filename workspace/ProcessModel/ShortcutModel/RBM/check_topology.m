function [azeo, hetaz, homaz, T_boil_sequence]= check_topology(properties, nComp, p_azeocheck)

%% Initiate mex
system = [pwd, '/ProcessModel/ShortcutModel/RBM/init_topo.dat']; % Folder for temporary files 
hdl=mexrbm('init',['',system]);   
    
    
% Read properties
antoineParam    = properties.AntoineParam;
alphaParam      = properties.AlphaParam;
tauParam        = properties.TauParam;   

%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 
% spec) to ln(p)= A+B/(T+C) in Pa
% HMF and water parameters are the same for every calculation

for i=1:nComp

    antc1 = antoineParam{i,1}(1) + log(100) ;
    antc2 = -antoineParam{i,1}(2);
    antc3 = antoineParam{i,1}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8, antc9,i) ;

end

%% Set NRTL parameters

for i=1:(nComp-1)
    for j=(i+1):nComp
        alpha   = alphaParam{i,j}(1) + 273.15*alphaParam{i,j}(2); % mexrbm expects alpha for C
        alphat  = alphaParam{i,j}(2) ;
        tau12_1 = tauParam{i,j}(1) ;
        tau12_2 = tauParam{i,j}(2) ;
        tau12_3 = tauParam{i,j}(3) ;
        tau12_4 = tauParam{i,j}(4) ;
        tau21_1 = tauParam{j,i}(1) ;
        tau21_2 = tauParam{j,i}(2) ;
        tau21_3 = tauParam{j,i}(3) ;
        tau21_4 = tauParam{j,i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,i,j) ;
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j) ;
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i) ;
    end
end

%% Calculate results
% Azeo
azeo = mexrbm('azeo',hdl,[],[],[],[],p_azeocheck);

hetaz = cell(0);
homaz = cell(0);

for j=1:length(azeo)
    % Find physically meaningful homogeneous azeotropes
    if (azeo(j).type == 1)
        homaz{end+1,1} = find(azeo(j).x);
        homaz{end,2} = azeo(j).t;
        homaz{end,3} = azeo(j).x;
    
    % Find physically meaningful heterogeneous azeotropes
    elseif (azeo(j).type == 3)
        hetaz{end+1,1} = find(azeo(j).x); 
        hetaz{end,2} = azeo(j).t;
        hetaz{end,3} = azeo(j).x;
        
    end
end

% Boiling point sequence
T_boil_sequence = cell(nComp+size(homaz,1)+size(hetaz,1),2);
index = 1;
for i=1:length(azeo)
    if ~(azeo(i).type == 7 || azeo(i).type == 17)
        T_boil_sequence{index,1} = azeo(i).t;
        if azeo(i).type == 4097
            T_boil_sequence{index,2} = 'pure';
            T_boil_sequence{index,3} = find(azeo(i).x);
        else
            T_boil_sequence{index,2} = 'azeo';
            T_boil_sequence{index,3} = find(azeo(i).x);
        end
        index = index + 1;
    end
end

clear mex;
end