function [n_in,n_out, Qreb, Qcond, RBMresults, ok] = main_RBM(properties,n_in,p,T,compsontop,Top_vap,nComp)
%% Execute RBM to get heating and cooling duties, reboiler and condenser temperatures and reflux and boil-up ratio using mexrbm  

%% Initiate mex
system = [pwd, '/ProcessModel/ShortcutModel/RBM/init.dat']; % Folder for temporary files 
hdl=mexrbm('init',['',system]);   

%% Initialize solution vector
n_out = zeros(2,length(n_in));
Qreb = NaN;
Qcond = NaN;
RBMresults = struct;

%% Check if valid input
if (max(n_in) == 0)
    ok = 0;
    return;
end

antoineParam    = properties.AntoineParam;
alphaParam      = properties.AlphaParam;
tauParam        = properties.TauParam;
dhvap           = properties.dhvap_para;
    
%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 
% spec) to ln(p)= A+B/(T+C) in Pa

for i=1:nComp
    antc1 = antoineParam{i,1}(1) + log(100) ;
    antc2 = -antoineParam{i,1}(2);
    antc3 = antoineParam{i,1}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 1000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8, antc9,i) ;
end
    
%% Set NRTL parameters

for i=1:(nComp-1)
    for j=(i+1):nComp
        alpha   = alphaParam{i,j}(1) + 273.15*alphaParam{i,j}(2); % mexrbm expects alpha for C
        alphat  = alphaParam{i,j}(2) ;
        tau12_1 = tauParam{i,j}(1) ;
        tau12_2 = tauParam{i,j}(2) ;
        tau12_3 = tauParam{i,j}(3) ;
        tau12_4 = tauParam{i,j}(4) ;
        tau21_1 = tauParam{j,i}(1) ;
        tau21_2 = tauParam{j,i}(2) ;
        tau21_3 = tauParam{j,i}(3) ;
        tau21_4 = tauParam{j,i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,i,j) ;
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j) ;
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i) ;
    end
end

%% Set parameters for heat of vaporization
for i=1:nComp 
    antc1 = dhvap{i,1}(1)*1000; % Change from kJ/kmol to J/kmol
    antc2 = dhvap{i,1}(2);
    antc3 = dhvap{i,1}(3);
    antc4 = dhvap{i,1}(4);
    antc5 = dhvap{i,1}(5);
    antc6 = 0;
    antc7 = 1000;
    ok = mexrbm('set_hvl',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,i) ;
end

%% Calculate results
% Topology
azeo = mexrbm('azeo',hdl,[],[],[],[],p);

hetaz = cell(0);
homaz = cell(0);

for j=1:length(azeo)
    % Find physically meaningful homogeneous azeotropes
    if (azeo(j).type == 1)
        homaz{end+1,1} = find(azeo(j).x);
        homaz{end,2} = azeo(j).t;
        homaz{end,3} = azeo(j).x;
    
    % Find physically meaningful heterogeneous azeotropes
    elseif (azeo(j).type == 3)
        hetaz{end+1,1} = find(azeo(j).x); 
        hetaz{end,2} = azeo(j).t;
        hetaz{end,3} = azeo(j).x;
        
    end
end

% Boiling point sequence
T_boil_sequence = cell(nComp+size(homaz,1)+size(hetaz,1),2);
index = 1;
for i=1:length(azeo)
    if ~(azeo(i).type == 7 || azeo(i).type == 17)
        T_boil_sequence{index,1} = azeo(i).t;
        if azeo(i).type == 4097
            T_boil_sequence{index,2} = 'pure';
            T_boil_sequence{index,3} = find(azeo(i).x);
        else
            T_boil_sequence{index,2} = 'azeo';
            T_boil_sequence{index,3} = find(azeo(i).x);
        end
        index = index + 1;
    end
end

%% Execute mexrbm
F = sum(n_in);
xF = n_in / F;

D = zeros(1,nComp);
D(compsontop) = n_in(compsontop);
xD = D / sum(D);
D = sum(D);

compsonbottom = 1:nComp;
compsonbottom(compsontop) = [];

B = zeros(1,nComp);
B(compsonbottom) = n_in(compsonbottom);
xB = B / sum(B);
B = sum(B);

t_func = T - 273.15;
D_result = mexrbm('CALC_RBM',hdl,p,azeo,xB,xD,D/F,t_func,Top_vap) ;

RBMresults.Qreb = D_result(1);
RBMresults.Qcond = D_result(2);
RBMresults.Treb = D_result(3);  % Careful! Temperature only accurate for homogeneous mixtures
RBMresults.Tcond = D_result(4); % Careful! Temperature only accurate for homogeneous mixtures
RBMresults.ok = D_result(5);
RBMresults.reflux = D_result(6);
RBMresults.boilup =D_result(7);

%% Check if condenser or reboiler temperature below azeotrop temperature,
% e.g. if heterogeneous azeotrope, which is not considered by RBM

% consider only components that are present
compsonbottom(n_in(compsonbottom) == 0) = [];
compsontop(n_in(compsontop) == 0) = [];

for i=1:size(hetaz,1) 
    % Reboiler   
    if strcmp(num2str(hetaz{i,1}), num2str(compsonbottom))
        t_hetaz = hetaz{i,2};
        t_comps = [T_boil_sequence{compsonbottom,1}];
        t_all = [t_hetaz, t_comps];
        [~, index] = sort(t_all); % Find boiling point order
        if index(1) == 1          % if low boiling point azeotrope
            violation = t_hetaz > (RBMresults.Treb-273.15); % Treb must be higher than t_hetaz
        else                      % else high boiling point azeotrope
            violation = t_hetaz < (RBMresults.Treb-273.15); % Treb must be lower than t_hetaz
        end
        if violation              % if violation set different temperature
            RBMresults.Treb = t_hetaz + 273.15;
        end
    % Condenser    
    elseif strcmp(num2str(hetaz{i,1}), num2str(compsontop))
        t_hetaz = hetaz{i,2};
        t_comps = [T_boil_sequence{compsontop,1}];
        t_all = [t_hetaz, t_comps];
        [~, index] = sort(t_all);
        if index(1) == 1          % if low boiling point azeotrope
            violation = t_hetaz > (RBMresults.Tcond-273.15); % Tcond must be higher than t_hetaz
        else                      % else high boiling point azeotrope
            violation = t_hetaz < (RBMresults.Tcond-273.15); % Tcond must be lower than t_hetaz
        end
        if violation              % if violation set different temperature
            RBMresults.Tcond = t_hetaz + 273.15;
        end
        
    end
end

%% Mol Balance
ok = RBMresults.ok;

if (ok == 1) && RBMresults.Qreb > 0 && RBMresults.Qcond < 0
    n_out(1,:) = D * xD;
    n_out(2,:) = B * xB;
    Qreb = RBMresults.Qreb * sum(n_in) / 10^3; %kJ/s
    Qcond = RBMresults.Qcond * sum(n_in) / 10^3; %kJ/s
end


%% Clear
% if exist([pwd,'/minenreport.rep'], 'file')
%     delete([pwd,'/minenreport.rep'])
% end

clear mex;

end