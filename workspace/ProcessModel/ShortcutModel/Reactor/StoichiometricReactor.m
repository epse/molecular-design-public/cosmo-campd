function [n_in, n_out] = StoichiometricReactor(n_in, turnover, targetcompound, v)
%% main execution of the stoichiometric Reactor model
% Input:    n_in (inflow of only relevant species)
%           turnover (of target compound)
%           target compound (index))
%           stoichiometric coefficients 
% Output:   nout


%% Preallocate fields
results = [];
n_out = zeros(1,length(n_in));

%% Check if previous block finished succesful

if (max(n_in) == 0)
    return;
end

%% Extent of reaction
xi = - (turnover * n_in(targetcompound)) / v(targetcompound);

%% Solve mass balance
n_out = n_in + v * xi;

end