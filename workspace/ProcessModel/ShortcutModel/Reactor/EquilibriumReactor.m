function [n_in, n_out, xi] = EquilibriumReactor(n_in, properties, p, T, phases, deltaGR_ig, v, xi0, liquids, firstliquid, reactionphase)
%% main execution of the VLLE Reactor model
% Input:    n_in (inflow of only relevant species)
%           p [bar], T [K], deltaGR [J/mol], v (stoichiometric coefficients, 
%           phases (only ideal gas: V, VLE, VLLE), key compounds of firstliquid,
%           reacting phase (only for VLLE), liquids (index of compounds
%           that should be treated as liquids), xi0 (initial guess for turnover))
%           specs (struct containing specifications
% Output:   nout (outlow (matrix with max three rows: 1st: V, 2nd: L1, 3rd: L2))
%           results (results struct (extent of reaction xi))

%% Preallocate fields
xi = NaN;

if strcmp(phases, 'V')
    n_out = zeros(1,length(n_in));
elseif strcmp(phases, 'VLE')
    n_out = zeros(2,length(n_in));        
elseif strcmp(phases, 'VLLE')
    n_out = zeros(3,length(n_in));
end

%% Check if valid input

if (max(n_in) == 0)
    return;
end

%% Initialise necessary parameter
% species considered
NC = length(n_in);

%% Define Function  to be solved

if strcmp(phases, 'V')
    func = @(xi) IGReactor(n_in, xi, p, T, v);
elseif strcmp(phases, 'VLE')
    func = @(xi) VLEReactor(n_in, properties, xi, p, T, v, deltaGR_ig, NC, liquids);        
elseif strcmp(phases, 'VLLE')
    func = @(xi) VLLEReactor(n_in, properties, xi, p, T, v, deltaGR_ig, NC, liquids, firstliquid, reactionphase);
end
                            
func(xi0);

% Solve the equation system with fsolve
options = optimoptions('fsolve', 'MaxFunEvals', 1e6, 'MaxIter', 5e3, 'TolFun', 1e-8, 'Display', 'off');
[xi_final, ~, exitflag, ~] = fsolve(func, xi0, options);

% Check solution and get results
if (exitflag > 0 && xi_final >= 0 && xi_final <= 1)
    xi = xi_final;
    if strcmp(phases, 'V')
        [~, n_out] = IGReactor(n_in, xi_final, p, T, v);
    elseif strcmp(phases, 'VLE')
        [~, n_out] = VLEReactor(n_in, properties, xi_final, p, T, v, deltaGR_ig, NC, liquids);        
    elseif strcmp(phases, 'VLLE')
        [~, n_out] = VLLEReactor(n_in, properties, xi_final, p, T, v, deltaGR_ig, NC, liquids, firstliquid, reactionphase);
    end                         
end

clear mex;


end

%% IG REACTOR
%########################################################################%
function [ error, nout ] = IGReactor( n_in, xi, p, T)
%% Solves the equation for an isothermal ideal gas reactor
% output is the error in reaction equilibrium

%% Definitions

% standard conditions
p0 = 1; % [bar]

% universal gas constant
R = 8.314; % [J / (mol * K)]

%% Mass balance

nout = n_in + v * xi;

y = nout / sum(nout);

%% Check Reaction equilibriabar

Kf = exp(-deltaGR_ig / R / T);
Ky = Kf * (p0 / p) ^ sum(v);

error = Ky - prod(y .^ v);

%% Mass balance

nout = n_in + v * xi;


end
%########################################################################%

%% VLE REACTOR
%########################################################################%
function [ error, nout ] = VLEReactor( n_in, properties, xi, p, T, v, deltaGR_ig, NC, liquids)
%% Solves the phase split problem for given turnover xi
% output is the error in reaction equilibrium

%% Definitions

% standard conditions
p0 = 1; % [bar]

% universal gas constant
R = 8.314; % [J / (mol * K)]

% liquids
NL = length(liquids);

% gases
gases = 1:NC;
gases(liquids) = [];
NG = length(gases);

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

%% Initiate mex

system = [pwd,'/ProcessModel/ShortcutModel/Reactor/init.dat']; % Data for mexprops  
hdl = mexprops('init', ['' system]);

%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm) 

for i = 1 : NC
    k1 = i - 1;
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexprops('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
end

%% Set NRTL parameters

for i = 1 : (NC - 1)
    for j = (i + 1) : NC
        k1 = i - 1;
        k2 = j - 1;
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mexprops('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
        ok = mexprops('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
        ok = mexprops('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
    end
end

%% calculate VLE split

% get feed by solving mass balances for given xi
for i = 1 : NC
    nF(i) = n_in(i) + v(i) * xi;
end
zF = nF / sum(nF);

% liquid flow
for i = 1 : NL
    nL(i) = n_in(liquids(i)) + v(liquids(i)) * xi;
end
x = nL / sum(nL);

% gaseous flow
for i = 1 : NG
    nG(i) = n_in(gases(i)) + v(gases(i)) * xi;
end
zG = nG / sum(nG);

vapour = sum(nG) / sum(nF); % vapor fraction
t = T - 273.15; % temperature in C

[x,y,~,~,vapour]=mexprops('tpz_flash',hdl,zF,x,zG,t,p,vapour);
if (isempty(x) || vapour < 0) % flash has failed
    error = 1000;
    return;
end
     
%% Check Reaction equilibria

% Activity coefficients
% get NRTL parameters
[Tau, Alpha] = nrtlParam(TauParam, AlphaParam, T);
gamma = NRTL(x, NC, Alpha, Tau);

% psat calculations
psat = Psat (AntoineParam, T);

% Equilibrium constant for solution in Solvent
Kf = exp(-deltaGR_ig / R / T);
Ka = prod((p0 ./ psat) .^ v) * Kf;

error = Ka - prod(x .^ v) * prod(gamma .^ v);

%% Calculated Streams

nVtot = sum(nF) * vapour;
nLtot = sum(nF) * (1 - vapour);

nout(1,:) = nVtot * y;
nout(2,:) = nLtot * x;

end

%########################################################################%

%% VLLE REACTOR
%########################################################################%
function [ error, nout ] = VLLEReactor( n_in, properties, xi, p, T, v, deltaGR_ig, NC, liquids, firstliquid, reactionphase)
%% Solves the phase split problem for given turnover xi
% output is the error in reaction equilibrium

%% Definitions

% standard conditions
p0 = 1; % [bar]

% universal gas constant
R = 8.314; % [J / (mol * K)]

% liquids
NL = length(liquids);
firstliquid = find(firstliquid == liquids);

% gases
gases = 1:NC;
gases(liquids) = [];
NG = length(gases);

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

%% Initiate mex

system = [pwd,'/ProcessModel/ShortcutModel/Reactor/init.dat']; % Data for mexprops
hdl=mexprops('init',['',system]);

%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm) 

for i = 1 : NC
    k1 =i;
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexprops('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
end

%% Set NRTL parameters

for i = 1 : (NC - 1)
    for j = (i + 1) : NC
        k1 =i;
        k2 =j;
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mexprops('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
        ok = mexprops('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
        ok = mexprops('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
    end
end

%% calculate LLE split for non gaseous components

% liquid flow
for i = 1 : NL
    nL(i) = n_in(liquids(i)) + v(liquids(i)) * xi;
end
zL = nL / sum(nL);

% Initial guess from spec
z0 = zeros(1,NL);
for i = 1 : length(firstliquid)
    z0(firstliquid(i)) = nL(firstliquid(i))/sum(nL(firstliquid));
end
    
% get NRTL parameters
AlphaLLE = AlphaParam(liquids, liquids);
TauLLE = TauParam(liquids, liquids);

% Solve LLE phase split problem
[xI, xII] = calcPhaseSplit(AlphaLLE, TauLLE, T, zL, z0);

%% calculate VLLE split

% get feed by solving mass balances for given xi
for i = 1 : NC
    nF(i) = n_in(i) + v(i) * xi;
end

zF = nF / sum(nF);

% estimates for phase compositions 
xI_new = zeros(1,NC);
xI_new(liquids) = xI;
xII_new = zeros(1,NC);
xII_new(liquids) = xII;

% gaseous flow
%nG = zeros(1,NC);
for i = 1 : NG
    nG(i) = n_in(gases(i)) + v(gases(i)) * xi;
end
zG = nG / sum(nG);

vapour = sum(nG) / sum(nF); % vapor fraction
t = T - 273.15; % temperature in C

% check phase split results
[x,y,~,~,vapour,xI,xII,phi,~] = mexprops('tpz_vllflash',hdl,zF,[],zG,t,p,vapour,[],[],xI_new,xII_new,[]);

if (isempty(x) || phi < 0 || vapour < 0 || xI(5) == 0) % flash has failed or no LLE
    error = 1000;
    return;
end
    
%% Check Reaction equilibria

% Activity coefficient
if xI(5) > xII(5)
    xeq = xI;
else
    xeq = xII;
end
gamma = getGamma(xeq, NC, AlphaParam, TauParam, T);

% psat calculations
psat = vapour_pressure (AntoineParam, T);

% Equilibrium constant for solution in Solvent
nonzeroelements = find(xeq~=0);
gamma = gamma(nonzeroelements);
psat = psat(nonzeroelements);
xeq = xeq(nonzeroelements);
v = v(nonzeroelements);

error = deltaGR_ig + R*T*sum(log(xeq.*gamma.*psat').*v);

% Kf = exp(-deltaGR_ig / R / T); % URSPRÜNGLICH
% Ka = prod((p0 ./ psat)' .^ v) * Kf;
% 
% error = Ka - prod(xeq .^ v) * prod(gamma .^ v);
    
%% Calculate Streams

nVtot = sum(nF) * vapour;
nLtot = sum(nF) * (1 - vapour);
nItot = nLtot * (1 - phi);
nIItot = nLtot * phi;

nout(1,:) = nVtot * y;
nout(2,:) = nItot * xI;
nout(3,:) = nIItot * xII;

end

%########################################################################%