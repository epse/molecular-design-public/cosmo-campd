% HEATINTEGRATION..........................................................
% this is a function performing Pinch Analysis cascade calculations of a 
% given set of thermal stream
% In addition the grand composite curve and the hot and cold composite
% curves are built
% INPUT..........................................................(sm,names)
% table "sm". "sm" is a matrix of (rows) thermal streams and 4 columns:
% Tin [K], Tout [K], q [kW], DTmin2 [K].
% OUTPUT.............................
% [casc,HU,CU,PPoint,casc_hot,casc_cold]
% casc: the cascade [t1; t2; Fcp; load; cumulative; cumulative_balanced]
% HU: hot utility [kW]
% CU: cold utility [kW]
% PPpoint: pinch point temperature [K]
% casc_hot: the cascade of hot streams
% casc_cold: the cascade of cold streams
% .........................................................................
% file created:     feb 2012.
% -------------------------------------------------------------------------
% Based on the script of 
% Matteo Morandin, PhD
% Division of Heat and Power Technology
% Dept. of Energy and Environment
% Chalmers University of Technology 
% Kemiv?gen,4
% SE-412 96 G?teborg ? Sweden
% tel.: +46-(0)31-772 8536
% e-mail: matteo.morandin@chalmers.se 
% -------------------------------------------------------------------------
function  [casc,HU,CU,PPoint,casc_hot,casc_cold,fig_hand] = heatintegration(sm, HRAT)
% initialize matrix with shifted temperatures
sms = zeros(length(sm(:,1)),3);

%% first operations on hot streams
% find hot streams
hot_i = find(sm(:,1)>sm(:,2)); % index
% display('the hot streams are')
% display(names(hot_i))
%  shifted inlet temperatures of hot streams
sms(hot_i,1)=sm(hot_i,1)-HRAT/2;
%  shifted outlet temperatures of hot streams
sms(hot_i,2)=sm(hot_i,2)-HRAT/2;
% evaluate Fcp of hot streams
sms(hot_i,3)=sm(hot_i,3)./(sms(hot_i,1) - sms(hot_i,2));

%% first operation on cold streams
% find cold streams
cold_i = find(sm(:,1)<sm(:,2)); % index
% display('the cold streams are')
% display(names(cold_i))
%  shifted inlet temperatures of cold streams
sms(cold_i,1)=sm(cold_i,1)+HRAT/2;
%  shifted outlet temperatures of cold streams
sms(cold_i,2)=sm(cold_i,2)+HRAT/2;
% evaluate Fcp of cold streams
sms(cold_i,3)=sm(cold_i,3)./(sms(cold_i,1) - sms(cold_i,2));

%% build temperature intervals
T_int = sort(unique([sms(:,1);sms(:,2)]),'descend') ;% sorting interval temperatures
int = [T_int(1:end-1) T_int(2:end)] ; % intervals

%% build cascade and hot and cold cascades
casc = [int zeros(length(int(:,1)),4)]; % initialize cascade matrix
casc_hot = [int zeros(length(int(:,1)),4)]; % initialize cascade matrix of hot streams
casc_cold = [int zeros(length(int(:,1)),4)]; % initialize cascade matrix of cold streams

for j = 1:length(int(:,1))
    hot_j = hot_i(sms(hot_i,1)>int(j,2) & sms(hot_i,2)<int(j,1));% interval hot stream assignment
    casc_hot(j,3)= sum(sms(hot_j,3)); % interval Fcp evaluation for hot matrix
    casc_hot(j,4) = casc_hot(j,3).*(casc_hot(j,1)-casc_hot(j,2)); % calculate loads for hot matrix
    cold_j = cold_i(sms(cold_i,1)<int(j,1) & sms(cold_i,2)>int(j,2));% interval cold stream assignment
    casc_cold(j,3)=sum(sms(cold_j,3)); % interval Fcp evaluation for cold matrix
    casc_cold(j,4) = casc_cold(j,3).*(casc_cold(j,1)-casc_cold(j,2)); % calculate loads for cold matrix
    casc(j,3)=sum(sms(cold_j,3)) + sum(sms(hot_j,3)); % interval Fcp evaluation
    casc(j,4)=casc(j,3)*(casc(j,1)-casc(j,2)); % interval load evaluation
    if j>1 % the first interval does not receive any load cascaded from an interval above
        casc_hot(j,5)=casc_hot(j-1,5)+casc_hot(j,4); % interval cumulative heat load hot matrix
        casc_cold(j,5)=casc_cold(j-1,5)+casc_cold(j,4); % interval cumulative heat load cold matrix
        casc(j,5)=casc(j-1,5)+casc(j,4); % interval cumulative heat load
    else
        % so cumulative heat load of the first interval is equal to its load
        casc_cold(1,5) = casc_cold(1,4);
        casc_hot(1,5) = casc_hot(1,4);
        casc(1,5)=casc(1,4);
    end
end

%% MER
HU = min(casc(casc(:,5)<0,5)); %
if isempty(HU) % threshold problem
    HU = 0;
    PPoint = int(1,1);
    display('THRESHOLD CASCADE!')
else
    PPoint = casc(casc(:,5)==HU,2) ;% pinch point
    HU = abs(HU); % MER hot utility
end
    
casc(:,6)=casc(:,5)+HU; % MER cascade 
CU = casc(end,6); % MER cold utility

%% elaborate cascades of hot and cold streams
% hot
% casc_hot(casc_hot(:,3)==0,:)=[]; % purge inactive intervals
casc_hot(:,6) = casc_hot(end,5) - casc_hot(:,5);
%cold
% casc_cold(casc_cold(:,3)==0,:)=[]; % purge inactive intervals
casc_cold(:,6) = casc_hot(end,5) + HU + casc_cold(:,5);

%% plotting GCC
% fig_hand = figure;
% subplot(1,2,1)
% plot([HU; casc(:,6)],[casc(1,1); casc(:,2)],'*k-','LineWidth',2)
% grid on
% title('Grand Composite Curve')
% ylabel('Shifted Temperature [K]')
% xlabel('Heat Load [kW]')
% ax1 = gca;
% set(ax1,'YMinorTick','off','XGrid','on','YGrid','on')
% Ylim=get(ax1,'yLim');
% text(Ylim(1)-30,Ylim(2)-10,' [deg C]','Color','b')
% ax2 = axes('Position',get(ax1,'Position'),...
%            'XAxisLocation','bottom',...
%            'YAxisLocation','right',...
%            'Color','none',...
%            'XColor','k','YColor','b',...
%            'yLim',get(ax1,'yLim')-273.15,...
%            'XTick',[],'XGrid','off','YGrid','on','YMinorTick','on');
% ylabel('Shifted Temperature [deg C]')

%% plotting CCCs
% subplot(1,2,2)
% grid on
% plot([casc_hot(end,5); casc_hot(:,6)],[casc_hot(1,1); casc_hot(:,2)],'r-','LineWidth',2)
% hold on
% plot([casc_hot(end,5)+ HU; casc_cold(:,6)],[casc_cold(1,1); casc_cold(:,2)],'b-','LineWidth',2)
% title('Hot and Cold Composite Curve')
% ylabel('Shifted Temperatures [K]')
% xlabel('Heat Load [kW]')
% ax1 = gca;
% set(ax1,'YMinorTick','off','XGrid','on','YGrid','on')
% Ylim=get(ax1,'yLim');
% text(0,Ylim(2)-10,' [deg C]','Color','b')
% ax2 = axes('Position',get(ax1,'Position'),...
%            'XAxisLocation','bottom',...
%            'YAxisLocation','right',...
%            'Color','none',...
%            'XColor','k','YColor','b',...
%            'yLim',get(ax1,'yLim')-273.15,...
%            'XTick',[],'XGrid','on','YGrid','on','YMinorTick','on');
% ylabel('Shifted Temperature [deg C]')
%% EoF
