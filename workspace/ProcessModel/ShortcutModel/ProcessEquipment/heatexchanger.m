function [ Q, T1, T2, Q_latent, Q_sensible1, Q_sensible2, T_boil, T_dew ] = heatexchanger( properties, n_in, T1, T2, vapfracI, vapfracII, p)
% Heat exchanger duty
% Input for flow rate in kmol/s, heat capacities in kJ/kmol Kelvin
% temperature in Kelvin, dhvap in kJ/kmol
% Output in kW

R = 8.314; % kJ/kmol/K

%% Distinguish between heat exchange with and without phase change

% 1. Just heat transfer, no phase change
if vapfracI == vapfracII
    
    % Get right cps from properties struct
    if vapfracI == 1
        cp_cos = properties.cp_gas_co;
    else
        cp_cos = properties.cp_liq_co;
    end
    
    % Calculate sensible heat from T1 to T2
    Q = 0;
    for i=1:length(n_in)
        Q = Q + n_in(i) *R* ( cp_cos{i}(1)*(T2-T1) + 1/2 * cp_cos{i}(2)*(T2^2-T1^2) ...
            + 1/3 * cp_cos{i}(3)* (T2^3-T1^3) + 1/4 * cp_cos{i}(4)* (T2^4-T1^4)+ 1/5 * cp_cos{i}(5)* (T2^5-T1^5));
    end

% 2. Phase change
else
    % Get cps, dhvap and T_critical from properties
    dhvap_dippr = properties.dhvap_para;   %kJ/kmol
    T_crit = properties.Tcrit;
    cp_liq_cos = properties.cp_liq_co;
    cp_gas_cos = properties.cp_gas_co;
    dhvap = zeros(length(n_in),1);

    Q_latent = 0; % kW
    Q_sensible1 = 0; % kW
    Q_sensible2 = 0; % kW
    
    % Get dew and bubble points
    T_boil = bubble_point(properties, n_in/sum(n_in), p); % Only valid for homogeneous mixtures!
    T_dew = dew_point(properties, n_in/sum(n_in), p);     % Only valid for homogeneous mixtures!
    
    
    % Calculate heat of vaporization at bubble_point
    Tr = T_boil/T_crit;
    for i=1:length(n_in)
       dhvap(i) = dhvap_dippr{i}(1)*(1-Tr)^(dhvap_dippr{i}(2) + dhvap_dippr{i}(3)*Tr + dhvap_dippr{i}(4)*Tr^2 + dhvap_dippr{i}(5)*Tr^3 );
    end
    
    % Calculate latent heat
    for i=1:length(n_in)
        Q_latent = Q_latent + n_in(i)*dhvap(i);
    end
    
    % Calculate sensible heat from bubble_point to dew_point and add to Q_latent
    for i=1:length(n_in)
        Q_latent = Q_latent + n_in(i) *R* ( cp_gas_cos{i}(1)*(T_dew-T_boil) + 1/2 * cp_gas_cos{i}(2)*(T_dew^2-T_boil^2) ...
            + 1/3 * cp_gas_cos{i}(3)* (T_dew^3-T_boil^3) + 1/4 * cp_gas_cos{i}(4)* (T_dew^4-T_boil^4)+ 1/5 * cp_gas_cos{i}(5)* (T_dew^5-T_boil^5));    
    end

    % Calculate sensible heat from dew_point on
    for i=1:length(n_in)
       Q_sensible2 = Q_sensible2 + n_in(i) *R* ( cp_gas_cos{i}(1)*(max(T2, T1)-T_dew) + 1/2 * cp_gas_cos{i}(2)*(max(T2, T1)^2-T_dew^2) ...
            + 1/3 * cp_gas_cos{i}(3)* (max(T2, T1)^3-T_dew^3) + 1/4 * cp_gas_cos{i}(4)* (max(T2, T1)^4-T_dew^4)+ 1/5 * cp_gas_cos{i}(5)* (max(T2, T1)^5-T_dew^5));  
    end
    
    % Calculate sensible heat to bubble_point
    for i=1:length(n_in)
       Q_sensible1 = Q_sensible1 + n_in(i) *R* ( cp_liq_cos{i}(1)*(T_boil-min(T2, T1)) + 1/2 * cp_liq_cos{i}(2)*(T_boil^2-min(T2, T1)^2) ...
            + 1/3 * cp_liq_cos{i}(3)* (T_boil^3-min(T2, T1)^3) + 1/4 * cp_liq_cos{i}(4)* (T_boil^4-min(T2, T1)^4)+ 1/5 * cp_liq_cos{i}(5)* (T_boil^5-min(T2, T1)^5));  
    end

    % Account for direction
    Q_latent = Q_latent * (vapfracII - vapfracI);
    Q_sensible1 = Q_sensible1 * (vapfracII - vapfracI);
    Q_sensible2 = Q_sensible2 * (vapfracII - vapfracI);
    
    Q = Q_sensible1 + Q_sensible2 + Q_latent; % kW
    
end

end

