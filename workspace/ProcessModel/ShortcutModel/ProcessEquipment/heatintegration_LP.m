% HEATINTEGRATION..........................................................
% this is a function performing Pinch Analysis cascade calculations of a 
% given set of thermal stream
% In addition the grand composite curve and the hot and cold composite
% curves are built
% INPUT..........................................................(sm,names)
% table "sm". "sm" is a matrix of (rows) thermal streams and 4 columns:
% Tin [K], Tout [K], q [kW], DTmin2 [K].
% OUTPUT.............................
% [casc,HU,CU,PPoint,casc_hot,casc_cold]
% casc: the cascade [t1; t2; Fcp; load; cumulative; cumulative_balanced]
% HU: hot utility [kW]
% CU: cold utility [kW]
% PPpoint: pinch point temperature [K]
% casc_hot: the cascade of hot streams
% casc_cold: the cascade of cold streams
% .........................................................................
% file created:     feb 2012.
% -------------------------------------------------------------------------
% Based on the script of 
% Matteo Morandin, PhD
% Division of Heat and Power Technology
% Dept. of Energy and Environment
% Chalmers University of Technology 
% Kemiv?gen,4
% SE-412 96 G?teborg ? Sweden
% tel.: +46-(0)31-772 8536
% e-mail: matteo.morandin@chalmers.se 
% -------------------------------------------------------------------------
function  [casc,HU,CU,E_total,E_heat,E_cool,PPoint,hot_util,cold_util] = heatintegration_LP(sm, hot_util, cold_util, HRAT, specs)
% initialize matrix with shifted temperatures
sms = zeros(length(sm(:,1)),3);

%% first operations on hot streams
% find hot streams
hot_i = find(sm(:,1)>sm(:,2)); % index
% display('the hot streams are')
% display(names(hot_i))
%  shifted inlet temperatures of hot streams
sms(hot_i,1)=sm(hot_i,1)-HRAT/2;
%  shifted outlet temperatures of hot streams
sms(hot_i,2)=sm(hot_i,2)-HRAT/2;
% evaluate Fcp of hot streams
sms(hot_i,3)=sm(hot_i,3)./(sms(hot_i,1) - sms(hot_i,2));

%% first operation on cold streams
% find cold streams
cold_i = find(sm(:,1)<sm(:,2)); % index
% display('the cold streams are')
% display(names(cold_i))
%  shifted inlet temperatures of cold streams
sms(cold_i,1)=sm(cold_i,1)+HRAT/2;
%  shifted outlet temperatures of cold streams
sms(cold_i,2)=sm(cold_i,2)+HRAT/2;
% evaluate Fcp of cold streams
sms(cold_i,3)=sm(cold_i,3)./(sms(cold_i,1) - sms(cold_i,2));

%% build temperature intervals
T_coldutil = [cold_util{:,2}]';
T_hotutil = [hot_util{:,2}]';
T_int = sort(unique([sms(:,1);sms(:,2);T_coldutil+HRAT/2;T_hotutil-HRAT/2]),'descend') ;% sorting interval temperatures
int = [T_int(1:end-1) T_int(2:end)] ; % intervals

%% build cascade and hot and cold cascades
casc = [int zeros(length(int(:,1)),4)]; % initialize cascade matrix
casc_hot = [int zeros(length(int(:,1)),4)]; % initialize cascade matrix of hot streams
casc_cold = [int zeros(length(int(:,1)),4)]; % initialize cascade matrix of cold streams

for j = 1:length(int(:,1))
    hot_j = hot_i(sms(hot_i,1)>int(j,2) & sms(hot_i,2)<int(j,1));% interval hot stream assignment
    casc_hot(j,3)= sum(sms(hot_j,3)); % interval Fcp evaluation for hot matrix
    casc_hot(j,4) = casc_hot(j,3).*(casc_hot(j,1)-casc_hot(j,2)); % calculate loads for hot matrix
    cold_j = cold_i(sms(cold_i,1)<int(j,1) & sms(cold_i,2)>int(j,2));% interval cold stream assignment
    casc_cold(j,3)=sum(sms(cold_j,3)); % interval Fcp evaluation for cold matrix
    casc_cold(j,4) = casc_cold(j,3).*(casc_cold(j,1)-casc_cold(j,2)); % calculate loads for cold matrix
    casc(j,3)=sum(sms(cold_j,3)) + sum(sms(hot_j,3)); % interval Fcp evaluation
    casc(j,4)=casc(j,3)*(casc(j,1)-casc(j,2)); % interval load evaluation
    if j>1 % the first interval does not receive any load cascaded from an interval above
        casc_hot(j,5)=casc_hot(j-1,5)+casc_hot(j,4); % interval cumulative heat load hot matrix
        casc_cold(j,5)=casc_cold(j-1,5)+casc_cold(j,4); % interval cumulative heat load cold matrix
        casc(j,5)=casc(j-1,5)+casc(j,4); % interval cumulative heat load
    else
        % so cumulative heat load of the first interval is equal to its load
        casc_cold(1,5) = casc_cold(1,4);
        casc_hot(1,5) = casc_hot(1,4);
        casc(1,5)=casc(1,4);
    end
end

%% MER
HU = min(casc(casc(:,5)<0,5)); %
if isempty(HU) % threshold problem
    HU = 0;
    PPoint = int(1,1);
    display('THRESHOLD CASCADE!')
else
    PPoint = casc(casc(:,5)==HU,2) ;% pinch point
    HU = abs(HU); % MER hot utility
end
    
casc(:,6)=casc(:,5)+HU; % MER cascade 
CU = casc(end,6); % MER cold utility

%% IF HEAT INTEGRATION: LP for utility
if specs.HI
    % Number of intervals, hot utilities and cold utilities
    numints = size(casc,1)+1;
    numhots = size(hot_util,1);
    numcolds = size(cold_util,1);

    % Equations
    Aeq = zeros(size(casc,1)+2, numints+numhots+numcolds);
    % Residuals
    for k=1:size(casc,1)
        Aeq(k,k) = 1;
        Aeq(k,k+1) = -1;
    end
    Aeq(end-1,1) = 1;
    Aeq(end,numints) = 1;

    % Utility
    % Hot
    for k=1:numhots
        indexhot = find(T_hotutil(k)-HRAT/2 == casc(:,1));
        Aeq(indexhot, numints+k) = 1;
    end
    % Cold
    for k=1:numcolds
        indexcold = find(T_coldutil(k)+HRAT/2 == casc(:,2));
        Aeq(indexcold, numints+numhots+k) = -1;
    end

    % Right hand side
    beq = -[casc(:,4); 0; 0];

    % bounds
    lb = zeros(numints+numhots+numcolds,1);

    % objective function
    f = zeros(numints+numhots+numcolds,1);
    %hot
    for k=1:numhots
        if hot_util{k,2}>298.15
            f(numints+k) = 1 - 298.15/hot_util{k,2};
        else
            f(numints+k) = -(1 - 298.15/hot_util{k,2});
        end
    end
    % Cold
    for k=1:numcolds
        if cold_util{k,2}>298.15
            f(numints+numhots+k) = 1 - 298.15/cold_util{k,2};
        else
            f(numints+numhots+k) = -(1 - 298.15/cold_util{k,2});
        end
    end


    % Solve 
    options = optimoptions('linprog', 'Display','none');
    [x, fval, exitflag, output] = linprog(f,[],[],Aeq,beq,lb,[],options);
    
    E_heat = 0;
    E_cool = 0;
    
    if isempty(x)
        E_total = 1e20;
    else

        % Gather results
        hot_util(:,3) = num2cell(x(numints+1:numints+numhots));
        cold_util(:,3) = num2cell(x(numints+numhots+1:numints+numhots+numcolds));

        for k=1:size(hot_util,1)
            if hot_util{k,2}>298.15
                E_heat = E_heat + hot_util{k,3} * (1 - 298.15/hot_util{k,2});
            else
                E_heat = E_heat + -hot_util{k,3} * (1 - 298.15/hot_util{k,2});
            end
        end

        for k=1:size(cold_util,1)
            if cold_util{k,2}>298.15
                E_cool = E_cool + cold_util{k,3} * (1 - 298.15/cold_util{k,2});
            else
                E_cool = E_cool + -cold_util{k,3} * (1 - 298.15/cold_util{k,2});
            end
        end

        E_total = fval;
    end
else
    %% Option 2: No HEAT Integration

    % Total heat load no HI
    total_cooling = abs(casc_hot(end, 5));

    % Intermediate: 
    intcold= find(casc_hot(:,2)==cold_util{2,2}+HRAT/2);
    intcoolingload = abs(casc_hot(intcold, 5));
    maxcoolingload = total_cooling - intcoolingload;

    % Total coolin load no HI
    total_heat = abs(casc_cold(end, 5));

    % Intermediate: 
    inthot = find(casc_cold(:,2)==hot_util{1,2}-HRAT/2);
    inthotload = total_heat - abs(casc_cold(inthot, 5));
    maxhotload = total_heat - inthotload;


    E_heat = 0;
    E_cool = 0;

    E_heat = E_heat + inthotload * (1 - 298.15/hot_util{1,2});
    E_heat = E_heat + maxhotload * (1 - 298.15/hot_util{2,2});


    E_cool = E_cool - maxcoolingload * (1 - 298.15/cold_util{1,2});
    E_cool = E_cool - intcoolingload * (1 - 298.15/cold_util{2,2});


    E_total = E_cool+E_heat;

end

%% plotting GCC
% figure
% plot([HU; casc(:,6)],[casc(1,1); casc(:,2)],'*k-','LineWidth',2)
% grid on
% title('Grand Composite Curve')
% ylabel('Shifted Temperature [K]')
% xlabel('Heat Load [kW]')
% ax1 = gca;
% set(ax1,'YMinorTick','off','XGrid','on','YGrid','on')
% Ylim=get(ax1,'yLim');
% text(Ylim(1)-30,Ylim(2)-10,' [deg C]','Color','b')
% ax2 = axes('Position',get(ax1,'Position'),...
%            'XAxisLocation','bottom',...
%            'YAxisLocation','right',...
%            'Color','none',...
%            'XColor','k','YColor','b',...
%            'yLim',get(ax1,'yLim')-273.15,...
%            'XTick',[],'XGrid','off','YGrid','on','YMinorTick','on');
% ylabel('Shifted Temperature [deg C]')
% 
% %% plotting CCC
% elaborate cascades of hot and cold streams
% hot
% casc_hot(casc_hot(:,3)==0,:)=[]; % purge inactive intervals
casc_hot(:,6) = casc_hot(end,5) - casc_hot(:,5);
%cold
% casc_cold(casc_cold(:,3)==0,:)=[]; % purge inactive intervals
casc_cold(:,6) = casc_hot(end,5) + HU + casc_cold(:,5);

% figure
% grid on
% plot([casc_hot(end,5); casc_hot(:,6)],[casc_hot(1,1); casc_hot(:,2)],'r-','LineWidth',2)
% hold on
% plot([casc_hot(end,5)+ HU; casc_cold(:,6)],[casc_cold(1,1); casc_cold(:,2)],'b-','LineWidth',2)
% title('Hot and Cold Composite Curve')
% ylabel('Shifted Temperatures [K]')
% xlabel('Heat Load [kW]')
% ax1 = gca;
% set(ax1,'YMinorTick','off','XGrid','on','YGrid','on')
% Ylim=get(ax1,'yLim');
% text(0,Ylim(2)-10,' [deg C]','Color','b')
% ax2 = axes('Position',get(ax1,'Position'),...
%            'XAxisLocation','bottom',...
%            'YAxisLocation','right',...
%            'Color','none',...
%            'XColor','k','YColor','b',...
%            'yLim',get(ax1,'yLim')-273.15,...
%            'XTick',[],'XGrid','on','YGrid','on','YMinorTick','on');
% ylabel('Shifted Temperature [deg C]')

end