function [ P_comp ] = compressor( n_in, p1, p2, T )
% Isothermal compressor power duty
% Input for flow rate in kmol/s, temperature in Kelvin, pressure
% consistently but selectable at will
% Output in kW

R = 8.314; % kJ/kmol K

P_comp = sum(n_in)*R*T*log(p2/p1); % formula for isothermal (=compressor with infinite number of stages) compression

end

