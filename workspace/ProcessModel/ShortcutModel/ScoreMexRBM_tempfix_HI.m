function [ molStruct ] = ScoreMexRBM_tempfix_HI( molStruct, specs, comments )
% Optimize over mextraction and RBM shortcut to evaluate molecule

% Preallocate fields
nComp = length(specs.system); % Number of compounds
for i=1:length(molStruct)
    molStruct(i).ExtrResults = struct;
    molStruct(i).ExtrResults.Smin = NaN;
    molStruct(i).RBMresults = struct;
    molStruct(i).RBMresults.Qreb = NaN;
    molStruct(i).Topology = struct; 
    molStruct(i).HeatX = NaN;
    molStruct(i).Exergydemand = NaN;
end

% Suppresses output during optimization
specs.Mode = 'optimization';

% Add paths
addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));
addpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/ProcessEquipment']));
addpath([specs.maindir,'/ProcessModel/Toolbox']);

% Write init-Files
write_init(nComp,'Mextraction');
write_init(nComp,'RBM');

%% Check Topology
write_init(nComp,'Topology');

p_azeocheck = specs.Topology.p_azeo;

parfor i=1:length(molStruct)
    [azeo, hetaz, homaz, T_boil_sequence] = check_topology(molStruct(i), nComp, p_azeocheck);
    molStruct(i).Topology.azeo = azeo;
    molStruct(i).Topology.Heteroazeo = hetaz;
    molStruct(i).Topology.Homoazeo = homaz;
    molStruct(i).Topology.T_boil_sequence = T_boil_sequence;
end

% Exclude systems with impossible separations because of azeotropes 
% Allowed azeotropes
Homoazeotropes = specs.Topology.Homoazeotropes;
Heteroazeotropes = specs.Topology.Heteroazeotropes;

for i=1:length(molStruct)
    % Heteroazeotropes
    ok = zeros(size(molStruct(i).Topology.Heteroazeo, 1)); % Number of heteroazeotropes in system
    molStruct(i).Topology.ok = 1;
    molStruct(i).Topology.ok = 1;
    
    for j=1:size(molStruct(i).Topology.Heteroazeo, 1)
        for k=1:length(Heteroazeotropes)
            if isequal(molStruct(i).Topology.Heteroazeo(j,1), Heteroazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Heteroazeo,1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        molStruct(i).Topology.ok = 0;
        continue;
    end

    % Homoazeotropes
    ok = zeros(size(molStruct(i).Topology.Homoazeo, 1)); % Number of homoazeotropes in system
    for j=1:size(molStruct(i).Topology.Homoazeo,1)
        for k=1:length(Homoazeotropes)
            if isequal(molStruct(i).Topology.Homoazeo(j,1), Homoazeotropes(k))
                ok(j) = 1; % 1 if allowed azeotrope
            end
        end
    end
    
    if sum(ok) ~= size(molStruct(i).Topology.Homoazeo, 1) % if all azeotropes are ok
        molStruct(i).Topology.ok = 0;
        molStruct(i).Topology.ok = 0;
        continue;
    end
    
end

% Exclude systems with impossible separations because of boiling point sequence
% Works for specifying bottom product
BP_order = specs.Topology.BP_order;

for i=1:length(molStruct)
    [~, index] = sort([molStruct(i).Topology.T_boil_sequence{:,1}], 'descend');  % Sorting of boiling points descending order
    for j=1:size(BP_order,1)
        if ~sum(index(BP_order{j,2}) == BP_order{j,1})
            molStruct(i).Topology.ok = 0;
            continue;
        end
    end
end


%% Optimize Tk_Extr
parfor k=1:length(molStruct)
    if molStruct(k).Topology.ok
        vars = molStruct(k).opt_vars;
        
        [E_total, ExtrResults, RBMresults, molStruct(k)] = calcMexRBM(vars, molStruct(k), specs);
    end
end

rmpath(genpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']));
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
   
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [E_total, ExtrResults, RBMresults] = calcMexRBM(vars, properties, specs,comments, point)

nComp = length(specs.system);
T_Extr = vars(1);
T_Dec = vars(2);
p_RBM = vars(3);

%% Mextraction
% Set inputs for extraction
p_Extr = specs.Extraction.pExtr;
purity = specs.Extraction.purity;
iRaffinate = specs.Extraction.iRaffinate;
iSolvent = specs.Extraction.iSolvent;

system = [iRaffinate iSolvent];
xFeed = [0.7 0.3];
x0 = [1 0];
xI = zeros(1, nComp);
xII = zeros(1, nComp);
[xI(system),  xII(system)] = calcPhaseSplit(properties.AlphaParam(system, system), properties.TauParam(system, system), T_Dec, xFeed, x0);
xS = xII;

for i=1:5

    % Execute Mextration
    [n_in, n_out, Smin, extrResult, ok] = main_Mextraction(properties, T_Extr, p_Extr, specs.n_in, purity, xS, nComp, iRaffinate, iSolvent);

    % Save Results
    ExtrResults.extrResult = extrResult;
    ExtrResults.n_in = n_in;
    ExtrResults.n_out = n_out;
    ExtrResults.Smin = Smin;
    ExtrResults.T_Extr_opt = T_Extr;
    ExtrResults.ok = ok;
    
    if ok == 0 || ok < 0
        E_total = 1e20;
        return;
    end

    % Decanter composition inlet
    x_Dec_in = zeros(1,3);
    x_Dec_in(1,system) = n_out(1,system) / sum(n_out(1,system));
    
    if x_Dec_in(iRaffinate) > xII(iRaffinate) && x_Dec_in(iRaffinate) < xI(iRaffinate)
        %% Gesetz der abgewandten Hebelarme wenn Decanter:
        nI = sum(n_out(1,system)) * (x_Dec_in(iRaffinate) - xII(iRaffinate))/(xI(iRaffinate)-xII(iRaffinate));
        nII = sum(n_out(1,system)) - nI;
        nI = nI * xI;
        nII = nII * xII;
        ExtrResults.Decanter = 1;
        ExtrResults.T_Dec_opt = T_Dec;
    else
        % No Decanter
        nI = zeros(1,nComp);
        nII = zeros(1,nComp);
        nII(system) = n_out(1,system);
        ExtrResults.Decanter = 0;
    end

    %% Waste water and make-up streams
    n_waste = nI + n_out(2,:);
    n_makeup = n_in(2,:) - nII;
        
    % Actual xS
    ExtrResults.n_waste = n_waste;
    ExtrResults.n_makeup = n_makeup;
    ExtrResults.xS_old = xS;
    S = nII;
    S(iSolvent) = S(iSolvent)+n_makeup(iSolvent);
    xS = S/sum(S);
    ExtrResults.xS_neu = xS;
    
    % Stop if no more iteration necessary
    if abs(ExtrResults.xS_neu(iRaffinate)- ExtrResults.xS_old(iRaffinate)) < 1e-9
        break
    end

end

%% HEAT EXCHANGER for heating up the feed to extraction temperature
T1 = 298.15;
T2 = T_Extr;
n_inHx = n_in(1,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for heating the extract to boiling point
T1 = T_Extr;
F_RBM = n_out(1,:); % Phase 1 is extractphase
T_RBM = bubble_point( properties, F_RBM/sum(F_RBM), p_RBM );
if isnan(T_RBM)
    E_total = 1e20;
    return;
end
T2 = T_RBM;
[ Q, T1, T2 ] = heatexchanger( properties, F_RBM, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for cooling the raffinate only
T1 = T_Extr;
T2 = 298.15;
n_inHx = n_out(2,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

if ExtrResults.Decanter
    %% HEAT EXCHANGER for cooling the waste from decanter if applicable
    T1 = T_Dec;
    T2 = 298.15;
    n_inHx = nI;
    [ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
    HeatX(end+1, :) = [ T1, T2, abs(Q) ];
    
    %% HEAT EXCHANGER for reheating the recycle to extraction temperature if applicable
    T1 = T_Dec;
    T2 = T_Extr;
    n_inHx = nII;
    [ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
    HeatX(end+1, :) = [ T1, T2, abs(Q) ]; 
end

%% HEAT EXCHANGER for heating up the solvent stream to extraction temperature
T1 = 298.15;
T2 = T_Extr;
n_inHx = n_makeup;
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];


%% RBM
% Inputs for distillation column
% Feed Input
F_vap = 0;
compsontop = specs.RBM.compsontop;

% Execute RBM
[n_in,n_out,Qreb, Qcond, rbmResults, ok] = main_RBM_cp(properties, F_RBM, p_RBM, T_RBM-0.1, compsontop, F_vap, nComp);

% Save Results
RBMresults.n_in = n_in;
RBMresults.n_out = n_out;
RBMresults.Qreb = Qreb;
RBMresults.Qcond = Qcond;
RBMresults.rbmResults = rbmResults;
RBMresults.p_RBM_opt = p_RBM;
RBMresults.ok = ok;

if isnan(Qreb)
    Qreb = 1e20;
end

% Heat Exchanger of RBM
T1 = RBMresults.rbmResults.Treb;
T2 = T1+0.01;
Q= Qreb;
HeatX(end+1, :) = [ T1, T2, abs(Q) ];
T1 = RBMresults.rbmResults.Tcond;
T2 = T1-0.01;
Q= Qcond;
HeatX(end+1, :) = [ T1, T2, abs(Q) ];


%% HEAT EXCHANGER for cooling the mixture for decanter
T1 = RBMresults.rbmResults.Tcond;
if ExtrResults.Decanter
    T2 = T_Dec;
else
    T2 = T_Extr;
end
n_inHx = n_out(1,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT EXCHANGER for cooling GVL for product
T1 = RBMresults.rbmResults.Treb;
T2 = 298.15;
n_inHx = n_out(2,:);
[ Q, T1, T2 ] = heatexchanger( properties, n_inHx, T1, T2, 0, 0);
HeatX(end+1, :) = [ T1, T2, abs(Q) ];

%% HEAT INTEGRATION
cold_util = { 'refrig', 233; 
              'cw'    , 283 };
hot_util = {  'LP'    , 410; 
              'HP', 558.15}; %K
HRAT = 10;
[casc,HU,CU,E_total, E_heat, E_cool,PPoint,~,~] = heatintegration_LP(abs(HeatX), hot_util, cold_util, HRAT, specs);

%% Save procedure
properties.ExtrResults = ExtrResults;
properties.RBMresults = RBMresults;
properties.Exergydemand = E_total/1000; %kJ/s --> kJ/kmol --> kJ/mol
properties.HeatX = HeatX;
properties.opt_vars = vars;

% Calculate Score
if isnan(properties.Exergydemand)
    properties.Score = 0;
else
    properties.Score = 1/properties.Exergydemand;
end

if exist([specs.Gate{3},'/LEA_', specs.Number,  '_', num2str(point), '.mat'], 'file')
    load([specs.Gate{3},'/LEA_', specs.Number,  '_', num2str(point), '.mat']);
    if isnan(molStruct.Exergydemand)
        molStruct = properties;
        save([specs.Gate{3}, '/LEA_', specs.Number, '_', num2str(point), ], 'molStruct')  % Save Struct
        printcomments(molStruct, comments, specs, point)                  % Save LEA
    elseif properties.Exergydemand < molStruct.Exergydemand
        molStruct = properties;
        save([specs.Gate{3}, '/LEA_', specs.Number, '_', num2str(point), ], 'molStruct')
        printcomments(molStruct, comments, specs, point)                  % Save LEA
    end
else
    molStruct = properties;
    save([specs.Gate{3}, '/LEA_', specs.Number, '_', num2str(point), ], 'molStruct')
    printcomments(molStruct, comments, specs, point)                  % Save LEA
end
    


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%