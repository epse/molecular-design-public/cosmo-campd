function Gate2LEA( job, gate, solvent, parametrization, noofcalc, precalc )
% Handling input and output arguments of LEA3D optimization and MATLAB
% property prediction and process model
% Two modes: (i) Scoring of molecules and 
%            (ii) collecting scored molecule structs
%            (iii) write specs-struct


    %% Load Inputs from corrsesponding job input file
    addpath('JobInput');
    
    % Heuristic partitition coefficient
    % loadInputs_logP;
    
    % Heuristic partitition coefficient from Gmehling et al.
    % loadInputs_Gmehling;
    
    % Hybrid Extraction-Distillation process
    %loadInputs_Mex_RBM_HI;

    % CO-Process including Absorption for DMF
    loadInputs_CO_Process;
    
    rmpath('JobInput');
    
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Mode (I): Scoring of molecule
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(job, 'Scoring')
    %% Turn off parpool-autocreate
    TurnOffParPool;
    
    %% Delete if any outleas
    if ~isempty(dir([gate{4}, '/out_LEA_',noofcalc,'_*.txt']))
        delete([gate{4}, '/out_LEA_',noofcalc,'_*.txt']);
    end
    if exist([gate{4}, '/out_LEA_',noofcalc,'.txt'], 'file')
        delete([gate{4}, '/out_LEA_',noofcalc,'.txt']);
    end
    
try
    %% Call property prediction and process model evaluation routine 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    % Check if molecule was already scored before
    % If yes, do not score again, if not, execute procedure
    if precalc
        nameofstruct = 'molStruct_precalculation.mat';
    else
        nameofstruct = 'molStruct_of_all_molecules.mat';
    end
    
    if exist(nameofstruct, 'file')
        load(nameofstruct);
        index = find(strcmp({molStruct_all.name}, solvent));
        if isempty(index)
            already_scored = 0;
        else
            molStruct = molStruct_all(index(1));
            already_scored = 1;
        end
    else already_scored = 0;
    end
    
    if (~already_scored)
        % Create a molstruct with the corresponding molecule
        name = ['LEA_', noofcalc];
        SMILESsolvent = strrep(solvent, '+', '#');
        SMILESsolvent = strrep(SMILESsolvent, '_', ']');
        SMILESsolvent = strrep(SMILESsolvent, '^', '[');
        molStruct = struct('name', solvent, 'SMILES', SMILESsolvent);
        addpath([specs.maindir,'/PropertyPrediction/']);

        %% Constraints
        % Check T_boil
        if isfield(specs, 'T_boil_min') && (~strcmp(specs.T_boil_min, 'no_T_min') || ~strcmp(specs.T_boil_max, 'no_T_max'))
            molStruct = job_T_boil_con(molStruct,specs.compounds,specs);
        else molStruct.T_boil_ok = 1;
        end

        % Check T_melt
        if isfield(specs, 'T_melt_min') && (~strcmp(specs.T_melt_min, 'no_T_min') || ~strcmp(specs.T_melt_max, 'no_T_max'))
            molStruct = job_T_melt_con(molStruct,specs.compounds,specs);
        else molStruct.T_melt_ok = 1;
        end
        
        % Check for LLE
        if isfield(specs, 'LLEreq')
            molStruct = job_LLE(molStruct,specs.compounds,specs); 
            if isnan(molStruct.x_LLE)
                molStruct.LLEok = 0;
            else molStruct.LLEok = 1;
            end
        else molStruct.LLEok = 1;
        end

        % Constraints fulfilled
        if molStruct.T_boil_ok && molStruct.T_melt_ok && molStruct.LLEok% && molStruct.COSMOfrag_Score_ok
            molStruct.ConCheck = 1;
        else
            molStruct.ConCheck = 0;
        end
    
    end
    
    % Output results of constraints check into comments section
    comments = cell(0);
    comments{length(comments)+1} = '__________________________________________________\n\n';
    
    % Constraints on T_boil
    if (~strcmp(specs.T_boil_min, 'no_T_min') || ~strcmp(specs.T_boil_max, 'no_T_max'))
	comments{length(comments)+1} = ['T_boiling by Antoine equation = ', num2str(molStruct.T_boil), ' K \n'];
        if (~strcmp(specs.T_boil_min, 'no_T_min') && molStruct.T_boil_ok)
            comments{length(comments)+1} = ['T_boil is ok (',num2str(molStruct.T_boil), ' K > ' num2str(specs.T_boil_min), ' K )\n'];
        elseif (~strcmp(specs.T_boil_min, 'no_T_min') && (~molStruct.T_boil_ok ))
            comments{length(comments)+1} = ['T_boil is too low (',num2str(molStruct.T_boil), ' K < ' num2str(specs.T_boil_min), ' K )\n'];
        end
        if (~strcmp(specs.T_boil_max, 'no_T_max') && molStruct.T_boil_ok )
                comments{length(comments)+1} = ['T_boil is ok (',num2str(molStruct.T_boil), ' K < ' num2str(specs.T_boil_max), ' K )\n'];
        elseif (~strcmp(specs.T_boil_max, 'no_T_max') && (~molStruct.T_boil_ok ))
                comments{length(comments)+1} = ['T_boil is too high (',num2str(molStruct.T_boil), ' K > ' num2str(specs.T_boil_max), ' K )\n'];
        end
        comments{length(comments)+1} = '\n';
    end

        % Constraints on T_melt
    if (~strcmp(specs.T_melt_min, 'no_T_min') || ~strcmp(specs.T_melt_max, 'no_T_max'))
            comments{length(comments)+1} = ['T_melt by COSMOquick QSPR = ', num2str(molStruct.T_melt), ' K \n'];
        if (~strcmp(specs.T_melt_min, 'no_T_min') && molStruct.T_melt_ok)
            comments{length(comments)+1} = ['T_melt is ok (',num2str(molStruct.T_melt), ' K > ' num2str(specs.T_melt_min), ' K )\n'];
        elseif (~strcmp(specs.T_melt_min, 'no_T_min') && (~molStruct.T_melt_ok))
            comments{length(comments)+1} = ['T_melt is too low (',num2str(molStruct.T_melt), ' K < ' num2str(specs.T_melt_min), ' K )\n'];
        end
        if (~strcmp(specs.T_melt_max, 'no_T_max') && molStruct.T_melt_ok)
                comments{length(comments)+1} = ['T_melt is ok (',num2str(molStruct.T_melt), ' K < ' num2str(specs.T_melt_max), ' K )\n'];
        elseif (~strcmp(specs.T_melt_max, 'no_T_max') && (~molStruct.T_melt_ok))
                comments{length(comments)+1} = ['T_melt is too high (',num2str(molStruct.T_melt), ' K > ' num2str(specs.T_melt_max), ' K )\n'];
        end
        comments{length(comments)+1} = '\n';
    end

        % Constraints on LLE
    if ~molStruct.LLEok
        comments{length(comments)+1} = ['\nNo LLE with water! Solvent not suitable. \n\n'];
    end

    
    %% 
    if (~already_scored)
        % Mextraction and RBM routine
        if (strcmp(specs.Objfunc, 'Mex-RBM') && molStruct.ConCheck)
                molStruct = ScoreWithMexRBM_HI(molStruct, specs, comments);

        % logP-Score
        elseif (strcmp(specs.Objfunc, 'logP') && molStruct.ConCheck)
                molStruct = calculatelogPScore(molStruct, specs);

        % Gmehling-Score
        elseif (strcmp(specs.Objfunc, 'Gmehling') && molStruct.ConCheck)
                molStruct = calculateGmehlingScore( molStruct, specs);
                
        % Co-Process-Score
        elseif (strcmp(specs.Objfunc, 'CO-Process') && molStruct.ConCheck)
                molStruct = ScoreCOProcessLEA( molStruct, specs, comments);
        
        end      
        
    end

%% Print all results and comments to out2LEA-File

    [molStruct, comments] = printComments( molStruct, comments, specs);

    output_LEA = fopen([specs.Gate{4}, '/out_LEA_', specs.Number, '_done.txt'],'wt');

    % Score
    formatSpec = '%10.6f';
    fprintf(output_LEA, formatSpec, molStruct.Score);
    fprintf(output_LEA, '\n');

    formatSpec = '%1.0f';
    fprintf(output_LEA, formatSpec, molStruct.ConCheck);
    fprintf(output_LEA, '\n');

    % Output further comments and intermediate results of scoring
    for i=1:length(comments)
        fprintf(output_LEA, comments{i});
    end
    
    fclose(output_LEA);
    
    %% Save molStruct
    if ~already_scored
        save([gate{3}, '/LEA_', noofcalc], 'molStruct');
    end
       
catch
	fprintf ('\n\nWarning! Error in MATLAB during process model evaluation!\n\n');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Mode (II): Collecting results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif strcmp(job, 'Sum_molStructs')
% Add up all molStructs of individually scored molecules
    % Gets molStructs from Gate2LEA and combines them to one cumulative
    % molStruct containing all molecules of LEA optimization run

    fprintf ('\n\nCollecting molStructs...\n\n');
    
    maindir = pwd;
    
    if precalc
        nameofstruct = 'molStruct_precalculation.mat';
    else
        nameofstruct = 'molStruct_of_all_molecules.mat';
    end
    
    % Pick up old molStruct if existing
    if exist(nameofstruct, 'file')
        load(nameofstruct);
        molStruct_exists = 1;
    else molStruct_exists = 0;
    end

    % Move to gate
    chdir(gate{3});

    % Get gate content
    content = dir(pwd); % you get everything in current directly of matlab.

    % add all structs from gate to cumulative molStruct
    for i=1:length(content)
        ismatfile = strfind(content(i).name, '.mat');
        if ~isempty(ismatfile)
            temp_struct = load(content(i).name);
            name_of_loaded_struct = fieldnames(temp_struct);
            if molStruct_exists
                molStruct_all = [molStruct_all,  temp_struct.(name_of_loaded_struct{1})];
            else
                molStruct_all = temp_struct.(name_of_loaded_struct{1});
                molStruct_exists = 1;
            end
        end
    end

    % % Clean gate directory
    system('rm -rf *.mat');
    
    % Rank all
    [~,index1] = sortrows([molStruct_all.Score].'); 
    molStruct_all = molStruct_all(index1(end:-1:1)); 
    clear index1

	%% Clean cumulative molStruct from multiple entries of one molecule
    % Step I: Determine number of molecules to delete
    restart = 1;
    while restart
    	current_mols = {molStruct_all.name};
    	index = cell(length(current_mols),1);
    	for k = 1:length(current_mols)
		index{k} = find(strcmp(current_mols, current_mols{k}));
    	end
    
    	% Step II: Delete molecules
    	for k = 1:length(index)
        	if length(index{k}) > 1
                for i=0:length(index{k})-2
	            		molStruct_all(index{k}(length(index{k})-i)) = [];       % delete all double entries	
                end
                break;
        	end
        end 
        if k==length(index)
            restart = 0;
        end
    end
        
    %% Save cumulative molStruct in workspace folder
    chdir(maindir);
    
    save(nameofstruct, 'molStruct_all')
    
    %% Output2LEA Handling
    
    % Move to gate
    chdir(gate{4});
    
    % Get gate content
    content = dir(pwd); % you get everything in current directly of matlab.

    % Get highest LEA-Number
    numbers = zeros(length(content),1);
    for i=1:length(numbers)
        line = strrep(content(i).name, '_', ' ');
        line = textscan(line, '%s %s %f %s');
        if ~isempty(line{3})
            numbers(i) = line{3};
        end
    end
    numsorted = sortrows(numbers, 'descend');
    LEAmax = numsorted(1);
    
    for i=0:LEAmax
        output2leas = find(contains({content(:).name}, ['out_LEA_',num2str(i),'_']));
        if ~isempty(output2leas)
            NoandScore = cell(length(output2leas),2);
            
            for k=1:length(output2leas)
                NoandScore{k,1} = content(output2leas(k)).name;
                % Read Score of file:
                output_LEA = fopen([specs.Gate{4}, '/', content(output2leas(k)).name],'rt');
                currentscore = fgetl(output_LEA);
                formatSpec = '%f';
                currentscore = textscan(currentscore,formatSpec); % Read line
                NoandScore{k,2}  = currentscore{1};
                fclose(output_LEA);
            end
            
            [~,indexout] = sortrows([NoandScore{:,2}].', 'descend');
            topout = NoandScore{indexout(1),1};
            targetname = ['out_LEA_',num2str(i),'.txt'];
            movefile(topout, targetname);
            
            for k=2:length(output2leas)
                delete(NoandScore{indexout(k),1});
            end
        end
    end
    
    chdir(maindir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Mode (III): Write specifications (specs-struct)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
elseif strcmp(job, 'Write_specs')
    % Open a text-file
    specs_out = fopen('specifications_of_current_run', 'wt');
    
    % Print specifications into a file for LEA
    fprintf(specs_out, ['Objective Function: ', objfunc, '\n\n']);
     
    % Output system used
    if length(components) == 3
        fprintf(specs_out, ['Ternary system: \n']);
    elseif length(components) == 4
        fprintf(specs_out, ['Quaternary system: \n']);
    elseif length(components) == 5
        fprintf(specs_out, ['Quinary system: ']);
    elseif length(components) == 6
        fprintf(specs_out, ['Senary system: \n']);
    elseif length(components) == 7
        fprintf(specs_out, ['Septenary system: \n']);
    elseif length(components) == 8
        fprintf(specs_out, ['Octonary system: \n']);
    elseif length(components) == 9
        fprintf(specs_out, ['Nonary system: \n']);
    else fprintf(specs_out, ['System: \n']);
    end
    for s=1:length(components)
        fprintf(specs_out, ['\t\t\t\t -', components{s}, '\n']);
    end
    
    if strcmp(objfunc, 'Mex-RBM')
        % NRTL-Calculation options:
        fprintf(specs_out, ['\nNRTL-parameter calculation options:\n']);
        fprintf(specs_out, ['\t\t\t\t - T_start = ', num2str(T_start), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_end = ', num2str(T_end), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_interval = ', num2str(T_interval), '°C\n\n']);
       
    elseif strcmp(objfunc, 'logP') || strcmp(objfunc, 'Gmehling')
        fprintf(specs_out, ['\nParameters for LLE- and gamma-calculation:\n']);
        fprintf(specs_out, ['\t\t\t\t - Tc = ', num2str(T), '°C \n']);

    elseif strcmp(objfunc, 'Exergy-Loss')
        % NRTL-Calculation options:
        fprintf(specs_out, ['\nNRTL-parameter calculation options:\n']);
        fprintf(specs_out, ['\t\t\t\t - T_start = ', num2str(T_start), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_end = ', num2str(T_end), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_interval = ', num2str(T_interval), '°C\n\n']);

        % Storage molecule
        fprintf(specs_out, ['CO-Process storage molecule:\n']);
        fprintf(specs_out, ['\t\t\t\t - storage  = ', storage, '\n\n']);
        
    elseif strcmp(objfunc, 'kinetics_liquid')
        fprintf(specs_out, ['\nTemperature for reaction rates calculation:\n']);
        fprintf(specs_out, ['\t\t\t\t - T = ', num2str(T_reaction_value), ' K \n']);
    end
    
    % Constraints on T_boil
    if ~strcmp(T_boil_min, 'no_T_min') || ~strcmp(T_boil_max, 'no_T_max')
        fprintf(specs_out, ['\nConstraint on T_boiling:\n']);
    end
    if ~strcmp(T_boil_min, 'no_T_min')
        fprintf(specs_out, ['\t\t\t\t - T_boil_min = ', num2str(T_boil_min), '\n']);
    end
    if ~strcmp(T_boil_max, 'no_T_max')
        fprintf(specs_out, ['\t\t\t\t - T_boil_max = ', num2str(T_boil_max), '\n']);
    end
    
    if ~strcmp(T_melt_min, 'no_T_min') || ~strcmp(T_melt_max, 'no_T_max')
        fprintf(specs_out, ['\nConstraint on T_melting:\n']);
    end
    if ~strcmp(T_melt_min, 'no_T_min')
        fprintf(specs_out, ['\t\t\t\t - T_melt_min = ', num2str(T_melt_min), '\n']);
    end
    if ~strcmp(T_melt_max, 'no_T_max')
        fprintf(specs_out, ['\t\t\t\t - T_melt_max = ', num2str(T_melt_max), '\n']);
    end
    
    fprintf(specs_out, ['\n']);
    
    fclose(specs_out);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% FUNCTION APPENDIX %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function TurnOffParPool
%% Turning off parpool autocreate function of Matlab
% Required for parallelisation done by LEA3D

    parallelsettings = fopen([prefdir, '/parallel.settings'], 'r');
    line = fgetl(parallelsettings);
    i = 1;
    while ~numel(strfind(line,'<key name="AutoCreate">')) && i < 1000 
           line = fgetl(parallelsettings); % MATLAB: moves one line (= get next line)
           i = i+1; % Prevent eternal loop
           targetline = i+2;
    end    
    fgetl(parallelsettings); %Ignore this one, just necesssary to get the right line
    line = fgetl(parallelsettings); 
    defaultline = '                </bool>';

    % If AutoCreate option is on default, add value = 0 line
    if strcmp(line, defaultline)
        desiredline = '                    <value>0<\/value>';
        defaultline = strrep(defaultline, '/', '\/');
        system(['sed -i ''', num2str(targetline), 's/.*/', desiredline,'\n', defaultline, '/'' ', prefdir, '/parallel.settings']);
    % If Autocreate option is set and not value = 0    
    else
        newline = strrep(line, '1', '0');
        newline = strrep(newline, '/', '\/');
        if ~strcmp(newline, line)
            system(['sed -i ''', num2str(targetline), 's/.*/', newline, '/'' ', prefdir, '/parallel.settings']);
        end
    end
    
    fclose(parallelsettings);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = calculateGmehlingScore(molStruct, specs)
% Score using the scoring function by Gmehling and Schedemann (2014)
% Required properties are Liquid-Liquid-equilibrium and acitivity coefficients

%% Property prediction and process model procedure 
    molStruct = main_cosmoData(molStruct,'Gmehling',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    % Calculate Score
    addpath([specs.maindir,'/ProcessModel/Gmehling']);        
    molStruct = GmehlingScore(molStruct, specs);
        
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = calculatelogPScore(molStruct, specs)
% Score using the mass-based logP-Score
% Required properties are Liquid-Liquid-equilibrium, acitivity coefficients
% and molecular weight

%% Property prediction and process model procedure 
    molStruct = main_cosmoData(molStruct,'logP',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    % Calculate Score
    addpath([specs.maindir,'/ProcessModel/logP']);	
    molStruct = logPScore(molStruct, specs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = ScoreWithMexRBM_HI(molStruct, specs, comments)
% Score using the Mextraction and RBM Shortcut models
% Required properties are NRTL-parameter and Antoine coefficients

%% Property prediction and process model procedure 
    % Get NRTL Para
    molStruct = job_Antoine(molStruct,specs.compounds,specs);
    molStruct = job_NRTL(molStruct,specs.compounds,specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    % Call MexRBM routine to evaluate and score
    addpath([specs.maindir,'/ProcessModel/ShortcutModel'])

    % Fixed temperature Tc_Extr
  	molStruct = ScoreMexRBM_tempopt_HI_Tdec( molStruct, specs, comments );
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = ScoreCOProcessLEA(molStruct, specs, comments)
% Score Exergy-Loss Superstructure
% Required properties are NRTL-parameter and Antoine coefficients

%% Property prediction and process model procedure 
addpath([specs.maindir,'/PropertyPrediction/']);

molStruct = job_Antoine(molStruct,specs.compounds,specs);
molStruct = job_NRTL(molStruct,specs.compounds,specs);
molStruct = job_Henry(molStruct,specs.compounds,specs);

rmpath([specs.maindir,'/PropertyPrediction/']);

%% Call optimizeProcess
addpath([specs.maindir,'/ProcessModel/ShortcutModel/']);
molStruct = ScoreCOProcess( molStruct, specs, comments );
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/']);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct, comments] = printComments(molStruct, comments, specs)
%% Print results and set Score for LEA3D
% Print comments which are displayed in LEA to understand the results of
% property prediction and process model evaluation

%% Mextraction and RBM
if (strcmp(specs.Objfunc, 'Mex-RBM') && molStruct.ConCheck)
    comments{length(comments)+1} = '--------------------------------\nMextraction Results:\n';
    if isnan(molStruct.RBMresults.Qreb)
        molStruct.Score = 0;
        if ~isnan(molStruct.ExtrResults.Smin)   
            comments{length(comments)+1} = ['S_min  =        ', num2str(molStruct.ExtrResults.Smin),'\n'];
            comments{length(comments)+1} = ['T_Extr =        ', num2str(molStruct.ExtrResults.T_Extr_opt),'\n'];  
            comments{length(comments)+1} = '--------------------------------------------------\nResults of RBM:\n';
            comments{length(comments)+1} = ['No solution of RBM shortcut for this solvent!\n'];
        elseif molStruct.Topology.ok == 0
            comments{length(comments)+1} = ['Topology of solvent not suitable.\n'];
        else
            comments{length(comments)+1} = ['No solution of Mextraction shortcut for this solvent.\n'];
        end

    else
        molStruct.Score = 1/molStruct.Exergydemand;
        comments{length(comments)+1} = ['S_min  =        ', num2str(molStruct.ExtrResults.Smin),'\n'];
        comments{length(comments)+1} = ['T_Extr =        ', num2str(molStruct.ExtrResults.T_Extr_opt),'\n'];      
        comments{length(comments)+1} = '--------------------------------------------------\nResults of RBM:\n';
        comments{length(comments)+1} = ['Q_reb =        ', num2str(molStruct.RBMresults.Qreb/1000),' kJ/mol\n'];
        comments{length(comments)+1} = ['p_RBM_opt =        ', num2str(molStruct.RBMresults.p_RBM_opt),' bar\n\n'];
        comments{length(comments)+1} = '--------------------------------------------------\nExergy demand after HI:\n';
        comments{length(comments)+1} = ['E_total =        ', num2str(molStruct.Exergydemand),' kJ/mol\n\n'];
    end

%% logP- and Gmehling--Score
elseif ((strcmp(specs.Objfunc, 'logP') || strcmp(specs.Objfunc, 'Gmehling')) && molStruct.ConCheck)
    if (isnan(molStruct.x_LLE))
        molStruct.Score = 0;
        comments{length(comments)+1} = '--------------------------------------------------\nNo LLE found!\n\n';
    else
        comments{length(comments)+1} = '--------------------------------------------------\nLLE:\n';
        if molStruct.x_LLE(1) >= molStruct.x_LLE(3)
            comments{length(comments)+1} = ['x_LLE_aq  =        ', num2str(molStruct.x_LLE(1:2)),'\n'];
            comments{length(comments)+1} = ['x_LLE_org =        ', num2str(molStruct.x_LLE(3:4)),'\n\n'];
            comments{length(comments)+1} = '--------------------------------------------------\nActivity coefficients:\n';
            comments{length(comments)+1} = ['gamma_aq  =        ', num2str(molStruct.gammaI),'\n'];
            comments{length(comments)+1} = ['gamma_org =        ', num2str(molStruct.gammaII),'\n\n'];
        else
            comments{length(comments)+1} = ['x_LLE_org  =        ', num2str(molStruct.x_LLE(1:2)),'\n'];
            comments{length(comments)+1} = ['x_LLE_aq   =        ', num2str(molStruct.x_LLE(3:4)),'\n\n'];
            comments{length(comments)+1} = '--------------------------------------------------\nActivity coefficients:\n';
            comments{length(comments)+1} = ['gamma_org =        ', num2str(molStruct.gammaI),'\n'];
            comments{length(comments)+1} = ['gamma_aq  =        ', num2str(molStruct.gammaII),'\n\n'];
        end
       
        % logP-Score
        if (strcmp(specs.Objfunc, 'logP'))
            molStruct.Score = molStruct.logP; 
            comments{length(comments)+1} = '--------------------------------------------------\nlogP-Score:\n';
            comments{length(comments)+1} = ['logP =             ', num2str(molStruct.logP),'\n'];

        % Gmehling-Score
        elseif (strcmp(specs.Objfunc, 'Gmehling'))
            molStruct.Score = molStruct.GmehlingScore;
            comments{length(comments)+1} = '--------------------------------------------------\nFactors are:\n';
            comments{length(comments)+1} = ['Score_ws  =   ', num2str(molStruct.Score_ws),'\n'];
            comments{length(comments)+1} = ['Score_wc  =   ', num2str(molStruct.Score_wc),'\n'];
            comments{length(comments)+1} = ['Score_wsl =   ', num2str(molStruct.Score_wsl),'\n\n'];
            comments{length(comments)+1} = '--------------------------------------------------\nGmehling-Score:\n';
            comments{length(comments)+1} = ['Gmehling_Score =   ', num2str(molStruct.GmehlingScore),'\n'];    
        end
    end

%% CO-Process
elseif (strcmp(specs.Objfunc, 'CO-Process') && molStruct.ConCheck)
    if ~(isnan(molStruct.Exergydemand) || molStruct.Exergydemand==1e20)
        comments{length(comments)+1} = '--------------------------------------------------\nMinimum Exergy Loss:\n';
        comments{length(comments)+1} = ['Exergy Loss =   ', num2str(molStruct.Exergydemand/1000),' kJ/mol\n\n'];
        comments{length(comments)+1} = ['optimal process conditions =   ', num2str(molStruct.opt_vars),'\n\n'];
    else
        molStruct.Score = 0;
        comments{length(comments)+1} = '--------------------------------------------------\nOptimization did not succeed: Score = 0\n';
    end

%% Constraints not fulfilled    
elseif (~molStruct.ConCheck)
    molStruct.Score = 0;
    comments{length(comments)+1} = '--------------------------------------------------\nConstraints are not satisfied! Scoring terminated!\n\n';
    if (strcmp(specs.Objfunc, 'Gmehling'))
        molStruct.x_LLE  = NaN;
        molStruct.gammaI = NaN;
        molStruct.gammaII= NaN;
        molStruct.Score_ws = NaN;
        molStruct.Score_wc = NaN;
        molStruct.Score_wsl = NaN;
        molStruct.GmehlingScore = NaN;
    elseif (strcmp(specs.Objfunc, 'logP'))
        molStruct.x_LLE  = NaN;
        molStruct.gammaI = NaN;
        molStruct.gammaII= NaN;
        molStruct.MW_solvent = NaN;
        molStruct.logP = NaN;
    elseif (strcmp(specs.Objfunc, 'Mex-RBM'))
        molStruct.AntoineParam = NaN;
        molStruct.dhvap = NaN;
        molStruct.Tcrit = NaN;
        molStruct.pcrit = NaN;
        molStruct.dhvap_para = NaN;
        molStruct.TauParam = NaN;
        molStruct.AlphaParam = NaN;
        molStruct.err_NRTL = NaN;
        molStruct.cp_gas_co = NaN;
        molStruct.cp_gas = NaN;
        molStruct.cp_liq_co = NaN;
        molStruct.cp_liq = NaN;
        molStruct.ExtrResults = NaN;
        molStruct.RBMresults = NaN;
        molStruct.Topology = NaN;
        molStruct.opt_vars = NaN;
        molStruct.HeatX = NaN;
        molStruct.Exergydemand = NaN;
    elseif (strcmp(specs.Objfunc, 'CO-Process'))          
		molStruct.AntoineParam = NaN;
        molStruct.dhvap = NaN;
        molStruct.Tcrit = NaN;
        molStruct.pcrit = NaN;
        molStruct.dhvap_para = NaN;
        molStruct.TauParam = NaN;
        molStruct.AlphaParam = NaN;
        molStruct.err_NRTL = NaN;
        molStruct.cp_gas_co = NaN;
        molStruct.cp_gas = NaN;
        molStruct.cp_liq_co = NaN;
        molStruct.cp_liq = NaN;
        molStruct.HenryCoeff_Data = NaN;
        molStruct.HenryCoeff = NaN;
        molStruct.Topology = [];
        molStruct.AbsResults = [];
        molStruct.VLLEReacResults = [];
        molStruct.StoichReacResults = [];
        molStruct.Dist1Results = [];
        molStruct.Dist2Results = [];
        molStruct.HeatX = [];
        molStruct.P_comp = [];
        molStruct.Exergydemand = [];
        molStruct.opt_vars = [];
    end
    
end

comments{length(comments)+1} = '__________________________________________________\n\n\n';

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
