#!/bin/csh

##########################################################
#
# Environment definitions for LEA 2.0
#
##########################################################

if(! -e $LEA3D/MAIN ) then
	        echo 'Check environment variable(setenv LEA3D)'
else

	if( $#argv == 3) then
		
        cp -f $LEA3D/XSCORE//RESIDUE .
        cp -f $LEA3D/XSCORE/ATOMTYPE .

	set log=$3
	set log2="bis$3"
	$LEA3D/XSCORE/score $1 $2 $log > & $log2 
	set score=`tail -1 $log | awk '{print $2}'` 
	rm -f RESIDUE
        rm -f ATOMTYPE
	cat $log2 $log > a
	mv -f a $log
	rm -f $log2
	
	#error2 can be due to Hydrogens
	set error=`cat $log | grep 'cannot open the MOL2 file'`
	set error2=`cat $log | grep 'Warning' | wc -l`
	if( "$error" != "" ) then
		echo 0.00 "see error in $log"
	else if ( "$error2" != "" ) then
		echo $score "see warning in $log"
	else
		echo $score
	endif	
	
	#if("$error" == "" && "$error2" == "") echo $score
	#if("$error" != "") echo 0.00 "see error in $log"        	
	#if("$error" == "" && "$error2" != "") echo $score "see warning in $log"

	else
		echo 'usage: xscore <protein.pdb>  <mono mol2 file> <log file>'
	endif
	
endif

