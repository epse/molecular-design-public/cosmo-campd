#!/usr/bin/perl

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

	$ref='';
	$test='';
	my($sim,$scale,$ref,$test)=@ARGV;
	if ($sim eq '' || $scale eq '' || $ref eq '' || $test eq ''){
	        die "usage: simgc  <tanimoto or tversky (embbeding, on bits only) or carhart (on bits or occ) or euclidian> <scale type: bit (no scale) or occ (number of occurences) or charge (partial charges)> <sdf or mol2 file of the reference> <sdf or mol2 file of the test>\nDEFINITIONS in literature may differ\n";
	}


#INPUT molecule in sdf
	
	$flagrm=0;
	if($ref=~/\.sdf/){
		$mol2ref=$ref;
		$mol2ref=~s/\.sdf/_1\.mol2/;
		if(!-e $mol2ref){
			chop($tmpmol2ref=`$leaexe/SDF_MOL2.pl $ref`);
			#$flagrm=1;
		};	
	}
	else{
		$mol2ref=$ref;
	};
	if($scale eq "charge"){
		chop($mol2refchg=`$leaexe/charge.pl $mol2ref gas`);
		#$mol2ref="charge".$mol2ref;
		rename "charge$mol2ref","$mol2ref";
	};
	
	$flagrmtest=0;
	if($test=~/\.sdf/){
		chop($mol2test=`$leaexe/SDF_MOL2.pl $test`);
		$mol2test=$test;
		$mol2test=~s/\.sdf/_1\.mol2/;
		
		# remove other compounds/conformers from the sdf
		$generic=$test;
		$generic=~s/\.sdf$//;
		$generic=$generic."_";
		
		$flagrmtest=1;
	}
	else{
		$mol2test=$test;
	};	
	if($scale eq "charge"){
		chop($mol2testchg=`$leaexe/charge.pl $mol2test gas`);
		#$mol2test="charge".$mol2test;
		rename "charge$mol2test","$mol2test";
	};
	
	chop($fingerref=`$leaexe/fingerprint_GC.pl $scale $mol2ref`);
	chop($fingertest=`$leaexe/fingerprint_GC.pl $scale $mol2test`);

#print "$mol2ref=$fingerref\n$mol2test=$fingertest\n";


	@fingerref=split(' ',$fingerref);
	@fingertest=split(' ',$fingertest);

###################################
#DIFFERENCES

#RQ: CG fingerprints contents
#C 0-43
#H 45-54
#O 55-60
#N 65-77
#F 79-84
#Cl 85-89
#Br 90-94
#I 95-99
#S 105-109

	@cgatom='';
	@difference="";
	$hetatm="";
	foreach $i (0..@fingerref-1){
		$cgatom[$i]="-";
		$difference[$i]="0";
		if($fingerref[$i]>0 && $fingertest[$i]==0){
			$difference[$i]=$fingerref[$i]-$fingertest[$i];
			if($i >=0 && $i <= 43){
				$cgatom[$i]="C";
			}	
			elsif($i >=55 && $i <= 60){
				$hetatm=$hetatm." O";
				$cgatom[$i]="O";
			}
			elsif($i >=65 && $i <=77){
				$hetatm=$hetatm." N";
				$cgatom[$i]="N";
			}
			elsif($i >=79 && $i <=84){
				$hetatm=$hetatm." F";
				$cgatom[$i]="F";
			}
			elsif($i >=85 && $i <=89){
				$hetatm=$hetatm." Cl";
				$cgatom[$i]="Cl";
			}
			elsif($i >=90 && $i <=94){
				$hetatm=$hetatm." Br";
				$cgatom[$i]="Br";
			}
			elsif($i >=95 && $i <=99){
				$hetatm=$hetatm." I";
				$cgatom[$i]="I";
			}
			elsif($i >=105 && $i <=109){
				$hetatm=$hetatm." S";
				$cgatom[$i]="S";
			}
			elsif($i >=45 && $i <=54){
				$cgatom[$i]="H";
			};	
		};
	};
	#print "difference:\n@cgatom\n@difference\n";
	#print "Miss $hetatm\n" if($hetatm ne "");



###################################
#SIMILARITIES
#


	#a is the count of bits on in object A but not in object B
	#b is the count of bits on in object B but not in object A.
	#c is the count of the bits on in both object A and object B.
	#d is the count of the bits off in both object A and object B.
	
	 
	$a=0;
	$b=0;
	$c=0;
	$d=0;
	$biga=0;
	$bigb=0;


	#Carhart: can be applied to positive reals or integers 
	#but less on bits (test it with bit ref=0 and bit test=0)
	#
	$car=0;
	$carref=0;
	$cartest=0;

	$rmsd=0;
	$prod=0;
	$proda=0;
	$prodb=0;

	foreach $i (0..@fingerref-1){

		#carhart
		if($fingerref[$i] < $fingertest[$i]){
			$car=$car+$fingerref[$i];
		}
		else{
			$car=$car+$fingertest[$i];
		};
		$carref=$carref+$fingerref[$i];
		$cartest=$cartest+$fingertest[$i];

		#euclidian #not normalized
		$rmsd=$rmsd+($fingerref[$i]-$fingertest[$i])*($fingerref[$i]-$fingertest[$i]);
		
		#Tanimoto on reals
		$prod=$prod+$fingerref[$i]*$fingertest[$i];
		$proda=$proda+($fingerref[$i]*$fingerref[$i]);
		$prodb=$prodb+($fingertest[$i]*$fingertest[$i]);
		
		# on bits:
		if($fingerref[$i]==0 && $fingertest[$i]==0){
			$d++;
		}
		elsif($fingerref[$i]!=0 && $fingertest[$i]!=0){
				$c++;
		}
		elsif($fingerref[$i]==0 && $fingertest[$i]!=0){
				$b++;
		}
		elsif($fingerref[$i]!=0 && $fingertest[$i]==0){
				$a++;
		};	
		#print "$i $fingerref[$i]\n";
	};

	$biga=$a+$c;
	$bigb=$b+$c;


#########$rmsd
	 	# asymetric if using alpha and beta as in twersky
		# weighted if using a specif weight for each cell
		 
		# euclidian as the sum of differences 
		$e=sqrt($rmsd); #from willett, JChemInf, 1998, 38, 983-996

#########Carhart
		$carhart=$car/(0.5*($carref+$cartest)) if(($carref+$cartest) != 0);

#########Tanimoto on reals from willett, JChemInf, 1998, 38, 983-996
		$t=$prod/($proda+$prodb-$prod);
		#print "tanimoto real $t\n"; 



	if($scale eq "bit"){

################# Euclidian is simply a matching coefficient (Daylight)
		#$n=@fingerref;
        	#print "n=$n is the total number of bits on or off in objects A or B\n";
		#$e=($c+$d)/$n; # be careful on the definition of $n
		
		#$n=$a+$b-$c+$d; # from willett, JChemInf, 1998, 38, 983-996
		#$e=sqrt($a+$b-2*$c); # from willett, JChemInf, 1998, 38, 983-996
		
		# keep $e from reals
		

################# Tanimoto (or Jaccard coefficient) may be regarded as the proportion of the "on-bits" which are shared
		#$t=$c/($a+$b+$c); 
		#print "tanimoto daylight $t\n";
		#$t=$c/($a+$b-$c); # from willett, JChemInf, 1998, 38, 983-996

		# keep $t from real


#Over the years there has been much discussion as to which type of coefficient to use. In chemistry it has generally been thought that, as most descriptor features are absent in most molecules, i.e. the bit string descriptors such as the Daylight fingerprint contains mainly zeros, coefficients such as the Tanimoto are more appropriate. Further, given that the size of a Daylight fingerprint can be arbitrarily doubled, thereby adding mainly random off bits, any measure using matching off-bits, d, would be inappropriate. However this may not be the case for fixed width key based fingerprints. Daylight therefore offers access to both types of measure. The user must ensure that an appropriate one is chosen. 


#################Tversky (alpha and beta from 0 to 1)
		$alpha=0.1; # here, tested molecule is embeded in reference
		$beta=0.9;
		$tv=$c/($alpha*$a + $beta*$b +$c);
		#$tv=$c/($alpha*($a-$c) + $beta*($b-$c) +$c); # from willett, JChemInf, 1998, 38, 983-996

#As of Release 4.51 the Tversky index (Tversky, A. Psychological Reviews (1977)84 (4) 327-352), provides a more powerful method for structural similarity searching. Its use and interpretation, however, are not not as simple as the Tanimoto index. The Tversky comparison is intrinsically asymmetric. As with Tanimoto the features present in two objects are compared. In the Tversky approach we have the concept of a "prototype" to which the objects or "variants" are compared. Note this differs from the Tanimoto index in which the similarity between two objects is estimated. This inherent asymmetry means that the Tversky index is very definitely not a metric. 
#Setting the weighting of prototype features to the same value does not use the power of this index. Indeed, setting . = .= 1, produces the Tanimoto index. If . = .= 0.5 we get the Dice index.
#The value of the index comes from setting the weighting of prototype and variant features asymmetrically, producing a similarity measure in a more-substructural or more-superstructural sense or reflecting the increased knowledge the user has about the prototype. Quite often one is looking for compounds like a known compound with appropriate properties. 

#	alpha=1 and beta=0 (ref(prototype) embedded in test) means that only the prototype features are important;this produces a "superstucture-likeness" measure. In this case, a Tversky similarity value of 1.0 means that all prototype features are represented in the variant, 0.0 that none are.

#	alpha=0 and beta=1 (test embedded in ref : embedded structures have a 1.0 value and "near-substructures" have values near 1.0.)

#Note: with no weight at all given to variant features, this measure is pretty sensitive to "noise" in Daylight fingerprints and settings of 90%/10% generally produce a more useful ranking.


	};


	if($sim=~/animoto/){
		print "Tanimoto= $t\n";
	}
	elsif($sim=~/uclidian/){
		print "Euclidian= $e\n";
	}
	elsif($sim=~/versky/){
		print "Tversky= $tv (embedding of the test molecule)\n";
	}
	elsif($sim=~/arhart/){
		print "Carhart= $carhart\n";
	}	
	else{
		print "Error the similarity method has not been recognized !\n";
	};

	system("rm -f $generic*.mol2") if($flagrmtest=1);
	#system("rm -f $mol2test") if($flagrmtest=1);
	#system("rm -f $mol2ref")  if($flagrm=1);

####################################################################

	print "difference:\n@cgatom\n@difference\n";

