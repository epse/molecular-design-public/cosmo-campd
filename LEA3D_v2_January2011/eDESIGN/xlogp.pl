#!/usr/bin/perl


	
	local($mol2)= @ARGV;

	if($mol2 eq ''){
		die "usage: xlogp <file.mol2 or .sdf>\n";
	};

	$leaexe=$ENV{LEA3D};
	die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");
	
	$convertorfrog=$ENV{LEA_FROG};
	$convertorcorina=$ENV{LEA_CORINA};
	$pathconvertor=$ENV{LEA_CONVERTOR};

	#convert in order to neutralize charges
	unlink "inxlogp.sdf";	
	
	if($pathconvertor eq $convertorfrog){
		if($mol2=~/\.mol2$/){
			system("$leaexe/MOL2_SDF.pl $mol2");
			$name=$mol2;
			$name=~s/\.mol2$/\.sdf/;
			system("$leaexe/frog $name sdf 1");
			$name3d=$name;
			$name3d=~s/\.sdf$/3D\.sdf/;
			rename "$name3d", "inxlogp.sdf";
		}
		else{
			system("$leaexe/frog $mol2 sdf 1");	
			$name3d=$mol2;
			$name3d=~s/\.sdf$/3D\.sdf/;
			print "$name3d\n";
			rename "$name3d", "inxlogp.sdf";
		};	
	}
	else{

		if($mol2=~/\.mol2$/){
			### translate mol2 in sdf, add H
			system("$pathconvertor -i t=mol2 -o t=sdf -d neu,wh  $mol2 inxlogp.sdf");
		}
		else{
			system("$pathconvertor -i t=sdf -o t=sdf -d neu,wh  $mol2 inxlogp.sdf");
		};	
	};


	## translate sdf in mol2 with XLOGP specifically atom type (five atom rings)
	system("$leaexe/XLOGP_SDF_MOL2.pl inxlogp.sdf");

	## Execute XLOGP
	system("$leaexe/XLOGP/xlogp2.1/xlogp/xlogp inxlogp_1.mol2");

	unlink "inxlogp_1.mol2";
	unlink "inxlogp.sdf";
	unlink "xlogp.mol2";
        #unlink "xlogp.log";

