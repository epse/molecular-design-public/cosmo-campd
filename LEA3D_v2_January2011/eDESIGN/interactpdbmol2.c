
/* gcc -o interactpdbmol2 interactpdbmol2.c -lm */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>


/* -------------------- */
/* CONSTANTES		*/
/* -------------------- */

#define LINE_LENGTH	80
#define LINE_L 2 /* 2 + 1 */
#define RES_LENGTH 4 /* 3 + 1 */
#define ATOM_LENGTH 5 /* 4 + 1 */
#define NBATOM	10000

/*----------------------------------------------------------------------*/
/* variables                                                            */
/*----------------------------------------------------------------------*/

  static FILE		*fp = NULL,
  			*fpmol2 = NULL;

  static char		line[LINE_LENGTH],
			chain[NBATOM][LINE_L],
			atoma[NBATOM][2],
			atomb[NBATOM][3],
			buffer[LINE_LENGTH],
			residua[NBATOM][RES_LENGTH],
			atom_namea[NBATOM][ATOM_LENGTH],
			atom_nameb[NBATOM][6],
			atom1[2],
			atom2[2];


  static float		x,dist,distmin,distmax,
  			y,
			z,
  			xa[NBATOM],
  			ya[NBATOM],
			za[NBATOM],
			xb[NBATOM],
  			yb[NBATOM],
			zb[NBATOM];



  int 			atomia=0,
  			atomib=0,
			res_numa[NBATOM],
			i,l,flag=0,
			k,j;

/*--------------------------------------------------------------*/
/* main program							*/
/*--------------------------------------------------------------*/

int	main(int argc, char **argv)
{

	/*fprintf(stdout,"\n\t Interact %s %s %s \n\n",argv[1],argv[2],argv[3],argv[4]);*/

	distmin=0.0;
	sscanf(argv[3], "%f", &distmin);
	distmax=3.0;
	sscanf(argv[4], "%f", &distmax);

	if ((fp = fopen(argv[1], "r")) == NULL){
		fprintf(stdout,"usage: interaction 	<file.pdb> 	<solution.mol2> <minimal_distance> <maximal_distance>   \n Fichier '%s' illisible\n", argv[1]);
  	    	exit(1);
	}

	if ((fpmol2 = fopen(argv[2], "r")) == NULL){
		fprintf(stdout,"usage: interaction      <file.pdb>      <solution.mol2> <minimal_distance>  <maximal_distance>   \n Fichier '%s' illisible\n", argv[2]);
  	    	exit(1);
	}


	/* lecture du pdb */

	while (fgets(line,LINE_LENGTH,fp) != NULL) {

		/*fprintf(stdout,"%s", line);*/

		/*modif april 2007 if (!strncmp(line, "ATOM  ", 6)){*/
		
		if (!strncmp(line, "ATOM  ", 6) || !strncmp(line, "HETATM", 6)){

			strncpy(buffer, line+21, 1);
			buffer[1] = '\0';
			sscanf(buffer,"%s", &chain[atomia][0]);
			/*fprintf(stdout,"%s\n", chain);*/


			strncpy(buffer, line+30, 8);
			/*buffer[8] = '\000';*/
			/*fprintf(stdout,"%s\n", buffer);*/
			sscanf(buffer,"%f", &x);

			strncpy(buffer, line+38, 8);
			/*buffer[8] = '\000';*/
			/*fprintf(stdout,"%s\n", buffer);*/
			sscanf(buffer,"%f", &y);

			strncpy(buffer, line+46, 8);
			/*buffer[8] = '\000';*/
			/*fprintf(stdout,"%s\n", buffer);*/
			sscanf(buffer,"%f", &z);

			/*fprintf(stdout,"%d %f %f %f\n", atomia, x, y,z);*/


			xa[atomia]=x;
			ya[atomia]=y;
			za[atomia]=z;
				
			strncpy(buffer, line+22, 4);
			sscanf(buffer,"%d", &res_numa[atomia]);

			strncpy(buffer, line+12, 4);
			buffer[4] = '\0';
			sscanf(buffer,"%s", &atom_namea[atomia][0]);

			strncpy(buffer, atom_namea[atomia], 1);
			buffer[1] = '\0';
			sscanf(buffer,"%s", &atoma[atomia][0]);

			if(atoma[atomia][0] != 'C' && atoma[atomia][0] != 'N' && atoma[atomia][0] != 'O' && atoma[atomia][0] != 'S' && atoma[atomia][0] != 'H'){
				/*fprintf(stdout,"old %s\n", &atoma[atomia][0]);*/
				strncpy(buffer, atom_namea[atomia]+1, 1);
				buffer[1] = '\0';
				sscanf(buffer,"%s", &atoma[atomia][0]);
				/*fprintf(stdout,"new %s\n", &atoma[atomia][0]);*/
			}	


			strncpy(buffer, line+17, 3);
			buffer[3] = '\0';
			sscanf(buffer,"%s", &residua[atomia][0]);
			/*fprintf(stdout,"%s\n", residua[atomia]);*/

			if(atoma[atomia][0] != 'C' && atoma[atomia][0] != 'N' && atoma[atomia][0] != 'O' && atoma[atomia][0] != 'S' && atoma[atomia][0] != 'H'){
				
				fprintf(stdout,"Uncommon protein element %s %s %d %s %f %f %f %s\n", chain[atomia],residua[atomia],res_numa[atomia],atom_namea[atomia],xa[atomia],ya[atomia],za[atomia],atoma[atomia]);
			};

			/*fprintf(stdout,"%s %s %d %s %f %f %f %s\n", chain[atomia],residua[atomia],res_numa[atomia],atom_namea[atomia],xa[atomia],ya[atomia],za[atomia],atoma[atomia]);*/
			atomia++;
		}

 	}
	fclose(fp);


	/*fprintf(stdout,"nbatoma %d\n", atomia);*/


	/* lecture du ligand mol2 */

	while (fgets(line,LINE_LENGTH,fpmol2) != NULL) {

		/*fprintf(stdout,"%s", line);*/

		if (!strncmp(line, "@<TRIPOS>BOND", 13)){
			flag=0;
			/*fprintf(stdout,"%s", line);*/
		}

		if(flag == 1){

			strncpy(buffer, line+14, 11);
			/*buffer[8] = '\000';*/
			/*fprintf(stdout,"%s\n", buffer);*/
			sscanf(buffer,"%f", &x);

			strncpy(buffer, line+26, 11);
			/*buffer[8] = '\000';*/
			/*fprintf(stdout,"%s\n", buffer);*/
			sscanf(buffer,"%f", &y);

			strncpy(buffer, line+37, 11);
			/*buffer[8] = '\000';*/
			/*fprintf(stdout,"%s\n", buffer);*/
			sscanf(buffer,"%f", &z);

		/*	fprintf(stdout,"%f %f %f\n", x, y,z);*/

/* ATTENTION ICI SELON ECRITURE FORMAT 47, 49 !!! */

			strncpy(buffer, line+47, 5);
			buffer[5] = '\0';
			sscanf(buffer,"%s", &atom_nameb[atomib][0]);

  			strncpy(buffer, atom_nameb[atomib], 1);
			buffer[1] = '\0';
			/*fprintf(stdout,"%s", buffer);*/
			sscanf(buffer,"%s", &atom1[0]);

			strncpy(buffer, atom_nameb[atomib]+1, 1);
			buffer[1] = '\0';
			/*fprintf(stdout," %s\n", buffer);*/
			sscanf(buffer,"%s", &atom2[0]);

			/*fprintf(stdout,"atom %s %s\n", atom1,atom2);*/

			if(atom2[0] == '.'){
    				strncpy(buffer, atom_nameb[atomib], 1);
				buffer[1] = '\0';
				/*fprintf(stdout,"%s", buffer);*/
				sscanf(buffer,"%s", &atomb[atomib]);
			}
			else{
				strncpy(buffer, atom_nameb[atomib],2);
				buffer[2] = '\0';
				/*fprintf(stdout,"%s", buffer);*/
				sscanf(buffer,"%s", &atomb[atomib]);
			}
			/*fprintf(stdout,"atom %s type %s\n", atomb[atomib],atom_nameb[atomib]);*/

			xb[atomib]=x;
			yb[atomib]=y;
			zb[atomib]=z;

			/*fprintf(stdout,"%s %f %f %f %s\n",atom_nameb[atomib],xb[atomib],yb[atomib],zb[atomib],atomb[atomib]);*/

			atomib++;

		};

		if (!strncmp(line, "@<TRIPOS>ATOM", 13)){
			flag=1;
			/*fprintf(stdout,"%s", line);*/
		}

 	}
	fclose(fpmol2);

 
	for (i = 0; i < atomia; i++){

  		for (j = 0; j < atomib; j++){

			if(strncmp(atoma[i], "H", 1) && strncmp(atomb[j], "H", 1)){

				dist=sqrt((xa[i]-xb[j])*(xa[i]-xb[j])+(ya[i]-yb[j])*(ya[i]-yb[j])+(za[i]-zb[j])*(za[i]-zb[j]));

				/*
				if(res_numa[i] == 63){
						fprintf(stdout,"ligand atom no %d type LIG_ATOMTYPE_%s LIG_ATOM_%s x=%f y=%f z=%f interacts with (PROT_CHAIN_%s) PROT_RES_%s PROT_NORES_%d PROT_ATOMTYPE_%s PROT_ATOM_%s x=%f y=%f z=%f = %f\n",j+1,atom_nameb[j],atomb[j],xb[j],yb[j],zb[j],chain[i],residua[i],res_numa[i],atom_namea[i],atoma[i],xa[i],ya[i],za[i],dist);
				};*/
				
						
				if(dist < distmax && dist >= distmin ){
					
					fprintf(stdout,"ligand atom no %d type LIG_ATOMTYPE_%s LIG_ATOM_%s interacts with (PROT_CHAIN_%s) PROT_RES_%s PROT_NORES_%d PROT_ATOMTYPE_%s PROT_ATOM_%s = %f\n",j+1,atom_nameb[j],atomb[j],chain[i],residua[i],res_numa[i],atom_namea[i],atoma[i],dist);
				}
			};
		}
	}
}



