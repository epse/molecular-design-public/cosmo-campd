# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <time.h>
# define TRUE 1
# define FALSE 0

class Atom
        {
         public:
                int id;			// ID 
                int valid;              // valid indicator
                int num_neib;           // number of neighboring atoms
                int neib[6];            // ID of neighboring atoms
                char name[20];          // name of the atom
                char type[20];          // SYBYL atom type
                char xtype[20];         // XLOGP atom type
		float coor[3];		// coordinates
		int weight;		// atomic weight
                float logp;             // atomic hydrophobic scale 
                char hb[3];             // h-bond property
                int ring;               // ring indicator

                Atom();                 // constructor
                ~Atom();                // deconstructor
        };

class Bond
        {
         public:
                int id;			// ID
                int valid;              // valid indicator
                int atom_1;             // ID of the atom_1
                int atom_2;             // ID of the atom_2
                char type[3];           // bond type
                int ring;               // ring indicator
                int num_neib;           // number of neighboring bonds
                int neib[10];           // ID of neighboring bonds

                Bond();                 // constructor
                ~Bond();                // deconstructor
        };

class Group
        {
         public:
                int valid;
                int num_neib;           // number of neighboring atoms
                int num_nonh;           // number of neighboring non-h atoms
                int num_h;              // number of neighboring hydrogen atoms
                int num_hetero;         // number of neighboring heteroatoms
		int num_pi;		// number of neighboring pi atoms
		int num_nar;		// number of neighboring N.ar atoms
		int num_car;		// number of neighboring C.ar atoms
		int db_type;		// type of double bonding

                Atom center;            // center atom of the group
                Atom neib[6];           // neighboring atoms
                Bond bond[10];          // bonds

                Group();                // constructor
                ~Group();               // deconstructor
        };

class Molecule 
        {
         public:
                int valid;
                char name[80];
                int weight;
                char formula[40];
                float logp;

                int num_atom;
                int num_bond;

                Atom *atom;
                Bond *bond;

		struct Factor
			{
			 float num;
			 char symbol[30];
			} factor[10];

		float ca[90];
		float cb[10];

                Molecule();                       // constructor
                ~Molecule();                      // deconstructor

                void Clean_Members();
                void Show_Molecule() const;
                void Read_From_Mol2(char *filename);
		int Extract_From_Mol2(FILE *fp);
                void Write_Out_Mol2(char *filename) const;
		void Write_Out_Log(char *filename) const;

		void First_Check();
		void Get_Weight();
		void Get_Formula();
		void Find_Neighbors_Of_An_Atom(int atom_id);
		void Find_Neighbors_Of_A_Bond(int bond_id);
		int Two_Bonds_Connection_Check(Bond b1, Bond b2) const;
		int Get_NonH_Neighbors_Num(int atom_id) const;
		Group Find_A_Group(int atom_id) const;
                int Connection_1_2_Check(int id1, int id2) const;
                int Connection_1_3_Check(int id1, int id2) const;
                int Connection_1_4_Check(int id1, int id2) const;
		int Connection_1_5_Check(int id1, int id2) const;
		int Connection_1_6_Check(int id1, int id2) const;
		int Connection_1_7_Check(int id1, int id2) const;
		int Connection_1_8_Check(int id1, int id2) const;
		int Connection_1_9_Check(int id1, int id2) const;
		int Connection_Check(int id1, int id2) const;
		int Check_Atom_Type(Group &group) const; 
		int Adjacent_Aromatic_Check(int id) const;
		int Adjacent_Ring_Check(int id) const;
		int Hydrophobic_Neighbor_Check(int id) const;

		void Detect_Rings();
                void Reset_Choices(int id, int choice[]) const;
                void Check_Choices(int head, int choice[]) const;
                void Clean_Choices(int wrong, int choice[]) const;
                int Judge_If_An_Bond_In_Ring(int bond_id) const;

		void Get_Coefficients();
		void Atom_Typing();
		void Factor_Detecting();
		float Count_Hydrophobic_Carbon();
		float Count_Internal_HBond();
		float Count_Halogen_1_3_Pair();
		float Count_Nar_1_4_Pair();
		float Count_O3_1_4_Pair();
		float Count_Acceptor_1_5_Pair();
		float Count_Remote_Donor_Pair();
		float Count_Amino_Acid();
		float Count_Salicylic_Acid();
		float Count_Sulfonic_Acid();
		float Count_Cyclic_Ester();
		void Calculate_LogP();
        };

// misc
char *Get_Time();
void Memory_Allocation_Error();
void Openning_File_Error(char *filename);
void Mol2_Format_Error(char *filename);
void Flag_Error();

