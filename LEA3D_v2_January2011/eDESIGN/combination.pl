#!/usr/bin/perl

   local($init,$full,$isolated)= @ARGV;

   	$nbmaxcombin=100;
	
	# 10 is best !
	$nbmaxfgt=10;
	#$nbmaxfgt=100;


        if($init eq ''){
		die "usage: <>.pl <file.sdf only one molecule> < (option1) the combination includes the entire molecule ? 1 (yes) 0 (no, default)> <option2  (option1 must be set) combination.sdf includes isolated fragments 1 (yes) 0 (no, default)> \n"; 
	};

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

	$init2=$init;

### retrieve datablock info

$datablock="";
$flagnew=1;
open(SDF,"<$init");
while(<SDF>){
	if($flagnew){
		$flagnew=0;
		@ligne='';
		$si=0;
		$go=0;
	};
	$conv2=$_;
	$gosdf=0 if ($conv2 =~/(\$\$\$\$)/);
	$gosdf=1 if ($conv2 =~/^>/);
	if($gosdf){
		$datablock=$datablock."$_";
	};
};
close(SDF);


### Fragmente le SDF
 
	system("$leaexe/MAKE_FGTS.pl $init2");
	
	#unlink "acyclic.sdf";
	#unlink "substituent.sdf"; #dec2006_patent
	
	system("$leaexe/splitsdf ring.sdf 1 ring > control_fgt") if(-e "ring.sdf" && !-z "ring.sdf");
   	system("$leaexe/splitsdf fused_rings.sdf 1 fring >> control_fgt") if(-e "fused_rings.sdf" && !-z "fused_rings.sdf");
   	system("$leaexe/splitsdf linker.sdf 1 linker >> control_fgt") if(-e "linker.sdf" && !-z "linker.sdf");
	system("$leaexe/splitsdf special.sdf 1 special >> control_fgt") if(-e "special.sdf" && !-z "special.sdf");
	system("$leaexe/splitsdf substituent.sdf 1 substituent >> control_fgt") if(-e "substituent.sdf" && !-z "substituent.sdf"); #dec2006_patent

	#if(-e "acyclic.sdf" && !-z "acyclic.sdf" && -e "substituent.sdf" && -z "substituent.sdf"){
	#	system("$leaexe/splitsdf acyclic.sdf 1 substituent >> control_fgt");
	#	print "warning cp acyclic in substituent\n";
	#	system("cat check_combin.sdf $init > a");
	#	rename "a", "check_combin.sdf";
	#};
	unlink "acyclic.sdf";

	chop($list=`ls ring_*.sdf`) if(-e "ring.sdf" && !-z "ring.sdf");
	#print "$list\n";

	chop($list2=`ls fring_*.sdf`) if(-e "fused_rings.sdf" && !-z "fused_rings.sdf");
	#print "$list2\n";
	
	chop($list4=`ls linker_*.sdf`) if(-e "linker.sdf" && !-z "linker.sdf");
	#print "$list4\n";
	
	chop($list5=`ls special_*.sdf`) if(-e "special.sdf" && !-z "special.sdf");
	#print "$list5\n";
	
	chop($list1=`ls substituent_*.sdf`) if(-e "substituent_1.sdf" && !-z "substituent_1.sdf"); #dec2006_patent
	#print "$list1\n"; #dec2006_patent

 	$list=$list." ".$list2." ".$list5." ".$list3." ".$list4." ".$list1; #dec2006_patent
	@fgt=split(' ',$list);
#	print "@fgt\n";


### GENERE LES COMBINAISONS

	@combin_num="";
	$reference="";
	foreach $i (0..@fgt-1){
		$combin_num[$i]=" $i ";
		$reference=$reference." $i ";
		#print "\tfgt indice no $i => $combin_num[$i]\n";
	};
	
	$k=@fgt;
	$nbfgtmax=$k;

if($nbfgtmax < $nbmaxfgt){

	$fin=@fgt-1;
	foreach $i (0..$fin){
		$combini=$i;
		$combinj=0;

		@pilei="";
		@pilej="";
		$pi=0;
		$depart=1;
		$depart=0 if($nbfgtmax==1);

		while($pilei[0] ne "" || $depart==1){
			if($combinj <= $fin){
				if($combin_num[$combini]!~/$combinj/){

					#print "combinj=$combinj;combin_num[$combini]=$combin_num[$combini];\n";
					chop($out3 =`$leaexe/LINK_2MOL.pl $fgt[$combini] 0 $fgt[$combinj] 0 1`);

					if($out3 !~/no atom/){
						rename "combin.sdf", "combin_$k.sdf";
						$fgt[$k]="combin_$k.sdf";
						$combin_num[$k]="$combin_num[$combini] $combin_num[$combinj]";

						#@imprime=split(' ',$combin_num[$k]);
						#print "\tfgt indice no $k => associate";
						#foreach $m (0..@imprime-1){
							#print " $fgt[$imprime[$m]] ";
						#};
						#print "\n";

						$pilej[$pi]=$combinj+1;
						$pilei[$pi]=$combini;
						$pi++;
						$depart=0;

						$combini=$k;
						$combinj=0;
						$k++;
					}
					else{
						$combinj++;
					};
				}
				else{
					$combinj++;
				};
			}
			elsif($pilei[0] ne ""){
				#print "avant pile i=@pilei j=@pilej\n";
				$combini=$pilei[$pi-1];
				$pilei[$pi-1]="";
				$combinj=$pilej[$pi-1];
				$pilej[$pi-1]="";
				$pi=$pi-1;
                                #print "apres pile i=@pilei j=@pilej\n";
			};
				

		};

	};	



### ENLEVE LES DOUBLONS

#foreach $q (0..@fgt-1){
#print "$q fgt=$fgt[$q] nom=$combin_num[$q]\n";
#};

        @range=split(' ',$reference);
	$nbfgtref=@range;
	$ratio=($nbfgtref * $nbmaxcombin)/100;
	$entier=int($ratio);
	if($entier == $ratio){
		$nbmaxcombin=$ratio;
	}
	else{
		$nbmaxcombin=$entier + 1;
	};

	print "Maximum number of associated fragments = $nbmaxcombin out of $nbfgtref possible\n";

        @range2=sort @range;
        $reference=join(' ',@range2);

	@combin_unique="";
	foreach $q (0..@combin_num-1){
		@range=split(' ',$combin_num[$q]);
		$nbrange=@range;
		#print "R @range\n";
		@range2=sort @range;
		#print "R2 @range2\n";
		$combin_tmp=join(' ',@range2);
		$vu=1;
		foreach $p (0..@combin_num-1){
			if($combin_unique[$p] ne ''){
				#$vu=0 if($combin_unique[$p] eq $combin_tmp || $combin_tmp eq $reference);
				$vu=0 if($combin_unique[$p] eq $combin_tmp);
			};
		};

# Contrainte nb de fgts combines
		#if($vu && $nbrange <= $nbmaxcombin && $fgt[$q]!~/substituent/ && $fgt[$q]!~/linker/){
		if($vu && $nbrange <= $nbmaxcombin){
			if($nbrange == $nbmaxcombin && $full){
				$combin_unique[$q]=$combin_tmp;
			}
			elsif($nbrange < $nbmaxcombin){
				$combin_unique[$q]=$combin_tmp;
			};	
		};
	};

#foreach $q (0..@fgt-1){
#    print "$q fgti=$fgt[$q] nom=$combin_num[$q]\n" if($combin_unique[$q] ne '');
#};


### AFFICHAGE SOLUTIONS

	$file=$init;
	$file=~s/\.sdf$//;

	$filesdf="combination.sdf";
	$nbfileout=0;
	chop($nbfileout=`$leaexe/NBSDF.pl $filesdf`) if(-e "$filesdf");
	$nbfileout=~s/ //g;
	print "$filesdf already exists (contains $nbfileout molecules), writing mode = Append\n" if($nbfileout> 0);
	
	$nbfilenew=0;
	foreach $i (0..@fgt-1){
		if($combin_unique[$i] ne '' ){
			$nbfileout++;
 	               	@nbfgt=split(' ',$combin_unique[$i]);
        	        $nbfgts=@nbfgt;
			#if($nbfgts == $nbmaxcombin){
			#if($nbfgts > 1){
			if(($isolated==0 && $nbfgts > 1) || ($isolated && $nbfgts >0)){
				$nbfilenew++;
				$lignesdf="";
				#$lignesdf="> <NO> ( $nbfileout )\n$nbfileout\n\n> <REFERENCE> ( $init )\nReference=$init NB_FGTS=$nbfgtref\n";
				$lignesdf=$lignesdf."> <COMBINATION>\nFGT=$fgt[$i] NB_FGTS=$nbfgts\n";
				&printsdf($fgt[$i],$lignesdf,$filesdf);
				#print "combination $fgt[$i] \t$nbfgts fragments\n";
			};	
		};
	};
	print "$nbfilenew additional combinations\n";

	if($nbfilenew==0){
		system("cat toosmall.sdf $init > a");
		system("mv -f a toosmall.sdf");
		unlink "a";
		print "Too small only $nbfgtmax fragments (full building option=$full) See toosmall.sdf\n";
	};	

#####################################################################################

}
else{
	system("cat discard.sdf $init > a");
	system("mv -f a discard.sdf");
	unlink "a";
	print "Too much fragments ($nbfgtmax > $nbmaxfgt) See discard.sdf\n";

};#if $fin > $nbmaxfgt fgts

### Nettoyage du repertoire ####

	system("rm -f ring*.sdf");
	system("rm -f fring*.sdf");
	system("rm -f linker*.sdf");
	system("rm -f fused_rings*.sdf");
	system("rm -f special*.sdf");
	system("rm -f substituent*.sdf"); #dec2006_patent
	
	foreach $q (0..@combin_num-1){
		system("rm -f combin_$q.sdf");
	};
	system("rm -f combin_*.sdf");

	system("rm -f control_fgt");


###########################################################################################
#SUBROUTINES

sub printsdf {

	local($fileopen,$phrase,$fileout)=@_;

	$flagnew=1;
	open(SDF,"<$fileopen");
	open(SDT,">>$fileout");
	while(<SDF>){
		$conv2=$_;
		if($flagnew){
			$flagnew=0;
			@ligne='';
			$si=0;
			$gosdf=1;
		};
		$gosdf=0 if ($conv2 =~/^>/);
		if($gosdf){
			$ligne[$si]=$_;
			$si++;
		};
		if ($conv2 =~/(\$\$\$\$)/){
			$flagnew=1;
			foreach $si (0..@ligne-1){
				print SDT $ligne[$si];
			};
		};
	};
	print SDT "$datablock";
	print SDT "$phrase\n";
	print SDT "\$\$\$\$\n";
	close(SDT);
	close(SDF);

};

