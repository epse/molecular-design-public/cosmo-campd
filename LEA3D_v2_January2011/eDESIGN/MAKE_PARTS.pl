#!/usr/bin/perl


#################################
#
# Les coordonn�es x, y, z sont dans les tableaux @strx, @stry, @strz
# $istrbond est le nombre de liaisons
# $istratom est le nombre d'atomes
# *** @radius contient le rayon de l'atome
# *** @stratomchg contient la charge si elle existe dans le mol2
# *** @type contient l'hybridation (type atomique) de l'atome
# *** @typeint contient l'hybridation (type atomique) de l'atome interne au pgm
# @atom contient le type d'�lement atomique
# *** l'atome @strbond1 est li� � l'atome @strbond2 par liaison de type @strbondtype
# *** le nom de la molecule est $name
#
#################################


$bigfgt=1; # permet de passer de make_scaffold a make_parts

local($filealire)=@ARGV;


if ($filealire eq ''){
	die "usage: makeparts  <file.sdf>\n";
};

$workdir='';
chop($workdir = `pwd` );

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

require "$leaexe/CYCLE_SDF.pl";

 
	system("/bin/rm -f $workdir/make_parts.sdf");
	$printpart=0;
	$debug=0;	
	$rasmol=0;

##### PROPRIETES FROM SDF
	
	$moli=0;
	$flagnew=1;
	$flagmend=0;
	$nbmol=0;
	$blanc=' ';
	
	open(MOL,"<$filealire");
	while(<MOL>){
	
		if($flagnew){
			
			$nbmol++;
			@tabfilesdf='';
			$compt=0;
			$ig=1;
			$jg=0;
			$moli++;
			
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			
			@coval='';
			@nbcoval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@listb='';
			
			$flagnew=0;
			$longaold=0;
			$longbold=0;
		};
		
		$tabfilesdf[$compt]=$_;
		@getstr = split(' ',$_);
		
		$compt++;
		
		if (($compt > 4) && ($ig <= $istratom)){
		
			$longaold++;
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];

			if($atom[$ig] eq 'X'){
		                print "Warning \'X\' dummy atom detected and changed to H !\n";
		                $atom[$ig]="H";
		        };

			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{

                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };					
                                }
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

				$longbold++;

                                $listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];

                                $listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];

				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];
                                $nbcoval[$getstr[0]]=$nbcoval[$getstr[0]]+1;

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
                                $nbcoval[$getstr[1]]=$nbcoval[$getstr[1]]+1;

				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

		
			#$compt++;
		};
		if ($_=~/\$\$\$\$/){
			
			$flagnew=1;
			$flagmend=0;
			print "MOL $moli " if($debug);
			
			#Recherche des cycles
			@cyclef='';
			&cyclesdf;
			
			$debug=0;

			#ecrit le make_base.sdf
			&makebase;
			
			print "\n" if($debug);
		};
		
		if ($_=~/>/ && $_=~/<ID>/){
			@getmoli=split(' ',$_);
			$moli=$getmoli[2];
			$moli=~s/\(//;
			$moli=~s/\)//;
		};	
			
	};
	close(MOL);
	

print "SEE make_parts.sdf\n" if($printpart);
print "No available segmentation !\n" if($printpart==0);	
	
###################################################################################
###################################################################################

sub makebase{


        @hionis='';
        foreach $cyc (1..$istratom){
                $hionis[$cyc]=0;    # contient les atomes a garder
        };

#        foreach $cyc (0..@cyclef-1){
#                print "$cyclef[$cyc]\n" ;
#        };

	# Rassemble ls cycles fusionnes
	$continue=1;
	while($continue){
	$continue=0;
	foreach $cyc (0..@cyclef-2){
		if($cyclef[$cyc] ne ""){
			@getcyc=split(' ',$cyclef[$cyc]);
			foreach $cyci (0..@getcyc-1){
				foreach $cyc2 ($cyc+1..@cyclef-1){
					if($cyclef[$cyc2] ne ""){
						@getcyc2=split(' ',$cyclef[$cyc2]);
						foreach $cyci2 (0..@getcyc2-1){
							if($getcyc2[$cyci2] == $getcyc[$cyci]){
								$cyclef[$cyc] = $cyclef[$cyc]." ".$cyclef[$cyc2];
								$cyclef[$cyc2]="";
								$continue=1;
							};
						};
					};
				};
			};
		};
	};
	};

#print "apres\n";
#	foreach $cyc (0..@cyclef-1){
#		print "$cyclef[$cyc]\n" ;
#	};

	$ifprint=0;	
	foreach $cyclefi (0..@cyclef-1){

        	foreach $cyc (1..$istratom){
                	$hionis[$cyc]=-1 if($hionis[$cyc] == 1);  
			$hionis[$cyc]=0 if($hionis[$cyc] >= 2);
        	};

		$car=" ".$cyclef[$cyclefi]." ";
		$car2="";
		foreach $cyc2 (0..@cyclef-1){
			if($cyc2 != $cyclefi){
				$car2=$car2." ".$cyclef[$cyc2]." ";
			};
		};
		
		@lignebond=''; # contient les nouveaux numero atomes
        	@accroch='';
        	# atomes a garder
		$longa=0;
		# atomes des cycles
		foreach $p (1..$istratom){
			if($car=~/ $p / && $hionis[$p] == 0){
				print "$p match $cyclef[$cyclefi] donc appartient a un cycle\n" if($debug);
				$hionis[$p]=1;
				$longa++;
				if($fonc[$p]=~/ 1-H /){
					@det=split(' ',$fonc[$p]);
					@det2=split(' ',$ifonc[$p]);
					foreach $k (0..@det-1){
			        		if($det[$k] =~/1-H/){
			        			$hionis[$det2[$k]]=1;
			        			$longa++;
			       	 		};
					};
				};
				@det=split(' ',$fonc[$p]);
				@det2=split(' ',$ifonc[$p]);
				foreach $k (0..@det-1){
					if($car2=~/ $det2[$k] / && $det[$k]=~/1-/){
					#if($car2=~/ $det2[$k] /){
						$hionis[$det2[$k]]=2;
						$accroch[$det2[$k]]=$p;
					}
					else{# special case: if double bond between 2 ring systems
						foreach $cyc2 ($cyclefi+1..@cyclef-1){
							$car3=" ".$cyclef[$cyc2]." ";
							if($car3=~/ $det2[$k] /){
								$cyclef[$cyc2]=" ";
								$car4="";
								foreach $cyc3 (0..@cyclef-1){
									if($cyc3 != $cyclefi && $cyc3 != $cyc2){
										$car4=$car4." ".$cyclef[$cyc3]." ";
									};
								}; 
								$car2=$car4;
								#print "del -$car4-\n";
							};
						};		
					};
				};
			};
		};

		#print "longa $longa \n";
	 	#foreach $cyc (1..$istratom){
		#	print "$cyc $hionis[$cyc] $correspond[$cyc] $accroch[$cyc]\n" if($hionis[$cyc] > 0);
		#};
		
		# atomes des substituants
		foreach $p (1..$istratom){
		#print "$p $atom[$p] $fonc[$p] [$hionis[$p]]\n";	
			if($hionis[$p] == 0 && $car2!~/ $p /){
				$suite=" $p ";
				@fget=split(' ',$suite);
				$qs=0;
				$contactcycle=0;
				$contactcycle2=0;
				$contactdl=0;
				while($qs <= (@fget-1)){
					#print "$qs $suite\n" ;
		        		@gsuite=split(' ',$ifonc[$fget[$qs]]);
		        		foreach $s (0..@gsuite-1){
						print "$gsuite[$s] $fonc[$gsuite[$s]]\n" if($debug);

		        			if($hionis[$gsuite[$s]]==0 && $suite!~/ $gsuite[$s] / && $car2!~/ $gsuite[$s] /){
		        	  			$suite=$suite."$gsuite[$s] ";
						}
						elsif($car2=~/ $gsuite[$s] /){
							$contactcycle2++;
						}
		        	 		elsif($car=~/ $gsuite[$s] /){
		        	 		 	$contactcycle++;
		        	 		 	
							#print "fonc $atom[$gsuite[$s]] : $fonc[$gsuite[$s]]\n";
						 	$entrezici=0;
		        	 		 	if($fonc[$gsuite[$s]] =~/ 2-/ || $fonc[$gsuite[$s]] =~/ 3-/ || $atom[$gsuite[$s]] eq "P" ){
		        	 		 		@det=split(' ',$fonc[$gsuite[$s]]);
						 		@det2=split(' ',$ifonc[$gsuite[$s]]);
								foreach $k (0..@det-1){
							
			        					if(($det[$k] =~/2-/ || $det[$k] =~/3-/ || ($det[$k] =~/1-/ && $atom[$gsuite[$s]] eq "P")) && $det2[$k] == $fget[$qs]){
			        					  	$contactdl=1;
										$entrezici=1;
										$accroch[$fget[$qs]]=$gsuite[$s];
			       	 					};
								};
							};
							if($fonc[$gsuite[$s]] =~/ 1-/ && $entrezici==0){
								@det=split(' ',$fonc[$gsuite[$s]]);
								@det2=split(' ',$ifonc[$gsuite[$s]]);
								foreach $k (0..@det-1){
			        					if($det[$k] =~/1-/  && $det2[$k] == $fget[$qs]){
			        						$accroch[$fget[$qs]]=$gsuite[$s];
			       	 					};
								};						
		    					};
		        			};
		        		};
		        		@fget=split(' ',$suite);
		        		$qs++;
		 		};
				#print "contactdl = $contactdl ;  contactcycle = $contactcycle ; contactcycle2 = $contactcycle2 \n" ;
		        	print "STRANGE contactcycle > 1 $contactcycle\n" if($contactcycle > 1 && $debug);
				if($contactdl){
		                	if($contactcycle==1 && $contactcycle2 == 0){
		        			foreach $s (0..@fget-1){
		        				$longa++  if($hionis[$fget[$s]] == 0);
	            					$hionis[$fget[$s]]=1 if($hionis[$fget[$s]] ==0);
							$accroch[$fget[$s]]="" if($accroch[$fget[$s]] ne '');
						};
					}
					elsif($contactcycle==1 && $contactcycle2 > 0){
				     		foreach $s (0..@fget-1){
							#$longa++ if($hionis[$fget[$s]] ==0);
							if($hionis[$fget[$s]] ==0 && $accroch[$fget[$s]] ne ''){
	            						$hionis[$fget[$s]]=4;
								@det=split(' ',$fonc[$fget[$s]]);
								@det2=split(' ',$ifonc[$fget[$s]]);
								foreach $k (0..@det-1){
									$car3=" ".$det2[$k]." ";
									if($car!~/$car3/){
										#print "_$car- et _$car3-\n";
										$hionis[$det2[$k]]=5;
										$accroch[$det2[$k]]=$fget[$s];	
									};
								};	
							};
						};	
					};
					print "ici longa=$longa \n" if($debug);	
				}
				elsif($contactcycle==1 && $contactcycle2 > 0){
					foreach $s (0..@fget-1){
					#	$longa++ if($hionis[$fget[$s]] ==0);
	            				$hionis[$fget[$s]]=2 if($hionis[$fget[$s]] ==0);
					};			
                           		print "ici2 longa=$longa \n" if($debug);	
				}
				elsif($contactcycle==1 && $contactcycle2 == 0){
					foreach $s (0..@fget-1){
						$longa++  if($hionis[$fget[$s]] == 0);
						$hionis[$fget[$s]]=1 if($hionis[$fget[$s]] ==0);
						$accroch[$fget[$s]]="" if($accroch[$fget[$s]] ne '');
					};
				};
			};
		};

	
		# remplace les linker par 1 X
		#print "longa $longa \n";
		#foreach $cyc (1..$istratom){
		#	print "hionis$cyc $hionis[$cyc] $accroch[$cyc]\n" if($hionis[$cyc] > 0);
		#};
		
		$ptsaccroch='';	
		@correspond='';
		$nbi=1;
		foreach $p (1..$istratom){
			if($hionis[$p] ==1){
		   		$correspond[$p]=$nbi;
		   		$nbi++;
			}
			elsif($hionis[$p]==2 && $accroch[$p] ne '') {
				print "accroch $p \n" if($debug);
		        	$hionis[$p]=3;
				print "X $p devient no $nbi\n" if($debug);
				$correspond[$p]=$nbi;
		 		if($ptsaccroch eq ''){
		  			$ptsaccroch=$ptsaccroch."$accroch[$p]";
		   		}
		   		else{
		   			$ptsaccroch=$ptsaccroch."-$accroch[$p]";
		   		};			
		   		$nbi++;
		   		$longa++;
			}
			elsif($hionis[$p]==4 && $accroch[$p] ne '') {
				$correspond[$p]=$nbi;
				$nbi++;
				$longa++;
			}
			elsif($hionis[$p]==5){
				@det=split(' ',$fonc[$p]);
				if(@det==1){
					$correspond[$p]=$nbi;
					$nbi++;	
					$longa++;
				}
				else{
					$hionis[$p]=3;
					print "accroch $p \n" if($debug);
					print "X $p devient no $nbi\n" if($debug);
					$correspond[$p]=$nbi;
					if($ptsaccroch eq ''){
						$ptsaccroch=$ptsaccroch."$accroch[$p]";
					}
					else{
						$ptsaccroch=$ptsaccroch."-$accroch[$p]";
					};
					$nbi++;
					$longa++; 
				};
			};
		};
	
		@getacc=split('-',$ptsaccroch);
		$ptsaccroch='';	
		foreach $p (0..@getacc-1){
			if($ptsaccroch eq ''){
				$ptsaccroch=$ptsaccroch."$correspond[$getacc[$p]]";
			}
			else{
				$ptsaccroch=$ptsaccroch."-$correspond[$getacc[$p]]";
			};	
		};
	
		#print "longa $longa \n" ;
        	#foreach $cyc (1..$istratom){
        	#        print "$cyc $hionis[$cyc] \n" if($hionis[$cyc] > 0);    # contient les atomes a garder
        	#};

 		print "longa $longa \n" if($debug);
        	if($debug){
		foreach $cyc (1..$istratom){
			print "hionis$cyc $hionis[$cyc] $correspond[$cyc]\n" if($debug);
		};
		};
		
        	$niveaubond=$istratom+3;
        	$niveaubondf=$istratom+3+$istrbond;

        	print "bond deb $tabfilesdf[$niveaubond+1]" if($debug);
        	print "bond fin $tabfilesdf[$niveaubondf]" if($debug);

        	$longb=0;
        	$z="  0  0  0  0";
		foreach $p ($niveaubond+1..$niveaubondf){
			@fget=split(' ',$tabfilesdf[$p]);
		       		print "$tabfilesdf[$p]\n" if($debug);

                                @coller=split(' *',$fget[0]);
                                @coller2=split(' *',$fget[1]);
                                if(@coller==6 && $fget[1] ne ""){
                                        $fget[0]=$coller[0].$coller[1].$coller[2];
                                        $fget[2]=$fget[1];
                                        $fget[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $fget[1] eq ""){
                                        $fget[0]=$coller[0].$coller[1];
                                        $fget[1]=$coller[2].$coller[3].$coller[4];
                                        $fget[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($tabfilesdf[$p]=~/^\s/){
                                                $fget[0]=$coller[0].$coller[1];
                                                $fget[2]=$fget[1];
                                                $fget[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $fget[0]=$coller[0].$coller[1].$coller[2];
                                                $fget[2]=$fget[1];
                                                $fget[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($tabfilesdf[$p]=~/^\s/){
                                                $fget[0]=$coller[0];
                                                $fget[2]=$fget[1];
                                                $fget[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $fget[0]=$coller[0].$coller[1].$coller[2];
						$fget[2]=$fget[1];
                                                $fget[1]=$coller[3];
                                        };					
                                }
                                elsif(@coller2==4){
                                        $fget[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $fget[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $fget[0]=$coller[0].$coller[1].$coller[2];
                                        $fget[1]=$coller[3].$coller[4].$coller[5];
                                        $fget[2]=$coller[6];
                                };
	
                 	print "b1 = $fget[0] b2 =$fget[1] \n" if($debug);
			if($correspond[$fget[0]] ne '' && $correspond[$fget[1]] ne ''){		
 				#$lignebond[$longb]=pack("A3A3A3A12",$correspond[$fget[0]],$correspond[$fget[1]],$fget[2],$z);
 			
 				$lignebond[$longb]=sprintf "%3s%3s%3s$z",$correspond[$fget[0]],$correspond[$fget[1]],$fget[2],$z;
 				print "\t$fget[0]($correspond[$fget[0]]) - $fget[1]($correspond[$fget[1]]) => $lignebond[$longb]\n" if($debug);
				$longb++;
			};
		};
	print "longa=$longa longb=$longb\n" if($debug);

	if($longa > 0 && $longaold > $longa){
		$printpart=1;
		$ifprint=1;
		open(OUG,">>make_parts.sdf");
		foreach $p (0..2){
			printf OUG "$tabfilesdf[$p]";
		};
		printf OUG "%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
							
		foreach $rt (4..$niveaubond){		
			$cup= $rt-3;
			print "n $cup $tabfilesdf[$rt]\n" if($debug);
			if($hionis[$cup] > 0){
				print "coord $cup\n" if(($hionis[$cup]==3 || $hionis[$cup]==1) && $debug);
				if($hionis[$cup]==3){
					@fget=split(' ',$tabfilesdf[$rt]);
					$fget[3]="X";
					printf OUG "%10s%10s%10s%1s%1s%1s%3s%3s%3s\n",$fget[0],$fget[1],$fget[2],$blanc,$fget[3],$blanc,$fget[4],$fget[5],$fget[6];
				}
				elsif($hionis[$cup]==1){
					printf OUG "$tabfilesdf[$rt]";
				}
				elsif($hionis[$cup]==4 || $hionis[$cup]==5){
					printf OUG "$tabfilesdf[$rt]";	
				};
			};
		};			
	
		foreach $rt (0..@lignebond-1){			
    			printf OUG "$lignebond[$rt]\n";
       		};
       	
       		$ptsaccroch="null" if($ptsaccroch eq '');
       	
		foreach $rt ($niveaubondf+1..@tabfilesdf-1){
	
			if($tabfilesdf[$rt]=~/\$\$\$\$/ && $ptsaccroch ne ''){
				printf OUG "> <POINTS>\n";
				printf OUG "$ptsaccroch\n";
				printf OUG "\n";
				printf OUG "$tabfilesdf[$rt]";
			}
			else{
				printf OUG "$tabfilesdf[$rt]";
			};
		};       	
		close(OUG);
	};
	 		
};
	print "$moli : no parts\n" if($ifprint==0);
		   								   			
};

###################################################################################

