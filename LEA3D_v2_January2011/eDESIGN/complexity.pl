#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

	local($onesdf)=@ARGV;

	if($onesdf eq ""){ 	
		die "usage: complexity <one molecule in sdf format>\n";
	};

	chop($nb = `$leaexe/NBSDF.pl $onesdf ` );
	if($nb > 1){
		die "input file must contain only one molecule\n";
	};

	### Nettoyage du repertoire ####
	system("rm -f ring*.sdf");
	system("rm -f substituent*.sdf");
	system("rm -f linker*.sdf");
	system("rm -f fused_rings*.sdf");
	system("rm -f special*.sdf");
	system("rm -f acyclic*.sdf");
	
	### Fragmente le SDF
	chop($rep = `$leaexe/MAKE_FGTS.pl $onesdf` );	
	
	chop($nbring = `$leaexe/NBSDF.pl ring.sdf ` ) if(-e "ring.sdf");
	#print "$nbring rings\n";
	
	#chop($nbfring = `$leaexe/NBSDF.pl fused_rings.sdf` ) if(-e "fused_rings.sdf");
	#print "$nbfring fused_rings\n";
	# takes account for multiple rings:
	$nbfring=0; 
	if(-e "fused_rings.sdf" && ! -z "fused_rings.sdf"){
		open(RIG,"<fused_rings.sdf");
		$flag=0;	
		while(<RIG>){	
			@get=split(' ',$_);
		       	$nbfring=$nbfring+$get[0] if($flag);	
			if($_=~/<ncycles>/){
				$flag=1;
			}
			else{
				$flag=0;
			};
		};
		close(RIG);
	};	
	#print "$nbfring fused_rings\n";		
	
	chop($nblinker = `$leaexe/NBSDF.pl linker.sdf` ) if(-e "linker.sdf");
	#print "$nblinker linker.sdf\n";

	chop($nbsubs = `$leaexe/NBSDF.pl substituent.sdf` ) if(-e "substituent.sdf");
	#print "$nbsubs substituent.sdf\n";

	chop($nbacyclic = `$leaexe/NBSDF.pl acyclic.sdf` ) if(-e "acyclic.sdf");
	#print "$nbacyclic acyclic.sdf\n";

	#chop($nbspecial = `$leaexe/NBSDF.pl special.sdf` ) if(-e "special.sdf");
	#print "$nbspecial special.sdf\n";
	$nbspecial=0;
	if(-e "special.sdf" && ! -z "special.sdf"){
		open(RIG,"<special.sdf");
		$flag=0;
		while(<RIG>){
			@get=split(' ',$_);
			$nbspecial=$nbspecial+$get[0] if($flag);
			if($_=~/<ncycles>/){
				$flag=1;
			}
			else{
				$flag=0;
			};
		};
		close(RIG);
	};	
		
	$complexity= $nbring + $nbfring + $nbacyclic + $nbspecial;

	if($complexity==0){
		print "1 = complexity (the molecule itself since no available fragmentation)\n";
	}
	else{	
		print "$complexity = complexity ($nbring rings ; $nbfring rings in fused_rings ; $nblinker linker.sdf ; $nbsubs substituent.sdf ; $nbspecial rings in special.sdf)\n";
	};
	
	### Nettoyage du repertoire ####
	system("rm -f ring*.sdf");
	system("rm -f substituent*.sdf");
	system("rm -f linker*.sdf");
	system("rm -f fused_rings*.sdf");
	system("rm -f special*.sdf");
	system("rm -f acyclic*.sdf");
	
