#!/bin/bash

#########################################################
# AUXILLIARY PROGRAMS
# Environment definitions for LEA3D
# either you update variables here or in your .cshrc/.bashrc
#########################################################

# CORINA has to be installed
#LEA_CORINA=/installation_corina/CORINA/corina_exe.lnx
#export LEA_CORINA

# FROG has to be installed
#LEA_FROG=/installation_FROG/www_iMolecule.py
#export LEA_FROG

# Choose one convertor either CORINA or FROG
#LEA_CONVERTOR=$LEA_CORINA
#LEA_CONVERTOR=$LEA_FROG
#export LEA_CONVERTOR

# FLEXX has to be installed 
#FLEXX_HOME=/installation_flexx/FLEXX
#export FLEXX_HOME
#LEA_FLEXX=$FLEXX_HOME/bin_Linux/flexx
#export LEA_FLEXX

#SURFLEX has to be installed
#SFSIMLIC=/installation_surflex/SURFLEX/Similarity/bin/.sfsimlicense
#export  SFSIMLIC
#SFDOCKLIC=/installation_surflex/SURFLEX/Docking/bin/.sfdocklicense
#export SFDOCKLIC
#LEA_SURFLEX='/installation_surflex/SURFLEX/Docking/bin/surflex-dock-linux.exe
#Export LEA_SURFLEX

#PLANTS has to be installed
#LEA_PLANTS=/installation_plants/PLANTS
#export LEA_PLANTS

# XLOGP
# already present in LEA directory

# XSCORE
# already present in LEA directory

# CHEMICALC-2 (Suzuki et al.)
# already present in LEA directory in SOLUBILITY 


#########################################################

alias lea='$LEA3D/lea'

# Format conversion 
alias sdfmol2='$LEA3D/SDF_MOL2.pl'
alias mol2sdf='$LEA3D/MOL2_SDF.pl'
alias mol2pdb='$LEA3D/MOL2PDB.pl'
alias sdfpdb='$LEA3D/SDF_PDB.pl'
alias prepisisbase='$LEA3D/PREP_FILE_FOR_ISIS_BASE.pl'

# Convert 1D or 2D into 3D and generate multiconformers
alias sdf='$LEA3D/sdf'
alias frog='$LEA3D/frog'

# split files
alias splitsdf='$LEA3D/splitsdf'
alias splitmol2='$LEA3D/cut_mol2.pl'
alias splitmol2flexx='$LEA3D/cut_mol2_flexx.pl'

# Ghose and Crippen
alias addgcsdf='$LEA3D/ADD_GC_DATABLOCK_SDF.pl'

# SDF tools
alias nbsdf='$LEA3D/NBSDF.pl'
alias nosdf='$LEA3D/searchsdfbyno.pl'

# Preparing chemical databases
alias nosel='$LEA3D/NO_SEL.pl'
alias drug='$LEA3D/DRUG.pl'

# to dissect and rebuild molecules
alias makescaffold='$LEA3D/MAKE_SCAFFOLD.pl'
alias makefgt='$LEA3D/MAKE_FGTS.pl'
alias makeparts='$LEA3D/MAKE_PARTS.pl'
alias combination='$LEA3D/combination.pl'
alias link2mol='$LEA3D/LINK_2MOL.pl'
alias classfgt='$LEA3D/CLASS_FGT.pl'
alias enleve_no_points='$LEA3D/ENLEVE_NO_POINTS.pl'
alias replacex='$LEA3D/REPLACE_X.pl'
alias getx='$LEA3D/GET_X.pl'

# LEGO for Genetic recombination
alias autolego='$LEA3D/AUTO_MK_LEGO.pl'

# For reactant databases
alias replacebyx='$LEA3D/REACTANT/REPLACE_ATOM_byX.pl'
alias splitx='$LEA3D/REACTANT/SPLIT_REACTANT.pl'

# PDB manipulation
alias pdbmol2='$LEA3D/pdbmol2.pl'

# Preparing flexx input files and LEA3D files
alias makeflexx='$LEA3D/FLEXX/MAKE_FLEXX.pl'

# GA convergence
alias plot='$LEA3D/plot'

